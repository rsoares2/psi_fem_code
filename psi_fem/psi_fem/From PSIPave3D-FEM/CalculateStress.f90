Module CalculateStressandStrains
    
    Use GlobalInputVariables
    Use InitializeGlobalVariables
    Use CalculateStressandStrain3D
    Use CalculateStressandStrain2D
    
    implicit Real*8 (A-H, O-Z)

    Contains    

	Subroutine CalculateStressandStrain
	
    !
    !   This subroutine calculates the global stresses and strains after the linear system solution
    !
    
    If (NumberOfDimensions == 2 ) Then
        
        Call InitializeGlobalStress2D
        
        Call CalculateGlobalStress2D
    
    ElseIf (NumberOfDimensions == 3 ) Then 

        Call InitializeGlobalStress3D
        
        Call CalculateGlobalStress3D
        
    Endif
    
    End Subroutine CalculateStressandStrain
        
 End Module CalculateStressandStrains