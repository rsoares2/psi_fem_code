Module CalculateForceMatrix2D

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleForceMatrix
    Use CohesiveZoneForce2D
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine CalculateGlobalForceMatrix2D
    
    !
    !   This subroutine calculates the global sparse stiffness matrix for 2D meshes
    !    
    
    Real*8 :: SEIGEN(3), DSIGRG(4), SG_Temp(4), C(3,3)
	Integer :: iElastic, iViscoElastic

    DSIGRG = 0
    SEIGEN = 0
    
    Do iel = 1, NumberOfGlobalElements  !Major loop over all elements
    
        If (mod(iel, iPrintToScreen) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) iel
 805        Format (10x,'Set up Global Force Matrix for Element =',I8)
            
        Endif        

    !   Initialize the element force matrix

        FElement = 0.0D0

    !
    !   Calculate element stiffness matrix
    !

        Call SetUpQuadraturePoints (NGIntegrationPoints, NumberOfGlobalNodesPerElement)  ! Set up quadrature points and weights

        Do iIntPoint = 1, NGIntegrationPoints
        
            Call ShapeFunction2D (iel, iIntPoint, NumberOfGlobalNodesPerElement, 0)  !Set up B matrix
            
            Call ReadGlobalMaterialType2D (iel, iIntPoint, 2, C)  !Add material properties (2, means Called from force matrix)
            
            Call CalculateElementForceMatrix2D (iel, iIntPoint)
            
        Enddo
        
        Call AssembleGlobalForceMatrix2D (iel)

    Enddo
    
    Call PostCalculationForce2D

    If (iGlobalCohesiveZones /=0) Call InterfaceForce2D    
    
    Call ApplyGlobalNodalForce
    
    Call ApplyBCForce2D
    
    End Subroutine CalculateGlobalForceMatrix2D

!*********************************************************************************************    
    
    Subroutine CalculateElementForceMatrix2D (iel, iIntPoint)
    
    !
    !   This subroutine calculates the element force matrix
    !
    
    !   This is the previous stress contribution
    
    FElement = Matmul(SG(iel,1:3,iIntPoint),BG) * Area * Weight

    !   Include contribution from body force
    

            If (iBodyForce == 1) Then   
                
                !This is not working for 2D yet
                Write(*,*) ('Body forces are not included in 2D yet')
                Stop
            
            Endif

	        If (MaterialTypeG (iel) == 5) Then
   
                FElement = FElement + Matmul (SEigen, BG) * Area * Weight
    
            Endif
        
    End Subroutine CalculateElementForceMatrix2D

!*********************************************************************************************        
    
    Subroutine ApplyGlobalNodalForce
    !
    !   Include nodal force contributions
    !

    If (NumberOfForces /= 0) Then
        
        ForceMatrixG (:) = ForceMatrixG (:) + FGLG (:) 
        
    Endif
    
    End Subroutine ApplyGlobalNodalForce

!********************************************************************************************* 
    
    Subroutine ApplyBCForce2D
    
    !
    !   This subroutine applies boundary conditions to the force matrix 2D
    !
    
    Do k = 1, NumberOfDisplacementBC 
    
        jj = NGlobalDOF(K,KTime)
        ForceMatrixG (jj) = KBig * DisplacementIncrementG (k,KTime)/FAC
    
    Enddo
            
    End Subroutine ApplyBCForce2D

!*********************************************************************************************        

    Subroutine PostCalculationForce2D
    
    IViscoElastic=0
    IViscoPlastic=0
	IElastic=0

	IF (Allocated (BG)) Deallocate(BG)	
    
    Deallocate (FElement)    
    Deallocate (C)
    
    End Subroutine PostCalculationForce2D

!*********************************************************************************************  
    
    End Module CalculateForceMatrix2D