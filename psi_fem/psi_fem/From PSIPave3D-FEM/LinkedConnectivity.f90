Module LinkedConectivity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real (A-H, O-Z)

    Contains    

    Subroutine LinkedConnectivity(i)
    
    Integer :: M, N, iFlag, iSize
    
    !
    !   This subroutine prepares data for the sparse matrix format
    !
    !   NZG = Number of Nonzeros in Global Scale
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    Do J=1,NumberOfGlobalNodesPerElement
    M = NodeG(I,J)
    Do K=1,NumberOfGlobalNodesPerElement
    iflag = 0
        N = NodeG(I,K)
        If(N>=M)THEN
            Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                If (LinkedNodeConnectivity(M,IK)==N) Then
                    iflag=1
                    exit
                Endif
            Enddo
            If (iflag/=1) then
                iSize = LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,iSize) = N
                NZG = NZG + 1       !    Flag to track number of non-zeros in the matrix
                iSize = iSize + 1
                LinkedNodeConnectivity (M,iNGlobalNodes) = iSize
            Endif
        Endif
     Enddo
    
    Enddo

    End Subroutine LinkedConnectivity

!*********************************************************************************************

    SUBROUTINE LinkedConnectivityCZ(I)

    !
    !   This subroutine prepares data for the CZ sparse matrix format
    !
    !   NZG = NUMBER OF NONZEROS IN GLOBAL SCALE
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    DO L=1,2 !SINCE THE CZ IS IMPLEMENTED IN FOUR NODES. THEY ARE ACTUALLY 2 CZ IN FORM OF ONE
    
    IMOD=MOD(L,2)   !THIS IS TO CAPTURE THE REST EITHER 1 OR 0 AND SUBTRACT FROM IL TO GET 
                    !EITHER 2*I-1 OR 2*I
    IL=2*I-IMOD
    
        M=NINT1G(IL) !THIS IS FOR THE FIRST NODE OF CZ
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1G(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7721
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7721 CONTINUE
     
        N=NINT2G(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7722
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                    LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7722 CONTINUE
 
        M=NINT2G(IL) !THIS IS FOR THE FIRST NODE OF CZ - NOW REVERSE
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1G(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7723
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7723 CONTINUE
     
        N=NINT2G(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7724
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7724 CONTINUE
     
    ENDDO
    
    END SUBROUTINE  LinkedConnectivityCZ
    
    Subroutine LinkedConnectivityLocal(i)
    
    Integer :: M, N, iFlag, iSize
    
    !
    !   This subroutine prepares data for the local sparse matrix format
    !
    !   NZL = Number of Nonzeros in Local Scale
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    Do J=1,NumberOfLocalNodesPerElement
    M = NodeL(I,J)
    Do K=1,NumberOfLocalNodesPerElement
    iflag = 0
        N = NodeL(I,K)
        If(N>=M)THEN
            Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                If (LinkedNodeConnectivityL(M,IK)==N) Then
                    iflag=1
                    exit
                Endif
            Enddo
            If (iflag/=1) then
                iSize = LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,iSize) = N
                NZL = NZL + 1       !    Flag to track number of non-zeros in the matrix
                iSize = iSize + 1
                LinkedNodeConnectivityL (M,iNLocalNodes) = iSize
            Endif
        Endif
     Enddo
    
    Enddo

    End Subroutine LinkedConnectivityLocal    

!*********************************************************************************************

    SUBROUTINE LinkedConnectivityCZL(I)

    !
    !   This subroutine prepares data for the CZ sparse matrix format
    !
    !   NZL = NUMBER OF NONZEROS IN LOCAL SCALE
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    DO L=1,2 !SINCE THE CZ IS IMPLEMENTED IN FOUR NODES. THEY ARE ACTUALLY 2 CZ IN FORM OF ONE
    
    IMOD=MOD(L,2)   !THIS IS TO CAPTURE THE REST EITHER 1 OR 0 AND SUBTRACT FROM IL TO GET 
                    !EITHER 2*I-1 OR 2*I
    IL=2*I-IMOD
    
        M=NINT1L(IL) !THIS IS FOR THE FIRST NODE OF CZ
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1L(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7721
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7721 CONTINUE
     
        N=NINT2L(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7722
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZL=NZL+1
                    LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7722 CONTINUE
 
        M=NINT2L(IL) !THIS IS FOR THE FIRST NODE OF CZ - NOW REVERSE
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1L(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7723
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZL=NZL+1
                LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7723 CONTINUE
     
        N=NINT2L(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7724
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZL=NZL+1
                LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7724 CONTINUE
     
    ENDDO
    
    END SUBROUTINE  LinkedConnectivityCZL
    
End Module LinkedConectivity
    