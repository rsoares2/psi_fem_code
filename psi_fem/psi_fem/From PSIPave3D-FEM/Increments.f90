Module Increment

    Use GlobalInputVariables

	implicit Real*8(A-H,O-Z)

    Contains

	Subroutine TimeCycleIncrement
	
    !
    !   This subroutine increments the current time for multiple cycles
    !
    
		Time_End = TimLim
		KTime = 1
		DTime = DeltaTime (KTime)
		Fac = TimeFactor(KTime)
		TimLim = Time_End + TimeEndTB(KTime)
    
    End Subroutine TimeCycleIncrement

!*********************************************************************************************  
    
	Subroutine TimeCheckIncrement
	
    !
    !   This subroutine checks the current time and increments to the next time step
    !
    
        If (ABS(timlim-time).LT.1E-8) Then
            Time = TimLim
        Endif
           
        If (Time >= TimLim) Then
                        
            KTime = KTime + 1
            DTime = DeltaTime (KTime)
            Fac = TimeFactor(KTime)
                        
        Endif
            
        If (iCycl == 1) Then
                
            TimLim = TimeEndTB(KTime)

        ElseIf (iCycl /= 1) Then
                
            TimLim = Time_End + TimeEndTB(KTime) 
            
        Endif

        Time = Time + DTime
    
    End Subroutine TimeCheckIncrement

!*********************************************************************************************  
    
    Subroutine ForceIncrement
    
    If (NumberOfForces == 0) Then
        
        Return
        
    ElseIf (NumberOfForces /= 0) Then
        
        If (iPavement == 1) Then
            
            If (iBodyForce==1) Then
            
                If(Incr == 1)Then
                
                    FGLG = 0      !This trick is to include body forces in the first time step and truck load in the second step
            
                Else
		        
                    DO i = 1, NGTotalDOF 
    			    
                        FGLG (i) = FORCEG (i) / Fac + FGLG (i)
		      	    
                    Enddo    
                    
                Endif
                
            Else 

                DO i = 1, NGTotalDOF 
    			    
                    FGLG (i) = FORCEG (i) / Fac + FGLG (i)
		      	    
                Enddo    
            
            Endif
             
        Elseif (iPavement == 0) Then 
                
                DO i = 1, NGTotalDOF 
    			    
                    FGLG (i) = FORCEG (i) / Fac + FGLG (i)
		      	    
                Enddo    
            
        Endif
    
    Endif
    

    End Subroutine ForceIncrement                    
                

End Module Increment