Module InputCoordinates
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine ReadInputCoordinates
    
    !
    !   This subroutine reads the X, Y and Z coordinates of node
    !
    !   XG = Global X coordinate of node iel
    !   YG = Global Y coordinate of node iel
    !   ZG = Global Z coordinate of node iel    (if in 3D)
    !
    
    If ( NumberOfDimensions == 2 ) Then
        
        Call Read2DInputCoordinates
        
    ElseIf ( NumberOfDimensions == 3 ) Then

        Call Read3DInputCoordinates

    Else
    
        Write (6,1007) 
1007    Format ('The number of dimensions must be 2 for 2D or 3 for 3D')
        Stop
        
    Endif
    
    End Subroutine ReadInputCoordinates    
    
    Subroutine Read2DInputCoordinates

    Allocate ( XG (NumberOfGlobalNodes) )
    Allocate ( YG (NumberOfGlobalNodes) )

    Write(6,1008)
1008 Format (//,10X,'Global Nodal Coordinates are: ',//,8X,'Node N0.',11X,'XG',16X,'YG',/)
     
    Do iel = 1, NumberOfGlobalNodes
        
        Read (5,*) i, XG(iel), YG(iel)
        Write (6,1009) i, XG(iel), YG(iel)

    Enddo   
1009 Format (8x,i5, 7x, D15.7, 3x, D15.7)
     
    End Subroutine Read2DInputCoordinates
    
    Subroutine Read3DInputCoordinates

    Allocate ( XG (NumberOfGlobalNodes) )
    Allocate ( YG (NumberOfGlobalNodes) )
    Allocate ( ZG (NumberOfGlobalNodes) )

    Write(6,1010)
1010 Format (//,10X,'Global Nodal Coordinates are: ',//,8X,'Node N0.',13X,'XG',16X,'YG',16X,'ZG',//)
     
    Do iel = 1, NumberOfGlobalNodes
        
        Read (5,*) i, XG(iel), YG(iel), ZG(iel)
        Write (6,1011) i, XG(iel), YG(iel), ZG(iel)

    Enddo   

1011 Format (10x,i5, 7x, D15.7, 3x, D15.7, 3x, D15.7)
     
    End Subroutine Read3DInputCoordinates

    Subroutine Read2DInputLocalCoordinates

    Allocate ( XL (NumberOfLocalNodes) )
    Allocate ( YL (NumberOfLocalNodes) )

    Write(6,1008)
1008 Format (//,10X,'Local Nodal Coordinates are: ',//,8X,'Node N0.',11X,'XL',16X,'YL',/)
     
    Do iel = 1, NumberOfLocalNodes
        
        Read (5,*) i, XL(iel), YL(iel)
        Write (6,1009) i, XL(iel), YL(iel)

    Enddo
1009 Format (8x,i5, 7x, D15.7, 3x, D15.7)
    
    !
    !   Calculate the maximum dimensions of RVE (xmax and ymax)
    !   Also calculate the nodes on the boundary to transfer displacement BC (used in RVEDisplacement)
    !
    
	If (iLocalBC == 1) Then
		
        XMax = maxval(XL)
        YMax = maxval(YL)
        
        Do i = 1, NumberOfLocalNodes
            
            If (XL(i) == XMax) Then
                NNXSide = NNXSide + 1
            Endif
            
            If (YL(i) == YMax) Then
                NNYSide = NNYSide + 1
            Endif
            
        Enddo
        
        Allocate (NodNumX (NNXSide))
        Allocate (NodNumY (NNYSide))
            ix=0
            iy=0
			Do i = 1, NumberOfLocalNodes

				If (XL(i) == XMax) Then
					ix = ix + 1					
					NodNumX(ix) = i
                Endif
                
				If (YL(i) == YMax) Then
					iy = iy + 1						
					NodNumY(iy) = i
                Endif
			Enddo
    
    Endif
     
    End Subroutine Read2DInputLocalCoordinates
    
End Module InputCoordinates