Module CalculateForceMatrix3D

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleForceMatrix
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine CalculateGlobalForceMatrix3D
    
    !
    !   This subroutine calculates the global sparse stiffness matrix for 3D meshes
    !    
    
    Real*8 :: PSI(8),SEIGEN(6),DSIGRG(6), SG_Temp(6)
	Integer :: NICON(2), iElastic, iViscoElastic

    DSIGRG=0
    SEIGEN=0
    
    Do iel = 1, NumberOfGlobalElements  !Major loop over all elements
    
        If (mod(iel, iPrintToScreen) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) iel
 805        Format (10x,'Set up Global Force Matrix for Element =',I8)
            
        Endif        

    !   Initialize the element force matrix

        FElement = 0.0D0    

    !
    !   Calculate element stiffness matrix
    !

        Call SetUpQuadraturePoints (NGIntegrationPoints, NumberOfGlobalNodesPerElement)  ! Set up quadrature points and weights

        Do iIntPoint = 1, NGIntegrationPoints
        
            Call ShapeFunction3D (iel, iIntPoint)  !Set up B matrix
            
            Call ReadGlobalMaterialType3D (iel, iIntPoint, 2)  !Add material properties (2, means Called from stifnness matrix)
            
            Call CalculateElementForceMatrix3D (iel, iIntPoint)
            
        Enddo
        
        Call AssembleGlobalForceMatrix3D (iel)

    Enddo
    
    Call PostCalculationForce3D
    
    Call ApplyGlobalNodalForce
    
    Call ApplyBCForce3D
    
    End Subroutine CalculateGlobalForceMatrix3D

!*********************************************************************************************    
    
    Subroutine CalculateElementForceMatrix3D (iel, iIntPoint)
    
    !
    !   This subroutine calculates the element force matrix
    !
    
    !   This is the previous stress contribution

    FElement = Matmul(SG(iel,:,iIntPoint),BG) * DETJ * Weight
    !   Include contribution from body force
    
            If (iBodyForce == 1) Then   
                
                ii = MaterialSetG (iel) !which material this element belongs to
                BodyForce = Density(ii) * Gravity * DetJ/6

            !   This is body force for tetrahedron elements.
            !   It includes vertical forces in each Y position (1, 4, 7, 10)
                FElement(2) = FElement(2) + (BodyForce)/4
                FElement(5) = FElement(5) + (BodyForce)/4
                FElement(8) = FElement(8) + (BodyForce)/4
                FElement(11) = FElement(11) + (BodyForce)/4
            
            Endif

	        If (MaterialTypeG (iel) == 5) Then
   
                FElement = FElement + Matmul (SEigen, BG) * DETJ * Weight
    
            Endif
        
    End Subroutine CalculateElementForceMatrix3D

!*********************************************************************************************        
    
    Subroutine ApplyGlobalNodalForce
    !
    !   Include nodal force contributions
    !
    
    If (NumberOfForces /= 0) Then
        
        ForceMatrixG (:) = ForceMatrixG (:) + FGLG (:) 
        
    Endif
    
    End Subroutine ApplyGlobalNodalForce

!********************************************************************************************* 
    
    Subroutine ApplyBCForce3D
    
    !
    !   This subroutine applies boundary conditions to the force matrix 3D
    !
    
    Do k = 1, NumberOfDisplacementBC 
    
        jj = NGlobalDOF(K,KTime)
        ForceMatrixG (jj) = KBig * DisplacementIncrementG (k,KTime)/FAC
    
    Enddo
            
    End Subroutine ApplyBCForce3D

!*********************************************************************************************        

    Subroutine PostCalculationForce3D
    
    IViscoElastic=0
    IViscoPlastic=0
	IElastic=0

	IF (Allocated (BG)) Deallocate(BG)	
    
    Deallocate (FElement)    
    Deallocate (C)
    
    End Subroutine PostCalculationForce3D

!*********************************************************************************************  
    
    End Module CalculateForceMatrix3D