Module OutputModule
    
    Use GlobalInputVariables
    Use Omp_lib
    Use TecplotOutputModule
    Use TecplotOutputFactorModule
    Use WriteGlobalOutput


    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine Output2DG

    If (iOutputGlobal == 1) Call WriteOutputG
   
  !  If (iOutputLocal == 1) Call WriteLocalOutput

    If (iTecplotGlobal == 1) Then
        
        Call WriteTecplotGlobalOutput
        Call WriteTecplotGlobalFactorOutput

    Endif
    
  !  If (iTecplotLocal == 1) Call WriteTecplotLocalOutput

  !  If (iPavement == 1) Call WritePavementOutput

    End Subroutine Output2DG

    Subroutine Output3DG

    If (iOutputGlobal == 1) Call WriteOutputG
   
  !  If (iOutputLocal == 1) Call WriteLocalOutput

   If (iTecplotGlobal == 1) Then
        
        Call WriteTecplotGlobalOutput
        Call WriteTecplotGlobalFactorOutput

    Endif

  !  If (iTecplotLocal == 1) Call WriteTecplotLocalOutput

  !  If (iPavement == 1) Call WritePavementOutput

    End Subroutine Output3DG
    
End Module OutputModule