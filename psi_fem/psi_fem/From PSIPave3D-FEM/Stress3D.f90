Module CalculateStressandStrain3D
    
    Use GlobalInputVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use ViscoelasticStress
    Use IterationModule
    
    implicit Real*8 (A-H, O-Z)

    Contains    

	Subroutine CalculateGlobalStress3D
    
    !
    !   This subroutine calculates the stresses and strains for 3D cases
    !
    
    iViscoelastic = 0
    
    DeltaStrainG = 0
    
    Allocate (ModulusI1J2 (NumberOfGlobalElements))

    Do iel = 1, NumberOfGlobalElements  !Major loop over all elements
    
!
!   Calculate strain and stress
!

        Call SetUpQuadraturePoints (NGIntegrationPoints, NumberOfGlobalNodesPerElement)  ! Set up quadrature points and weights

        Do iIntPoint = 1, NGIntegrationPoints
        
            Call ShapeFunction3D (iel, iIntPoint)  !Set up B matrix
            
            Call CalculateStrain3D (iel, iIntPoint)
            
            Call ReadGlobalMaterialType3D (iel, iIntPoint, 0)  !Add material properties (1, means Called from stifnness matrix)
            
            Call CalculateStress3D (iel, iIntPoint)
            
            SG(iel,:,iIntPoint) = SG(iel,:,iIntPoint) + DeltaStressG(:) !Calculate total stress

            Call CalculateStressInvariants (iel,iIntPoint)

            DSIGRG = 0.D0 !Necessary if viscoelasticity is used

        Enddo
            
        If (mod(iel, iPrintToScreen) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) iel
 805        Format (10x,'Calculate Global Stress for Element =',I8)
            
        Endif        

    Enddo
          !      Write(6,21) time, sg(2,2,1)
21 format (7e15.7)           

    If (it > 1) Call IterationCheck

  !  Call PostCalculationStress3D
    
    End Subroutine CalculateGlobalStress3D
	
!*********************************************************************************************        

    Subroutine CalculateStrain3D (iel, iIntPoint)

    Real*8:: Q(IBRangeG)
    !
    !   This subroutine calculates the strain increment
    !
    Q = 0.0
    
    Do i = 1, NumberOfGlobalNodesPerElement
    
        ii = 3 * i
        Q (ii-2) = DeltaDisplacementG  (NodeG (iel, i) * 3 - 2)
        Q (ii-1) = DeltaDisplacementG  (NodeG (iel, i) * 3 - 1)
        Q (ii) = DeltaDisplacementG  (NodeG (iel, i) * 3)
        
    Enddo
    
    ! Multiply BG and Q --> strain displacement equations
    

    DeltaStrainG (iel,:,iIntPoint) = MatMul(BG, Q)
    
    StrainGold(iel,:,iIntPoint) = StrainG (iel,:,iIntPoint)    
    StrainG (iel,:,iIntPoint) = StrainG (iel,:,iIntPoint) + DeltaStrainG (iel,:,iIntPoint)  
    
    End Subroutine CalculateStrain3D

!*********************************************************************************************        
    
    Subroutine CalculateStress3D (iel, iIntPoint)

    !
    !   This subroutine calculates the stress increment
    !
    Real*8 DSigRG(6)
    
    DeltaStressG = MatMul(C, DeltaStrainG (iel,:,iIntPoint))

    If (MaterialTypeG(Iel) == 5) Then
        
        Call ViscoelasticStress3D (iel, iIntPoint, DSigRG, iLocation)
        
        DeltaStressG = DeltaStressG + DSigRG
    
    Endif
    

    End Subroutine CalculateStress3D
    
!*********************************************************************************************        
    
    Subroutine CalculateStressInvariants (iel, iIntPoint)

    If(.Not.Allocated(J2_Inv)) Allocate(J2_Inv(NGEL))
    If(.Not.Allocated(I1_Inv)) Allocate(I1_Inv(NGEL))
    If(.Not.Allocated(SGOctahedral)) Allocate(SGOctahedral(NGEL))


    !
    !   This subroutine calculates the stress invariants I1, J2 and Octahedral Stress
    !
    
    J2_Inv(iel) = (1.0/6.0)*((SG(iel,1,iIntPoint)-SG(iel,2,iIntPoint))**2+(SG(iel,2,iIntPoint)-SG(iel,3,iIntPoint))**2+(SG(iel,3,iIntPoint)-SG(iel,1,iIntPoint))**2)+(SG(iel,4,iIntPoint))**2+(SG(iel,5,iIntPoint))**2+(SG(iel,6,iIntPoint))**2
    I1_Inv(iel) = SG(iel,1,iIntPoint) + SG(iel,2,iIntPoint) + SG(iel,3,iIntPoint)
    aNormal1 = (SG(iel,1,iIntPoint)-SG(iel,2,iIntPoint))**2
    aNormal2 = (SG(iel,2,iIntPoint)-SG(iel,3,iIntPoint))**2
    aNormal3 = (SG(iel,3,iIntPoint)-SG(iel,1,iIntPoint))**2
    aNormal = aNormal1 + aNormal2 + aNormal3
    Shear = (SG(iel,4,iIntPoint))**2+(SG(iel,5,iIntPoint))**2+(SG(iel,6,iIntPoint))**2
    squareroot = (dsqrt(anormal+6*Shear))
    SGOctahedral(iel) = 1/3.*squareroot 
    

    End Subroutine CalculateStressInvariants
    
!*********************************************************************************************        
    
    Subroutine PostCalculationStress3D

    !
    !   This subroutine deallocates variables and reset others
    !
    
    Deallocate (DeltaStressG)
    
    IViscoElastic = 0
	IElastic = 0
    
    If (Allocated(BG)) Deallocate(BG)
    Deallocate (C)
    
 !   Deallocate (DeltaStrainG)
    Deallocate (ForceMatrixG)

    If (Allocated (DeltaDisplacementG)) Deallocate (DeltaDisplacementG)
    
    End Subroutine PostCalculationStress3D

!*********************************************************************************************
    
End Module CalculateStressandStrain3D