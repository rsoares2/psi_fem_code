Module CalculateForceMatrix2DL

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleForceMatrix
    Use CohesiveZoneForce2D
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine CalculateLocalForceMatrix2D
    
    !
    !   This subroutine calculates the Local force matrix for 2D meshes
    !    
    
    Real*8 :: SEIGEN(3), DSIGRG(4), SG_Temp(4), C(3,3)
	Integer :: iElastic, iViscoElastic

    DSIGRG = 0
    SEIGEN = 0
    
    If (iLocalCohesiveZones /=0) Call InterfaceForce2DLocal
    
!    Call ApplyLocalNodalForce
    
!    Call ApplyBCForce2D
    
    End Subroutine CalculateLocalForceMatrix2D

!*********************************************************************************************    
    
    Subroutine PostCalculationForce2DL  !may not be necessary
    
    IViscoElastic=0
    IViscoPlastic=0
	IElastic=0

	IF (Allocated (BG)) Deallocate(BG)	
    
    Deallocate (FElement)    
    Deallocate (C)
    
    End Subroutine PostCalculationForce2DL

!*********************************************************************************************  
    
    End Module CalculateForceMatrix2DL