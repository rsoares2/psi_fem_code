Module ElastoPastic
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine DruckerPrager2D  (iel, iIntPoint, ipc, MSeti, C)

    !
    !   This subroutine is a constitutive package for Drucker-Prager plasticity
    !
    
    ! 
    ! THIS IS A CONSTITUTIVE PACKAGE FOR RATE INDEPENDENT 
    ! DRUCKER-PRAGER PLASTICITY
    !
    ! THIS PROGRAM DRIVES AN INCREMENTAL CONSTITUTIVE ROUTINE 
    ! IN THE FORM DS=(C)DE THIS ROUTINE USES RATE INDEPENDENT 
    ! INCREMENTAL PLASTICITY THEORY 
    ! TO DETERMINE THE STRESS INCREMENT FOR A GIVEN STRAIN INCREMENT 
    ! AT A 2-D MATERIAL POINT 
    ! 

    Real*8:: C(3,3), AAlpha2(4)
    Real*8:: Stress(4), Strain(4), DStran(4), DSG(4), SGuess(4), SOld(4), EOld(4), DStres(4)
    Real*8:: ES(NumberOfGlobalElements,4,NGIntegrationPoints), DE(4),DFDS(6),DDFDS(6),DEPSE(4),DDEPSP(4)
    Real*8:: AA0, AA1, AA2, AA3

    
    YoungsModulus = EEGDP (MaterialSetG (iel)) 
    PoissonsRatio = VNUGDP (MaterialSetG (iel)) 
    YieldStress = YieldGDP (MaterialSetG (iel))
    BetaA = BBeta(MaterialSetG (iel))
    AAlpha1 = Alpha11 (iel,iIntPoint)
    RMui = MU(MaterialSetG (iel))

    DSG = 0.0
    DEPSP = 0.0
    Stress(:)=SGOld(iel,:,iIntPoint)
    Strain(:)=StrainGOld (iel,:,iIntPoint)  !Check here
    DSTRAN(:)=DeltaStrainG(iel,:,iIntPoint)
    AALPHA2(:)=ALPHA2(iel,:,iIntPoint)

    C = 0.0 !Initialize C

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2
    D44 = C1 / 2.0
        
    If (iSolutionType == 1) Then
        
        C(1,1) = (D11-D12*D12/D11) 
        C(1,2) = (D12-D12*D12/D11)
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
        C(3,3) = D44
    
    Elseif (iSolutionType == 2) Then

        C(1,1) = D11 
        C(1,2) = D12 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
	    C(3,3) = D44     
        
    Else

        Write(6,*)('Material model not in the code yet')
        Stop
    
    Endif

        DSG(1)=C(1,1)*DSTRAN(1)+C(1,2)*DSTRAN(2) 
        DSG(2)=C(2,1)*DSTRAN(1)+C(2,2)*DSTRAN(2) 
        DSG(3)=C(3,3)*DSTRAN(3) 
        IF(ISolutionType.EQ.2) DSG(4)=D12*(DSTRAN(1)+DSTRAN(2))
        IF(ISolutionType.EQ.1) DSTRAN(4)=-PoissonsRatio/YoungsModulus*(DSG(1)+DSG(2)) 
        SGuess = STRESS + DSG      
        RI1=SGuess(1)+SGuess(2)
        IF(ISolutionType.EQ.2) RI1=RI1+SGuess(4)
        ALPHA2SUB1=AALPHA2(1)+AALPHA2(2)
        IF(ISolutionType.EQ.2) ALPHA2SUB1=ALPHA2SUB1+AALPHA2(4)
        SGP1=SGuess(1)-RI1/3.
        SGP2=SGuess(2)-RI1/3.
        SGP4=SGuess(4)-RI1/3.
        SGP3=SGuess(3)
        ALPHA2P1=AALPHA2(1)-ALPHA2SUB1/3.
        ALPHA2P2=AALPHA2(2)-ALPHA2SUB1/3.
        ALPHA2P4=AALPHA2(4)-ALPHA2SUB1/3.
        ALPHA2P3=AALPHA2(3)

!
! NOW CALCULATE RJ2P AND RI1P   - J2 and I1 invariants
!
      RJ2P=.5*((SGP1-ALPHA2P1)**2+(SGP2-ALPHA2P2)**2+(SGP4-ALPHA2P4)**2+2.*(SGP3-ALPHA2P3)**2)
      RI1P=RI1-ALPHA2SUB1
!
! CALCULATE VALUE OF YIELD FUNCTION
!

      FDP=DSQRT(RJ2P)+RMUI*RI1P/SQRT(6.)-AALPHA1

!
! CHECK FOR YIELDING
!
      IF(DABS(F).LT.1.0E-6) F=0.0
      IF(FDP) 20,30,40 
   20 IPLAS(iel)=1 
      GO TO 2000 
   30 IPLAS(iel)=2 
 2000 CONTINUE

!
! RETURN IF CALCULATING TANGENT STIFFNESS MATRIX ONLY
!
      IF(IPC.NE.0) RETURN

      DO 3000 II=1,4 
      DeltaStressG=DSG 
      STRESS=STRESS+DeltaStressG
 3000 CONTINUE
      DS(IEL,:,iIntPoint)=DeltaStressG(:)   !Check which stress is here, DS?
      IF(ISolutionType.GT.1) GO TO 4999
      DeltaStrainG(IEL,4,iIntPoint)=DSTRAN(4)
 4999 CONTINUE
      RETURN 
   40 CONTINUE
! 
! YIELDING IS PREDICTED
! THIS BEGINS THE POST-YIELDED LOOP
!
      DO 4020 II=1,3 
      SOLD(II)=STRESS(II) 
 4020 EOLD(II)=STRAIN(II)
      SOLD(4)=STRESS(4)
      IF(ISolutionType.EQ.1) EOLD(4)=STRAIN(4)
      IF(IPLAS(IEL).GT.1) GO TO 1000
!
! THIS IS THE RADIAL RETURN ALGORITHM
! IT IS REQUIRED ONLY WHEN THE PREVIOUS STEP WAS NOT YIELDED
!

      DSGKK=DSG(1)+DSG(2)
      IF(ISolutionType.EQ.2) DSGKK=DSGKK+DSG(4)
      DSGP1=DSG(1)-DSGKK/3.
      DSGP2=DSG(2)-DSGKK/3.
      DSGP3=DSG(3)
      DSGP4=DSG(4)-DSGKK/3.
      STRESSKK=STRESS(1)+STRESS(2)+STRESS(3)
      STRESSP1=STRESS(1)-STRESSKK/3.
      STRESSP2=STRESS(2)-STRESSKK/3.
      STRESSP3=STRESS(3)
      STRESSP4=STRESS(4)-STRESSKK/3.
      AA=1./2.*(DSGP1**2+DSGP2**2+2.*DSGP3**2+DSGP4**2)+&
     MU(MSETI)**2/6.*(DSG(1)**2+DSG(2)**2+DSG(4)**2&
     +2.*DSG(1)*DSG(2)+2.*DSG(1)*DSG(4)+2.*DSG(2)*DSG(4))
      ALPHA2KK=AALPHA2(1)+AALPHA2(2)+AALPHA2(3)
      BBA=1./2.*(2.*STRESSP1*DSGP1-2.*DSGP1*ALPHA2P1&
     +2.*STRESSP2*DSGP2-2.*DSGP2*ALPHA2P2+2.*STRESSP4*DSGP4&
     -2.*DSGP4*ALPHA2P4+4.*STRESSP3*DSGP3-4.*DSGP3*AALPHA2(3))&
     +MU(MSETI)**2/6.*(2.*STRESS(1)*DSG(1)+2.*STRESS(2)*DSG(2)&
     +2.*STRESS(4)*DSG(4)+2.*DSG(1)*STRESS(2)+2.*STRESS(1)*DSG(2)&
     +2.*STRESS(1)*DSG(4)+2.*STRESS(4)*DSG(1)+2.*STRESS(2)*DSG(4)&
     +2.*DSG(2)*STRESS(4)-2.*DSG(1)*ALPHA2KK-2.*DSG(2)*ALPHA2KK&
     -2.*DSG(4)*ALPHA2KK)
      BBB=2.*AALPHA1*MU(MSETI)/SQRT(6.)*(DSG(1)+DSG(2)+DSG(4))
      BB=BBA+BBB
      CCA=1./2.*(STRESSP1**2+ALPHA2P1**2-2.*STRESSP1*ALPHA2P1&
     +STRESSP2**2+ALPHA2P2**2-2.*STRESSP2*ALPHA2P2+STRESSP4**2&
     +ALPHA2P4**2-2.*STRESSP4*ALPHA2P4+2.*STRESS(3)**2&
     +2.*AALPHA2(3)**2-4.*STRESS(3)*AALPHA2(3))
      CCB=MU(MSETI)**2/6.*(STRESS(1)**2+STRESS(2)**2+STRESS(4)**2-&
     ALPHA2KK**2+2.*STRESS(1)*STRESS(2)+2.*STRESS(1)*STRESS(4)&
     +2.*STRESS(2)*STRESS(4)-2.*STRESS(1)*ALPHA2KK&
     -2.*STRESS(2)*ALPHA2KK-2.*STRESS(4)*ALPHA2KK)&
     +2.*AALPHA1*MU(MSETI)/SQRT(6.)*(STRESSKK-ALPHA2KK)
      CC=CCA+CCB-AALPHA1**2
      ROOT=BB**2-4.*AA*CC
      IF(ROOT.LE.1E-10) ROOT=0.0 
      IF(DABS(AA).LT.1.D-5) GO TO 595
      AAtest=AA
      AA0 = DSQRT(ROOT)      
      AA1 = -BB+DSQRT(ROOT)
      AA2 = (-BB+DSQRT(ROOT))/2.
      AA3 = (-BB+DSQRT(ROOT))/2./AA
      ZETA=(-BB+DSQRT(ROOT))/2./AA 
      GO TO 596
  595 CONTINUE
      ZETA=0.
  596 CONTINUE
   !   WRITE(6,5099) ZETA
 !5099 FORMAT(//,10X,'ZETA = ',D15.7,/) 
      DO 500 I=1,3 
      STRESS(I)=STRESS(I)+ZETA*DSG(I) 
  500 STRAIN(I)=STRAIN(I)+ZETA*DSTRAN(I)
      STRESS(4)=STRESS(4)+ZETA*DSG(4)
      DO 50 I=1,3 
   50 DSTRAN(I)=(1.-ZETA)*DSTRAN(I) 
      GO TO 1001 
!
! END OF RADIAL RETURN ALGORITHM
!
 1000 ZETA=0.
        iPlas(IEL)=2
 1001 CONTINUE
       IF(IPC==1) GO TO 3030

      IF(iSubIncrementation.EQ.0) GO TO 3030
!
! CALCULATE SUBINCREMENTATION STEP SIZE
! 
      DEE=(4./3.*(DSTRAN(1)**2+DSTRAN(2)**2+2.*DSTRAN(4)**2+DSTRAN(3)**2))**.5 
      NSUB=DEE/DeltaStrainSubIncrement+.1 
      IF(NSUB.EQ.0) NSUB=1 
      DO 400 I=1,3
  400 DSTRAN(I)=DSTRAN(I)/NSUB
      GO TO 3090 
 3030 NSUB=1
 3090 CONTINUE
!
! SUBINCREMENTATION LOOP
!
      DO 5000 ISUBINC=1,NSUB
      IJ=1 
 1002 IF(EPBAR(IEL,iIntPoint).LE.(EPX(IJ,MSETI)-.0000002)) GO TO 1010 
      IF(IJ.GT.NUNIAX(MSETI)) GO TO 1003 
      IJ=IJ+1 
      GO TO 1002 
 1003 WRITE(6,7001) 
 7001 FORMAT(10X,'STOP - EPBAR IS TOO BIG') 
      WRITE(6,7019) MSETI,IEL,EPBAR(IEL,iIntPoint) 
 7019 FORMAT(10X,'MATERIAL NO. ',I3,2X,'ELEMENT NO. ',I3,/,10X,'EPBAR = ',E15.7,/) 
      STOP     
 1010 IF(IJ.EQ.1) IJ=2
!
! CALCULATE TANGENT MODULUS
!
      HPRIME=(SP(IJ,MSETI)-SP(IJ-1,MSETI))/(EPX(IJ,MSETI)-EPX(IJ-1,MSETI))
      CCC=2./3.*HPRIME
      RI1=STRESS(1)+STRESS(2)
      IF(ISolutionType.EQ.2) RI1=RI1+STRESS(4)
      SP1=STRESS(1)-RI1/3.
      SP2=STRESS(2)-RI1/3.
      SP3=STRESS(3)
      SP4=STRESS(4)-RI1/3.
      ALPHASUB1=AALPHA2(1)+AALPHA2(2)
      IF(ISolutionType.EQ.2) ALPHASUB1=SALPHA2SUB1+AALPHA2(4)
      ALPHAP1=AALPHA2(1)-ALPHASUB1/3.
      ALPHAP2=AALPHA2(2)-ALPHASUB1/3.
      ALPHAP3=AALPHA2(3)
      ALPHAP4=AALPHA2(4)-ALPHASUB1/3.
      RJ2P=.5*((SP1-ALPHAP1)**2+(SP2-ALPHAP2)**2+(SP4-ALPHAP4)**2+2.*(SP3-ALPHAP3)**2)
      FAC1=1./2./SQRT(RJ2P)
      FAC2=SQRT(6.)
      DFDS(1)=FAC1*(SP1-ALPHAP1)+MU(MSETI)/FAC2
      DFDS(2)=FAC1*(SP2-ALPHAP2)+MU(MSETI)/FAC2
      DFDS(3)=FAC1*(SP4-ALPHAP4)+MU(MSETI)/FAC2
      DFDS(4)=0.
      DFDS(5)=0.
      DFDS(6)=FAC1*(SP3-ALPHAP3) 
      DDFDS(1)=D11*DFDS(1)+D12*DFDS(2)+D12*DFDS(3) 
      DDFDS(2)=D12*DFDS(1)+D11*DFDS(2)+D12*DFDS(3) 
      DDFDS(3)=D12*DFDS(1)+D12*DFDS(2)+D11*DFDS(3) 
      DDFDS(4)=D44*DFDS(4) 
      DDFDS(5)=D44*DFDS(5) 
      DDFDS(6)=D44*DFDS(6) 
      SDFDS=DFDS(1)**2+DFDS(2)**2+DFDS(3)**2+2.*DFDS(4)**2+2.*DFDS(5)**2+2.*DFDS(6)**2 
      DFDDF=DDFDS(1)*DFDS(1)+DDFDS(2)*DFDS(2)+DDFDS(3)*DFDS(3)&
     +2.*DDFDS(4)*DFDS(4)+2.*DDFDS(5)*DFDS(5)+2.*DDFDS(6)*DFDS(6) 
      DENOM=CCC*SDFDS+DFDDF 
      C11=D11-DDFDS(1)*DDFDS(1)/DENOM 
      C12=D12-DDFDS(1)*DDFDS(2)/DENOM 
      C13=D12-DDFDS(1)*DDFDS(3)/DENOM  
      C14=-DDFDS(1)*DDFDS(4)/DENOM 
      C15=-DDFDS(1)*DDFDS(5)/DENOM 
      C16=-DDFDS(1)*DDFDS(6)/DENOM 
      C22=D11-DDFDS(2)**2/DENOM 
      C23=D12-DDFDS(2)*DDFDS(3)/DENOM 
      C24=-DDFDS(2)*DDFDS(4)/DENOM 
      C25=-DDFDS(2)*DDFDS(5)/DENOM 
      C26=-DDFDS(2)*DDFDS(6)/DENOM 
      C33=D11-DDFDS(3)**2/DENOM 
      C34=-DDFDS(3)*DDFDS(4)/DENOM 
      C35=-DDFDS(3)*DDFDS(5)/DENOM 
      C36=-DDFDS(3)*DDFDS(6)/DENOM 
      C44=D44-DDFDS(4)**2/DENOM 
      C45=-DDFDS(4)*DDFDS(5)/DENOM 
      C46=-DDFDS(4)*DDFDS(6)/DENOM 
      C55=D44-DDFDS(5)**2/DENOM 
      C56=-DDFDS(5)*DDFDS(6)/DENOM 
      C66=D44-DDFDS(6)**2/DENOM 

      If(ISolutionTYPE.EQ.1) GO TO 7771
! 
! REDUCE TANGENT MODULUS TO 2-D
! 
! 
! PLANE STRAIN 
! 
      C(1,1)=C11 
      C(1,2)=C12
      C(1,3)=C16 
      C(2,1)=C(1,2) 
      C(2,2)=C22
      C(2,3)=C26 
      C(3,1)=C(1,3) 
      C(3,2)=C(2,3) 
      C(3,3)=C66 
      GO TO 340 
  339 CONTINUE 
  340 CONTINUE 
      GO TO 7772 
 7771 CONTINUE 
! 
! PLANE STRESS 
! 
      C(1,1)=C11-C13*C13/C33 
      C(1,2)=C12-C13*C23/C33 
      C(1,3)=C16-C13*C36/C33 
      C(2,1)=C(1,2) 
      C(2,2)=C22-C23*C23/C33 
      C(2,3)=C26-C23*C36/C33 
      C(3,1)=C(1,3) 
      C(3,2)=C(2,3) 
      C(3,3)=C66-C36*C36/C33 
 7772 CONTINUE 
      IF(IPC.NE.0) RETURN 

      !!!!!!!!!!!!!!!!!!! RETURN FROM HERE IF NOT CALCULATING STRESS
!
! CALCULATE STRESS INCREMENT
!
      DSTRES(1)=C(1,1)*DSTRAN(1)+C(1,2)*DSTRAN(2)+C(1,3)*DSTRAN(3) 
      DSTRES(2)=C(2,1)*DSTRAN(1)+C(2,2)*DSTRAN(2)+C(2,3)*DSTRAN(3) 
      DSTRES(3)=C(3,1)*DSTRAN(1)+C(3,2)*DSTRAN(2)+C(3,3)*DSTRAN(3) 
      IF(ISolutionType.EQ.2) GO TO 555 
      DSTRES(4)=0. 
      DSTRAN(4)=(-1./C33)*(C13*DSTRAN(1)+C23*DSTRAN(2)+2.*C36*DSTRAN(3))
      GO TO 556 
  555 CONTINUE 
      DSTRES(4)=C13*DSTRAN(1)+C23*DSTRAN(2)+C36*DSTRAN(3) 
      DSTRAN(4)=0.
  556 CONTINUE

!
! UPDATE STRESS AND STRAIN
!
        STRESS = STRESS + DSTRES 
        STRAIN = DSTRAN + STRAIN
!
! UPDATE INTERNAL VARIABLE ALPHA1
!
      SKK=STRESS(1)+STRESS(2)+STRESS(3)
      SP1=STRESS(1)-SKK/3.
      SP2=STRESS(2)-SKK/3.
      SP3=STRESS(3)
      SP4=STRESS(4)-SKK/3.
      RJ2=1./2.*(SP1**2+SP2**2+2.*SP3**2+SP4**2)
      FACALPH1=SP1*DSTRES(1)+SP1*DSTRES(2)+2.*SP3*DSTRES(3)+SP4*DSTRES(4)
      FAC1ALPH1=FACALPH1/2./DSQRT(RJ2)  !CHECK THIS
      FAC2ALPH1=RMUI/SQRT(6.)*(DSTRES(1)+DSTRES(2)+DSTRES(4))

      DALPHA1=BETAA*(FAC1ALPH1+FAC2ALPH1)
      AALPHA1=AALPHA1+DALPHA1

! 
! CALCULATE PLASTIC STRAIN INCREMENT 
! 
      IF(ISolutionType.EQ.1) GO TO 4500         !STOP HERE FOR MODULUS ONLY
! 
! PLANE STRAIN 
! 
      DDEPSP(1)=DSTRAN(1)-DSTRES(1)/YoungsModulus+PoissonsRatio*DSTRES(2)/YoungsModulus+PoissonsRatio*DSTRES(4)/YoungsModulus 
      DDEPSP(2)=DSTRAN(2)+PoissonsRatio*DSTRES(1)/YoungsModulus-DSTRES(2)/YoungsModulus+PoissonsRatio*DSTRES(4)/YoungsModulus 
      DDEPSP(3)=DSTRAN(3)-2.*(1.+PoissonsRatio)*DSTRES(3)/YoungsModulus 
      DDEPSP(4)=0.+PoissonsRatio*(DSTRES(1)+DSTRES(2))/YoungsModulus-DSTRES(4)/YoungsModulus 
      GO TO 4550 
 4500 CONTINUE 
! 
! PLANE STRESS 
! 
      DDEPSP(1)=DSTRAN(1)-(DSTRES(1)-PoissonsRatio*(DSTRES(2)))/YoungsModulus 
      DDEPSP(2)=DSTRAN(2)-(DSTRES(2)-PoissonsRatio*(DSTRES(1)))/YoungsModulus 
      G=YoungsModulus/2./(1.+PoissonsRatio) 
      DDEPSP(3)=DSTRAN(3)-DSTRES(3)/G 
      RLAMBDAA=SQRT(3.)/HPRIME*DALPHA1/DTIME/DSQRT(1.+RMUI**2)      
      DDEPSP(4)=3.*RLAMBDAA*RMUI/SQRT(6.)-DDEPSP(1)-DDEPSP(2)

 4550 CONTINUE 
!
! UPDATE ELASTIC AND PLASTIC STRAIN INCREMENTS
!
        DEPSP(IEL,:,iIntPoint)=DEPSP(IEL,:,iIntPoint)+DDEPSP(:) 
        DEPSE(:)=DSTRAN(:)-DEPSP(IEL,:,iIntPoint) 
!
! UPDATE EQUIVALENT UNIAXIAL PLASTIC STRAIN
!
      DDEPBAR=DSQRT(2./3.*(DDEPSP(1)**2+DDEPSP(2)**2+2.*DDEPSP(3)**2+DDEPSP(4))**2)
      EPBAR(IEL,iIntPoint)=EPBAR(IEL,iIntPoint)+DDEPBAR 
!
! UPDATE INTERNAL VARIABLE ALPHA2
!
      SMA1=STRESS(1)-AALPHA2(1) 
      SMA2=STRESS(2)-AALPHA2(2) 
      SMA3=STRESS(3)-AALPHA2(3) 
      SMA4=STRESS(4)-AALPHA2(4) 
      DEN=DFDS(1)*SMA1+DFDS(2)*SMA2+DFDS(3)*SMA4+2.*DFDS(6)*SMA3 
      DMUBAR=(DFDS(1)*DSTRES(1)+DFDS(2)*DSTRES(2)+DFDS(3)*DSTRES(4)+2.*DFDS(6)*DSTRES(3)-DALPHA1)/DEN
      IF(BETAA.GT.0.999999) DMUBAR=0.
      AALPHA2(1)=AALPHA2(1)+DMUBAR*SMA1 
      AALPHA2(2)=AALPHA2(2)+DMUBAR*SMA2 
      AALPHA2(3)=AALPHA2(3)+DMUBAR*SMA3 
      AALPHA2(4)=AALPHA2(4)+DMUBAR*SMA4 
 5000 CONTINUE    

      !!!!!!!!!!!!!!!!!!! END RETURN FROM HERE IF NOT CALCULATING STRESS
!
! END OF SUBINCREMENTATION LOOP
!
        DSTRES=STRESS-SOLD 
        DS(IEL,:,iIntPoint)=DSTRES(:) 
        DSTRAN=STRAIN-EOLD
!
! UPDATE GLOBAL VARIABLES
!
      ALPHA11(IEL,iIntPoint)=AALPHA1
      ALPHA2(IEL,1,iIntPoint)=AALPHA2(1) 
      ALPHA2(IEL,2,iIntPoint)=AALPHA2(2) 
      ALPHA2(IEL,3,iIntPoint)=AALPHA2(3) 
      ALPHA2(IEL,4,iIntPoint)=AALPHA2(4) 
      IPLAS(IEL)=2 
      IF(ISolutionType.GT.1) GO TO 9999
      DeltaStrainG(IEL,4,iIntPoint)=DSTRAN(4)
 9999 CONTINUE
      RETURN

    End Subroutine DruckerPrager2D
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    
End Module ElastoPastic