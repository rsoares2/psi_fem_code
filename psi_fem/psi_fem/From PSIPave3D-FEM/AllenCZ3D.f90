Module AllenCZModel3D
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    Use LinkedConectivity
    
    implicit Real (A-H, O-Z)

    Contains 

    Subroutine ReadGlobalInputAllenCZModel3D
	
    !
    !   This subroutine Reads properties for the Allen cohesive zone model in 3D
    !
    !   iGlobalCohesiveZones = 4
    !

    Write(6,9502)
 9502	Format(/,1X,'Allen 3D Viscoelastic Cohesive Zone',/)
	Read (5,*) NGlobalInterfElementsG, NGlobalInterfMaterialG, NumberPronyTermsCZ 
 9108	Format(3I5) 

	Allocate ( SigmaNormalG (NGlobalInterfElementsG) )
	Allocate ( DeltaNormalG (NGlobalInterfElementsG) )
	Allocate ( SigmaTangG (NGlobalInterfElementsG) )
	Allocate ( DeltaTangG (NGlobalInterfElementsG) )
	Allocate ( SigmaTangSG (NGlobalInterfElementsG) )   !TangS means the second tangential (shear) component
	Allocate ( DeltaTangSG (NGlobalInterfElementsG) )
	Allocate ( AlphG (NGlobalInterfElementsG) ) !This is the alpha coefficient for the nonlinear damage evolution law
	Allocate ( RMG (NGlobalInterfElementsG) )   !this is the exponent for the nonlinear damage evolution law
	Allocate ( ECinfG (NGlobalInterfElementsG) )

	Write(6,9150) NGlobalInterfElementsG,NGlobalInterfMaterialG,NumberPronyTermsCZ 
9150	Format(10X,'Number of interface elements = ',I5,/, &
	10X,'Number of interface material groups = ',I5,/,&
	10X,'NUmber of terms in Prony Series =     ',I5,//,&
	5X,'Group number',6X,'SigmaNormalG',6X,'DeltaNormalG',6X,'SigmaTangG',6X,'DeltaTangG',8X,'AlphG',10X,'RMG',8X,'EInfG',/)
	
    Do i = 1, NGlobalInterfElementsG
        
        Read(5,*) SigmaNormalG(i),DeltaNormalG(i),SigmaTangG(i),DeltaTangG(i),AlphG(i),RMG(i),ECinfG(i) 
		Write(6,9857) i,SigmaNormalG(i),DeltaNormalG(i),SigmaTangG(i),DeltaTangG(i),AlphG(i),RMG(i),ECinfG(i)
    
    Enddo
9857    Format(11X,i3,7D12.5) 
    
		Write(6,9859)
9859    Format(/,5X,'GROUP NO.',2X,'PRONY NO.',8X,'E(I)',6X,'z(I)',//)

	Allocate ( ECG(NGlobalInterfElementsG, NumberPronyTermsCZ) )
	Allocate ( ETACG(NGlobalInterfElementsG, NumberPronyTermsCZ) )

		Do i = 1, NGlobalInterfElementsG
            
			Do j = 1, NumberPronyTermsCZ
			
                Read(5,*) ECG(I,J),ETACG(I,J)
				Write(6,2020) I,J,ECG(I,J),ETACG(I,J)
        
            Enddo
            
        Enddo
        
2019    Format(2D15.7)
2020    Format(11X,I3,9X,I3,2D12.5)
        
    If(iGlobalCohesiveZones == 5)	NGlobalInterfElementsG = 0    !Use for automatic insertion of CZ

    If(iGlobalCohesiveZones == 4) Then
        
        Write(6,9123) 
 9123	Format(/,10X,'INTERFACE CONNECTIVITY MATRIX',//, &
		10X,'NODE 1',4X,'NODE 2',3X,'MAT NO.',12X,'WIDTH',12X,'PHIAV',/)

		Allocate ( NINT1G(NGlobalInterfElementsG*2) )
		Allocate ( NINT2G(NGlobalInterfElementsG*2) )


		Allocate ( iMaterialCZG(NGlobalInterfElementsG*2) )
		Allocate ( WIDTHG(NGlobalInterfElementsG*2) ) 
		Allocate ( PHIAVG(NGlobalInterfElementsG*2) )

		Allocate ( TractionNormalG(NGlobalInterfElementsG*2) )
		Allocate ( TractionTangentG(NGlobalInterfElementsG*2) )
        
! THE COHESIVE ZONE INPUT SHOULD BE COUNTER-CLOCKWISE. FIRST ONE EDGE LOWER AND UPPER NODES AND THEN THE SECOND EDGE, UPPER AND LOWER.

	!   |               |
	!   |               |
	!   1_______________2
	!
	!   3_______________4
	!   |               |
	!   |               |
	!
	!   The correct numbering order should be:
	!
	!   2 1 (1) 3 4  (1)
    
    Do i = 1, NGlobalInterfElementsG
            
		Read(5,*) NINT1G(2*I-1),NINT1G(2*I),iMatCZ, NINT2G(2*I),NINT2G(2*I-1),iMatCZ    !iMatCZ is the CZ material
		
        Call SetUpAllenCZangles2D(i, IGR)
 
        Call LinkedConnectivityCZ(i)    !subroutine to assemble CZ in sparse Format
    
    Enddo
    
		Do i = 1, NGlobalInterfElementsG
		    
            j = 2*i-1
            TractionNormalG (j) = SigmaNormalG ( iMaterialCZG(i) )
			TractionTangentG (j) = SigmaTangG ( iMaterialCZG(i) )
            TractionNormalG (j+1) = SigmaNormalG ( iMaterialCZG(i) )
			TractionTangentG (j+1) = SigmaTangG ( iMaterialCZG(i) )            
        
        Enddo
                
	    NGlobalInterfElementsG = 2 * NGlobalInterfElementsG
        
    EndIf
	
    End Subroutine ReadGlobalInputAllenCZModel3D

!*********************************************************************************************            
    
    Subroutine SetUpAllenCZangles2D(i, IGR)

    !
    !   This subroutine calculates the angles and widths for Allen CZ
    !
    
    WIDTH = SQRT((XG(NINT1G(2*I-1))-XG(NINT1G(2*I)))**2+(YG(NINT1G(2*I-1))-YG(NINT1G(2*I)))**2)/2
	WIDTHG = WIDTH 
    iDOF1 = 2*i - 1
    iDOF2 = 2*i
	Y2Y1 =  YG (NINT1G(iDOF2)) - YG (NINT1G(iDOF1))
	X2X1 =  XG (NINT1G(iDOF2)) - XG (NINT1G(iDOF1)) 
	PHIA = ATAN2(Y2Y1,X2X1) !TO CALCULATE ANGLES OF CZ - Y2Y1 = Y2- Y1
		

!		PHIANGLEDEGREES=PHIA*180/3.141592653589790


	!	PI=3.141592653589790/2
	!	PHIA=PHIA+PI

	!    PHIANGLEDEGREES2=PHIA*180/3.141592653589790

	PHIAVG = PHIA
	iMaterialCZG = iMatCZ  !Setting up equal materials, angle and widths for all CZ pairs.

	Write(6,9134) NINT1G(iDOF1),NINT2G(iDOF1),NINT1G(iDOF2),NINT2G(iDOF2),iMaterialCZG(iDOF2),WIDTHG(iDOF1),PHIAVG(iDOF2) 
9134	Format(13X,I5,6X,I5,6X,I5,6X,I5,6X,I5,5X,D12.5,5X,D12.5) 
        
    End Subroutine SetUpAllenCZangles2D
    

!*********************************************************************************************            
    
End Module AllenCZModel3D