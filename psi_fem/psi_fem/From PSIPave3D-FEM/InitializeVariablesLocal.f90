MODULE InitializeLocalVariables

    Use LocalInputVariables
    Use LinearElasticity
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine LocalInitial
	
    If (NumberOfDimensions == 2 ) Then 
        
        iRangeL = 4 !This is 4 for 2-D and 6 for 3-D. Relates to the size of the stress matrix
        TL = 1.0
        
    ElseIf (NumberOfDimensions == 3 ) Then 
        
        iRangeL = 6
        
	    If (MaterialNumber3L /= 0) Then

            Allocate (X(NumberOfLocalElements, iRange, NGIntegrationPoints))
            X=0
        
        Endif
    
    Endif
    
       
    IBRangeL = NumberOfDimensions * NumberOfLocalNodesPerElement    !Range of the B Matrix        
    
    iPrintToScreenL = NLocalElements/5   !Flag to print progress of Stiffness matrix to screen
    If(iPrintToScreenL == 0) iPrintToScreenL = 1  !Only prints five lines

    !
    !   Initialize stress, strains and deflections
    !
    
	Allocate ( SL(NumberOfLocalElements, iRangeL, NLIntegrationPoints))
	Allocate ( StrainL(NumberOfLocalElements, iRangeL, NLIntegrationPoints))
	Allocate ( DisplacementL(NLTotalDOF+2*NLINEL ))
    
    DisplacementL = 0.0
    SL = 0.0
    StrainL = 0


    
	If(iLocalCohesiveZones /=0 .OR.iLocalCohesiveZones <= 4) Then !If there is CZ a priori. If there is automatic insertion, skip
	
	    Allocate ( iDebndL (NLocalInterfElementsL) )
		Allocate ( DamageL (NLocalInterfElementsL) )
	    IDEBNDL = 0
        DamageL = 0.0
            
    Endif
    
 	If(iLocalCohesiveZones == 4 ) Then  !Only allocate here if using Allen CZ a priori
            
		Allocate ( OLAMBDAL(NLocalInterfElementsL))
		Allocate ( SIGCOHTL(NLocalInterfElementsL,NumberPronyTermsCZ))
		Allocate ( SIGCOHNL(NLocalInterfElementsL,NumberPronyTermsCZ))

		OLAMBDAL = 0.
		SIGCOHTL = 0.
		SIGCOHNL = 0.        
        
    Endif
    

	If (iVisL == 1) Then !If there is V/E material

        If (.NOT. Allocated (SIL)) Allocate ( SIL (NumberOfLocalViscoElasticElement, NumberOfLocalElements, NLPronyL, iRangeL, iRangeL, NLIntegrationPoints))
        If (.NOT. Allocated (CC3L)) Allocate ( CC3L (iRangeL,iRangeL))
        Allocate ( SILAvg (NumberOfLocalViscoElasticElement, NumberOfLocalElements, NLPronyL, iRangeL, iRangeL, NLIntegrationPoints))
        SIL = 0.
        SiLAvg = 0
    Endif
    

    Open (Unit = 13,FILE ='LocalStress.OUT', Status = "Replace")  !Needed because the files are written with "append".
    Close (13)
    Open (Unit= 14,FILE ="LocalStrain.OUT", Status = "Replace")  !This statement will restart the files after each simulation.
    Close (14)
    Open (Unit = 33,FILE ='LocalDisplacement.OUT', Status = "Replace")
    Close(33)    

    Call CalculateInitialAVGStiffness
    
    End Subroutine LocalInitial
    
    Subroutine CalculateInitialAVGStiffness
    
!      
!   This subroutine calculates averaged [C] matrix for the local scale materials at time t = 0
!   In addition it also calculates the volume fractions of each material in the local scale
!

	Real*8 :: Area1, Area2, AreaTOTAL, AreaAVG(MaterialNumber1L + MaterialNumber5L)
	Real*8 :: CLINF11AVG (1),CLINF12AVG (1),CLINF22AVG (1),CLINF23AVG (1)
	Real*8 :: CLINF44AVG (1),CLINF66AVG (1),CL11AVG(1,NLPronyL),CL12AVG(1,NLPronyL)
	Real*8 :: CL22AVG(1,NLPronyL),CL23AVG( 1,NLPronyL),CL44AVG(1,NLPronyL),CL66AVG(1,NLPronyL)
	Real*8 :: ETA11AVG(1,NLPronyL),ETA12AVG(1,NLPronyL),ETA22AVG(1,NLPronyL),ETA23AVG(1,NLPronyL)
	Real*8 :: ETA44AVG(1,NLPronyL),ETA66AVG(1,NLPronyL)
	Real*8 :: C(3,3),DTSRANL(4),EMODULUS    
    
    AreaAVG = 0
    CL11AVG = 0
    CL12AVG = 0
    CL22AVG = 0
    CL23AVG = 0
    CL44AVG = 0
    CL66AVG = 0
    
    ETA11AVG = 0
    ETA12AVG = 0
    ETA22AVG = 0
    ETA23AVG = 0
    ETA44AVG = 0
    ETA66AVG = 0   
    
    Do Iel = 1, 1   !If using different local element meshes, need to modify this number
        
    Do Jel = 1, NumberOfLocalElements
    
		MType = MaterialTypeL(Jel)
		MSet = MaterialSetL(Jel)        

        X1 = XL(NodeL(Jel,1)) 
		X2 = XL(NodeL(Jel,2)) 
		X3 = XL(NodeL(Jel,3)) 
		Y1 = YL(NodeL(Jel,1)) 
		Y2 = YL(NodeL(Jel,2)) 
		Y3 = YL(NodeL(Jel,3))

		Area = (X2*Y3+X1*Y2+X3*Y1-X2*Y1-X3*Y2-X1*Y3)/2.

		AreaAVG(MSet) = AreaAVG(MSet) + Area
        AreaTotal = AreaTotal + Area
        
        If (MType == 1) Then !This is for linear elastic constituive relation

	        Call IsotropicLinearElasticLocal2D (Jel, C)

			CLInf11AVG(Iel)=CLInf11AVG(Iel)+(C(1,1)*Area)
			CLInf12AVG(Iel)=CLInf12AVG(Iel)+(C(1,2)*Area)
			CLInf22AVG(Iel)=CLInf22AVG(Iel)+(C(1,1)*Area)
			CLInf23AVG(Iel)=CLInf23AVG(Iel)+(C(1,2)*Area)
			CLInf44AVG(Iel)=CLInf44AVG(Iel)+(C(3,3)*Area)
			CLInf66AVG(Iel)=CLInf66AVG(Iel)+(C(3,3)*Area)
            
        ElseIf (MType == 3) Then !This is for Viscoplastic constitutive relation
    
            Write(6,*) 'Viscoplasticity is not implemented at local scale'
    !	    Call BODNER3D (Iel,C,1,MSETI,IINTPT)
		    STOP
        
        ElseIf (MType == 5) Then !This is for linear Viscoelastic constitutive relation
        
			CLInf11AVG(Iel)=CLInf11AVG(Iel)+(CInf11L(MSet-MaterialNumber1L)*Area)
			CLInf12AVG(Iel)=CLInf12AVG(Iel)+(CInf12L(MSet-MaterialNumber1L)*Area)
			CLInf22AVG(Iel)=CLInf22AVG(Iel)+(CInf22L(MSet-MaterialNumber1L)*Area)
			CLInf23AVG(Iel)=CLInf23AVG(Iel)+(CInf23L(MSet-MaterialNumber1L)*Area)
			CLInf44AVG(Iel)=CLInf44AVG(Iel)+(CInf44L(MSet-MaterialNumber1L)*Area)
			CLInf66AVG(Iel)=CLInf66AVG(Iel)+(CInf66L(MSet-MaterialNumber1L)*Area)
	
			Do K=1,NLPronyL
				
				CL11AVG(Iel,K)=CL11AVG(Iel,K)+(CL11L(MSet-MaterialNumber1L,K)*Area)
				CL12AVG(Iel,K)=CL12AVG(Iel,K)+(CL12L(MSet-MaterialNumber1L,K)*Area)
				CL22AVG(Iel,K)=CL22AVG(Iel,K)+(CL22L(MSet-MaterialNumber1L,K)*Area)
				CL23AVG(Iel,K)=CL23AVG(Iel,K)+(CL23L(MSet-MaterialNumber1L,K)*Area)
				CL44AVG(Iel,K)=CL44AVG(Iel,K)+(CL44L(MSet-MaterialNumber1L,K)*Area)
				CL66AVG(Iel,K)=CL66AVG(Iel,K)+(CL66L(MSet-MaterialNumber1L,K)*Area)
	
				Eta11AVG(Iel,K)=Eta11AVG(Iel,K)+(Eta11L(MSet-MaterialNumber1L,K)*Area)
				Eta12AVG(Iel,K)=Eta12AVG(Iel,K)+(Eta12L(MSet-MaterialNumber1L,K)*Area)
				Eta22AVG(Iel,K)=Eta22AVG(Iel,K)+(Eta22L(MSet-MaterialNumber1L,K)*Area)
				Eta23AVG(Iel,K)=Eta23AVG(Iel,K)+(Eta23L(MSet-MaterialNumber1L,K)*Area)
				Eta44AVG(Iel,K)=Eta44AVG(Iel,K)+(Eta44L(MSet-MaterialNumber1L,K)*Area)
				Eta66AVG(Iel,K)=Eta66AVG(Iel,K)+(Eta66L(MSet-MaterialNumber1L,K)*Area)
	
            Enddo
            
        Endif
    
		If (Jel == NumberOfLocalElements) Then

			Do i = 1, MaterialNumber1L + MaterialNumber5L
			! Write the total volume fractions of each material at the last step
		
				If (AreaAVG(i) /= 0) Then
					Write(6,8229) I,AreaAVG(i)/AreaTotal * 100, AreaAVG(i)
				Endif
		
			8229 Format (/,5X,'Volume Fraction of Material ',I3,' : ',1F8.4,' % ;   Area = 'F7.3)
		
				If (MaterialNumber1L == 0) Then
					IMaterial = i
				Else
					IMaterial = MaterialNumber1L + i
				Endif
		
			Enddo
	
        Endif
        
    Enddo
    
    CLInf11AVG(Iel)=CLInf11AVG(Iel)/AreaTotal
	CLInf12AVG(Iel)=CLInf12AVG(Iel)/AreaTotal
	CLInf22AVG(Iel)=CLInf22AVG(Iel)/AreaTotal
	CLInf23AVG(Iel)=CLInf23AVG(Iel)/AreaTotal
	CLInf44AVG(Iel)=CLInf44AVG(Iel)/AreaTotal
	CLInf66AVG(Iel)=CLInf66AVG(Iel)/AreaTotal

	If(MaterialNumber1L /= 0) Then

	    Do K=1,NLPronyL

		    CL11AVG(Iel,K) = CL11AVG(Iel,K)/AreaTotal
		    CL12AVG(Iel,K) = CL12AVG(Iel,K)/AreaTotal
		    CL22AVG(Iel,K) = CL22AVG(Iel,K)/AreaTotal
		    CL23AVG(Iel,K) = CL23AVG(Iel,K)/AreaTotal
		    CL44AVG(Iel,K) = CL44AVG(Iel,K)/AreaTotal
		    CL66AVG(Iel,K) = CL66AVG(Iel,K)/AreaTotal
	
		    Eta11AVG(Iel,K) = Eta11AVG(Iel,K)/AreaTotal
		    Eta12AVG(Iel,K) = Eta12AVG(Iel,K)/AreaTotal
		    Eta22AVG(Iel,K) = Eta22AVG(Iel,K)/AreaTotal
		    Eta23AVG(Iel,K) = Eta23AVG(Iel,K)/AreaTotal
		    Eta44AVG(Iel,K) = Eta44AVG(Iel,K)/AreaTotal
		    Eta66AVG(Iel,K) = Eta66AVG(Iel,K)/AreaTotal

        Enddo
    
	Endif    
    Enddo
    
    End Subroutine CalculateInitialAVGStiffness


!**************************************************************************************    
    
    Subroutine InitializeLocalStiffnessMatrix2D

        Allocate (BL (3,IBRangeL))   !This is the B Matrix (shape functions) 
        Allocate (KElementL (IBRangeL, IBRangeL))
        Allocate (KElement_TempL (IBRangeL, IBRangeL/2))
        Allocate (DSigrL(6))

        IViscoElasticL = 0
	    IElasticL = 0
        DSigrL = 0

        ! Initialize sparse stiffness matrix
        Allocate (SparseValuesL (NZL) )
        SparseValuesL = 0.0D0
        
        Allocate (FRGlobal (NLTotalDOF) )   !This matrix is int([B]*[DSIGR]) + INT([Ncz]*[DTRACTIONR])
        FRGlobal = 0.0D0

        !Allocate (CL (3,3)) !need to revisit
        !CL = 0.0
        
       ! Allocate (C (3,3))  !needed. It was breaking "access violation"
        !C = 0.0

    End Subroutine InitializeLocalStiffnessMatrix2D
    
!**************************************************************************************        

    Subroutine InitializeLocalForceMatrix2D

        IViscoElasticL = 0
	    IElasticL = 0
        DSigrL = 0

    End Subroutine InitializeLocalForceMatrix2D

!**************************************************************************************    
   
        
End Module InitializeLocalVariables