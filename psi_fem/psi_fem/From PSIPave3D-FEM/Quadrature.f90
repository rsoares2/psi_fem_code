Module Quadrature
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine SetUpQuadraturePoints (NIntegrationPoints, NumberOfNodesPerElement)

    !
    !   This subroutine sets up the weights and integration points for each element type
    !
    
    Integer::NIntegrationPoints, NumberOfNodesPerElement

    If (.Not.Allocated (WB)) Allocate ( WB(NIntegrationPoints) )
    If (.Not.Allocated (XIB)) Allocate (XIB(3, NIntegrationPoints) )
    
    If (NumberOfDimensions == 2 ) Then
        
        If ( NumberOfNodesPerElement == 3 ) Then
        
            Call QuadratureCST  !3-node CST Triangle elements

        ElseIf ( NumberOfNodesPerElement == 4 ) Then            
            
            Call QuadratureQuad !4-node quadrilateral element
            
        Endif
    
    ElseIf (NumberOfDimensions == 3 ) Then 

        If ( NumberOfNodesPerElement == 4 ) Then
        
            Call QuadratureTetrahedron  ! 4-noded linear tetrahedron

        ElseIf ( NumberOfNodesPerElement == 8 ) Then            
            
            Call QuadratureBrick    !8-node brick element
            
        Endif        
        
    Endif
    
    End Subroutine SetUpQuadraturePoints

!*********************************************************************************************   

    Subroutine QuadratureCST
    
        WB= 1.
        Weight = 1.
    
    End Subroutine QuadratureCST
    
!*********************************************************************************************   

    Subroutine QuadratureQuad
        
        A=3.D+00
   	    XIB(1,1) = -1./DSQRT(A)
	    XIB(1,2) = 1./DSQRT(A)
	    XIB(1,3) = 1./DSQRT(A)
	    XIB(1,4) = -1./DSQRT(A)
	    XIB(2,1) = -1./DSQRT(A)
	    XIB(2,2) = -1./DSQRT(A)
	    XIB(2,3) = 1./DSQRT(A)
	    XIB(2,4) = 1./DSQRT(A)

        WB= 1.
    
    End Subroutine QuadratureQuad

!*********************************************************************************************  
    
    Subroutine QuadratureTetrahedron
    
        XIB= 1.0/4.0
        Weight = 1.0/6.0
    
    End Subroutine QuadratureTetrahedron    

!*********************************************************************************************  
    
    Subroutine QuadratureBrick
    
        A=3.D+00
        XIB(1,1) = 1./DSQRT(A)
        XIB(1,2) = 1./DSQRT(A)
        XIB(1,3) = -1./DSQRT(A)
        XIB(1,4) = -1./DSQRT(A)
        XIB(1,5) = 1./DSQRT(A)
        XIB(1,6) = 1./DSQRT(A)
        XIB(1,7) = -1./DSQRT(A)
        XIB(1,8) = -1./DSQRT(A)
        XIB(2,1) = -1./DSQRT(A)
        XIB(2,2) = 1./DSQRT(A)
        XIB(2,3) = 1./DSQRT(A)
        XIB(2,4) = -1./DSQRT(A)
        XIB(2,5) = -1./DSQRT(A)
        XIB(2,6) = 1./DSQRT(A)
        XIB(2,7) = 1./DSQRT(A)
        XIB(2,8) = -1./DSQRT(A)
        XIB(3,1) = -1./DSQRT(A)
        XIB(3,2) = -1./DSQRT(A)
        XIB(3,3) = -1./DSQRT(A)
        XIB(3,4) = -1./DSQRT(A)
        XIB(3,5) = 1./DSQRT(A)
        XIB(3,6) = 1./DSQRT(A)
        XIB(3,7) = 1./DSQRT(A)
        XIB(3,8) = 1./DSQRT(A)   
      
        WB = 1.
      
    End Subroutine QuadratureBrick        

!*********************************************************************************************   
    
End Module Quadrature