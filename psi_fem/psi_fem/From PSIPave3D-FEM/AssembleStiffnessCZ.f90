Module AssembleCZStiffnessMatrix

    Use GlobalInputVariables
    Use LocalInputVariables
           
	implicit Real*8(A-H,O-Z)

    Contains

	Subroutine AssembleInterfaceGlobalSparse(RKINT,NICON)
    
    !
    !   This subroutine assembles the values from the elemtent cohesive zone stiffness matrix into the global CZ sparse vector
    !

      REAL*8 RKINT(4,4) 
      DIMENSION NICON(2) 

	DO I=1,2
		IRDOFX = 2*NICON(I)-1
		IRDOFY = IRDOFX+1
		DO J = 1, 2
			ICDOFX = 2*NICON(J)-1
			ICDOFY = ICDOFX+1
			IF(ICDOFX < IRDOFX)GOTO 88
			
			IF (IRDOFX == 1) THEN
				DO K = 1, IRowCompact(IRDOFX+1)-IRowCompact(IRDOFX)
					IF (ICDOFX == JColCompact(K)) THEN
						SparseValuesG(K) = SparseValuesG(K) + RKINT(2*I-1,2*J-1)
						SparseValuesG(K+1) = SparseValuesG(K+1) + RKINT(2*I-1,2*J)
						IF(IRDOFX < ICDOFX)Then
		    				SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)=SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)+RKINT(2*I,2*J-1)
			            Endif
						SparseValuesG(IROWCompact(IRDOFX+1)+(K-1))=SparseValuesG(IROWCompact(IRDOFX+1)+(K-1))+RKINT(2*I,2*J)
						GOTO 88
					ENDIF
				ENDDO
			ELSE
                DO K = 1, IRowCompact(IRDOFX+1)-IRowCompact(IRDOFX)
                    IF(ICDOFX == JColCompact(IRowCompact(IRDOFX)+K-1)) THEN
						SparseValuesG((IRowCompact(IRDOFX) + K-1)) = SparseValuesG((IRowCompact(IRDOFX)+K-1))+RKINT(2*I-1,2*J-1)
						SparseValuesG((IRowCompact(IRDOFX) + K-1)+1) = SparseValuesG((IRowCompact(IRDOFX)+K-1)+1)+RKINT(2*I-1,2*J)
						IF(IRDOFX < ICDOFX)Then
		    				SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1) = SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)+RKINT(2*I,2*J-1)
			            Endif
						    SparseValuesG(IRowCompact(IRDOFX+1)+(K-1)) = SparseValuesG(IRowCompact(IRDOFX+1)+(K-1))+RKINT(2*I,2*J)
						GOTO 88
					ENDIF
				ENDDO
			ENDIF
   88	ENDDO
	ENDDO

      END SUBROUTINE AssembleInterfaceGlobalSparse
       
!***************************************************************************************

	Subroutine AssembleInterfaceLocalSparse(RKINT,NICON)
    
    !
    !   This subroutine assembles the values from the elemtent cohesive zone stiffness matrix into the local CZ sparse vector
    !

      REAL*8 RKINT(4,4) 
      DIMENSION NICON(2) 

	DO I=1,2
		IRDOFX = 2*NICON(I)-1
		IRDOFY = IRDOFX+1
		DO J = 1, 2
			ICDOFX = 2*NICON(J)-1
			ICDOFY = ICDOFX+1
			IF(ICDOFX < IRDOFX)GOTO 88
			
			IF (IRDOFX == 1) THEN
				DO K = 1, IRowCompactL(IRDOFX+1)-IRowCompactL(IRDOFX)
					IF (ICDOFX == JColCompactL(K)) THEN
						SparseValuesL(K) = SparseValuesL(K) + RKINT(2*I-1,2*J-1)
						SparseValuesL(K+1) = SparseValuesL(K+1) + RKINT(2*I-1,2*J)
						IF(IRDOFX < ICDOFX)Then
		    				SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)=SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)+RKINT(2*I,2*J-1)
			            Endif
						SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1))=SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1))+RKINT(2*I,2*J)
						GOTO 88
					ENDIF
				ENDDO
			ELSE
                DO K = 1, IRowCompactL(IRDOFX+1)-IRowCompactL(IRDOFX)
                    IF(ICDOFX == JColCompactL(IRowCompactL(IRDOFX)+K-1)) THEN
						SparseValuesL((IRowCompactL(IRDOFX) + K-1)) = SparseValuesL((IRowCompactL(IRDOFX)+K-1))+RKINT(2*I-1,2*J-1)
						SparseValuesL((IRowCompactL(IRDOFX) + K-1)+1) = SparseValuesL((IRowCompactL(IRDOFX)+K-1)+1)+RKINT(2*I-1,2*J)
						IF(IRDOFX < ICDOFX)Then
		    				SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1) = SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)+RKINT(2*I,2*J-1)
			            Endif
						    SparseValuesL(IRowCompactL(IRDOFX+1)+(K-1)) = SparseValuesL(IRowCompactL(IRDOFX+1)+(K-1))+RKINT(2*I,2*J)
						GOTO 88
					ENDIF
				ENDDO
			ENDIF
   88	ENDDO
	ENDDO

      END SUBROUTINE AssembleInterfaceLocalSparse
        
End Module AssembleCZStiffnessMatrix