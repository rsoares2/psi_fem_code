Module InputHeader
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine ReadInputHeader
    
	Open (Unit = 5, File = 'input.dat', Status = 'Old')    !Open input file 
	Open (Unit = 6, File = 'Output.out', Status = 'Unknown')    !Open output file 
    
    !   The subroutine ReadInputHeader reads the following parameters:
    !
    !   Part I: Time steps, dimensions, single/multiscale
    !
    !       1. NumberOfTimeSteps - Number of solution time steps
    !       2. NumberOfDimensions - Dimensionality of the problem, 2D = 2 or 3D = 3
    !       3. NumberOfScales - Single scale = 1, Multiscale = 2
    !       4. NumberOfTimeBlocks - Time blocks are used to apply load/displacement patterns
    !       5. NumberOfGlobalNodesPerElement - Number of nodes in each element - Global scale
    
    Read(5,*) NumberOfTimeSteps, NumberOfDimensions, NumberOfScales, NumberOfTimeBlocks, NumberOfGlobalNodesPerElement
    Write(6,1001) NumberOfTimeSteps, NumberOfDimensions, NumberOfScales, NumberOfTimeBlocks, NumberOfGlobalNodesPerElement
    
1001 Format (5X, 'The Input File is as follows:',//,10x, 'Part I: Time steps, dimensions, single/multiscale',//,&
     &15x,'Number of solution time steps: ',i7,/,15x,'Dimensionality of the problem: ',i7,/,15x,'Number of scales: ',&
     &i7,/,15x,'Number of time blocks: ',i7,/,15x,'Number of global nodes per element: ',i7)

     If (NumberOfScales /= 1) iMultiscale = 1   !This flag = 1 indicates multiscale is in effect
    !
    !   Part II: Nodes, elements, cohesive zones, boundary conditions, body forces
    !
    !       1. NumberOfGlobalNodes - Number of nodes in the global scale
    !       2. NumberOfGlobalElements - Number of elements in the global scale
    !       3. NumberOfForces - Number of forces
    !       4. NumberOfDisplacementBC - Number of displacement boundary conditions
    !       5. iBodyForce - Flag to control body forces. = 1, density required; = 0, no body forces.
    !       6. iGlobalCohesiveZones - = 0, no cohesive zones
    !       7. iSolutionType = 0 if using 3D
    !                        = 1 if Plane Stress
    !                        = 2 if Plane Strain
    !                        = 3 if Generalized Plane Strain
    

    Read(5,*) NumberOfGlobalNodes, NumberOfGlobalElements, NumberOfForces, NumberOfDisplacementBC, iBodyForce, iGlobalCohesiveZones, iSolutionType
    Write(6,1002) NumberOfGlobalNodes, NumberOfGlobalElements, NumberOfForces, NumberOfDisplacementBC, iBodyForce, iGlobalCohesiveZones, iSolutionType
    
1002 Format (/,10x, 'Part II: Nodes, elements, cohesive zones, boundary conditions, body forces',//,&
     &15x,'Number of global nodes: ',i7,/,15x,'Number of global elements: ',i7,/,15x,'Number of forces: ',&
     &i7,/,15x,'Number of displacement boundary conditions: ',i7,/,15x,'Body force flag: ',i5, /, 15x, 'Global cohesive zone flag: ',i5, /, 15x, &
     &'Solution Type: ',i5,/)
     
    !
    !   Part III: Output control flags, output file, tecplot, stress, strain
    !
    !       1. iOutputGlobal --> = 1 prints global output file
    !       2. iOutputLocal --> = 1 prints local output file
    !       3. iTecplotGlobal --> = 1 prints global tecplot file
    !       4. iTecplotLocal --> = 1 prints local tecplot file
    !       5. iPavement --> = 1 expects input of a pavement (top of HMA nodes and top of subgrade nodes)
    !
    
    Read(5,*) iOutputGlobal, iOutputLocal, iTecplotGlobal, iTecplotLocal, iPavement
    Write(6,1003) iOutputGlobal, iOutputLocal, iTecplotGlobal, iTecplotLocal, iPavement
    
1003 Format (/,10x, 'Part III: Output control flags, output file, tecplot, stress, strain',//,&
     &15x,'Global output flag: ',i7,/,15x,'Local output flag: ',i7,/,15x&
     &'Global Tecplot file: ',i7,/,15x,'Local Tecplot file: ', i7,/,15x,'iPavement flag: ',i7)
    
    !
    !   Part IV: Iteration control flags, tolerance, maximum number of iterations
    !
    !       1. IterationFlag = 0 if no iterations included
    !                        = 1 if Newton-Raphson iteration scheme is used (update Stiffness Matrix)
    !                        = 2 if Modified Newton-Raphson iteration scheme is used (do not update Stiffness Matrix)
    !       2. Tolerance = Tolerance check for convergence
    !       3. iMaximumNumberofIterations = Maximum number of iterations
    !
    
    Read(5,*) iTerationFlag, Tolerance, iMaximumNumberofIterations
    Write(6,1007) iTerationFlag, Tolerance, iMaximumNumberofIterations
    
1007 Format (/,10x, 'Part IV: Iteration control flags, tolerance, maximum number of iterations',//,&
     &15x,'Iteration Flag: ',i7,/,15x,'Tolerance: ',E15.7,/,15x,'Maximum number of iterations: ',&
     &i7,/)
    

    If (NumberOfTimeBlocks == 0) Then
        
        Write(6,1004) 
1004    Format ('The number of time blocks must be different than zero')
        Stop
        
    Endif
        
    !
    !   Part V: Timeblocks - Load and displacement shapes
    !
    !       1. TimeEndTB(i) - Ending time of ith time block
    !       2. TimeFactor(i) - Multiplicative factor for all inputs during ith time block
    !       3. DeltaTime(i) - Time increment during ith time block (time step)
    !
   
    Allocate ( TimeEndTB (NumberOfTimeBlocks) )
    Allocate ( TimeFactor (NumberOfTimeBlocks) )
    Allocate ( DeltaTime (NumberOfTimeBlocks) )
    

	!ALLOCATE ( TIMEB(IVARI) )
    !ALLOCATE ( TFAC(IVARI) )
    !ALLOCATE ( DTIM(IVARI) )
    
    Write(6,1005)
1005 Format (/,10x,'Part IV: Timeblocks',/,/,17X,'Timeblock',9X,'Time Factor',7X,'Time Step',/) 
    
    Do i = 1, NumberOfTimeBlocks
    
        Read(5,*) TimeEndTB(i), TimeFactor(i), DeltaTime(i)
        Write(6,1006) TimeEndTB(i), TimeFactor(i), DeltaTime(i)
    
    Enddo

1006 Format(14X,E15.8,4X,E15.8,4X,E15.8)
    
    End Subroutine ReadInputHeader
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    Subroutine ReadLocalInputHeader
           
    !   This subroutine applies when the simulation has more than one scale (multiscale cases).
    
    !   Part I: Number of local analyses, number of local nodes per element
    
    !   The subroutine ReadLocalInputHeader reads the following parameters:
    !
    !       1. NumberOfLocalAnalyses - Number of local RVE's to be analyzed
    !       2. NumberOfLocalNodesPerElement - Number of nodes in each element - Local scale
    
    Write(6,1003)
    Read(5,*) NumberOfLocalAnalyses, NumberOfLocalNodesPerElement
    Write(6,1001) NumberOfLocalAnalyses, NumberOfLocalNodesPerElement
    
1001 Format (5X, 'The Local Input File is as follows:',//,10x, 'Part I: Number of local analyses, number of local nodes per element',//,&
     &15x,'Number of local analyses: ',i7,/,15x,'Number of nodes per element: ',i7,/,15x)
1003 Format (5X, 'MULTISCALE ANALYSIS IN EFFECT',/)
    !
    !   Part II: Nodes, elements, cohesive zones, boundary conditions
    
    !       1. NumberOfLocalNodes - Number of local nodes
    !       2. NumberOfLocalElements - NUmber of local elements
    !       3. NumberOfLocalDisplacementBC - Number of local displacement boundary conditions
    !       4. iLocalCohesiveZones - = 0, no cohesive zones
    !       5. iLocalBC = 0 - Transfer Strain from global to local scale
    !                   = 1 - Transfer Stress from global to local scale
    

    Read(5,*) NumberOfLocalNodes, NumberOfLocalElements, NumberOfLocalDisplacementBC, iLocalCohesiveZones, iLocalBC
    Write(6,1002) NumberOfLocalNodes, NumberOfLocalElements, NumberOfLocalDisplacementBC, iLocalCohesiveZones, iLocalBC
    
1002 Format (/,10x, 'Part II: Nodes, elements, cohesive zones, boundary conditions',//,&
     &15x,'Number of local nodes: ',i7,/,15x,'Number of local elements: ',&
     &i7,/,15x,'Number of local displacement boudary conditions: ',i7,/,15x,'Local cohesive zone flag: ',i5, /, 15x, &
     &'iLocalBC Flag =',i2, ' ( = 0: Need to supply nodes on the boundary)',/, 33x,'( = 1: Automatic calculation of RVE boundary nodes)') 
    End Subroutine ReadLocalInputHeader

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    
End Module InputHeader