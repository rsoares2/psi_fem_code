Module OutputFromInput
    
    Use GlobalInputVariables
    Use Omp_lib
        
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine PrintOutputFromInput
    
        If ( iTecplotGlobal == 1 ) Then
            
            Call WriteGlobalInputTecplot
        Else
        
            Close (89, Status = "delete")
            Close (90, Status = "delete")
            
        Endif

        If ( iPavement == 1 ) Call ReadPavementNodes

    End Subroutine PrintOutputFromInput
    
!*********************************************************************************************    
    
    Subroutine WriteGlobalInputTecplot

    Real*8 :: C(3,3)
    
    Open (Unit = 90, File = 'TecGStrain.OUT', Status = 'Unknown') 
    Open (Unit = 89, File = 'TecGStress.OUT', Status = 'Unknown') 
    Open (Unit = 290, File = 'TecGStrainFactor.OUT', Status = 'Unknown') 
    Open (Unit = 289, File = 'TecGStressFactor.OUT', Status = 'Unknown') 
    

    If ( NumberOfScales == 1) Then

        If ( NumberOfDimensions == 2 )Then  !See Formats below

            If ( NumberOfGlobalNodesPerElement == 3 ) Then
        
		        Write(89,1040) NumberOfGlobalNodes, NumberOfGlobalElements
		        Write(289,1040) NumberOfGlobalNodes, NumberOfGlobalElements
    
            ElseIf ( NumberOfGlobalNodesPerElement == 4 ) Then
        
                Write(89,1041) NumberOfGlobalNodes,NumberOfGlobalElements
                Write(289,1041) NumberOfGlobalNodes,NumberOfGlobalElements
            
            EndIf
            
        ElseIf ( NumberOfDimensions == 3 ) Then
        
            If ( NumberOfGlobalNodesPerElement == 8 ) Then
        
                Write(89,1042) NumberOfGlobalNodes,NumberOfGlobalElements
    		    Write(90,1043) NumberOfGlobalNodes,NumberOfGlobalElements

                Write(289,1042) NumberOfGlobalNodes,NumberOfGlobalElements
    		    Write(290,1043) NumberOfGlobalNodes,NumberOfGlobalElements
            
            ElseIf ( NumberOfGlobalNodesPerElement == 4 ) Then
    
                Write(89,1044) NumberOfGlobalNodes,NumberOfGlobalElements
                Write(90,1045) NumberOfGlobalNodes,NumberOfGlobalElements
            		
                Write(289,1044) NumberOfGlobalNodes,NumberOfGlobalElements
                Write(290,1045) NumberOfGlobalNodes,NumberOfGlobalElements

            EndIf
            
        EndIf
                			
    ElseIf ( NumberOfScales == 2 ) Then
		
        If ( NumberOfGlobalNodesPerElement == 3 ) Then

		    Write(89,1046) NumberOfGlobalNodes,NumberOfGlobalElements
        
        Else 
            
            Write(6,1047)
            Stop
            
        EndIf

    EndIf
    
                1040	Format('VARIABLES= "X" "Y" "MATERIAL" "Stress XX" "Stress YY" "Stress XY" "Strain XX" "Strain YY" "Strain XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETRIANGLE',&
		        /,'VARLOCATION=([3,4,5,6,7,8,9]=CELLCENTERED) C=BLACK',/)

                1041	Format('VARIABLES= "X" "Y" "MATERIAL" "Stress XX" "Stress YY" "Stress XY" "Strain XX" "Strain YY" "Strain XY"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FEQUADRILATERAL',&
		        /,'VARLOCATION=([3,4,5,6,7,8]=CELLCENTERED) C=BLACK',/)

                1042	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Stress XX" "Stress YY" "Stress ZZ" "Stress YZ" "Stress ZX" "Stress XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FEBRICK',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK',/)
    		
                1043	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Strain XX" "Strain YY" "Strain ZZ" "Strain YZ" "Strain ZX" "Strain XY" "Deflection X"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FEBRICK',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK',/)
    
                1044	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Stress XX" "Stress YY" "Stress ZZ" "Stress YZ" "Stress ZX" "Stress XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETETRAHEDRON',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK',/)
    		
                1045	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Strain XX" "Strain YY" "Strain ZZ" "Strain YZ" "Strain ZX" "Strain XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETETRAHEDRON',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK',/)

                1046	Format('VARIABLES= "X" "Y" "MATERIAL" "Stress XX" "Stress YY" "Stress XY" "Strain XX" "Strain YY" "Strain XY" "C11" "C12" "C22" ',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETRIANGLE',&
		        /,'VARLOCATION=([3,4,5,6,7,8,9,10,11,12]=CELLCENTERED) C=BLACK',/)

                1047 Format ("There is no Multiscale code for LST or rectangular elements yet! Please change NumberOfGlobalNodesPerElement to 3")
                     
    Close (89)
    Close (90)
    Close (289)
    Close (290)
    
    !--------------HERE---------------------
    !$omp parallel sections
    !$omp section
    Call WriteTecStress()
    !$omp section
    Call WriteTecStrain()	
    !$omp end parallel sections

!	
!   This part is for damage dependent stiffness
!

	If ( NumberOfScales == 2 ) Then

	    Allocate (C11L(NumberOfGlobalElements))
	    Allocate (C12L(NumberOfGlobalElements))
	    Allocate (C22L(NumberOfGlobalElements))
    
	    Do I=1,NumberOfGlobalElements
    		C11L(I)=C(1,1)
	    	C12L(I)=C(1,2)
		    C22L(I)=C(2,2)
	    Enddo

	    Write(89,22) (C11L(I),I=1, NumberOfGlobalElements)
	    Write(89,22) (C12L(I),I=1, NumberOfGlobalElements)
	    Write(89,22) (C22L(I),I=1, NumberOfGlobalElements)
        
    22 Format (10E15.7)

	    DeAllocate (C11L)
	    DeAllocate (C12L)
	    DeAllocate (C22L)
	
    EndIf

    End Subroutine WriteGlobalInputTecplot

!*********************************************************************************************    

    Subroutine WriteTecStress()
    
    !
    !   This subroutine prints the Stresses in Tecplot file Format
    !
    
    integer I, IEL
    Open (Unit = 89, File = 'TecGStress.OUT', Status = "Old", POSITION = "Append")
    Open (Unit = 289, File = 'TecGStressFactor.OUT', Status = "Old", POSITION = "Append") 
    
    Write(89,2092) (XG(IEL), IEL = 1, NumberOfGlobalNodes)
 	Write(89,2092) (YG(IEL), IEL = 1, NumberOfGlobalNodes)
	Write(89,2093)
	Write(89,2093)
   
    Write(289,2092) (XG(IEL), IEL = 1, NumberOfGlobalNodes)
 	Write(289,2092) (YG(IEL), IEL = 1, NumberOfGlobalNodes)
	Write(289,2093)
	Write(289,2093)

 2092	Format(10F10.4)
 2093	Format(2/)

    If ( NumberOfDimensions == 3 ) Then

	    Write(89,2092) (ZG(IEL), IEL = 1, NumberOfGlobalNodes)
	    Write(89,2093)

	    Write(289,2092) (ZG(IEL), IEL = 1, NumberOfGlobalNodes)
	    Write(289,2093)
	    
    !   Write TECSTRESS components
       
    ENDIF    

    Write(89,9) (MaterialSetG(I),I=1,NumberOfGlobalElements)
    Write(289,9) (MaterialSetG(I),I=1,NumberOfGlobalElements)

    9	Format(10(2X,I5))  
	Write(89,12)
	Write(289,12)
    12	Format(2/)
   
	!This is for stress variables
   
    Do k = 1, 6
        
	    Write(89,16) (0,I=1,NumberOfGlobalElements)
        Write(89,12)
	    Write(289,16) (0,I=1,NumberOfGlobalElements)
        Write(289,12)
        
    Enddo
	
   16	Format(10(1X,F15.7)) 
  
	!This is for the deflection Y variables
   	Write(89,16) (0, I= 1, NumberOfGlobalNodes)
	Write(89,12)
   	Write(289,16) (0, I= 1, NumberOfGlobalNodes)
	Write(289,12)

!	
!   Write node connectivity
!   

    If ( NumberOfGlobalNodesPerElement == 3 ) Then
	    Do IEL=1,NumberOfGlobalElements
			    Write(89,18) (NodeG(IEL,J),J=1,NumberOfGlobalNodesPerElement)
			    Write(289,18) (NodeG(IEL,J),J=1,NumberOfGlobalNodesPerElement)

    18		Format(8(3X,I5))
        Enddo
    Else
	    Do IEL=1,NumberOfGlobalElements
			    Write(89,18) (NodeG(IEL,J),J=1,NumberOfGlobalNodesPerElement)
			    Write(289,18) (NodeG(IEL,J),J=1,NumberOfGlobalNodesPerElement)
        END DO
    ENDIF
        		
	Write(89,23)
	Write(289,23)

 23	Format(2/)

    Close(89)
    Close(289)    

    End Subroutine WriteTecStress
	
!*********************************************************************************************    

    Subroutine WriteTecStrain()
    Integer I, IEL
    Open(Unit = 90, File = 'TecGStrain.OUT',Status = "Old", Position = "Append")
    Open(Unit = 290, File = 'TecGStrainFactor.OUT',Status = "Old", Position = "Append")
 
 2092	Format(10F10.4)
 2093	Format(2/)
 9	    Format(10(2X,I5))
 12	    Format(2/)
 16	    Format(10(1X,F15.7)) 	

    If (NumberOfDimensions == 3) Then
        
	    !Write TECSTRAIN components
        Write(90,2092) (XG(IEL),IEL=1,NumberOfGlobalNodes)
    	Write(90,2093)
    	Write(90,2092) (YG(IEL),IEL=1,NumberOfGlobalNodes)
    	Write(90,2093)
	    Write(90,2092) (ZG(IEL),IEL=1,NumberOfGlobalNodes)
	    Write(90,2093)
 
        Write(90,9) (MaterialSetG(I),I=1,NumberOfGlobalElements)
        Write(90,12)

! Stress Factor
        Write(290,2092) (XG(IEL),IEL=1,NumberOfGlobalNodes)
    	Write(290,2093)
    	Write(290,2092) (YG(IEL),IEL=1,NumberOfGlobalNodes)
    	Write(290,2093)
	    Write(290,2092) (ZG(IEL),IEL=1,NumberOfGlobalNodes)
	    Write(290,2093)
 
        Write(290,9) (MaterialSetG(I),I=1,NumberOfGlobalElements)
        Write(290,12)


    EndIf    

    If ( NumberOfDimensions == 3 ) Then
        
        Do k = 1, 6 !Six strain deflections
	    
            Write(90,16) (0,I=1,NumberOfGlobalElements)
    	    Write(90,12)
    	
            Write(290,16) (0,I=1,NumberOfGlobalElements)
    	    Write(290,12)
        Enddo
        !Deflection
       	Write(90,16) (0,I=1,NumberOfGlobalNodes)
    	Write(90,12)
       	
        Write(290,16) (0,I=1,NumberOfGlobalNodes)
    	Write(290,12)
    Endif		

!	
!   Write node connectivity
!   

18		Format(8(3X,I5))
    
	    Do Iel = 1, NumberOfGlobalElements
            Write(90,18) (NodeG(IEL,J),J=1, NumberOfGlobalNodesPerElement)
            Write(290,18) (NodeG(IEL,J),J=1, NumberOfGlobalNodesPerElement)
        Enddo
            			
        Close(90)
        Close(290)
    
    End Subroutine WriteTecStrain

!*********************************************************************************************    
    
    Subroutine ReadPavementNodes
    
    !
    !   This section reads and stores the nodes at each layer interface, so stress and strain at nodes can be calculated
    !
    
    
    Read(5,*) NNTopHMACLane
    Allocate (NodesTopHMACLane(NNTopHMACLane))
    
    Do i = 1,NNTopHMACLane
        Read(5,*)NodesTopHMACLane(i)
    Enddo

    Read(5,*) NNTopHMACShoulder
    Allocate(NodesTopHMACShoulder(NNTopHMACShoulder))
    
    Do i = 1,NNTopHMACShoulder
        Read(5,*)NodesTopHMACShoulder(i)
    Enddo

    Read(5,*)NNBottomHMACLane
    Allocate(NodesBottomHMACLane(NNBottomHMACLane))
    
    Do i = 1,NNBottomHMACLane
        Read(5,*)NodesBottomHMACLane(i)
    Enddo

    Read(5,*)NNBottomHMACShoulder
    Allocate(NodesBottomHMACShoulder(NNBottomHMACShoulder))
    
    Do i = 1,NNBottomHMACShoulder
        Read(5,*)NodesBottomHMACShoulder(i)
    Enddo
    
    Read(5,*)NNTopSubgradeLane
    Allocate(NodesTopSubgradeLane(NNTopSubgradeLane))
    
    Do i = 1,NNTopSubgradeLane
        Read(5,*)NodesTopSubgradeLane(i)
    Enddo

    Read(5,*)NNTopSubgradeShoulder
    Allocate(NodesTopSubgradeShoulder(NNTopSubgradeShoulder))
    
    Do i = 1,NNTopSubgradeShoulder
        Read(5,*)NodesTopSubgradeShoulder(i)
    Enddo    
    
    
    End Subroutine ReadPavementNodes


End Module OutputFromInput