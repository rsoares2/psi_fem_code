Module InputBCandForce
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real (A-H, O-Z)

    Contains    

    Subroutine ReadGlobalInputBCandForce
    
    !
    !   This subroutine reads the boundary conditions (BC) and forces applied to the problem
    !
    
    Call ReadInputBC
    
    If (NumberOfForces /= 0) Then
        
        Call ReadInputForce
        
    Endif
    
    End Subroutine ReadGlobalInputBCandForce

    Subroutine ReadInputBC
  
    !
    !   The global displacement boundary conditions can constrained to zero or non-zero values.
    !   This is used to apply displacement control as well as "fix" boundaries with zero displacements
    !   NumberOfDisplacementBC must be > 0 otherwise the global stiffness matrix will be singular.
    !

    !	NGlobalDOF(I) - Global degree of freedom of ith degree of fredoom to be input
    !
    !   For 2D: 
    !   X: 2*NodeNumber-1
    !   Y: 2*NodeNumber
    !
    !   For 3D: 
    !   X: 3*NodeNumber-2
    !   Y: 3*NodeNumber-1
    !   Z: 3*NodeNumber
    !

    Allocate ( NumberOfGlobalDisplacement ( NumberOfTimeBlocks ) )  !Number of displacement boundary conditions in each timeblock
    
    Read(5,*) ( NumberOfGlobalDisplacement (i), i = 1, NumberOfTimeBlocks)
    
    Allocate ( NGlobalDOF (NumberOfGlobalDisplacement(NumberOfTimeBlocks), NumberOfTimeBlocks) ) !What are the degree of freedoms in each timeblock
    Allocate ( DisplacementIncrementG (NumberOfGlobalDisplacement(NumberOfTimeBlocks), NumberOfTimeBlocks) )!What are the diplacement boundary conditions in each timeblock

	Do j = 1, NumberOfTimeBlocks
		
        Write (6,1019) J,NumberOfGlobalDisplacement(J)
		Read (5,*) (NGlobalDOF(i,j),i=1,NumberOfGlobalDisplacement(j)) 
		Write (6,1020) 
        Write (6,1021) (NGlobalDOF(i,j),i=1,NumberOfGlobalDisplacement(j))
    
1019	Format(/,5X,'Input global displacement data for timeblock number'&
		,I3,//,5X,'The number of displacement bc is ',I9,/)
    
1020	Format(/,10X,'The global disp. boundary conditions are applied at',/,10x,'degrees of freedom',/) 

1021    Format (10x, 10i8)

    !
    !   Now input the values of the increments of the displacements
    !   to be applied at the global degrees of freedom input in NGlobalDOF (i)
    !   If a boundary is fixed, these will be zero. If displacement is applied, it will be non-zero.
    !   Note that these will be divided by TimeFactor(i)
    !
    
    Read (5,*) (DisplacementIncrementG(i,j), i=1,NumberOfGlobalDisplacement(j))
    
    Write (6,1022) 
1022 Format (//,10X,'The global displacement increments are', //)
    Write (6,1023) (DisplacementIncrementG(i,j), i=1,NumberOfGlobalDisplacement(j)) 
1023 Format (4(2X,E15.7))
    
    Enddo

    NGTotalDOF = NumberOfDimensions * NumberOfGlobalNodes  !Number of  total Global degrees of freedom
     
    End Subroutine ReadInputBC
    
!*********************************************************************************************    

    Subroutine ReadInputForce
    
    !
    !   The next section inputs global externally applied loads if NumberOfForces != 0
    !   
    !
    !   Input is performed by using load curves (Removed) and then assigning
    !   these curves to the intended global degrees of freedom
    !
    !   iNode(i) - global node number for ith load
    !   FA1 - force component in x coordinate direction
    !   FA2 - force component in y coordinate direction
    !   FA3 - force component in z coordinate direction
    !    

	ALLOCATE ( FGLG(NGTotalDOF) )
    FGLG = 0
    
    ALLOCATE (iNode(NumberOfForces))

    If ( NumberOfDimensions == 2 ) Then
        
        !
        !   2-D Case
        !
        Write (6,1021) 
    1021 Format (//,10X,'Global forces are applied as follows',//,10X,'Node N0.',8X,'Force X',12X, 'Force Y',/) 
    
        Do I=1,NumberOfForces

            Read(5,*) iNode (i), ForceX, ForceY
            Write(6,1022) iNode (i), ForceX, ForceY
    1022 Format (10X,i5,2(5X,D15.7)) 
            NN1 = 2*iNode(I)-1 
            NN2 = NN1 + 1
            FGLG(NN1) = FGLG(NN1) + ForceX 
            FGLG(NN2) = FGLG(NN2) + ForceY
        
        Enddo
        
    ElseIf ( NumberOfDimensions == 3 ) Then

        !
        !   3-D Case
        !
        Write (6,1024) 
    1024 Format (//,10X,'Global forces are applied as follows',//,10X,'Node N0.',8X,'Force X',12X, 'Force Y',12X, 'Force Z',/) 
    
        Do i = 1, NumberOfForces

            Read(5,*) iNode(i), ForceX, ForceY, ForceZ
            Write(6,1023) iNode(i), ForceX, ForceY, ForceZ
    1023    Format (10X,i5,3(5X,D15.7)) 
     
            NN1 = 3*iNode(i) - 2
            NN2 = NN1 + 1
            NN3 = NN2 + 1
            FGLG(NN1) = FGLG(NN1) + ForceX     !FGLG is the vector that stores the forces applied
            FGLG(NN2) = FGLG(NN2) + ForceY
            FGLG(NN3) = FGLG(NN3) + ForceZ

    Enddo

    Else
    
        Write (6,1007) 
1007    Format ('The number of dimensions must be 2 for 2D or 3 for 3D')
        Stop
        
    Endif
    
    End Subroutine ReadInputForce    

    Subroutine ReadLocalInputBC
  
    !
    !   The local displacement boundary conditions selects all the nodes around the boundaries.
    !   This is used to apply displacements from the global to the local scale.
    !

    !	NLocalDOF(I) - Local degree of freedom of ith degree of fredoom to be input
    !
    !   For 2D: 
    !   X: 2*NodeNumber-1
    !   Y: 2*NodeNumber
    !
    !   For 3D: 
    !   X: 3*NodeNumber-2
    !   Y: 3*NodeNumber-1
    !   Z: 3*NodeNumber
    !

    !Allocate ( NumberOfLocalDisplacement ( NumberOfTimeBlocks ))  !Number of displacement boundary conditions in each timeblock
    
    !Read(5,*) ( NumberOfLocalDisplacement(i), i = 1, NumberOfTimeBlocks)
    
    Allocate ( NLocalDOF (NumberOfLocalDisplacementBC, NumberOfTimeBlocks) ) !What are the degree of freedoms in each timeblock
    
    Read (5,*) (NLocalDOF(i,1),i=1,NumberOfLocalDisplacementBC) 
    Write (6,1020) 
    Write (6,1021) (NLocalDOF(i,1),i=1,NumberOfLocalDisplacementBC)
    
    Do j = 2, NumberOfTimeBlocks
		NLocalDOF(:,j) = NLocalDOF(:,1)
    Enddo
    
1019	Format(/,5X,'Input local displacement data for timeblock number'&
		,I3,//,5X,'The number of displacement bc is ',I9,/)
    
1020	Format(/,10X,'The local disp. boundary conditions are applied at degrees of freedom',/) 

1021    Format (10x, 10i8)

    NLTotalDOF = NumberOfDimensions * NumberOfLocalNodes  !Number of total Local degrees of freedom
     
    End Subroutine ReadLocalInputBC
    
End Module InputBCandForce