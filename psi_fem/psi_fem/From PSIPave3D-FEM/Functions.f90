Module Functions

    Use GlobalInputVariables
    Use LocalInputVariables

	implicit Real*8(A-H,O-Z)

    Contains

	Subroutine MultiscaleElementSearch(val, isMultiscale)
	
    !
    !   This subroutine searches for the element to be multiscaled
    !   val is the number of the global element (iel)
    
    integer :: range, start, finish, mid
    integer :: i, n, val, isMultiscale

    start =  1
    finish = size(iMultiscaleElements)  !size of imultiscale
    range = finish - start
    mid = (start + finish) /2

    do while( iMultiscaleElements(mid) /= val .and. range >  0)
      if (val > iMultiscaleElements(mid)) then
        start = mid + 1
      else
        finish = mid - 1
      end if
      range = finish - start
      mid = (start + finish)/2
    end do

    if( iMultiscaleElements(mid) /= val) then
      print *, val, 'NOT FOUND'
      isMultiscale = 0  !isMultiscale is a flag that indicates whether or not an element to be multiscaled was found
    else
      print *, 'VALUE AT' , mid
      isMultiscale = 1
    end if

    End Subroutine MultiscaleElementSearch

End Module Functions