Module CalculateForceMatrix

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    Use CalculateForceMatrix3D
    Use CalculateForceMatrix2D
    Use CalculateForceMatrix2DL
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine GlobalForceMatrix
	
    !
    !   This subroutine calculates the global stiffness matrix in sparse format
    !
    
    
    If (NumberOfDimensions == 2 ) Then
        
        Call InitializeGlobalForceMatrix2D
        
        Call CalculateGlobalForceMatrix2D
    
    ElseIf (NumberOfDimensions == 3 ) Then 

        Call InitializeGlobalForceMatrix3D
        
        Call CalculateGlobalForceMatrix3D
        
    Endif
    
    End Subroutine GlobalForceMatrix
    
	Subroutine LocalForceMatrix
	
    !
    !   This subroutine calculates the local "force" matrix
    !
    !   There is no real force matrix in local scale, so this only calculates
    !   the contribution from CZ to the FR matrix. (old FRTRACTION)
    !
    
    If (NumberOfDimensions == 2 ) Then
        
        Call InitializeLocalForceMatrix2D
        
        Call CalculateLocalForceMatrix2D
    
    ElseIf (NumberOfDimensions == 3 ) Then 

        !Call InitializeLocalForceMatrix3D
        
        !Call CalculateLocalForceMatrix3D
        
    Endif
    
    End Subroutine LocalForceMatrix
    
End Module CalculateForceMatrix