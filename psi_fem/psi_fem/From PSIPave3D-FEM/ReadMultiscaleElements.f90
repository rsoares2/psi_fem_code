Module MultiscaleElements
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine ReadMultiscaleElements
    
    !
    !   This subroutine calculates the nodes on the RVE boundary (if necessary) and reads elements to be multiscaled
    !
    
    If  (iLocalBC /= 1) Then
        
    !
    !   This section reads the geometry of RVE boundary
    !   !It is not necessary if using automatic calculation (iLocalBC = 1)
    
    !   NNXSIDE - Number of nodes on the plus X-face of the local RVE
    !   NNYSIDE - Number of nodes on the plus Y-face of the local RVE
       
        Read (5,*) NNXSide, NNYSide
        Write (6,6066) NNXSide, NNYSide
6066    Format(10X,'Number of nodes on the plus X-face of the local RVE',I7,/,&
		10X,'Number of nodes on the plus Y-face of the local RVE',I7,//)
    
    !
    !   XDimension(I) - Width between node I and node I + 1 on plus X-face of RVE
    !   NodNumX(I) - Node number of each node on plus X-face
    !
        Allocate (XDimension (NNXSide))
        Allocate (YDimension (NNYSide))
        Allocate (NodNumX (NNXSide))
        Allocate (NodNumY (NNXSide))       
    
        Read(5,*) (XDimension(I),I=1,NNXSide)
		Write(6,6178)
 6178	Format(//,10X,'X Coordinates along plus X-face',//)
		Write(6,6069) (XDimension(I),I=1,NNXSide)
 6069	Format(10X,4D15.7)

		Read(5,*) (NodNumX(I),I=1,NNXSide)
		Write(6,6078)
 6078	Format(//,10X,'Node numbers of nodes on plus X-face of RVE',/)
		Write(6,6079) (NodNumX(I),I=1,NNXSide)
 6079	Format(10X,12I5)  
    
    !
    !   YDimension(I) - Width between node I and node I + 1 on plus Y-face of RVE
    !   NodNumY(I) - Node number of each node on plus Y-face
    !   
    
        Read(5,*) (YDimension(I),I=1,NNYSide)
 		Write(6,7178)
 7178	Format(//,10X,'Y Coordinates along plus Y-face',//)
		Write(6,7069) (YDimension(I),I=1,NNYSide)
 7069	Format(10X,4D15.7)

		Read(5,*) (NodNumY(I),I=1,NNYSide)
		Write(6,7078)
 7078	Format(//,10X,'Node numbers of nodes on plus Y-face of RVE',/)
		Write(6,7079) (NodNumY(I),I=1,NNYSide)
 7079	Format(10X,12I5)          
        
    Endif
    
    !
    !   Read global elements to be multiscaled
    !
    
    Allocate (iMultiscaleElements (NumberOfLocalAnalyses))
    Allocate (XL (NumberOfLocalNodes))
    Allocate (YL (NumberOfLocalNodes))

    Read(5,*) (iMultiscaleElements(i), i = 1, NumberOfLocalAnalyses)
    
    Write(6,2054) 
 2054	Format(/,10X,'Global elements to be multiscaled',/)
		Write(6,2055) (iMultiscaleElements(i), i = 1, NumberOfLocalAnalyses)
 2055	Format(15X,16I7)
        
    End Subroutine ReadMultiscaleElements
    
End Module MultiscaleElements