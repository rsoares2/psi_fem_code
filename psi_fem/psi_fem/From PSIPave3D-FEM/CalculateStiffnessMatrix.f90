Module CalculateStifnessMatrix

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    Use InitializeLocalVariables    !This module may not be required. It is listed in GLobalInitialize
    Use CalculateStifnessMatrix3D
    Use CalculateStifnessMatrix2DG
    Use CalculateStifnessMatrix2DL

	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine GlobalStiffnessMatrix
	
    !
    !   This subroutine calculates the global stiffness matrix in sparse format
    !
    
    If (iTerationFlag <= 1 .or. it == 1 .AND. iTerationFlag == 2) Then  !Modified Newton-Raphson. Does not update Stiffness

        If (NumberOfDimensions == 2 ) Then
            
            Call InitializeGlobalStiffnessMatrix2D
            
            Call CalculateGlobalStiffnessMatrixSparse2D
        
        ElseIf (NumberOfDimensions == 3 ) Then 
    
            Call InitializeGlobalStiffnessMatrix3D
            
            Call CalculateGlobalStiffnessMatrixSparse3D
            
        Endif

    Endif
    
    End Subroutine GlobalStiffnessMatrix
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	Subroutine LocalStiffnessMatrix
	
    !
    !   This subroutine calculates the Local stiffness matrix in sparse format
    !
    
    If (NumberOfDimensions == 2 ) Then
        
        Call InitializeLocalStiffnessMatrix2D
        
        Call CalculateLocalStiffnessMatrixSparse2D
    
    ElseIf (NumberOfDimensions == 3 ) Then 

       ! Call InitializeLocalStiffnessMatrix3D
        
       ! Call CalculateLocalStiffnessMatrixSparse3D
        
    Endif
    
    End Subroutine LocalStiffnessMatrix

End Module CalculateStifnessMatrix