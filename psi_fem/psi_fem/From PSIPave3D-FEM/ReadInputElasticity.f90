Module InputElasticity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)
    
    Contains    
    
    Subroutine ReadGlobalInputElastic
    
    ! 
    !   Read in material properties for isotropic elastic elements
    ! 

    Allocate ( ElasticPosition (NumberOfTotalMaterials) )  !Array to keep track number of elastic materials independent of matset number.
    ElasticPosition = 0

	Allocate ( EEG (NumberOfTotalMaterials) )
	Allocate ( VNUG (NumberOfTotalMaterials) )
	Allocate ( YieldG (NumberOfTotalMaterials) )
	Allocate ( AlphaG (NumberOfTotalMaterials) )
	Allocate ( StrainLimitG (NumberOfTotalMaterials) )
    Allocate ( Density (NumberOfTotalMaterials) )
    

    Write (6,1023) MaterialNumber1G 
1023 Format (/,10X,'There are ',I3,' 3-D isotropic elastic material sets', &
		//,10X,'Set N0.',9X,'E',10X,'NU',11X,'Y',9X,'Alpha',6x,'Density',2x,'StrainLimitG'/) 
        
    Do I=1, MaterialNumber1G
                
        Read (5,*) MSetiG, EEG(I), VNUG(I), YieldG(I), AlphaG(I), Density(I), StrainLimitG(i)
        Write(6,1024) MSetiG, EEG(I), VNUG(I), YieldG(I), AlphaG(I), Density(I), StrainLimitG(i)
1024        Format(11X,I3,5X,8(1X,E11.4))

        ElasticPosition (MsetiG) = i      !Array to store material numbers and positions         
                
    Enddo
            
    End Subroutine ReadGlobalInputElastic

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    Subroutine ReadLocalInputElastic
    
    ! 
    !   Read in material properties for local isotropic elastic elements
    ! 

    Allocate ( ElasticPositionL (NumberOfTotalMaterialsL) )  !Array to keep track number of elastic materials independent of matset number.
    ElasticPositionL = 0

	Allocate ( EEL (MaterialNumber1L) )
	Allocate ( VNUL (MaterialNumber1L) )
	Allocate ( YieldL (MaterialNumber1L) )
	!Allocate ( AlphaL (MaterialNumber1L) )  used for thermal code
    
    Write (6,1023) MaterialNumber1L
1023 Format (/,10X,'There are ',I3,' local 2-D isotropic elastic material sets', &
		//,10X,'Set N0.',12X,'E',10X,'NU',11X,'Y',/) 
        
    Do i = 1, MaterialNumber1L
                
        Read (5,*) MsetiL,EEL(I),VNUL(I),YieldL(I)  !,AlphaL(I)
        Write(6,1025) MsetiL,EEL(I),VNUL(I),YieldL(I)   !,AlphaL(I)
1025        Format(11X,I3,4X,5(1X,E11.4)) 

        ElasticPositionL (MsetiL) = i      !Array to store material numbers and positions         
                
    Enddo
            
    End Subroutine ReadLocalInputElastic

End Module InputElasticity