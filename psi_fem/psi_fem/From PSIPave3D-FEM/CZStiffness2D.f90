Module CohesiveZoneStiffness2D

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use AssembleCZStiffnessMatrix
    Use AllenCZStiffness
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine InterfaceStiffness2D
    
    Integer::NICON(2)
    REAL*8 RKINT(4,4)
    !
    !   This subroutine calculates the global sparse stiffness matrix for 2D cohesive zones
    !    
    
    Do i = 1, NGlobalInterfElementsG 

        I1 = NINT1G(I)
        I2 = NINT2G(I)

        CALL InterfaceStifnessModels2DG(I,I1,I2,RKINT)
		
        NICON(1) = I1 
        NICON(2) = I2
		
        CALL AssembleInterfaceGlobalSparse(RKINT,NICON)
    
    Enddo
        
    End Subroutine InterfaceStiffness2D

!*********************************************************************************************    
    
    Subroutine InterfaceStifnessModels2DG (ii,I1,I2,RKINT)
    
    REAL*8 RKINT(4,4)
    !
    !   This subroutine calculates the stiffness matrix for interface elements
    !

	
	!If (IDYNAMIC == 1) Then
!		QG => REALLOCATE (QG,NGTOT) 

     !   Do I=NGNOLD+1,NGTOT
      !      DisplacementG(I)=0.
       ! Enddo
    !Endif

	Phi = PHIAVG(II) 
	W = WIDTHG(II)

! 
! CALCULATE INTERFACE DISPLACEMENTS UN1,UN2,UT1,UT2 
! 

	N1X=2*I1-1 
	N1Y=N1X+1 
	N2X=2*I2-1 
	N2Y=N2X+1 
	UX=DisplacementG(N2X)-DisplacementG(N1X)    !should it be DeltaDisplacement?
	UY=DisplacementG(N2Y)-DisplacementG(N1Y) 
	UN=UX*DCOS(PHI)+UY*DSIN(PHI) 
	UT=-UX*DSIN(PHI)+UY*DCOS(PHI)
	
	!If (iGlobalCohesiveZones==1) Call NeedlemanModelG
	!If (iGlobalCohesiveZones==2) Call TvergaardModelG
	!If (iGlobalCohesiveZones==3) Call ModifiedTvergaardModelG
	 If (iGlobalCohesiveZones == 4) Call AllenCZModelStiffnessG(ii, W, Phi, UN, UT, RKInt)
     
    End Subroutine InterfaceStifnessModels2DG
 
 !*********************************************************************************************    

	Subroutine InterfaceStiffness2DL
    
    Integer::NICON(2)
    REAL*8 RKINT(4,4)
    !
    !   This subroutine calculates the local sparse stiffness matrix for 2D cohesive zones
    !    
    
    Do i = 1, NLocalInterfElementsL

        I1 = NINT1L(I)
        I2 = NINT2L(I)

        CALL InterfaceStifnessModels2DL(I,I1,I2,RKINT)
		
        NICON(1) = I1 
        NICON(2) = I2
		
        CALL AssembleInterfaceLocalSparse(RKINT,NICON)
    
    Enddo
        
    End Subroutine InterfaceStiffness2DL

!*********************************************************************************************    
    
    Subroutine InterfaceStifnessModels2DL (ii,I1,I2,RKINT)
    
    REAL*8 RKINT(4,4)
    !
    !   This subroutine calculates the stiffness matrix for interface elements
    !

	
	!If (IDYNAMIC == 1) Then
!		QG => REALLOCATE (QG,NGTOT) 

     !   Do I=NGNOLD+1,NGTOT
      !      DisplacementG(I)=0.
       ! Enddo
    !Endif

	Phi = PHIAVL(II) 
	W = WIDTHL(II)

! 
! CALCULATE INTERFACE DISPLACEMENTS UN1,UN2,UT1,UT2 
! 

	N1X=2*I1-1 
	N1Y=N1X+1 
	N2X=2*I2-1 
	N2Y=N2X+1 
	UX=DisplacementL(N2X)-DisplacementL(N1X)    !should it be DeltaDisplacement?
	UY=DisplacementL(N2Y)-DisplacementL(N1Y)
	UN=UX*DCOS(PHI)+UY*DSIN(PHI)
	UT=-UX*DSIN(PHI)+UY*DCOS(PHI)
	
	!If (iLocalCohesiveZones==1) Call NeedlemanModelG
	!If (iLocalCohesiveZones==2) Call TvergaardModelG
	!If (iLocalCohesiveZones==3) Call ModifiedTvergaardModelG
	 If (iLocalCohesiveZones == 4) Call AllenCZModelStiffnessL(ii, W, Phi, UN, UT, RKInt)
     
    End Subroutine InterfaceStifnessModels2DL
        
End Module CohesiveZoneStiffness2D