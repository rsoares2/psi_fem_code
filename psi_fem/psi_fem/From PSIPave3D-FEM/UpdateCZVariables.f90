Module UpdateCZs
    
    Use GlobalInputVariables
    Use LocalInputVariables

    implicit Real (A-H, O-Z)

    Contains 

    Subroutine UpdateCZGlobal
	
    !
    !   This subroutine updates stored variables in Allen cohesive zone model
    !
    !   iGlobalCohesiveZones = 4
    !
    Real*8 Phi

	IF (INCR == 1.AND.ICYCL==1) THEN
	
		ALLOCATE (UNOLDG(NGlobalInterfElementsG))
		ALLOCATE (UTOLDG(NGlobalInterfElementsG))
	
    ENDIF	

		DO 7971 II = 1, NGlobalInterfElementsG

            I1=NINT1G(II)
			I2=NINT2G(II)
			N1X=2*I1-1 
			N1Y=N1X+1 
			N2X=2*I2-1 
			N2Y=N2X+1 
			UX=DisplacementG(N2X)-DisplacementG(N1X) 
			UY=DisplacementG(N2Y)-DisplacementG(N1Y)
			PHI=PHIAVG(II)
			UN=UX*DCOS(PHI)+UY*DSIN(PHI) 
			UT=-UX*DSIN(PHI)+UY*DCOS(PHI)
			UNOLDG(II)=UN
			UTOLDG(II)=UT
            
			IF(iGlobalCohesiveZones == 4)THEN
				DELTASUBN=DeltaNormalG(iMaterialCZG(ii))
				DELTASUBT=DeltaTangG(iMaterialCZG(ii))
				OLAMBDAG(II)=DSQRT((UNOLDG(II)/DELTASUBN)**2+(UTOLDG(II)/DELTASUBT)**2)
			ENDIF
	
		7971 CONTINUE
    
    	DAMAGEGIter = DAMAGEG
	    TractionNormalGiter = TractionNormalG
	    TractionTangentGIter = TractionTangentG
        SIGCOHNGIter = SIGCOHNG
        SIGCOHTGIter = SIGCOHTG



    End Subroutine UpdateCZGlobal
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Subroutine UpdateCZLocal
	
    !
    !   This subroutine updates stored variables in Allen cohesive zone model
    !
    !   iLocalCohesiveZones = 4
    !
    Real*8 Phi

	IF (INCR == 1.AND.ICYCL==1) THEN
	
		ALLOCATE (UNOLDL(NLocalInterfElementsL))
		ALLOCATE (UTOLDL(NLocalInterfElementsL))
		

		DO 7971 II = 1, NLocalInterfElementsL

            I1=NINT1L(II)
			I2=NINT2L(II)
			N1X=2*I1-1 
			N1Y=N1X+1 
			N2X=2*I2-1 
			N2Y=N2X+1 
			UX=DisplacementL(N2X)-DisplacementL(N1X) 
			UY=DisplacementL(N2Y)-DisplacementL(N1Y)
			PHI=PHIAVL(II)
			UN=UX*DCOS(PHI)+UY*DSIN(PHI) 
			UT=-UX*DSIN(PHI)+UY*DCOS(PHI)
			UNOLDL(II)=UN
			UTOLDL(II)=UT
            
			IF(iLocalCohesiveZones == 4)THEN
				DELTASUBN=DeltaNormalL(iMaterialCZL(ii))
				DELTASUBT=DeltaTangL(iMaterialCZL(ii))
				OLAMBDAL(II)=DSQRT((UNOLDL(II)/DELTASUBN)**2+(UTOLDL(II)/DELTASUBT)**2)
			ENDIF
	
		7971 CONTINUE
    
    ENDIF

    End Subroutine UpdateCZLocal


End Module UpdateCZs
    