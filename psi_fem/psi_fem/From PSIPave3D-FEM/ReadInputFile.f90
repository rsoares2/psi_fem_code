Module GlobalInput

    !Input Modules
    Use InputHeader
    Use InputCoordinates
    Use InputConnectivity
    Use InputBCandForce
    Use InputMaterials
    Use InputCohesiveZones
    Use SparseConectivity
    Use SparseLocalConectivity
    Use OutputFromInput
    Use MultiscaleElements
    
    implicit Real*8 (A-H, O-Z)

	Contains

	Subroutine ReadInput
    
    !
    !   This Subroutine reads from the input file for both single scale and multi scale simulations
    !
    !   All formats should be labeled 10XX
    !
   
    Call ReadInputHeader    !Reads overall variables, nodes, elements, flags, etc.
    
    Call ReadInputCoordinates   !Reads coordinates X, Y and Z
    
    Call ReadInputConnectivity  !Read element connectivity and material set and type (elastic, v/e, etc.)
    
    Call ReadGlobalInputBCandForce  !Reads boundary conditions and force
    
    Call ReadGlobalInputMaterials   !Reads material properties for each material set

	If(iGlobalCohesiveZones/=0) Call ReadGlobalInputInterfaceModel  !Reads cohesive zone properties

    Call SparseInitialConnectivity  !Sets up connectivity into CSR sparse format
    
    Read (5,*) NumberOfCycles
    Write(6,1050) NumberOfCycles

    Call PrintOutputFromInput   !Controls outputs (Tecplot file, stresses, strains, etc.)
    
 !   This is inside PrintOutputFromInput (REMOVE THIS) If(iPavement == 1) Call ReadPavement !Reads input from pavement (nodes at HMAC and Base interfaces and Subgrade interfaces)
    
1050 Format (/,3X,I6,2X,'Cycles applied',/)

    If(NumberOfScales > 1) Call ReadLocalInput
        
    End Subroutine ReadInput
    
    Subroutine ReadLocalInput
    
    Call ReadLocalInputHeader    !Reads overall variables, nodes, elements, flags, etc.
    
    Call ReadMultiscaleElements !Reads nodes on the boudary and elements to be multiscaled
    
    Call Read2DInputLocalCoordinates   !Reads coordinates X, Y and Z
    
    Call ReadLocalInputConnectivity  !Read element connectivity and material set and type (elastic, v/e, etc.)
    
    Call ReadLocalInputBC  !Reads local boundary conditions
    
    Call ReadLocalInputMaterials   !Reads material properties for each material set

	If(iLocalCohesiveZones/=0) Call ReadLocalInputInterfaceModel  !Reads cohesive zone properties

    Call SparseInitialLocalConnectivity  !Sets up connectivity into CSR sparse format
    
   ! Call PrintOutputFromLocalInput   !Controls outputs (Tecplot file, stresses, strains, etc.)
    
    End Subroutine ReadLocalInput

End Module GlobalInput