Module SparseConectivity
    
    Use GlobalInputVariables
    
    implicit Real (A-H, O-Z)

    Contains    

    Subroutine  SparseInitialConnectivity

    !
    !   This subroutine assembles the JCOL and IROW arrays for the sparse matrix in CSR format
    !   JCOL AND IROW represent the vectors with node information. 
    !   
    !   There is also a need to organize the columns in ascending order. This is done in a dIfferent Subroutine (inside SPARSE INITIAL module).
    !   NZG = NUMBER OF NONZEROS IN GLOBAL SCALE
    
    Allocate ( IROW(NumberOfGlobalNodes+1) )
    Allocate ( JCOL(NZG) )
    LinkedNodeConnectivity(:,iNGlobalNodes)=LinkedNodeConnectivity(:,iNGlobalNodes)-1   !NEED TO REDUCE 1, B/C II OR JJ IS ALSO INCLUDED.
    IPOSITION=0 !This variable keeps track of number of non-zeros and where to include
    
    !
    !   Update the Number of Non-Zeros to include the DOFs and not only the NumberOfGlobalNodes
    !
    
    If(NumberOfDimensions==2)Then
        NZG = (NZG*NumberOfDimensions)+((NZG*NumberOfDimensions)-NumberOfGlobalNodes)
        Allocate(IRowCompact(2*NumberOfGlobalNodes+1))
    ELSE
        NZG = (NZG*NumberOfDimensions)+((NZG*NumberOfDimensions)-NumberOfGlobalNodes)+((NZG*NumberOfDimensions)-NumberOfGlobalNodes)-NumberOfGlobalNodes
        Allocate(IRowCompact(3*NumberOfGlobalNodes+1))
    EndIf
    
    IROW(1)=1
    IRowCompact(1)=1
    
    Do I=1, NumberOfGlobalNodes
        Do J=1,LinkedNodeConnectivity(I,iNGlobalNodes)
            IPOSITION=IPOSITION+1
            JCOL(IPOSITION)=LinkedNodeConnectivity(I,J)
        Enddo
        IROW(I+1)=IPOSITION+1
        
        !
        ! Calculate IRowCompact, which includes the DOF. It is an "expanded" version of the IRow
        !
        
        If (NumberOfDimensions==2) Then
        
            Index1=(2*I-1)+1
            Index2=Index1+1
            IDIfference=IRow(I+1)-IRow(I)
            IRowCompact(Index1)=IDIfference*NumberOfDimensions+IRowCompact(Index1-1)
            IRowCompact(Index2)=IDIfference*NumberOfDimensions-1+IRowCompact(Index1)
    
        ElseIf (NumberOfDimensions==3) Then
   
            Index1=(3*I-2)+1
            Index2=Index1+1
            Index3=Index1+2
            IDIfference=IRow(I+1)-IRow(I)
            IRowCompact(Index1)=IDIfference*NumberOfDimensions+IRowCompact(Index1-1)
            IRowCompact(Index2)=IDIfference*NumberOfDimensions-1+IRowCompact(Index1)    
            IRowCompact(Index3)=IDIfference*NumberOfDimensions-2+IRowCompact(Index2)

        EndIf        
               
    Enddo
    
 !   If(NumberOfGlobalNodes>81)Then  !this If is due to array bounds If NumberOfGlobalNodes<27*3
      !  do ir=1,iNGlobalNodes
    !        ie=LinkedNodeConnectivity(ir,iNGlobalNodes)
   !         Write(778,182)i,ie
   !     Enddo
    182 format(2i8)
    !EndIf
    
    Deallocate(LinkedNodeConnectivity)

    Call ReorganizeGlobalSparseVectors_NEW  !This is in SparseInitial, however for this new way of populating the sparse matrix
    
    END Subroutine  SparseInitialConnectivity

!*********************************************************************************************    
    
    Subroutine ReorganizeGlobalSparseVectors_NEW
    
!
!   This subroutine should be Called after SparseInitialConnectivity
!   to reorganize in ascending order JCol and IRow vectors, so the appropriate compact format can be used
!   It works for 2D and 3D!!!!

!
!   First, need to reorder JCOL and IROW
!    
    Allocate(JColCompact(NZG)) !this is to include all degrees of freedom and not only the nodes

    kk=0
    DO I=1,NumberOfGlobalNodes
        DO J=iRow(I),IRow(I+1)-1
            N1=JCol(J)
            DO K=j+1,IRow(I+1)-1
                N2=JCOL(K)
                IF(N1 > N2)THEN
                    JCol(J)=JCol(K)
                    JCol(K)=N1
                    N1=N2
                ENDIF
            ENDDO
        ENDDO

        IF(NumberOfDimensions == 2)THEN

            Index1=2*I-1
              
            IndexPosition=IRowCompact(Index1)
                
            DO ij=IRow(I),IRow(I+1)-1
                
                JCOLCompact(IndexPosition)=2*JCol(ij)-1
                IndexPosition=IndexPosition+1
                JCOLCompact(IndexPosition)=2*JCol(ij)
                IndexPosition=IndexPosition+1
                
            ENDDO
            
            Index1=2*I
            IndexPosition=IRowCompact(Index1)
            
            DO ij=IRow(I),IRow(I+1)-1
            
                !this is for the second line due to 2D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                IF (ij/=iRow(I)) THEN
                    JCOLCompact(IndexPosition)=2*JCol(ij)-1
                    IndexPosition=IndexPosition+1
                ENDIF
                    JCOLCompact(IndexPosition)=2*JCol(ij)
                        IndexPosition=IndexPosition+1
          
            ENDDO
            
        ELSEIF(NumberOfDimensions == 3)THEN

            Index1=3*I-2
            IndexPosition=IRowCompact(Index1)
                
            DO ij=IRow(I),IRow(I+1)-1
                
                JCOLCompact(IndexPosition)=3*JCol(ij)-2
                IndexPosition=IndexPosition+1
                JCOLCompact(IndexPosition)=3*JCol(ij)-1
                IndexPosition=IndexPosition+1
                JCOLCompact(IndexPosition)=3*JCol(ij)
                IndexPosition=IndexPosition+1
                
            ENDDO
            
            Index1=3*I-1
            IndexPosition=IRowCompact(Index1)
            
            DO ij=IRow(I),IRow(I+1)-1
            
                !this is for the second and third lines contribution to the stiffness matrix due to 3D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                IF (ij/=iRow(I)) THEN
                    JCOLCompact(IndexPosition)=3*JCol(ij)-2
                    IndexPosition=IndexPosition+1
                ENDIF
                    JCOLCompact(IndexPosition)=3*JCol(ij)-1
                    IndexPosition=IndexPosition+1
                    JCOLCompact(IndexPosition)=3*JCol(ij)
                    IndexPosition=IndexPosition+1
          
            ENDDO
            
            Index1=3*I
            IndexPosition=IRowCompact(Index1)
            
            DO ij=IRow(I),IRow(I+1)-1
            
                !this is for the second and third lines contribution to the stiffness matrix due to 3D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                IF (ij/=iRow(I)) THEN
                    JCOLCompact(IndexPosition)=3*JCol(ij)-2
                    IndexPosition=IndexPosition+1
                    JCOLCompact(IndexPosition)=3*JCol(ij)-1
                    IndexPosition=IndexPosition+1
                ENDIF
                    JCOLCompact(IndexPosition)=3*JCol(ij)
                    IndexPosition=IndexPosition+1
          
            ENDDO            
       ENDIF
                   
    ENDDO  
    
!
!   Now convert JCOL to include DOFs - JCOLCompact. The name comes from a previous way to set up these matrices.
!
   

    
    End Subroutine ReorganizeGlobalSparseVectors_NEW    
    
End Module SparseConectivity    