Module InputMaterials
    
    Use GlobalInputVariables
    Use LocalInputVariables    
    Use InputViscoElasticity
    Use LocalInputViscoElasticity
    Use InputPlasticity
    Use InputElasticity
    Use InputElasticityUzan

    implicit Real (A-H, O-Z)

    Contains    

    Subroutine ReadGlobalInputMaterials
    
    !
    !   This subroutine reads the global material properties associated with each element
    !

	Read(5,*) MaterialNumber1G, MaterialNumber2G, MaterialNumber3G, MaterialNumber4G, MaterialNumber5G, MaterialNumber6G !Number of materials of each kind 
      
    NumberOfTotalMaterials = MaterialNumber1G + MaterialNumber2G + MaterialNumber3G + MaterialNumber4G + MaterialNumber5G + MaterialNumber6G
    
!	IVISG=0 !Not used anymore?
 
    If (MaterialNumber1G /= 0) Call ReadGlobalInputElastic
    If (MaterialNumber2G /= 0) Call ReadGlobalInputDruckerPrager
  !  If (MaterialNumber3G /= 0) Call ReadGlobalInputViscoPlastic
  !  If (MaterialNumber4G /= 0) Call ReadGlobalInputElasticOrthotropic
    If (MaterialNumber5G /= 0) Call ReadGlobalInputViscoElastic
    If (MaterialNumber6G /= 0) Call ReadGlobalInputElasticUzan
 
    End Subroutine ReadGlobalInputMaterials
    
    Subroutine ReadLocalInputMaterials
    
    !
    !   This subroutine reads the local material properties associated with each element
    !

	Read(5,*) MaterialNumber1L, MaterialNumber2L, MaterialNumber3L, MaterialNumber4L, MaterialNumber5L !Number of materials of each kind 
      
    NumberOfTotalMaterialsL = MaterialNumber1L + MaterialNumber2L + MaterialNumber3L + MaterialNumber4L + MaterialNumber5L 
     
    If (MaterialNumber1L /= 0) Call ReadLocalInputElastic
  !  If (MaterialNumber2L /= 0) Call ReadLocalInputElastic_Plastic
  !  If (MaterialNumber3L /= 0) Call ReadLocalInputViscoPlastic
  !  If (MaterialNumber4L /= 0) Call ReadLocalInputElasticOrthotropic
    If (MaterialNumber5L /= 0) Call ReadLocalInputViscoElastic
 
    End Subroutine ReadLocalInputMaterials
    
End Module InputMaterials