Module TecplotOutputFactorModule
    
    Use GlobalInputVariables
    Use Omp_lib
        
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine  WriteTecplotGlobalFactorOutput

        Real*8:: StressWithFactor(NumberOfGlobalElements)
    
    Open (Unit = 290, File = 'TecGStrainFactor.OUT', Status = 'Old', Position = "Append") 
    Open (Unit = 289, File = 'TecGStressFactor.OUT', Status = 'Old', Position = "Append") 
    
    If ( NumberOfScales == 1) Then

        If ( NumberOfDimensions == 2 )Then  !See Formats below

            If ( NumberOfGlobalNodesPerElement == 3 ) Then
        
		        Write(289,1040) NumberOfGlobalNodes, NumberOfGlobalElements
    
            ElseIf ( NumberOfGlobalNodesPerElement == 4 ) Then
        
                Write(289,1041) NumberOfGlobalNodes,NumberOfGlobalElements
            
            EndIf
            
        ElseIf ( NumberOfDimensions == 3 ) Then
        
            If ( NumberOfGlobalNodesPerElement == 8 ) Then
        
                Write(289,1042) NumberOfGlobalNodes,NumberOfGlobalElements
    		
                Write(290,1043) NumberOfGlobalNodes,NumberOfGlobalElements
    
            ElseIf ( NumberOfGlobalNodesPerElement == 4 ) Then
    
                Write(289,1044) NumberOfGlobalNodes,NumberOfGlobalElements
    		
                Write(290,1045) NumberOfGlobalNodes,NumberOfGlobalElements
            		
            EndIf
            
        EndIf
                			
    ElseIf ( NumberOfScales == 2 ) Then
		
        If ( NumberOfGlobalNodesPerElement == 3 ) Then

		    Write(289,1046) NumberOfGlobalNodes,NumberOfGlobalElements
        
        Else 
            
            Write(6,1047)
            Stop
            
        EndIf

    EndIf
    
                1040	Format('VARIABLES= "X" "Y" "MATERIAL" "Stress XX" "Stress YY" "Stress XY" "Strain XX" "Strain YY" "Strain XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETRIANGLE',&
		        /,'VARLOCATION=([3,4,5,6,7,8,9]=CELLCENTERED) C=BLACK CONNECTIVITYSHAREZONE=1 VARSHARELIST=([3]=1)',/)

                1041	Format('VARIABLES= "X" "Y" "MATERIAL" "Stress XX" "Stress YY" "Stress XY" "Strain XX" "Strain YY" "Strain XY"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FEQUADRILATERAL',&
		        /,'VARLOCATION=([3,4,5,6,7,8]=CELLCENTERED) C=BLACK',/)

                1042	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Stress XX" "Stress YY" "Stress ZZ" "Stress YZ" "Stress ZX" "Stress XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FEBRICK',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK',/)
    		
                1043	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Strain XX" "Strain YY" "Strain ZZ" "Strain YZ" "Strain ZX" "Strain XY" "Deflection X"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FEBRICK',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK',/)
    
                1044	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Stress XX" "Stress YY" "Stress ZZ" "Stress YZ" "Stress ZX" "Stress XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETETRAHEDRON',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK CONNECTIVITYSHAREZONE=1 VARSHARELIST=([4]=1)',/)
    		
                1045	Format('VARIABLES= "X" "Y" "Z" "MATERIAL" "Strain XX" "Strain YY" "Strain ZZ" "Strain YZ" "Strain ZX" "Strain XY" "Deflection Y"',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETETRAHEDRON',&
		        /,'VARLOCATION=([4,5,6,7,8,9,10]=CELLCENTERED) C=BLACK CONNECTIVITYSHAREZONE=1 VARSHARELIST=([4]=1)',/)

                1046	Format('VARIABLES= "X" "Y" "MATERIAL" "Stress XX" "Stress YY" "Stress XY" "Strain XX" "Strain YY" "Strain XY" "C11" "C12" "C22" ',/,&
		        'ZONE T="TIME=0.0", N=',I6,', E= ',I6,', DATAPACKING=BLOCK ZONETYPE=FETRIANGLE',&
		        /,'VARLOCATION=([3,4,5,6,7,8,9,10,11,12]=CELLCENTERED) C=BLACK',/)

                1047 Format ("There is no Multiscale code for LST or rectangular elements yet! Please change NumberOfGlobalNodesPerElement to 3")
                     
    Close (29)
    Close (290)
		
    !--------------HERE---------------------
    !$omp parallel sections
    !$omp section
    Call WriteTecStressFactor()
    !$omp section
    !Call WriteTecStrainFactor()	
    !$omp end parallel sections

!	
!   This part is for damage dependent stiffness
!

	If ( NumberOfScales == 2 ) Then

	    Allocate (C11L(NumberOfGlobalElements))
	    Allocate (C12L(NumberOfGlobalElements))
	    Allocate (C22L(NumberOfGlobalElements))
    
	    Do I=1,NumberOfGlobalElements
    		C11L(I)=C(1,1)
	    	C12L(I)=C(1,2)
		    C22L(I)=C(2,2)
	    Enddo

	    Write(89,22) (C11L(I),I=1, NumberOfGlobalElements)
	    Write(89,22) (C12L(I),I=1, NumberOfGlobalElements)
	    Write(89,22) (C22L(I),I=1, NumberOfGlobalElements)
        
    22 Format (10E15.7)

	    DeAllocate (C11L)
	    DeAllocate (C12L)
	    DeAllocate (C22L)
	
    EndIf

    End Subroutine WriteTecplotGlobalFactorOutput

!*********************************************************************************************    

    Subroutine WriteTecStressFactor()
    
    !
    !   This subroutine prints the Stresses in Tecplot file Format
    !
    

    integer I, IEL

    If ( NumberOfDimensions == 2 ) Then

    Open (Unit = 289, File ='TecGStressFactor.OUT',Status = "OLD", Position = "APPEND") 
    
    Write(289,16) (XG(i)+DeformationFactor*DisplacementG(2*i-1), i = 1, NumberOfGlobalNodes)
	Write(289,12)
    Write(289,16) (YG(i)+DeformationFactor*DisplacementG(2*i), i = 1, NumberOfGlobalNodes)
    Write(289,12)

   12	Format(2/)
   
	!This is for stress variables
   
  		WRITE(289,16) (SG(i,1,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		WRITE(289,12)

  		WRITE(289,16) (SG(i,2,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		WRITE(289,12)

  		WRITE(289,16) (SG(i,3,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		WRITE(289,12)


  		WRITE(289,16) (StrainG(i,1,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		WRITE(289,12)		

  		WRITE(289,16) (StrainG(i,2,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		WRITE(289,12)		

  		WRITE(289,16) (StrainG(i,3,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		WRITE(289,12)		

        WRITE(289,16) (DisplacementG(2*i),i=1,NumberOfGlobalNodes)
		WRITE(289,12)		
	
   16	Format(10(1X,E15.7)) 
  
    ElseIf ( NumberOfDimensions == 3 ) Then

    Open (Unit = 289, File ='TecGStressFactor.OUT',Status = "OLD", Position = "APPEND") 
    !need to fix StrainLimitG for uzan model
    Write(289,16) (XG(i)+DeformationFactor*DisplacementG(3*i-2), i = 1, NumberOfGlobalNodes)
	Write(289,12)
    Write(289,16) (YG(i)+DeformationFactor*DisplacementG(3*i-1), i = 1, NumberOfGlobalNodes)
    Write(289,12)
    Write(289,16) (ZG(i)+DeformationFactor*DisplacementG(3*i-0), i = 1, NumberOfGlobalNodes)
	Write(289,12)
	       
 !   Write(89,16) (MaterialSetG(i), i = 1, NumberOfGlobalElements)
	!Write(89,12)

	!This is for stress variables
   
  		Write(289,16) (SG(i,1,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		Write(289,12)

  		Write(289,16) (SG(i,2,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		Write(289,12)

  		Write(289,16) (SG(i,3,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		Write(289,12)

  		Write(289,16) (SG(i,4,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		Write(289,12)		

  		Write(289,16) (SG(i,5,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		Write(289,12)		

  		Write(289,16) (SG(i,6,1)/StrainLimitG(MaterialSetG(i)), i = 1, NumberOfGlobalElements)
		Write(289,12)		

        Write(289,16) (DisplacementG(3*i-1),i=1,NumberOfGlobalNodes)
		Write(289,12)		
	
   Endif

    Close(289)
    
    End Subroutine WriteTecStressFactor

End Module TecplotOutputFactorModule