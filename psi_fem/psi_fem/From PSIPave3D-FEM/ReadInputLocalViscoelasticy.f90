Module LocalInputViscoElasticity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real (A-H, O-Z)
    
    Contains    

    Subroutine ReadLocalInputViscoElastic
    
    ! 
    !   Read in material properties for viscoelastic Elements
    ! 
    
    iVisL = 1 !Flag to indicate there is a viscoelastic material
    
    Allocate ( ViscoElasticPositionL (NumberOfTotalMaterialsL) )
    ViscoElasticPositionL = 0
            
    If ( NumberOfDimensions == 2 ) Then
        
        Call Read2DInputLocalViscoelasticity
        
    ElseIf ( NumberOfDimensions == 3 ) Then

        Write(*,2334)
2334 Format ('Multiscale in 3D is not yet available')        
        STOP

    Else
    
        Write (6,1007) 
1007    Format ('The number of dimensions must be 2 for 2D or 3 for 3D')
        Stop
        
    Endif


    End Subroutine ReadLocalInputViscoElastic
    
    Subroutine Read2DInputLocalViscoelasticity

    !  
    !   Read material properties for orthotropic linear viscoelasticity in 2D
    !   
    !   The traditional E(t) = Einf + Sum(Ei*exp(-Ei/Etai*DeltaTime) is converted to the C matrix.
    !
    !   NumberOfTerms??L is the number of terms in the Prony series
    
    Write(6,1024) MaterialNumber5L
		
    Allocate ( NumberOfTerms11L (MaterialNumber5L) )
	Allocate ( NumberOfTerms12L (MaterialNumber5L) )
	Allocate ( NumberOfTerms22L (MaterialNumber5L) )
	Allocate ( NumberOfTerms23L (MaterialNumber5L) )
	Allocate ( NumberOfTerms44L (MaterialNumber5L) )
	Allocate ( NumberOfTerms66L (MaterialNumber5L) )
    
	Allocate ( CInf11L (MaterialNumber5L) )
	Allocate ( CInf12L (MaterialNumber5L) )
	Allocate ( CInf22L (MaterialNumber5L) )
	Allocate ( CInf23L (MaterialNumber5L) )
	Allocate ( CInf44L (MaterialNumber5L) )
    Allocate ( CInf66L (MaterialNumber5L) )
    
	Do i=1, MaterialNumber5L

		Read(5,*) MSetiL,NumberOfTerms11L(I),NumberOfTerms12L(I),NumberOfTerms22L(I),NumberOfTerms23L(I),NumberOfTerms44L(I),NumberOfTerms66L(I)
		Write(6,1025) MsetiL
		Write(6,1026) NumberOfTerms11L(I),NumberOfTerms12L(I),NumberOfTerms22L(I),NumberOfTerms23L(I),NumberOfTerms44L(I),NumberOfTerms66L(I)
 		
		Read(5,*) CInf11L(I),CInf12L(I),CInf22L(I),CInf23L(I),CInf44L(I),CInf66L(I)
		Write(6,1028)  CInf11L(I),CInf12L(I),CInf22L(I),CInf23L(I),CInf44L(I),CInf66L(I)
		
            If(.Not.Allocated (CL11L)) Allocate ( CL11L (MaterialNumber5L,NumberOfTerms11L(I)) )
		    If(.Not.Allocated (CL12L)) Allocate ( CL12L (MaterialNumber5L,NumberOfTerms12L(I)) )
		    If(.Not.Allocated (CL22L)) Allocate ( CL22L (MaterialNumber5L,NumberOfTerms22L(I)) )
		    If(.Not.Allocated (CL23L)) Allocate ( CL23L (MaterialNumber5L,NumberOfTerms23L(I)) )
		    If(.Not.Allocated (CL44L)) Allocate ( CL44L (MaterialNumber5L,NumberOfTerms44L(I)) )
		    If(.Not.Allocated (CL66L)) Allocate ( CL66L (MaterialNumber5L,NumberOfTerms66L(I)) )

		    If(.Not.Allocated (Eta11L)) Allocate ( Eta11L (MaterialNumber5L,NumberOfTerms11L(I)) )
		    If(.Not.Allocated (Eta12L)) Allocate ( Eta12L (MaterialNumber5L,NumberOfTerms12L(I)) )
		    If(.Not.Allocated (Eta22L)) Allocate ( Eta22L (MaterialNumber5L,NumberOfTerms22L(I)) )
		    If(.Not.Allocated (Eta23L)) Allocate ( Eta23L (MaterialNumber5L,NumberOfTerms23L(I)) )
		    If(.Not.Allocated (Eta44L)) Allocate ( Eta44L (MaterialNumber5L,NumberOfTerms44L(I)) )
		    If(.Not.Allocated (Eta66L)) Allocate ( Eta66L (MaterialNumber5L,NumberOfTerms66L(I)) )
!
! Read in Elements of CL11
!
		Read(5,*) (CL11L(i, j), J=1, NumberOfTerms11L(I))
		Read(5,*) (Eta11L(i, j), J=1, NumberOfTerms11L(I))
		Write(6,1030)
		Do j = 1, NumberOfTerms11L(I)
			Write(6,1029) J, CL11L(i, j), Eta11L(i, j)
        Enddo
!
! Read in Elements of CL12
!
		Read(5,*) (CL12L(i, j), J = 1, NumberOfTerms12L(I))
		Read(5,*) (Eta12L(i, j), J = 1, NumberOfTerms12L(I))
		Write(6,1031)
		Do j = 1, NumberOfTerms12L(I)
			Write(6,1029) J, CL12L(i, j), Eta12L(i, j)
        Enddo

        !
! Read in Elements of CL22
!
		Read(5,*) (CL22L(i, j), J = 1, NumberOfTerms22L(I))
		Read(5,*) (Eta22L(i, j), J = 1, NumberOfTerms22L(I))
		Write(6,1032)
		Do j = 1, NumberOfTerms22L(I)
			Write(6,1029) J, CL22L(i, j), Eta22L(i, j)
        Enddo
!
! Read in Elements of CL23
!
		Read(5,*) (CL23L(i, j), J = 1, NumberOfTerms23L(I))
		Read(5,*) (Eta23L(i, j), J = 1, NumberOfTerms23L(I))
		Write(6,1033)
		Do j = 1, NumberOfTerms23L(I)
			Write(6,1029) J, CL23L(i, j), Eta23L(i, j)
        Enddo
        
!
! Read in Elements of CL44
!
		Read(5,*) (CL44L(i, j),J = 1, NumberOfTerms44L(I))
		Read(5,*) (Eta44L(i, j),J = 1, NumberOfTerms44L(I))
		Write(6,1034)
		Do j = 1,NumberOfTerms44L(I)
			Write(6,1029) J, CL44L(i, j), Eta44L(i, j)
        Enddo   
    
!
! Read in Elements of CL66
!
		Read(5,*) (CL66L(i, j),J=1,NumberOfTerms66L(I))
		Read(5,*) (Eta66L(i, j),J=1,NumberOfTerms66L(I))
		Write(6,1035)
		Do j = 1, NumberOfTerms66L(I)
			Write(6,1029) J,CL66L(i, j),Eta66L(i, j)
        Enddo
 
    Enddo
    
    !
    !   Formats used in viscoelasticity
    !

 1024 Format(/,10X,'There is ',i10,' orthotropic linear viscoelastic material set',/)
 1025	Format(/,10X,'Viscoelastic Model Number = ',I5,/)
 1026	Format(10X,'Number of Prony Terms in CL11 = ',I5,/,&
		10X,'Number of Prony Terms in CL12 = ',I5,/,&
		10X,'Number of Prony Terms in CL22 = ',I5,/,&
		10X,'Number of Prony Terms in CL23 = ',I5,/,&
	    10X,'Number of Prony Terms in CL44 = ',I5,/,&
		10X,'Number of Prony Terms in CL66 = ',I5,/)
 		
 1028	Format(/,10X,'CInf11 = ',D15.7,/,&
		10X,'CInf12 = ',D15.7,/,&
		10X,'CInf22 = ',D15.7,/,&
		10X,'CInf23 = ',D15.7,/,&
		10X,'CInf44 = ',D15.7,/,&
		10X,'CInf66 = ',D15.7,/)
		
        
     1029   Format(15X,I5,5X,D15.7,5X,D15.7)
            
    !
    !   Read in Elements OF CL11, 12, 22, 23, 44, 66
    !
        
     1030	Format(/,10X,'Prony Terms for CL11',//,19X,'I',13X,'CL11L(I)',12X,'Eta11L(I)',/)
     1031	Format(/,10X,'Prony Terms for CL12',//,19X,'I',13X,'CL12L(I)',12X,'Eta12L(I)',/)
     1032	Format(/,10X,'Prony Terms for CL22',//,19X,'I',13X,'CL22L(I)',12X,'Eta22L(I)',/)
     1033	Format(/,10X,'Prony Terms for CL23',//,19X,'I',13X,'CL23L(I)',12X,'Eta23L(I)',/)
     1034	Format(/,10X,'Prony Terms for CL44',//,19X,'I',13X,'CL44L(I)',12X,'Eta44L(I)',/)
     1035	Format(/,10X,'Prony Terms for CL66',//,19X,'I',13X,'CL66L(I)',12X,'Eta66L(I)',/)
 
    Do i = 1, MaterialNumber5L
        
        Max = Max0(NumberOfTerms11L(I),NumberOfTerms12L(I),NumberOfTerms22L(I),NumberOfTerms23L(I),NumberOfTerms44L(I),NumberOfTerms66L(I))
	    If (MAX > NLPronyL) Then
            NLPronyL = MAX
        EndIf
    	
    Enddo

455 CONTINUE
    
    End Subroutine Read2DInputLocalViscoelasticity
    
End Module LocalInputViscoElasticity