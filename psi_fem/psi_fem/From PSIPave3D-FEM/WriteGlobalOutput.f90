Module WriteGlobalOutput

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    Use TetrahedronShapeFunction
        
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine WriteOutputG
    
    !
    !   This subroutine prints outputs for the global scale
    !    
    
    If (NumberOfDimensions == 2 ) Then
        
        Call WriteDisplacementOutput2D
        Call WriteStressOutput2D
            
    ElseIf (NumberOfDimensions == 3 ) Then 
        
        Call WriteDisplacementOutput3D
        Call WriteStressOutput3D
        
    Endif    
    
    End Subroutine WriteOutputG
    
!*******************************************************************************

    Subroutine WriteDisplacementOutput2D

    Open (Unit = 33, File = 'GlobalDisplacement.OUT', Status = "Old", Position = "Append")
    
        Do i = 1, NumberOfGlobalNodes

            Write (33,2046) i,it, Time, DisplacementG(2*i-1),DisplacementG(2*i)
    
        Enddo

    Close(33)

    2046	Format(2I5,6E19.10)   
       
    End Subroutine WriteDisplacementOutput2D

!*******************************************************************************

    Subroutine WriteDisplacementOutput3D
    
    Open (Unit = 33, File = 'GlobalDisplacement.OUT', Status = "Old", Position = "Append")
        
        Do i = 1, NumberOfGlobalNodes

            Write (33,2046) i,Time, DeltaDisplacementG(3*i-2),DisplacementG(3*i-1), DisplacementG(3*i)
    
        Enddo

    Close(33)

    2046	Format(I5,7E19.10)   

            
    End Subroutine WriteDisplacementOutput3D

!*******************************************************************************

    Subroutine WriteStressOutput3D
    
    Open (Unit = 13, File = 'GlobalStress.OUT', Status = "Old", Position = "Append")  !Needed because the files are written with "append".
    Open (Unit= 14, File = "GlobalStrain.OUT", Status = "Old", Position = "Append")  !This statement will restart the files after each simulation.
    
    If (NumberOfGlobalNodesPerElement == 4)Then
        Write(13,1008)

        Do i = 1, NumberofGlobalElements
        
	        Do intp = 1, NGIntegrationPoints
                Call TetrahedronCentroid(I,XBAR,YBAR,ZBAR)  !Plots centroid of element
                If(MaterialTypeG(i) == 6) Then
    	    		Write(13,1015) I,XBar, YBar, ZBar,(SG(I,J,intp),J=1,6), I1_Inv(i), J2_Inv(i), MaterialSetG(i),ModulusI1J2(i), SGOctahedral(i),ck1(MaterialSetG(i))*Pa*(abs(I1_Inv(i)/Pa))**ck2(MaterialSetG(i)) * (SGOctahedral(i)/Pa+1)**ck3(MaterialSetG(i))
                Else
    	    		Write(13,1015) I,XBar, YBar, ZBar,(SG(I,J,intp),J=1,6), I1_Inv(i), J2_Inv(i), MaterialSetG(i),ModulusI1J2(i), SGOctahedral(i)
                Endif
    			Write(14,1018) I,XBar, YBar, ZBar,(StrainG(I,J,intp),J=1,6)
            Enddo
        

 1015	FORMAT(12X,1I6,11(1X,E15.7),i7,3E15.7) 
 1018	FORMAT(12X,1I6,12(1X,E15.7),i7) 
 1008   Format(10X,"ElemNumber",6x,"X",14x,"Y",15x,"Z",12x,"StressXX",8x,"StressYY",8x,"StressZZ",8x,"StressYZ",8x,"StressXZ",8x,"StressXY",11x,"I1",14x,"J2",8x,"MatNumber")
        Enddo
    Endif

    Close(33)

    2046	Format(2I5,6E19.10)   

            
    End Subroutine WriteStressOutput3D

!*******************************************************************************

    Subroutine WriteStressOutput2D

    Open (Unit = 13, File = 'GlobalStress.OUT', Status = "Old", Position = "Append")  !Needed because the files are written with "append".
    Open (Unit= 14, File = "GlobalStrain.OUT", Status = "Old", Position = "Append")  !This statement will restart the files after each simulation.

        Do i = 1, NumberofGlobalElements

	        Do intp = 1, NGIntegrationPoints
            !   CALL SHAPE3DCENTROID(I,XBAR,YBAR,ZBAR)  !Plots centroid of element
    			!Write(13,1017) I,IINTPT,Xbar,Ybar,Zbar,(StressG(I,J,IINTPT),J=1,6)
                Write(13,1017) i, it, intp, (SG(i,j,intp), j = 1, iRange)
			    !Write(14,1017) I,IINTPT,Xbar,Ybar,Zbar,(StrainG(I,J,IINTPT),J=1,6)
                Write(14,1017) i, it, intp, (StrainG(i,j,intp), j = 1, iRange)
            Enddo
        Enddo

        If (MaterialNumber2G /= 0) Then
        Do i = 1, NumberofGlobalElements

	!        Do intp = 1, NGIntegrationPoints

             !   Write(6, 1017) incr, i, it, (EPSP(i,j,intp), j = 1, iRange)

        !    Enddo
        
        Enddo

        Endif


 1017	    Format(3i5,6E15.7)

    Close(14)
    Close(13)

    End Subroutine WriteStressOutput2D

End Module WriteGlobalOutput