Module CalculateStifnessMatrix3D

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleStiffnessMatrix
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine CalculateGlobalStiffnessMatrixSparse3D
    
    !
    !   This subroutine calculates the global sparse stiffness matrix for 3D meshes
    !    
    
	Integer::NICON(2), iElastic, iViscoElastic
    
    Do iel = 1, NumberOfGlobalElements  !Major loop over all elements
    
        If (mod(iel, iPrintToScreen) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) iel
 805        Format (10x,'Set up Global Stiffness Matrix for Element =',I8)
            
        Endif        

!   Initialize the element stiffness matrix

        KElement = 0.0D0
        
!
!   Calculate element stiffness matrix
!

        Call SetUpQuadraturePoints (NGIntegrationPoints, NumberOfGlobalNodesPerElement)  ! Set up quadrature points and weights

        Do iIntPoint = 1, NGIntegrationPoints
        
            Call ShapeFunction3D (iel, iIntPoint)  !Set up B matrix
            
            Call ReadGlobalMaterialType3D (iel, iIntPoint, 1)  !Add material properties (1, means Called from stifnness matrix)
            
            Call CalculateElementStiffnessMatrix3D
            
        Enddo
    
        Call AssembleGlobalStiffnessSparse3D (iel) !Assembles each local element into a global stiffness matrix
        
        DSIGRG = 0.D0 !Necessary if viscoelasticity is used

    Enddo
    
    Call PostCalculationStiffness3D
    
    Call ApplyBCStiffness3D
    
    End Subroutine CalculateGlobalStiffnessMatrixSparse3D    
    
    Subroutine CalculateElementStiffnessMatrix3D
   
    !   
    !   This subroutine calculates the element stiffness matrix 
    !   
    !   It first multiplies B transpose matrix (deriv. of shape function) * C matrix (Stiffness/Modulus) * B matrix
    !   and numeriCally integrates. Using DGEMM form MKL to multiply matrix.
            
            Call DGEMM( 'T'  , 'N'  , IBRangeG, 6 , 6 , 1.0D0 , BG , 6 , C , 6 , 0.0D0 , KElement_Temp , IBRangeG ) !MKL matrix multiplication function

            Call DGEMM( 'N'  , 'N'  , 12 , 12 , 6 , 1.0D0 , KElement_Temp , 12 , BG , 6 , 0.0D0 , KElement , 12 ) 
            
            KElement = KElement* DETJ * Weight  !Uses output from DGEMM function and integrates
   
    End Subroutine CalculateElementStiffnessMatrix3D

!*********************************************************************************************        
    
    Subroutine ApplyBCStiffness3D
   
    !   
    !   This subroutine applies boundary conditions to the global stiffness matrix 
    !   

	JC=1
    !$omp parallel

    !$omp single
        nthr = OMP_GET_NUM_THREADS()
  !  Print*, 'OpenMP number of threads: ', nthr
    !$omp end single

      
    
    !$omp do private(JJ)
	
    Do i = 1, NumberOfDisplacementBC
	    jj = NGlobalDOF (i, KTime)
        SparseValuesG (IROWCompact(jj)) = KBig
    Enddo
    
    !$omp end do 
    !$omp end parallel
    
    
   End Subroutine ApplyBCStiffness3D
   
End Module CalculateStifnessMatrix3D
    