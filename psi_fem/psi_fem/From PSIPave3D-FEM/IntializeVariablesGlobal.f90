MODULE InitializeGlobalVariables

    Use GlobalInputVariables
    Use LocalInputVariables
    Use LinearElasticity
    Use InitializeLocalVariables

	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine GlobalInitial
	
    If (NumberOfDimensions == 2 ) Then 
        
        iRange = 4 !This is 4 for 2-D and 6 for 3-D. Relates to the size of the stress matrix
        TG = 1.0
        If (IterationFlag == 0) iMaximumNumberofIterations = 1  !this avoids having to change maximum number of iterations in input 
                                                                !every time iteration is not involved. By changing iMaxNum... to 1,
                                                                !it skips the iteration loop.
    ElseIf (NumberOfDimensions == 3 ) Then 
        
        iRange = 6
        If (IterationFlag == 0) iMaximumNumberofIterations = 1  !this avoids having to change maximum number of iterations in input 
                                                                !every time iteration is not involved. By changing iMaxNum... to 1,
                                                                !it skips the iteration loop.
        
	    If (MaterialNumber3G /= 0) Then

            Allocate (X(NumberOfGlobalElements, iRange, NGIntegrationPoints))
            X=0
        
        Endif
    
    Endif
    
       
    IBRangeG = NumberOfDimensions * NumberOfGlobalNodesPerElement    !Range of the B Matrix        
    
	If (NumberOfForces /= 0) Then   
        
		Allocate (ForceG (NGTotalDOF))
        
        FORCEG = FGLG
        FGLG = 0.0
        
    Endif	
    
    DeformationFactor = 1
    Pa = 101325 !Atmospheric pressure.  Used in Uzan's model stress calculation

    !iPrintToScreen = NumberOfGlobalElements/5   !Flag to print progress of Stiffness matrix to screen
    iPrintToScreen = 1e8   !Flag to print progress of Stiffness matrix to screen
    If(iPrintToScreen == 0) iPrintToScreen = 1  !Only prints five lines

    !
    !   Initialize stress, strains and deflections
    !
    
	Allocate ( SG(NumberOfGlobalElements, iRange, NGIntegrationPoints))
	Allocate ( SGOld (NumberOfGlobalElements, iRange, NGIntegrationPoints)) !Old Strain. Implemented for Plasticity
	Allocate ( StrainG(NumberOfGlobalElements, iRange, NGIntegrationPoints))
	Allocate ( DisplacementG(NGTotalDOF))
    Allocate ( DisplacementGold(NGTotalDOF))
    Allocate ( StrainGOld(NumberOfGlobalElements, iRange, NGIntegrationPoints))
    ! The allocate statement came from InitializeGlobalStress2D, because DeltaStrain is needed in plasticity.
    !   Initialize the element strains
    Allocate (DeltaStrainG (NumberOfGlobalElements, iRange, NGIntegrationPoints))
	

    DeltaStrainG = 0.0D0    


	DTime = 1.0
	KTime = 1.0
    FAC = 1.0

    DisplacementG = 0.0
    SG =0.0 !test
    StrainG = 0.0

	TIME=0.
    
	If (NumberOfTimeBlocks /= 0) Then
		
		DTime = DeltaTime(1)
		Fac = TimeFactor(1) 
		TimLim = TimeEndTB(1)
		KTIME = 1
		NDBC = NumberOfGlobalDisplacement(1)

    Endif
    
	If(MaterialNumber2G /=0) Then !If there is CZ a priori. If there is automatic insertion, skip
	
	    Allocate ( Alpha11 (NumberOfGlobalElements, NGIntegrationPoints) )
	    Allocate ( Alpha2 (NumberOfGlobalElements, iRange, NGIntegrationPoints) )
	    Allocate ( EPBar (NumberOfGlobalElements, NGIntegrationPoints) )
	    Allocate ( iPlas (NumberOfGlobalElements) )
        Allocate ( EPSP(NumberOfGlobalElements, iRange, NGIntegrationPoints))
    	Allocate ( DEPSP(NumberOfGlobalElements, iRange, NGIntegrationPoints))
    	Allocate ( DEPSPT(NumberOfGlobalElements, iRange, NGIntegrationPoints))
	    Allocate ( DS(NumberOfGlobalElements, iRange, NGIntegrationPoints))

        Do i = 1, NumberOfGlobalElements
            Alpha11(i,:) = YieldGDP(MaterialSetG(i))/SQRT(3.)
	    Enddo
        Alpha2 = 0.0
        iPlas = 0
        EPBar = 0.0
        EPSP = 0.0
        DEPSP = 0.0
        DEPSPT = 0.0
        DS = 0.0
    Endif

	If(iGlobalCohesiveZones /=0 .or.iGlobalCohesiveZones <= 4) Then !If there is CZ a priori. If there is automatic insertion, skip
	
	    Allocate ( iDebndG (NGlobalInterfElementsG) )
		Allocate ( DamageG (NGlobalInterfElementsG) )
		Allocate ( DamageGIter (NGlobalInterfElementsG) )
        IDEBNDG = 0
        DamageG = 0.0
        DamageGIter = 0.0
    Endif
    
 	If(iGlobalCohesiveZones == 4 ) Then  !Only allocate here if using Allen CZ a priori
            
		Allocate ( OLAMBDAG(NGlobalInterfElementsG))
		Allocate ( SIGCOHTG(NGlobalInterfElementsG,NumberPronyTermsCZ))
		Allocate ( SIGCOHNG(NGlobalInterfElementsG,NumberPronyTermsCZ))

		Allocate ( SIGCOHTGIter(NGlobalInterfElementsG,NumberPronyTermsCZ))
		Allocate ( SIGCOHNGIter(NGlobalInterfElementsG,NumberPronyTermsCZ))

		OLAMBDAG = 0.
		SIGCOHTG = 0.
		SIGCOHNG = 0.
		SIGCOHTGIter = 0.
		SIGCOHNGIter = 0.        

        
    Endif
    

	If (iVisG == 1) Then !If there is V/E material

        If (.NOT. Allocated (SIG)) Allocate ( SIG (NumberOfGlobalViscoElasticElement, NGPRONYG, iRange, iRange, NGIntegrationPoints))
        If (.NOT. Allocated (CC3G)) Allocate ( CC3G (iRange,iRange))
        SIG = 0.
 
    Endif
    

    Open (Unit = 13,FILE ='GlobalStress.OUT', Status = "Replace")  !Needed because the files are written with "append".
    Close (13)
    Open (Unit= 14,FILE ="GlobalStrain.OUT", Status = "Replace")  !This statement will restart the files after each simulation.
    Close (14)
    Open (Unit = 33,FILE ='GlobalDisplacement.OUT', Status = "Replace")
    Close(33)    
    Open (Unit = 999,FILE ='OutputStrains.OUT', Status = "Replace")
    Close (999)

    KBig = 1.E35   !Used to apply boundary conditions
    Gravity = 9.81
    
    If(NumberOfScales > 1) Call LocalInitial

    
    End Subroutine GlobalInitial
	
!**************************************************************************************    
    
    Subroutine InitializeGlobalStiffnessMatrix3D

        Allocate (BG (6,IBRangeG))   !This is the B Matrix (shape functions) 
        Allocate (KElement (IBRangeG, IBRangeG))
        Allocate (KElement_Temp (IBRangeG, IBRangeG/2))    
    
        IViscoElastic = 0
	    Ielastic = 0
    
        ! Initialize sparse stiffness matrix
        Allocate (SparseValuesG (NZG) )
        SparseValuesG = 0.0D0
        
        Allocate (C (6,6))
        C = 0.0
        
    End Subroutine InitializeGlobalStiffnessMatrix3D
    
!**************************************************************************************        
    
    Subroutine PostCalculationStiffness3D

        IViscoElastic=0
	    IViscoPlastic=0
	    Ielastic=0
        iPlastic = 0

	    IF (Allocated (BG)) Deallocate(BG)	
        Deallocate (KElement, KElement_Temp)   
        Deallocate (C)
    
    End Subroutine PostCalculationStiffness3D

!**************************************************************************************    
    
    Subroutine InitializeGlobalForceMatrix3D

    ! Initialize force matrix
    
        Allocate (BG (6,IBRangeG))   !This is the B Matrix (shape functions) 
        Allocate (FElement (IBRangeG))
        Allocate (C (6,6))
        C = 0.0
        
        Allocate (ForceMatrixG (NGTotalDOF))
        ForceMatrixG = 0
        
        IViscoElastic = 0
	    Ielastic = 0
        iPlastic = 0

    End Subroutine InitializeGlobalForceMatrix3D
    
!**************************************************************************************    
    
    Subroutine InitializeGlobalStress3D

        Allocate (BG (6,IBRangeG))   !This is the B Matrix (shape functions) 
    
        IViscoElastic = 0
	    Ielastic = 0
        iPlastic = 0

        !   Initialize the element strains
   !     Allocate (DeltaStrainG (NumberOfGlobalElements, iRange, NGIntegrationPoints))

        DeltaStrainG = 0.0D0
        
        Allocate (DeltaStressG (iRange))

        DeltaStressG = 0.0D0
        
        Allocate (C (6,6))
        C = 0.0
        
    End Subroutine InitializeGlobalStress3D

!**************************************************************************************    
    
    Subroutine InitializeGlobalStiffnessMatrix2D

        Allocate (BG (3,IBRangeG))   !This is the B Matrix (shape functions) 
        Allocate (KElement (IBRangeG, IBRangeG))
        Allocate (KElement_Temp (IBRangeG, IBRangeG/2))    

        IViscoElastic = 0
	    Ielastic = 0
        iPlastic = 0
    
        ! Initialize sparse stiffness matrix
        Allocate (SparseValuesG (NZG) )
        SparseValuesG = 0.0D0
        
        Allocate (C (3, 3))
        C = 0.0
        
    End Subroutine InitializeGlobalStiffnessMatrix2D
    
!**************************************************************************************        
    
    Subroutine PostCalculationStiffness2D

        IViscoElastic=0
	    IViscoPlastic=0
	    IElastic=0
        iPlastic=0

	    IF (Allocated (BG)) Deallocate(BG)
        Deallocate (KElement, KElement_Temp)   
        Deallocate (C)
    
    End Subroutine PostCalculationStiffness2D

!**************************************************************************************        
    
    Subroutine PostCalculationStiffness2DL

        IViscoElastic=0
	    IViscoPlastic=0
	    IElastic=0

	    IF (Allocated (BG)) Deallocate(BG)
        Deallocate (KElementL, KElement_TempL)
        !Deallocate (C)
    
    End Subroutine PostCalculationStiffness2DL

!**************************************************************************************    
    
    Subroutine InitializeGlobalForceMatrix2D

    ! Initialize force matrix
    
        Allocate (BG (3,IBRangeG))   !This is the B Matrix (shape functions) 
        Allocate (FElement (IBRangeG))
        Allocate (C (3, 3))
        C = 0.0
        
        Allocate (ForceMatrixG (NGTotalDOF))
        ForceMatrixG = 0
        
        IViscoElastic = 0
	    Ielastic = 0
        iPlastic = 0

    End Subroutine InitializeGlobalForceMatrix2D

!**************************************************************************************    
    
    Subroutine InitializeGlobalStress2D

        Allocate (BG (3,IBRangeG))   !This is the B Matrix (shape functions) 
    
        IViscoElastic = 0
	    Ielastic = 0
        iPlastic = 0

        !   Initialize the element strains
       ! Allocate (DeltaStrainG (NumberOfGlobalElements, iRange, NGIntegrationPoints))

      !  DeltaStrainG = 0.0D0
        
        Allocate (DeltaStressG (iRange))
        Allocate (DeltaStressGOld (iRange))

        DeltaStressG = 0.0D0

        
        Allocate (C (3, 3))
        C = 0.0
        
    End Subroutine InitializeGlobalStress2D
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    

    
End Module InitializeGlobalVariables
