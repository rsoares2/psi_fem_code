Module TetrahedronShapeFunction

    Use GlobalInputVariables

	implicit Real*8 (A-H,O-Z)

	Contains

	Subroutine ShapeTetrahedronGlobal (iel)
	
    !
    !   This subroutine calculates the shape functions for 4-noded linear tetrahedrons
    ! 

    REAL*8 BC(12,6),DXDS(3,3),DPSI(4,3),DSDX(3,3),DPSIX(4),DPSIY(4),DPSIZ(4),&
    PSI(4),SEIGEN(6),FEL(12), C0(3),C1(3),C2(3),C3(3),XGauss(3)
      
    XI = 0.2500000000  !Quadrature of 1 point over a tetrahedron
    ETA = 0.2500000000
    ZETA = 0.2500000000
    Weight = 0.16666666666
      
!   Transform coordinates. 
!   Convert:        X=x1*N1+x2*N2+x3*N3+x4*N4
!   To:             X=x1*(1-Ksi-Eta-Si)+x2*Ksi+x3*Eta+x4*Si

!   Calculate first X1, X21, X31, X41 and then Y1, Y21... and Z1, Z21, Z31, Z41.

	    C0(1)=XG(NODEG(IEL,1)) 
	    C1(1)=XG(NODEG(IEL,2))-XG(NODEG(IEL,1))
	    C2(1)=XG(NODEG(IEL,3))-XG(NODEG(IEL,1))
	    C3(1)=XG(NODEG(IEL,4))-XG(NODEG(IEL,1))

	    C0(2)=YG(NODEG(IEL,1)) 
	    C1(2)=YG(NODEG(IEL,2))-YG(NODEG(IEL,1))
	    C2(2)=YG(NODEG(IEL,3))-YG(NODEG(IEL,1))
	    C3(2)=YG(NODEG(IEL,4))-YG(NODEG(IEL,1))

	    C0(3)=ZG(NODEG(IEL,1)) 
	    C1(3)=ZG(NODEG(IEL,2))-ZG(NODEG(IEL,1))
	    C2(3)=ZG(NODEG(IEL,3))-ZG(NODEG(IEL,1))
	    C3(3)=ZG(NODEG(IEL,4))-ZG(NODEG(IEL,1))

        DXDS=0.0

!   Set up Jacobian elements. Derivatives w.r.t XI, ETA, ZETA
! X = X1 + (X2 - X1)*XI + (X3 - X1)*ETA + (X4 - X1)*ZETA

!   XGauss is the coordinate of Gauss in the element

        
        Do i = 1,3
            XGauss(I) = C0(I) + C1(I)* XI + C2(I)* ETA + C3(I)* ZETA
        
            DXDS(1,I) = C1(i)   !DXDS(:,1) is DXDXI, DYDXI, DZDXI
            DXDS(2,I) = C2(i)   !DXDS(:,2) is DXDETA, DYDETA, DZDETA
            DXDS(3,I) = C3(i)   !DXDS(:,3) is DXDZETA, DYDZETA, DZDZETA
        Enddo
    
!
! CALCULATE COFACTORS AND DETJ
!

      A11=  DXDS(2,2)*DXDS(3,3)-DXDS(3,2)*DXDS(2,3)
      A12=-(DXDS(2,1)*DXDS(3,3)-DXDS(3,1)*DXDS(2,3))
      A13=  DXDS(2,1)*DXDS(3,2)-DXDS(3,1)*DXDS(2,2)
      A21=-(DXDS(1,2)*DXDS(3,3)-DXDS(3,2)*DXDS(1,3))
      A22=  DXDS(1,1)*DXDS(3,3)-DXDS(3,1)*DXDS(1,3)
      A23=-(DXDS(1,1)*DXDS(3,2)-DXDS(3,1)*DXDS(1,2))
      A31=  DXDS(1,2)*DXDS(2,3)-DXDS(2,2)*DXDS(1,3)
      A32=-(DXDS(1,1)*DXDS(2,3)-DXDS(2,1)*DXDS(1,3))
      A33=  DXDS(1,1)*DXDS(2,2)-DXDS(2,1)*DXDS(1,2)
      DETJ=DXDS(1,1)*A11+DXDS(1,2)*A12+DXDS(1,3)*A13
     ! Write(993,*)iel,detj,a11,a12,a13,a21,a22,a23,a31,a32,a33

    IF (DABS(DETJ).LE.0.0D-20) THEN
        Write(6,4321) IEL,IINTPT,DETJ
 4321   FORMAT(5X,'IN GLOB3 - IEL = ',I5,5X,'IINTPT = ',I5,5X,&
        /,5X,'DET J = ',D15.7,5X,'STOP!',/)
        Write(6,4322) XIB(1,IINTPT),XIB(2,IINTPT),XIB(3,IINTPT)
 4322   FORMAT(5X,'XIB = ',3(1X,D14.6))
        Write(6,4323) A11,A12,A13
 4323   FORMAT(5X,'A = ',3(1X,D14.6))
        Write(6,4324) DXDS(1,1),DXDS(1,2),DXDS(1,3)
 4324   FORMAT(5X,'DXDS = ',3(1X,D14.6))
        STOP
    ENDIF
    
!
! CALCULATE DSDX
!
      DSDX(1,1)=A11/DETJ    !DXIDX
      DSDX(1,2)=A12/DETJ    !DXIDY
      DSDX(1,3)=A13/DETJ    !DXIDZ
      DSDX(2,1)=A21/DETJ    !DETADX
      DSDX(2,2)=A22/DETJ    !DETADY
      DSDX(2,3)=A23/DETJ    !DETADZ
      DSDX(3,1)=A31/DETJ    !DZETADX
      DSDX(3,2)=A32/DETJ    !DZETADY
      DSDX(3,3)=A33/DETJ    !DZETADZ
      
   ! Call DGETRF(3, 3, DXDS, 3, ipiv, info)      
    
    !Call DGETRI(3, DXDS, 3, ipiv, work, 3, info)
    
!
! CALCULATE D(PSI)/DX
!
      DPSIX(1)=-DSDX(1,1)-DSDX(2,1)-DSDX(3,1)
      DPSIX(2)=DSDX(1,1)
      DPSIX(3)=DSDX(2,1)
      DPSIX(4)=DSDX(3,1)

      DPSIY(1)=-DSDX(1,2)-DSDX(2,2)-DSDX(3,2)
      DPSIY(2)=DSDX(1,2)
      DPSIY(3)=DSDX(2,2)
      DPSIY(4)=DSDX(3,2)

      DPSIZ(1)=-DSDX(1,3)-DSDX(2,3)-DSDX(3,3)
      DPSIZ(2)=DSDX(1,3)
      DPSIZ(3)=DSDX(2,3)
      DPSIZ(4)=DSDX(3,3)
      
!
! INITIALIZE B MATRIX
!
      BG=0.
!
! FORM B AND BT (Sequence is XX, YY, ZZ, YZ, XZ, XY)
!

    DO i = 1, NumberOfGlobalNodesPerElement
        I3M2=3*I-2
        I3M1=3*I-1
        I3=3*I
        BG(1,I3M2)=BG(1,I3M2)+DPSIX(I)
        BG(2,I3M1)=BG(2,I3M1)+DPSIY(I)
        BG(3,I3)=BG(3,I3)+DPSIZ(I)
        BG(4,I3M1)=BG(4,I3M1)+DPSIZ(I)
        BG(4,I3)=BG(4,I3)+DPSIY(I)
        BG(5,I3M2)=BG(5,I3M2)+DPSIZ(I)
        BG(5,I3)=BG(5,I3)+DPSIX(I)
        BG(6,I3M2)=BG(6,I3M2)+DPSIY(I)
        BG(6,I3M1)=BG(6,I3M1)+DPSIX(I)
    Enddo
      
!
! CALCULATE PHYSICAL LOCATION OF INTEGRATION POINT
!

      X1=0.
      X2=0.
      X3=0.1
    Do k = 1, NumberOfGlobalNodesPerElement
        X1 = X1 + XG (NodeG(IEL,K)) * PSI(K)   !is this correct? It should be XG(as a function of global coord and not only from 1,8)
        X2 = X2 + YG (NodeG(IEL,K)) * PSI(K)
        X3 = X3 + ZG (NodeG(IEL,K)) * PSI(K)
    Enddo
    
       !Write(445,122)INCR,IEL,Time,X1,X2,X3
       !122 Format(2i7,4e15.7)
       
    End Subroutine ShapeTetrahedronGlobal

      Subroutine TetrahedronCentroid(i,a1,a2,a3)

        Real*8:: a1,a2,a3
        integer:: i
        
        
        a1 = (XG(NODEG(I,1))+XG(NODEG(I,2))+XG(NODEG(I,3))+XG(NODEG(I,4)))/4
        a2 = (YG(NODEG(I,1))+YG(NODEG(I,2))+YG(NODEG(I,3))+YG(NODEG(I,4)))/4
        a3 = (ZG(NODEG(I,1))+ZG(NODEG(I,2))+ZG(NODEG(I,3))+ZG(NODEG(I,4)))/4


      End Subroutine TetrahedronCentroid

    End Module TetrahedronShapeFunction