Module CohesiveZoneForce2D

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use AssembleStiffnessMatrix
    Use AllenCZForce
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine InterfaceForce2D
    
    Integer::NICON(2)
    REAL*8 RKINT(4,4)
    
    !
    !   This subroutine calculates the force matrix components for 2D cohesive zones (interface elements)
    !    
    
    Eta = 1.    !necessary?
    
    Do i = 1, NGlobalInterfElementsG 

        I1 = NINT1G(I)
        I2 = NINT2G(I)

        CALL InterfaceForce2DG(I,I1,I2)
		
        NICON(1) = I1 
        NICON(2) = I2
		
        ! CALL AssembleInterfaceGlobalSparse(RKINT,2,NICON,2)
    
    Enddo
        
    End Subroutine InterfaceForce2D

!*********************************************************************************************    
    
    Subroutine InterfaceForce2DG (ii,I1,I2)
    
    REAL*8 RKINT(4,4)
    !
    !   This subroutine calculates the normal and tangential force components (FN and FT) for interface elements 
    !

	Phi = PHIAVG(II) 
	W = WIDTHG(II)

    ! 
    !   Calculate interface displacements UN1,UN2,UT1,UT2
    ! 

	N1X = 2*I1-1 
	N1Y = N1X+1 
	N2X = 2*I2-1 
	N2Y = N2X+1 
	UX = DisplacementG(N2X)-DisplacementG(N1X)    !should it be DeltaDisplacement?
	UY = DisplacementG(N2Y)-DisplacementG(N1Y) 
	UN = UX*DCOS(PHI)+UY*DSIN(PHI) 
	UT = -UX*DSIN(PHI)+UY*DCOS(PHI)
	
	!If (iGlobalCohesiveZones==1) Call NeedlemanModelG
	!If (iGlobalCohesiveZones==2) Call TvergaardModelG
	!If (iGlobalCohesiveZones==3) Call ModifiedTvergaardModelG
	 If (iGlobalCohesiveZones == 4) Call AllenCZModelForceG (ii, W, Phi, UN, UT, N1X, N1Y, N2X, N2Y)
     
    End Subroutine InterfaceForce2DG

!*********************************************************************************************    
    
	Subroutine InterfaceForce2DLocal
    
    Integer::NICON(2)
    REAL*8 RKINT(4,4)
    
    !
    !   This subroutine calculates the force matrix components for 2D cohesive zones (interface elements)
    !    
    
    Eta = 1.    !necessary?
    
    Do i = 1, NLocalInterfElementsL

        I1 = NINT1L(I)
        I2 = NINT2L(I)

        CALL InterfaceForce2DL(I,I1,I2)
		
        NICON(1) = I1 
        NICON(2) = I2
		
        ! CALL AssembleInterfaceGlobalSparse(RKINT,2,NICON,2)
    
    Enddo
        
    End Subroutine InterfaceForce2DLocal

!*********************************************************************************************    

    Subroutine InterfaceForce2DL (ii,I1,I2) !does rkint should be included in the arguments?
    
    Real*8 RKINT(4,4)
    
    !
    !   This subroutine calculates the stiffness matrix for interface elements
    !

	
	!If (IDYNAMIC == 1) Then
!		QG => REALLOCATE (QG,NGTOT) 

     !   Do I=NGNOLD+1,NGTOT
      !      DisplacementG(I)=0.
       ! Enddo
    !Endif

	Phi = PHIAVL(II) 
	W = WIDTHL(II)

! 
! CALCULATE INTERFACE DISPLACEMENTS UN1,UN2,UT1,UT2 
! 

	N1X=2*I1-1 
	N1Y=N1X+1 
	N2X=2*I2-1 
	N2Y=N2X+1 
	UX=DisplacementL(N2X)-DisplacementL(N1X)    !should it be DeltaDisplacement?
	UY=DisplacementL(N2Y)-DisplacementL(N1Y)
	UN=UX*DCOS(PHI)+UY*DSIN(PHI)
	UT=-UX*DSIN(PHI)+UY*DCOS(PHI)
	
	!If (iLocalCohesiveZones==1) Call NeedlemanModelG
	!If (iLocalCohesiveZones==2) Call TvergaardModelG
	!If (iLocalCohesiveZones==3) Call ModifiedTvergaardModelG
	 If (iLocalCohesiveZones == 4) Call AllenCZModelForceL(ii, W, Phi, UN, UT, N1X, N1Y, N2X, N2Y)
     
    End Subroutine InterfaceForce2DL

End Module CohesiveZoneForce2D