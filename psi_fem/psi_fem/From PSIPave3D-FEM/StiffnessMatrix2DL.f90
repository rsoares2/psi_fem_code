Module CalculateStifnessMatrix2DL

    Use LocalInputVariables
    Use InitializeLocalVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialTypeLocal
    Use AssembleStiffnessMatrix
    Use CohesiveZoneStiffness2D

    Use Functions    
    Use OMP_LIB
    
	implicit none

	Contains

	Subroutine CalculateLocalStiffnessMatrixSparse2D
    
    !
    !   This subroutine calculates the Local sparse stiffness matrix for 2D meshes
    !    
    
	Integer::NICON(2), iElastic, iViscoElastic
    Real*8:: C(3,3), G_EleMatrix(6,3), FR_Element(6)
    Integer:: i, jel, IintPoint
    
    Do jel = 1, NumberOfLocalElements  !Major loop over all elements
    
        If (mod(jel, iPrintToScreenL) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) jel
 805        Format (10x,'Set up Local Stiffness Matrix for Element =',I8)
            
        Endif        

!   Initialize the element stiffness matrix

        KElementL = 0.0D0
        
!
!   Calculate element stiffness matrix
!

        Call SetUpQuadraturePoints (NLIntegrationPoints, NumberOfLocalNodesPerElement)   ! Set up quadrature points and weights

        Do iIntPoint = 1, NLIntegrationPoints
        
            Call ShapeFunction2D (jel, iIntPoint, NumberOfLocalNodesPerElement, 1)  !Set up B matrix
            
            Call ReadLocalMaterialType2D (jel, iIntPoint, 1, C)  !Add material properties (1, means Called from stifnness matrix)
            
            Call CalculateElementStiffnessMatrix2DL (C, G_EleMatrix, FR_Element) !It calculates both G (BT*C) and K (Stiffness matrix = BT*C*B)
            
        Enddo
    
        Call AssembleLocalStiffnessSparse2D (jel) !Assembles each local element into a Local stiffness matrix
 
        Call AssembleLocalFRMatrix2D (jel, FR_Element) !Assembles each local element Fr matrix into a global one. No CZ until force matrix
        
        DSIGRL = 0.D0 !Necessary if viscoelasticity is used (MAY NOT BE NECESSARY!) DSIGR is not used in stiff. matrix

    Enddo
    
    Call PostCalculationStiffness2DL
    
    If (iLocalCohesiveZones /=0) Call InterfaceStiffness2DL
    
    !Call ApplyBCStiffness2D
    
    End Subroutine CalculateLocalStiffnessMatrixSparse2D

!*********************************************************************************************  
    
    Subroutine CalculateElementStiffnessMatrix2DL (C, G_EleMatrix, FR_Element)

    Real*8:: C(3,3),  G_EleMatrix(IBRangeL, IBRangeL/2) !this subroutine should also be combined into one general function
    Real*8:: FR_Element(IBRangeL)
    !   
    !   This subroutine calculates the element stiffness matrix 
    !   
    !   It first multiplies B transpose matrix (deriv. of shape function) * C matrix (Stiffness/Modulus) * B matrix
    !   and numeriCally integrates. Using DGEMM from MKL to multiply matrix.
            
            Call DGEMM( 'T'  , 'N'  , IBRangeL, 3 , 3 , 1.0D0 , BL , 3 , DSIGRL , 3 , 0.0D0 , FR_Element , IBRangeL ) !MKL matrix multiplication function

            FR_Element = FR_Element * Area * Weight   !calculate frglobal contribution due to dsigrl

            Call DGEMM( 'T'  , 'N'  , IBRangeL, 3 , 3 , 1.0D0 , BL , 3 , C , 3 , 0.0D0 , KElement_TempL , IBRangeL ) !MKL matrix multiplication function

            G_EleMatrix = KElement_TempL * Area * Weight
            
            Call DGEMM( 'N'  , 'N'  , 6 , 6 , 3 , 1.0D0 , KElement_TempL , 6 , BL , 3 , 0.0D0 , KElementL , 6 ) 
            
            KElementL = KElementL * Area * Weight  !Uses output from DGEMM function and integrates
   
    End Subroutine CalculateElementStiffnessMatrix2DL

!*********************************************************************************************        
   
End Module CalculateStifnessMatrix2DL