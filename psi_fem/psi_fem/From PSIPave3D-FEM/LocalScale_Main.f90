Module LocalScale_Main
    
    Use GlobalInputVariables
    Use LocalInputVariables
    Use UpdateCZs

    Use CalculateStifnessMatrix
    Use CalculateForceMatrix

    implicit none
    
    Contains   
    
    Subroutine SetUpLocalScale (iel)
    
    Integer :: iel
    
    ! 
    !   This subroutine (old Sublocal) sets up the local scale meshes. It is also where there are calls to calculate
    !   Start date: Nov. 08, 2013
    !
    !   1. Local Stiffness matrix
    !   2. Local "Force" including [ G ] and [ FR ] matrices
    !   3. CZ contributions to both stiffness and [ FR ] matrices
    !   4. Calculate [ Localization tensor ]= - [ K ]^(-1)* [ G ]
    !   5. Calculate [ DeltaDisplacementR ] = [ K ] * [ FR ]
    !   6. Calculate [ DeltaStrainR ] = [ B ] * [ DeltaDisplacementR ]
    !   7. Calculate updated global modulus matrix [ C ]
    !   8. Calculate updated global Delta Sigma R
    !   9. Calculate (update) local displacement, strains and stresses.
    ! 

    If (iLocalCohesiveZones /= 0) Call UpdateCZLocal
            
    Call LocalStiffnessMatrix
            
    Call LocalForceMatrix
            
   ! Call PardisoSolver
            
   ! Call CalculateStressandStrain
            
   ! If (iLocalCohesiveZones == 4) Call InterfaceTraction2D
            
   ! Call LocalOutput

    End Subroutine SetUpLocalScale

End Module LocalScale_Main