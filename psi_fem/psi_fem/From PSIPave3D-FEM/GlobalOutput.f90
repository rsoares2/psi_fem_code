Module Output

    Use GlobalInputVariables
    Use InitializeGlobalVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleForceMatrix
    Use CohesiveZoneForce2D
    Use OutputModule
    Use CalculateStressandStrain3D
    Use CalculateStressandStrain2D

    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine GlobalOutput
    
    !
    !   This subroutine prints outputs for the global scale
    !    
    
    If (NumberOfDimensions == 2 ) Then
        
        Call Output2DG
        Call PostCalculationStress2D
                    
    ElseIf (NumberOfDimensions == 3 ) Then 
        
        Call Output3DG
        Call PostCalculationStress3D

    Endif    
    
    If(Allocated(ModulusI1J2)) Deallocate (ModulusI1J2)

    End Subroutine GlobalOutput
    
End Module Output