Module InputViscoElasticity
    
    Use GlobalInputVariables
    
    implicit Real (A-H, O-Z)
    
    Contains    

    Subroutine ReadGlobalInputViscoElastic
    
    ! 
    !   Read in material properties for viscoelastic Elements
    ! 
    
    iVisG = 1 !Flag to indicate there is a viscoelastic material
    
    Allocate ( ViscoElasticPosition (NumberOfTotalMaterials) )
    ViscoElasticPosition = 0
    
    If (iBodyForce == 1) Then
        Allocate ( Density (NumberOfTotalMaterials) )
        Read(5,*) Density (NumberOfTotalMaterials)
       
    Endif
        
    If ( NumberOfDimensions == 2 ) Then
        
        Call Read2DInputViscoelasticity
        
    ElseIf ( NumberOfDimensions == 3 ) Then

        Call Read3DInputViscoelasticity

    Else
    
        Write (6,1007) 
1007    Format ('The number of dimensions must be 2 for 2D or 3 for 3D')
        Stop
        
    Endif


    End Subroutine ReadGlobalInputViscoElastic
    
    Subroutine Read2DInputViscoelasticity

    !  
    !   Read material properties for orthotropic linear viscoelasticity in 2D
    !   
    !   The traditional E(t) = Einf + Sum(Ei*exp(-Ei/Etai*DeltaTime) is converted to the C matrix.
    !
    !   NumberOfTerms??G is the number of terms in the Prony series
    
    Write(6,1024) MaterialNumber5G
		
    Allocate ( NumberOfTerms11G (MaterialNumber5G) )
	Allocate ( NumberOfTerms12G (MaterialNumber5G) )
	Allocate ( NumberOfTerms22G (MaterialNumber5G) )
	Allocate ( NumberOfTerms23G (MaterialNumber5G) )
	Allocate ( NumberOfTerms44G (MaterialNumber5G) )
	Allocate ( NumberOfTerms66G (MaterialNumber5G) )
    
	Allocate ( CInf11G (MaterialNumber5G) )
	Allocate ( CInf12G (MaterialNumber5G) )
	Allocate ( CInf22G (MaterialNumber5G) )
	Allocate ( CInf23G (MaterialNumber5G) )
	Allocate ( CInf44G (MaterialNumber5G) )
    Allocate ( CInf66G (MaterialNumber5G) )
    
	Do i=1, MaterialNumber5G

		Read(5,*) MSetiG,NumberOfTerms11G(I),NumberOfTerms12G(I),NumberOfTerms22G(I),NumberOfTerms23G(I),NumberOfTerms44G(I),NumberOfTerms66G(I)
		Write(6,1025) MsetiG
		Write(6,1026) NumberOfTerms11G(I),NumberOfTerms12G(I),NumberOfTerms22G(I),NumberOfTerms23G(I),NumberOfTerms44G(I),NumberOfTerms66G(I)
 		
		Read(5,*) CInf11G(I),CInf12G(I),CInf22G(I),CInf23G(I),CInf44G(I),CInf66G(I)
		Write(6,1028)  CInf11G(I),CInf12G(I),CInf22G(I),CInf23G(I),CInf44G(I),CInf66G(I)
		
            If(.Not.Allocated (CL11G)) Allocate ( CL11G (MaterialNumber5G,NumberOfTerms11G(I)) )
		    If(.Not.Allocated (CL12G)) Allocate ( CL12G (MaterialNumber5G,NumberOfTerms12G(I)) )
		    If(.Not.Allocated (CL22G)) Allocate ( CL22G (MaterialNumber5G,NumberOfTerms22G(I)) )
		    If(.Not.Allocated (CL23G)) Allocate ( CL23G (MaterialNumber5G,NumberOfTerms23G(I)) )
		    If(.Not.Allocated (CL44G)) Allocate ( CL44G (MaterialNumber5G,NumberOfTerms44G(I)) )
		    If(.Not.Allocated (CL66G)) Allocate ( CL66G (MaterialNumber5G,NumberOfTerms66G(I)) )

		    If(.Not.Allocated (Eta11G)) Allocate ( Eta11G (MaterialNumber5G,NumberOfTerms11G(I)) )
		    If(.Not.Allocated (Eta12G)) Allocate ( Eta12G (MaterialNumber5G,NumberOfTerms12G(I)) )
		    If(.Not.Allocated (Eta22G)) Allocate ( Eta22G (MaterialNumber5G,NumberOfTerms22G(I)) )
		    If(.Not.Allocated (Eta23G)) Allocate ( Eta23G (MaterialNumber5G,NumberOfTerms23G(I)) )
		    If(.Not.Allocated (Eta44G)) Allocate ( Eta44G (MaterialNumber5G,NumberOfTerms44G(I)) )
		    If(.Not.Allocated (Eta66G)) Allocate ( Eta66G (MaterialNumber5G,NumberOfTerms66G(I)) )
!
! Read in Elements of CL11
!
		Read(5,*) (CL11G(i, j), J=1, NumberOfTerms11G(I))
		Read(5,*) (Eta11G(i, j), J=1, NumberOfTerms11G(I))
		Write(6,1030)
		Do j = 1, NumberOfTerms11G(I)
			Write(6,1029) J, CL11G(i, j), Eta11G(i, j)
        Enddo
!
! Read in Elements of CL12
!
		Read(5,*) (CL12G(i, j), J = 1, NumberOfTerms12G(I))
		Read(5,*) (Eta12G(i, j), J = 1, NumberOfTerms12G(I))
		Write(6,1031)
		Do j = 1, NumberOfTerms12G(I)
			Write(6,1029) J, CL12G(i, j), Eta12G(i, j)
        Enddo

        !
! Read in Elements of CL22
!
		Read(5,*) (CL22G(i, j), J = 1, NumberOfTerms22G(I))
		Read(5,*) (Eta22G(i, j), J = 1, NumberOfTerms22G(I))
		Write(6,1032)
		Do j = 1, NumberOfTerms22G(I)
			Write(6,1029) J, CL22G(i, j), Eta22G(i, j)
        Enddo
!
! Read in Elements of CL23
!
		Read(5,*) (CL23G(i, j), J = 1, NumberOfTerms23G(I))
		Read(5,*) (Eta23G(i, j), J = 1, NumberOfTerms23G(I))
		Write(6,1033)
		Do j = 1, NumberOfTerms23G(I)
			Write(6,1029) J, CL23G(i, j), Eta23G(i, j)
        Enddo
        
!
! Read in Elements of CL44
!
		Read(5,*) (CL44G(i, j),J = 1, NumberOfTerms44G(I))
		Read(5,*) (Eta44G(i, j),J = 1, NumberOfTerms44G(I))
		Write(6,1034)
		Do j = 1,NumberOfTerms44G(I)
			Write(6,1029) J, CL44G(i, j), Eta44G(i, j)
        Enddo   
    
!
! Read in Elements of CL66
!
		Read(5,*) (CL66G(i, j),J=1,NumberOfTerms66G(I))
		Read(5,*) (Eta66G(i, j),J=1,NumberOfTerms66G(I))
		Write(6,1035)
		Do j = 1, NumberOfTerms66G(I)
			Write(6,1029) J,CL66G(i, j),Eta66G(i, j)
        Enddo
 
    Enddo
    
    !
    !   Formats used in viscoelasticity
    !

 1024 Format(/,10X,'There is ',i10,' orthotropic linear viscoelastic material set',/)
 1025	Format(/,10X,'Viscoelastic Model Number = ',I5,/)
 1026	Format(10X,'Number of Prony Terms in CL11 = ',I5,/,&
		10X,'Number of Prony Terms in CL12 = ',I5,/,&
		10X,'Number of Prony Terms in CL22 = ',I5,/,&
		10X,'Number of Prony Terms in CL23 = ',I5,/,&
	    10X,'Number of Prony Terms in CL44 = ',I5,/,&
		10X,'Number of Prony Terms in CL66 = ',I5,/)
 		
 1028	Format(/,10X,'CInf11 = ',D15.7,/,&
		10X,'CInf12 = ',D15.7,/,&
		10X,'CInf22 = ',D15.7,/,&
		10X,'CInf23 = ',D15.7,/,&
		10X,'CInf44 = ',D15.7,/,&
		10X,'CInf66 = ',D15.7,/)
		
        
     1029   Format(15X,I5,5X,D15.7,5X,D15.7)
            
    !
    !   Read in Elements OF CL11, 12, 22, 23, 44, 66
    !
        
     1030	Format(/,10X,'Prony Terms for CL11',//,19X,'I',13X,'CL11G(I)',12X,'Eta11G(I)',/)
     1031	Format(/,10X,'Prony Terms for CL12',//,19X,'I',13X,'CL12G(I)',12X,'Eta12G(I)',/)
     1032	Format(/,10X,'Prony Terms for CL22',//,19X,'I',13X,'CL22G(I)',12X,'Eta22G(I)',/)
     1033	Format(/,10X,'Prony Terms for CL23',//,19X,'I',13X,'CL23G(I)',12X,'Eta23G(I)',/)
     1034	Format(/,10X,'Prony Terms for CL44',//,19X,'I',13X,'CL44G(I)',12X,'Eta44G(I)',/)
     1035	Format(/,10X,'Prony Terms for CL66',//,19X,'I',13X,'CL66G(I)',12X,'Eta66G(I)',/)
 
    Do i = 1, MaterialNumber5G
        
        Max = Max0(NumberOfTerms11G(I),NumberOfTerms12G(I),NumberOfTerms22G(I),NumberOfTerms23G(I),NumberOfTerms44G(I),NumberOfTerms66G(I))
	    If (MAX > NGPronyG) Then
            NGPronyG = MAX
        EndIf
    	
    Enddo

455 CONTINUE
    
    End Subroutine Read2DInputViscoelasticity

    Subroutine Read3DInputViscoelasticity

    !  
    !   Read material properties for orthotropic linear viscoelasticity in 3D
    !   
    !   The traditional E(t) = Einf + Sum(Ei*exp(-Ei/Etai*DeltaTime) is converted to the C matrix.
    !
    !   NumberOfTerms??G is the number of terms in the Prony series
    
    Write(6,1024) MaterialNumber5G

    Allocate ( NumberOfTerms11G (MaterialNumber5G) )
	Allocate ( NumberOfTerms12G (MaterialNumber5G) )
	Allocate ( NumberOfTerms13G (MaterialNumber5G) )
	Allocate ( NumberOfTerms22G (MaterialNumber5G) )
	Allocate ( NumberOfTerms23G (MaterialNumber5G) )
	Allocate ( NumberOfTerms33G (MaterialNumber5G) )
	Allocate ( NumberOfTerms44G (MaterialNumber5G) )
	Allocate ( NumberOfTerms55G (MaterialNumber5G) )
	Allocate ( NumberOfTerms66G (MaterialNumber5G) )

	Allocate ( CInf11G (MaterialNumber5G) )
	Allocate ( CInf12G (MaterialNumber5G) )
	Allocate ( CInf13G (MaterialNumber5G) )
	Allocate ( CInf22G (MaterialNumber5G) )
	Allocate ( CInf23G (MaterialNumber5G) )
	Allocate ( CInf33G (MaterialNumber5G) )
	Allocate ( CInf44G (MaterialNumber5G) )
	Allocate ( CInf55G (MaterialNumber5G) )
    Allocate ( CInf66G (MaterialNumber5G) )
    

	Do i=1, MaterialNumber5G

		Read(5,*) MSetiG,NumberOfTerms11G(I),NumberOfTerms12G(I),NumberOfTerms13G(I),NumberOfTerms22G(I),NumberOfTerms23G(I),NumberOfTerms33G(I),&
        NumberOfTerms44G(I),NumberOfTerms55G(I),NumberOfTerms66G(I)
		Write(6,1025) MsetiG
		Write(6,1025)  MSetiG,NumberOfTerms11G(I),NumberOfTerms12G(I),NumberOfTerms13G(I),NumberOfTerms22G(I),NumberOfTerms23G(I),NumberOfTerms33G(I),&
        NumberOfTerms44G(I),NumberOfTerms55G(I),NumberOfTerms66G(I)
 		
		Read(5,*) CInf11G(I),CInf12G(I),CInf13G(I),CInf22G(I),CInf23G(I),CInf33G(I),CInf44G(I),CInf55G(I),CInf66G(I)
		Write(6,1028)  CInf11G(I),CInf12G(I),CInf13G(I),CInf22G(I),CInf23G(I),CInf33G(I),CInf44G(I),CInf55G(I),CInf66G(I)
		
        If(.Not.Allocated (CL11G)) Allocate ( CL11G (MaterialNumber5G,NumberOfTerms11G(I)) )
        If(.Not.Allocated (CL12G)) Allocate ( CL12G (MaterialNumber5G,NumberOfTerms12G(I)) )
        If(.Not.Allocated (CL22G)) Allocate ( CL22G (MaterialNumber5G,NumberOfTerms22G(I)) )
        If(.Not.Allocated (CL23G)) Allocate ( CL23G (MaterialNumber5G,NumberOfTerms23G(I)) )
        If(.Not.Allocated (CL44G)) Allocate ( CL44G (MaterialNumber5G,NumberOfTerms44G(I)) )
        If(.Not.Allocated (CL66G)) Allocate ( CL66G (MaterialNumber5G,NumberOfTerms66G(I)) )
        If(.Not.Allocated (CL13G)) Allocate ( CL13G (MaterialNumber5G,NumberOfTerms13G(I)) )
        If(.Not.Allocated (CL33G)) Allocate ( CL33G (MaterialNumber5G,NumberOfTerms33G(I)) )
        If(.Not.Allocated (CL55G)) Allocate ( CL55G (MaterialNumber5G,NumberOfTerms55G(I)) )

        If(.Not.Allocated (Eta11G)) Allocate ( Eta11G (MaterialNumber5G,NumberOfTerms11G(I)) )
        If(.Not.Allocated (Eta12G)) Allocate ( Eta12G (MaterialNumber5G,NumberOfTerms12G(I)) )
        If(.Not.Allocated (Eta22G)) Allocate ( Eta22G (MaterialNumber5G,NumberOfTerms22G(I)) )
        If(.Not.Allocated (Eta23G)) Allocate ( Eta23G (MaterialNumber5G,NumberOfTerms23G(I)) )
        If(.Not.Allocated (Eta44G)) Allocate ( Eta44G (MaterialNumber5G,NumberOfTerms44G(I)) )
        If(.Not.Allocated (Eta66G)) Allocate ( Eta66G (MaterialNumber5G,NumberOfTerms66G(I)) )
        If(.Not.Allocated (Eta13G)) Allocate ( Eta13G (MaterialNumber5G,NumberOfTerms13G(I)) )
        If(.Not.Allocated (Eta33G)) Allocate ( Eta33G (MaterialNumber5G,NumberOfTerms33G(I)) )
        If(.Not.Allocated (Eta55G)) Allocate ( Eta55G (MaterialNumber5G,NumberOfTerms55G(I)) )

!
! Read in Elements of CL11
!
		Read(5,*) (CL11G(i, j), J=1, NumberOfTerms11G(I))
		Read(5,*) (Eta11G(i, j), J=1,NumberOfTerms11G(I))
		Write(6,1030)
		Do j = 1, NumberOfTerms11G(I)
			Write(6,1029) J, CL11G(i, j), Eta11G(i, j)
        Enddo
!
! Read in Elements of CL12
!
		Read(5,*) (CL12G(i, j), J = 1, NumberOfTerms12G(I))
		Read(5,*) (Eta12G(i, j), J = 1, NumberOfTerms12G(I))
		Write(6,1031)
		Do j = 1, NumberOfTerms12G(I)
			Write(6,1029) J, CL12G(i, j), Eta12G(i, j)
        Enddo
!
! Read in Elements of CL13
!
		Read(5,*) (CL13G(i, j), J=1, NumberOfTerms13G(I))
		Read(5,*) (Eta13G(i, j), J=1,NumberOfTerms13G(I))
		Write(6,1032)
		Do j = 1, NumberOfTerms13G(I)
			Write(6,1029) J, CL13G(i, j), Eta13G(i, j)
        Enddo

!
! Read in Elements of CL22
!
		Read(5,*) (CL22G(i, j), J = 1, NumberOfTerms22G(I))
		Read(5,*) (Eta22G(i, j), J = 1, NumberOfTerms22G(I))
		Write(6,1033)
		Do j = 1, NumberOfTerms22G(I)
			Write(6,1029) J, CL22G(i, j), Eta22G(i, j)
        Enddo
!
! Read in Elements of CL23
!
		Read(5,*) (CL23G(i, j), J = 1, NumberOfTerms23G(I))
		Read(5,*) (Eta23G(i, j), J = 1, NumberOfTerms23G(I))
		Write(6,1034)
		Do j = 1, NumberOfTerms23G(I)
			Write(6,1029) J, CL23G(i, j), Eta23G(i, j)
        Enddo
!
! Read in Elements of CL33
!
		Read(5,*) (CL33G(i, j), J=1, NumberOfTerms33G(I))
		Read(5,*) (Eta33G(i, j), J=1,NumberOfTerms33G(I))
		Write(6,1035)
		Do j = 1, NumberOfTerms33G(I)
			Write(6,1029) J, CL33G(i, j), Eta33G(i, j)
        Enddo
        
!
! Read in Elements of CL44
!
		Read(5,*) (CL44G(i, j),J = 1, NumberOfTerms44G(I))
		Read(5,*) (Eta44G(i, j),J = 1, NumberOfTerms44G(I))
		Write(6,1036)
		Do j = 1,NumberOfTerms44G(I)
			Write(6,1029) J, CL44G(i, j), Eta44G(i, j)
        Enddo   
!
! Read in Elements of CL55
!
		Read(5,*) (CL55G(i, j), J=1, NumberOfTerms55G(I))
		Read(5,*) (Eta55G(i, j), J=1,NumberOfTerms55G(I))
		Write(6,1037)
		Do j = 1, NumberOfTerms55G(I)
			Write(6,1029) J, CL55G(i, j), Eta55G(i, j)
        Enddo
    
!
! Read in Elements of CL66
!
		Read(5,*) (CL66G(i, j),J=1,NumberOfTerms66G(I))
		Read(5,*) (Eta66G(i, j),J=1,NumberOfTerms66G(I))
		Write(6,1038)
		Do j = 1, NumberOfTerms66G(I)
			Write(6,1029) J,CL66G(i, j),Eta66G(i, j)
        Enddo
 
    Enddo
    
    !
    !   Formats used in viscoelasticity
    !
 1024   Format(/,10X,'There is ',i10,' orthotropic linear viscoelastic material set',/)
        
 1025	Format(/,10X,'Viscoelastic Model Number = ',I5,/)
 1026	Format(10X,'Number of Prony Terms in CL11 = ',I5,/,&
		10X,'Number of Prony Terms in CL12 = ',I5,/,&
		10X,'Number of Prony Terms in CL13 = ',I5,/,&
		10X,'Number of Prony Terms in CL22 = ',I5,/,&
		10X,'Number of Prony Terms in CL23 = ',I5,/,&
		10X,'Number of Prony Terms in CL33 = ',I5,/,&
	    10X,'Number of Prony Terms in CL44 = ',I5,/,&
		10X,'Number of Prony Terms in CL55 = ',I5,/,&
		10X,'Number of Prony Terms in CL66 = ',I5,/)
 		
 1028	Format(/,10X,'CInf11 = ',D15.7,/,&
		10X,'CInf12 = ',D15.7,/,&
		10X,'CInf13 = ',D15.7,/,&
		10X,'CInf22 = ',D15.7,/,&
		10X,'CInf23 = ',D15.7,/,&
		10X,'CInf33 = ',D15.7,/,&
		10X,'CInf44 = ',D15.7,/,&
		10X,'CInf55 = ',D15.7,/,&
		10X,'CInf66 = ',D15.7,/)
		
     1029   Format(15X,I5,5X,D15.7,5X,D15.7)
    !
    !   Read in Elements OF CL11, 12, 13, 22, 23, 33, 44, 55, 66
    !
    1030    Format(/,10X,'Prony Terms for CL11',//,19X,'I',13X,'CL11G(I)',12X,'Eta11G(I)',/)
    1031    Format(/,10X,'Prony Terms for CL12',//,19X,'I',13X,'CL12G(I)',12X,'Eta12G(I)',/)
    1032	Format(/,10X,'Prony Terms for CL13',//,19X,'I',13X,'CL13G(I)',12X,'Eta13G(I)',/)
    1033	Format(/,10X,'Prony Terms for CL22',//,19X,'I',13X,'CL22G(I)',12X,'Eta22G(I)',/)
    1034	Format(/,10X,'Prony Terms for CL23',//,19X,'I',13X,'CL23G(I)',12X,'Eta23G(I)',/)
    1035	Format(/,10X,'Prony Terms for CL33',//,19X,'I',13X,'CL33G(I)',12X,'Eta33G(I)',/)
    1036	Format(/,10X,'Prony Terms for CL44',//,19X,'I',13X,'CL44G(I)',12X,'Eta44G(I)',/)
    1037	Format(/,10X,'Prony Terms for CL55',//,19X,'I',13X,'CL55G(I)',12X,'Eta55G(I)',/)
    1038	Format(/,10X,'Prony Terms for CL66',//,19X,'I',13X,'CL66G(I)',12X,'Eta66G(I)',/)
 
  
    Do i = 1, MaterialNumber5G
        
        Max = Max0(NumberOfTerms11G(I),NumberOfTerms12G(I),NumberOfTerms22G(I),NumberOfTerms23G(I),NumberOfTerms44G(I),NumberOfTerms66G(I))
	    If (MAX > NGPronyG) Then
            NGPronyG = MAX
        EndIf
    	
    Enddo

455 CONTINUE
    
    End Subroutine Read3DInputViscoelasticity
    
End Module InputViscoElasticity