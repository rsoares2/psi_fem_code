Module InputPlasticity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)
    
    Contains    
    
    Subroutine ReadGlobalInputDruckerPrager
    
    ! 
    !   Read in material properties for Drucker-Prager plastic elements
    ! 

! 
!  THIS IS THE DRUCKER-PRAGER RATE-INDEPENDENT ELASTO-PLASTIC MATERIAL MODEL
! 
!  IT CONTAINS COMBINED HARDENING, WHEREIN
!      ALPHA1 IS THE RADIUS OF THE YIELD SURFACE, AND
!      ALPHA2ij ARE THE COMPONENTS OF THE YIELD SURFACE CENTER
!
! THE YIELD FUNCTION IS OF THE FORM: 
! 
!      F=SQRT(J2')+MU*I1/SQRT(6.)-ALPHA1
!
! WHERE
!      I1=Skk/3.
!      J2'=1/2*(S'ij-ALPHA2'ij)(S'ij-ALPHA2'ij)
!
! REQUIRED INPUT DATE ARE AS FOLLOWS:
!
! NUNIAX - NUMBER OF UNIAXIAL STRESS-STRAIN POINTS 
! ISUB - SUBINCREMENTATION FLAG(.NE.0) NO SUBINCS 
! EM - YOUNG'S MODULUS 
! VNU - POISSON'S RATIO
! ALPHA1 - INITIAL RADIUS OF YIELD CONE 
! BBETA - HARDENING RATIO 
! MU - COHESION COEFFICIENT
! DEPSAL - ALLOWABLE STRAIN SUBINCREMENT 
!
! THE MODEL CURRENTLY USES DATA FROM A UNIAXIAL TEST TO PREDICT HARDENING
! THESE ARE TO BE INPUT PIECEWISE LINEARLY AS FOLLOWS 
! (NOTE THAT TENSION IS ASSUMED TO BE POSITIVE):
!
! SX(J) - VALUE OF STRESS ON SIGMA-X VS EPSILON-X CURVE 
! EX(J) - VALUE OF STRAIN ON SIGMA-X VS EPSILON-X CURVE 
! 
! IF THE USER WANTS TO UTILIZE EXPERIMENTAL DATA FROM A MULTIAXIAL TEST TO PREDICT HARDENING
! THEN THE CODE MUST BE MODIFIED BY THE USER
!

    Allocate ( PlasticPosition (NumberOfTotalMaterials) )  !Array to keep track number of plastic materials independent of matset number.
    PlasticPosition = 0

	Allocate ( EEGDP (MaterialNumber2G) )
	Allocate ( VNUGDP (MaterialNumber2G) )
	Allocate ( YieldGDP (MaterialNumber2G) )
	Allocate ( AlphaGDP (MaterialNumber2G) )
	Allocate ( Mu (MaterialNumber2G) )
	Allocate ( BBeta (MaterialNumber2G) )
	Allocate ( Nuniax (MaterialNumber2G) )

    Allocate ( Density (NumberOfTotalMaterials) )
	Allocate ( StrainLimitG (MaterialNumber2G) )
    

    Write (6,1023) MaterialNumber2G
1023 Format (/,10X,'There are ',i3,' 2-D isotropic Drucker-Prager elastic-plastic material sets', &
		//,10X,'Set N0.',9X,'E',10X,'NU',11X,'Y',9X,'Alpha',6x,'Density',2x,'StrainLimitG'/) 
        
    Do i=1, MaterialNumber2G
                
        Read (5,*) MSetiG, EEGDP(I), VNUGDP(I), Density(I), Nuniax(I), YieldGDP(I), Mu(i), BBeta(i), StrainLimitG(i)
        Write(6,1024) MSetiG
1024    Format(i3)
        Write(6,1025) EEGDP(I), VNUGDP(I),  Density(I), YieldGDP(I), Mu(i), BBeta(I)
 1025 Format(10X,'Modulus of Elasticity = ',E14.7,/, 10X,'Poisson''s Ratio = ',E14.7,/, &
            10x, 'Density = ', E14.7,/,&
            10X,'Initial Uniaxial Yield Stress in Tension = ',E14.7,/,&
            10X,'Cohesion Coefficient = ',E14.7,/,&
            10X,'Hardening Ratio = ',E14.7,//)        

        PlasticPosition (MsetiG) = i      !Array to store material numbers and positions         
                
    Enddo
     Read (5,*) iSubIncrementation, DeltaStrainSubIncrement
    Write (6,2160) iSubIncrementation, DeltaStrainSubIncrement 
 2160 Format(10X,'SubIncrementation Flag = ',I5,/,10X,'Allowable Strain Subincrement = ',F10.4,//) 

	Allocate ( SX (NUNIAX(MaterialNumber2G),MaterialNumber2G) )
	Allocate ( EX (NUNIAX(MaterialNumber2G),MaterialNumber2G) )
	Allocate ( EPX (NUNIAX(MaterialNumber2G),MaterialNumber2G) )
	Allocate ( SP (NUNIAX(MaterialNumber2G),MaterialNumber2G) )

    Do j = 1, NUNIAX(MSETIG) 
      Read (5,*) SX(j,MSETIG),EX(j,MSETIG) 

      Write (6,2190) SX(j,MSETIG),EX(j,MSETIG) 
 2190 Format (10X,F10.2,10X,F10.5) 
    Enddo 
      WRITE(6,3100) 
 3100 FORMAT(//,10X,'The uniaxial K vs EPBar date are',//,19X,'K',15X,'EPBAR',//) 
    Do J=1,NUNIAX(MSETIG) 
        EPX(J,MSetiG)=EX(J,MSetiG)-SX(J,MSetiG)/EEGDP(MSetiG) 
        SP(J,MSetiG)=YieldGDP(MSetiG)+(SX(J,MSETIG)-YieldGDP(MSetiG))*BBETA(MSetiG) 
      WRITE(6,2190) SP(J,MSetiG),EPX(J,MSetiG) 
    Enddo
 1363 CONTINUE 
 1452 CONTINUE 
            
    End Subroutine ReadGlobalInputDruckerPrager


End Module InputPlasticity