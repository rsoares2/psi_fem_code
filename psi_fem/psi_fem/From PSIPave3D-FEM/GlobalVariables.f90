Module GlobalInputVariables

!
!   This Module stores all Global variables initiated from the ReadInput
!

!
!   From Module InputHeader, Subroutine ReadInputHeader
!
    Real*8, Allocatable, dimension ( : ) :: TimeEndTB ( : )
 	Real*8, Allocatable, dimension ( : ) :: TimeFactor ( : )
	Real*8, Allocatable, dimension ( : ) :: DeltaTime ( : )
     
    Integer:: NumberOfTimeSteps, NumberOfDimensions, NumberOfScales, NumberOfTimeBlocks, NumberOfGlobalNodesPerElement     
    Integer:: NumberOfGlobalNodes, NumberOfGlobalElements, NumberOfForces, NumberOfDisplacementBC, iBodyForce, iGlobalCohesiveZones, iSolutionType 
    Integer:: iOutputGlobal, iOutputLocal, iTecplotGlobal, iTecplotLocal, iPavement
    
    !Should they be Pointers instead??

!
!   From Module InputCoordinates, Subroutine Read2DInputCoordinates and Read3DInputCoordinates
!

	Real*8, Pointer, dimension ( : ) :: XG ( : )
	Real*8, Pointer, dimension ( : ) :: YG ( : )
	Real*8, Pointer, dimension ( : ) :: ZG ( : )

!
!   From Module InputCoordinates, Subroutine Read2DInputCoonnectivity and Read3DInputCoonnectivity
!
 	Integer, Allocatable, dimension ( : ) :: NodeG ( :, : )
	Integer, Allocatable, dimension ( : ) :: MaterialSetG ( : )
	Integer, Allocatable, dimension ( : ) :: MaterialTypeG ( : )

    Integer, Allocatable, dimension ( : ) :: LinkedNodeConnectivity ( :, : ) !   Variable to calculate sparse initial data
    Integer:: NGIntegrationPoints  !number of global integration points
    Integer:: iNGlobalNodes !it allocates memory to the linked connectivity array.
    Integer:: NZG   !Number of non-zeros in sparse matrix
    Integer :: DeformationFactor
!
!   From Module InputBCandForce, Subroutine ReadInputBC
!
     
	Integer, Pointer, dimension ( : ) :: NumberOfGlobalDisplacement ( : )
	Integer, Allocatable, dimension ( : ) :: NGlobalDOF ( :, : )
    Real*8, Pointer, dimension ( : ) :: DisplacementIncrementG ( :, : )
     
    Integer:: NGTotalDOF
     
!
!   From Module InputBCandForce, Subroutine ReadInputForce
!
	Real*8, Pointer, dimension ( : ) :: FGLG ( : )
	Integer, Allocatable, dimension ( : ) :: iNode ( : )
     
    Real*8:: ForceX, ForceY, ForceZ
     
!
!  From Module InputMaterials, Subroutine ReadGlobalInputMaterials
!
    Integer:: MaterialNumber1G, MaterialNumber2G, MaterialNumber3G, MaterialNumber4G, MaterialNumber5G, MaterialNumber6G
    Integer:: NumberOfTotalMaterials, NumberOfGlobalDruckerPragerElement
    
    Integer, Allocatable, dimension ( : ) :: ElasticPosition ( : )
    Integer, Allocatable, dimension ( : ) :: PlasticPosition ( : )
    Integer, Allocatable, dimension ( : ) :: ElasticPlasticPosition ( : )
    Integer, Allocatable, dimension ( : ) :: ViscoElasticPosition ( : )
    Integer, Allocatable, dimension ( : ) :: ViscoPlasticPosition ( : )

!
!  From Module InputMaterials, Subroutine ReadGlobalInputElastic
!
 	Real*8, Allocatable, dimension ( : ) :: EEG ( : )   ! Young's modulus
 	Real*8, Allocatable, dimension ( : ) :: VNUG ( : )  ! Poisson's ratio
	Real*8, Allocatable, dimension ( : ) :: AlphaG ( : )   ! Angle (used only in fiber composite)
    Real*8, Allocatable, dimension ( : ) :: YieldG ( : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: StrainLimitG ( : )    ! Yield Stress

	Real*8, Allocatable, dimension ( : ) :: Density ( : )    ! Include body forces

!
!  From Module InputMaterials, Subroutine ReadGlobalInputDruckerPrager
!
 	Real*8, Allocatable, dimension ( : ) :: EEGDP ( : )   ! Young's modulus
 	Real*8, Allocatable, dimension ( : ) :: VNUGDP ( : )  ! Poisson's ratio
	Real*8, Allocatable, dimension ( : ) :: AlphaGDP ( : )   ! Angle (used only in fiber composite)
    Real*8, Allocatable, dimension ( : ) :: YieldGDP ( : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: StrainLimitGDP ( : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: SX ( :, : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: EX ( :, : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: SP ( :, : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: EPX ( :, : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: Mu ( : )    ! Yield Stress
    Real*8, Allocatable, dimension ( : ) :: BBeta ( : )    ! Yield Stress
    Integer, Allocatable, dimension ( : ) :: Nuniax ( : )    ! Yield Stress

    Real*8, Allocatable, dimension ( : ) :: Alpha11 ( :, : )    ! Yield/Sqrt(3) - It is Alpha1 in DHA's code. Conflicts with CZ
    Real*8, Allocatable, dimension ( : ) :: Alpha2 ( :, :, : )    ! Yield surface transformation tensor
    Integer, Allocatable, dimension ( : ) :: iPlas ( : )    ! Plasticity flag
    Real*8, Allocatable, dimension ( : ) :: EPBar ( :, : )   

    Integer :: IPlastic
!
!  From Module InputMaterials, Subroutine ReadGlobalInputViscoElastic
!

    Real*8, Allocatable, dimension ( : ) :: CInf11G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf12G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf13G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf22G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf23G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf33G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf44G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf55G( : )
    Real*8, Allocatable, dimension ( : ) :: CInf66G( : )


    Real*8, Allocatable, dimension ( : ) :: CL11G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL12G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL13G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL22G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL23G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL33G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL44G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL55G( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL66G( :, : )

    Real*8, Allocatable, dimension ( : ) :: ETA11G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA12G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA13G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA22G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA23G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA33G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA44G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA55G( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA66G( :, : )
	
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms11G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms12G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms13G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms22G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms23G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms33G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms44G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms55G( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms66G( : )	 

	Integer:: NGPronyG ! Maximum number of prony terms
    
!
!  From Module AllenCZModel, Subroutine ReadGlobalInputAllenCZModel
!
    Integer:: NGlobalInterfElementsG, NGlobalInterfMaterialG, NumberPronyTermsCZ, iMatCZ
    
    Real*8, Allocatable, dimension ( : ) :: SIGMAXG( : )	 
    Real*8, Allocatable, dimension ( : ) :: DELTAG( : )	 
    Real*8, Allocatable, dimension ( : ) :: DBFACG( : )	 
    Real*8, Pointer, dimension ( : ) :: WIDTHG( : )	 
    Real*8, Pointer, dimension ( : ) :: PHIAVG( : )	 
	 
    Integer, Pointer, dimension ( : ) :: IDEBNDG( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT1G( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT2G( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT3G( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT4G( : )	 
	 
    Real*8, Allocatable, dimension ( : ) :: SigmaNormalG( : )	 
    Real*8, Allocatable, dimension ( : ) :: SigmaTangG( : )	 
    Real*8, Allocatable, dimension ( : ) :: SigmaTangSG( : )	 

    Real*8, Allocatable, dimension ( : ) :: RMG( : )	 

    Real*8, Allocatable, dimension ( : ) :: ECInfG( : )	 
    Real*8, Allocatable, dimension ( : ) :: ECG( :, : )	 
    Real*8, Allocatable, dimension ( : ) :: ETACG( :, : )
    Real*8, Allocatable, dimension ( : ) :: RAMBDA1G( : )	 
    Real*8, Allocatable, dimension ( : ) :: RAMBDA2G( : )	 
	 
    Real*8, Allocatable, dimension ( : ) :: DeltaNormalG( : )	 
    Real*8, Allocatable, dimension ( : ) :: DeltaTangG( : )	 
    Real*8, Allocatable, dimension ( : ) :: DeltaTangSG( : )	 
    Real*8, Allocatable, dimension ( : ) :: AlphG( : )	    !AlphaG is used in elasticity
	 
    Integer, Pointer, dimension ( : ) :: iMaterialCZG( : )	 
      
    Real*8, Pointer, dimension ( : ) :: SIGCOHNG( :, : )	 
    Real*8, Pointer, dimension ( : ) :: SIGCOHTG( :, : )	 

    Real*8, Pointer, dimension ( : ) :: SIGCOHNGIter( :, : )
    Real*8, Pointer, dimension ( : ) :: SIGCOHTGIter( :, : )	 

    Real*8, Pointer, dimension ( : ) :: OLAMBDAG( : )	 
    Real*8, Pointer, dimension ( : ) :: UNOLDG( : )	 
    Real*8, Pointer, dimension ( : ) :: UTOLDG( : )	 
    Real*8, Allocatable, dimension ( : ) :: USOLDG( : )	 
    Real*8, Pointer, dimension ( : ) :: DAMAGEG( : )	 
    Real*8, Pointer, dimension ( : ) :: TractionTangentG( : )	 
    Real*8, Pointer, dimension ( : ) :: DAMAGEGIter( : ) 
    Real*8, Pointer, dimension ( : ) :: TractionNormalG( : )	 
    Real*8, Pointer, dimension ( : ) :: TractionTangentGIter( : )	 
    Real*8, Pointer, dimension ( : ) :: TractionNormalGIter( : )	 
    Real*8, Allocatable, dimension ( : ) :: TCSG( : )	 
    Real*8, Allocatable, dimension ( : ) :: TCSGG( : )

!
!   From Module SparseConectivity, Subroutine SparseInitialConnectivity
!
	 
    Integer, Pointer, dimension ( : ) :: IROW1( :, : ) !FOR SPARSE SOLVER
    Integer, Pointer, dimension ( : ) :: IROW( : ) !FOR SPARSE SOLVER - ROWS
    Integer, Pointer, dimension ( : ) :: JCOL( : ) !FOR SPARSE SOLVER - COLUMNS
    Integer, Pointer, dimension ( : ) :: IROWSUM ( : ) !FOR SPARSE SOLVER - SUM OF NROWS
    Integer, Pointer, dimension ( : ) :: IROWNZG ( : ) !FOR SPARSE SOLVER
	 
    Integer, Pointer, dimension ( : ) :: IRowCompact( : )	 
    Integer, Pointer, dimension ( : ) :: JcolCompact( : )

!
!   From Module OutputFromInput, Subroutine PrintOutputFromInput
!

    Real*8, Allocatable, dimension ( : ) :: C11L( : )
    Real*8, Allocatable, dimension ( : ) :: C12L( : )
    Real*8, Allocatable, dimension ( : ) :: C22L( : )	 

   
    Integer:: NumberOfCycles

!
!   From Module InitializeGlobalVariables, Subroutine GlobalInitial
!
    
    Real*8, Pointer, dimension ( : ) :: FORCEG( : )	 	 
    Real*8, Allocatable, dimension ( : ) :: SG( :, :, : )
    Real*8, Allocatable, dimension ( : ) :: DSTRANG( : )	 	
    Real*8, Allocatable, dimension ( : ) :: StrainG( :, :, : )
    Real*8, Pointer, dimension ( : ) :: DisplacementG( : )	 
    Real*8, Allocatable, dimension ( : ) :: SGOld( :, :, : )

    Real*8, Allocatable, dimension ( : ) :: SIG ( :, :, :, :, : )
    Real*8, Allocatable, dimension ( : ) :: CC3G ( :, : )
    Real*8 :: KBig, TG
    Integer :: IBRangeG, iPrintToScreen
    
    Real*8,Allocatable, dimension ( : ) :: EPSPDOT(:, :, :)
    Real*8,Allocatable, dimension ( : ) :: SDOT(:, :, :)
    Real*8,Allocatable, dimension ( : ) :: X(:, :, :)
    Real*8,Allocatable, dimension ( : ) :: SIGBAR(:)
    Real*8,Allocatable, dimension ( : ) :: DS(:, :, :)
    Real*8,Allocatable, dimension ( : ) :: DEPSPT(:, :, :)
    Real*8,Allocatable, dimension ( : ) :: DEPSP(:, :, :)
    Real*8,Allocatable, dimension ( : ) :: EPSP(:, :, :)
    Integer :: iSubIncrementation
    Real*8:: DeltaStrainSubIncrement

    Real*8 :: Gravity
    Integer :: iRange
    
!
!   From Main 
!    
    Integer :: KTime, Incr, iCycl
    Real*8 :: TimLim, Fac, Time, DTime
    
!
!   From Module CalculateStifnessMatrix, Subroutine GlobalStiffnessMatrix
!	 	 
    Real*8, Pointer, dimension ( : ) :: SparseValuesG( : )
    Real*8, Allocatable, dimension ( : ) :: BG( :, : ) 
    Real*8, Allocatable, dimension ( : ) :: KElement( :, : )      
    Real*8, Allocatable, dimension ( : ) :: KElement_Temp( :, : )
	Real*8, Allocatable, dimension ( : ) :: C ( :, : )     	 
!
!   From Global Stiffness Matrix --> Global Stiffness Matrix --> ShapeFunctions
!	 	 
	 
	Real*8, Allocatable, dimension ( : ) :: XIB( :, : )
	Real*8, Allocatable, dimension ( : ) :: WB( : ) 
    Real*8 DetJ, Weight, Area, AreaTotal
 
!
!   From Module CalculateStifnessMatrix, Subroutine GlobalForceMatrix
!	 	 
    Real*8, Allocatable, dimension ( : ) :: FElement ( : )
    Real*8, Allocatable, dimension ( : ) :: SEigen ( : )    
    Real*8, Allocatable, dimension ( : ) :: DSigrG ( : )    
    Real*8, Allocatable, dimension ( : ) :: ForceMatrixG ( : )    
!
!   From Module LinearSystemSolvers, Subroutine PardisoSolver
!	 	 
    Real*8, Allocatable, dimension ( : ) :: DeltaDisplacementG( : )   

!
!   From CalculateStress3D --> Subroutine CalculateGlobalStress3D
!	 	 
	 
	Real*8, Allocatable, dimension ( : ) :: DeltaStrainG( :, :, : )	 
	Real*8, Allocatable, dimension ( : ) :: DELTAQG( : )
	Real*8, Allocatable, dimension ( : ) :: DeltaStressG( : )

!
!   From iterations
!
    Integer:: Iteration, IterationFlag, iMaximumNumberofIterations, it
    Real*8:: Tolerance
    Real*8, Allocatable, dimension ( : ) :: StrainGOld ( :, :, : )	
   	Real*8, Allocatable, dimension ( : ) :: DeltaStressGOld( : ) 
    Real*8, Pointer, dimension ( : ) :: DisplacementGold( : )

!
!   From Viscoelasticity
!

    Integer:: iViscoelastic, iVisG, NumberOfGlobalViscoElasticElement, NumberOfGlobalUzanElasticElement
    
!
!   From Module OutputFromInput, Subroutine ReadPavementNodes
!
    

    Real*8, Allocatable, dimension ( : ) :: StressAtNodes( :, : )
    Real*8, Allocatable, dimension ( : ) :: StrainAtNodes( :, : )

    Integer, Allocatable, dimension ( : ) :: NodesTopHMACLane( : )
    Integer, Allocatable, dimension ( : ) :: NodesTopHMACShoulder( : )
    Integer, Allocatable, dimension ( : ) :: NodesBottomHMACLane( : )
    Integer, Allocatable, dimension ( : ) :: NodesBottomHMACShoulder( : )
    Integer, Allocatable, dimension ( : ) :: NodesTopSubgradeLane( : )
    Integer, Allocatable, dimension ( : ) :: NodesTopSubgradeShoulder( : )

    Real*8, Allocatable, dimension ( : ) :: StressTopHMACLane( : , : )
    Real*8, Allocatable, dimension ( : ) :: StressTopHMACShoulder( : , : )
    Real*8, Allocatable, dimension ( : ) :: StressBottomHMACLane( : , : )
    Real*8, Allocatable, dimension ( : ) :: StressBottomHMACShoulder( : , : )
    Real*8, Allocatable, dimension ( : ) :: StressTopSubgradeLane( : , : )
    Real*8, Allocatable, dimension ( : ) :: StressTopSubgradeShoulder( : , : )

    Real*8, Allocatable, dimension ( : ) :: StrainTopHMACLane( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainTopHMACShoulder( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainBottomHMACLane( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainBottomHMACShoulder( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainTopSubgradeLane( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainTopSubgradeShoulder( : , : )

    Real*8, Allocatable, dimension ( : ) :: StrainBottomHMACLaneShearyz( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainBottomHMACLaneShearxz( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainBottomHMACLaneShearxy( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainTopSubgradeLaneShearyz( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainTopSubgradeLaneShearxz( : , : )
    Real*8, Allocatable, dimension ( : ) :: StrainTopSubgradeLaneShearxy( : , : )

    Real*8, Allocatable, dimension ( : ) :: ElementsConnectedToNodes( : )

    Real*8, Allocatable, dimension ( : ) :: J2_Inv ( : )
    Real*8, Allocatable, dimension ( : ) :: J2StressAtNodes( : ) 
    Real*8, Allocatable, dimension ( : ) :: I1_Inv( : ) 
    Real*8, Allocatable, dimension ( : ) :: C_IEL (:, :, :)
    Real*8, Allocatable, dimension ( : ) :: I1MaxVe( : ) 
    Real*8, Allocatable, dimension ( : ) :: J2MaxVe( : ) 
    Real*8, Allocatable, dimension ( : ) :: I1Max( : ) 
    Real*8, Allocatable, dimension ( : ) :: J2Max( : )      
    Real*8, Allocatable, dimension ( : ) :: SGOctahedral ( : )

!   Elasticity k1k2k3
!
    Real*8, Allocatable, dimension ( : ) :: EEGk( : )
    Real*8, Allocatable, dimension ( : ) :: VNUGk( : )
    Real*8, Allocatable, dimension ( : ) :: ALPHAGk( : )
    Real*8, Allocatable, dimension ( : ) :: StrainLimitGk( : )
    Real*8, Allocatable, dimension ( : ) :: YieldGk( : )
    Real*8, Allocatable, dimension ( : ) :: Densityk( : )
    Real*8, Allocatable, dimension ( : ) :: YGk( : )   
    Real*8, Allocatable, dimension ( : ) :: ck1( : )   !coefficients k1, k2, k3 from Mr = k1*I1^k2*J2^k3
    Real*8, Allocatable, dimension ( : ) :: ck2( : )
    Real*8, Allocatable, dimension ( : ) :: ck3( : )
    Real*8, Allocatable, dimension ( : ) :: vnuk1( : )   !coefficients k1, k2, k3 from Mr = k1*I1^k2*J2^k3
    Real*8, Allocatable, dimension ( : ) :: vnuk2( : )
    Real*8, Allocatable, dimension ( : ) :: vnuk3( : )
    Real*8, Allocatable, dimension ( : ) :: ModulusI1J2( : )
    Real*8, Allocatable, dimension ( : ) :: I1plus( : )
    Real*8, Allocatable, dimension ( : ) :: EEGkMinimum( : )
    Real*8, Allocatable, dimension ( : ) :: VNUGkMinimum( : )
    Real*8:: Pa

End Module GlobalInputVariables