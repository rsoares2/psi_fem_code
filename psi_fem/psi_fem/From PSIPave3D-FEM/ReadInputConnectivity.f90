Module InputConnectivity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    Use LinkedConectivity
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine ReadInputConnectivity
    
    !
    !   This subroutine reads the nodal connectivity of elements of the following type
    !
    !   1. CST triangles (3 nodes) - 2D
    !   2. Quad (4 nodes) - 2D
    !   3. Tetrahedron (4 nodes) - 3D
    !   4. Bricks (8 nodes) - 3D
    !
    !   It is followed by material set (how many materials) and material type (elastic, v/e, v/p...)
    !
    
    Allocate ( NodeG (NumberOfGlobalElements, NumberOfGlobalNodesPerElement) )
    Allocate ( MaterialSetG (NumberOfGlobalElements) )
    Allocate ( MaterialTypeG (NumberOfGlobalElements) )
    
    If ( NumberOfDimensions == 2 ) Then

        If ( NumberOfGlobalNodesPerElement == 3 ) Then  !CST Elements

            NGIntegrationPoints = 1 !CST has only one integration point
            iNGlobalNodes = 6*3 ! It allocates memory to this linked array.
            Write(6, 1014) 
            
        ElseIf ( NumberOfGlobalNodesPerElement == 4 ) Then  !Quad Elements

            NGIntegrationPoints = 4 !Quad have 4 integration points
            iNGlobalNodes = 6*4 ! It allocates memory to this linked array. (Review number for Quad elements)
            Write(6, 1015) 
            
        Else
            
            Write (6,1012)
            Stop
        
        Endif
        
    ElseIf ( NumberOfDimensions == 3 ) Then

        If ( NumberOfGlobalNodesPerElement == 4 ) Then  !Tetrahedron Elements
            
            NGIntegrationPoints = 1 !Tetrahedron has only one integration point
            iNGlobalNodes = (8+4)*3 ! It allocates memory to this linked array.
            Write(6, 1017) 
        ElseIf ( NumberOfGlobalNodesPerElement == 8 ) Then  !Brick Elements
            
            NGIntegrationPoints = 1 !Brick has only one integration point
            iNGlobalNodes = 27*3 ! It allocates memory to this linked array.
            Write(6, 1018) 

        Else
            
            Write (6,1013)
            Stop
            
        Endif

    Else
        
        Write (6,1007) 
        Stop
        
    Endif

    Allocate ( LinkedNodeConnectivity (NumberOfGlobalNodes,iNGlobalNodes)) !Stores the linked connectivity used to calculate the sparse stiffness matrix    
    ! The LinkedConnectivity relates to the MAXIMUM nodes connected to one particular node (used to create sparse matrix size a priori and avoid an additional loop).
    LinkedNodeConnectivity = 0
    LinkedNodeConnectivity (:,iNGlobalNodes) = 1

    
    Do Iel = 1, NumberOfGlobalElements 
	
        Read (5,*) i,(NodeG (Iel,j),j=1,NumberOfGlobalNodesPerElement), MaterialSetG(Iel), MaterialTypeG(Iel)
		
        If (MaterialTypeG(Iel) == 1) Then   !Allows for any numbering order when inputting materials
            
		    NumberOfGlobalElasticElement = NumberOfGlobalElasticElement + 1
            
        ElseIf (MaterialTypeG(Iel) == 2) Then
            
		    NumberOfGlobalDruckerPragerElement = NumberOfGlobalDruckerPragerElement + 1

        ElseIf (MaterialTypeG(Iel) == 3) Then
            
		    NumberOfGlobalViscoPlasticElement = NumberOfGlobalViscoPlasticElement + 1
            
        ElseIf (MaterialTypeG(Iel) == 5) Then
            
		    NumberOfGlobalViscoElasticElement = NumberOfGlobalViscoElasticElement + 1
        
        ElseIf (MaterialTypeG(Iel) == 6) Then
            
		    NumberOfGlobalUzanElasticElement = NumberOfGlobalUzanElasticElement + 1
            
        Else
            
            Write(6,1016)
            Stop
            
        Endif

		Write(6,1019) i,(NodeG (Iel,j),j=1,NumberOfGlobalNodesPerElement), MaterialSetG(Iel), MaterialTypeG(Iel)
    
        Call LinkedConnectivity(Iel)
        
    Enddo

    ! Formats used above
    
1007    Format ('The number of dimensions must be 2 for 2D or 3 for 3D')
    
1012    Format ('Element is not in the current library. Please choose 3 (CST) or 4 (Quad) elements.')

1013    Format ('Element is not in the current library. Please choose 4 (Tetrahedron) or 8 (Brick) elements.')
            
1014    Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Material Set Global',5X,'Material Type Global',//)

1015    Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4x,'Material Set Global',5X,'Material Type Global',//)

1016    Format ('Material type not in the current library')

1017    Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4X,'Material Set Global',5X,'Material Type Global',//)

1018    Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4X,'Node 5',4X,'Node 6',4X,'Node 7',4X,'Node 8',4X,'Material Set Global',5X,'Material Type Global',//)
            
1019    Format (12X,i5,10(5X,i5))

    End Subroutine ReadInputConnectivity

    Subroutine ReadLocalInputConnectivity
    
    !
    !   This subroutine reads the local nodal connectivity of elements of the following type
    !
    !   1. CST triangles (3 nodes) - 2D
    !   2. Quad (4 nodes) - 2D !Only implemented in an earlier multiscale version
    !
    !   It is followed by material set (how many materials) and material type (elastic, v/e, v/p...)
    !
    
    Allocate ( NodeL (NumberOfLocalElements, NumberOfLocalNodesPerElement) )
    Allocate ( MaterialSetL (NumberOfLocalElements) )
    Allocate ( MaterialTypeL (NumberOfLocalElements) )
    
    If ( NumberOfDimensions == 2 ) Then

        If ( NumberOfLocalNodesPerElement == 3 ) Then  !CST Elements

            NLIntegrationPoints = 1 !CST has only one integration point
            iNLocalNodes = 6*3 ! It allocates memory to this linked array.
            Write(6, 1014) 
            
        ElseIf ( NumberOfLocalNodesPerElement == 4 ) Then  !Quad Elements

            NLIntegrationPoints = 4 !Quad have 4 integration points
            iNLocalNodes = 6*4 ! It allocates memory to this linked array. (Review number for Quad elements)
            Write(6, 1015) 
            
        Else
            
            Write (6,1012)
            Stop
        
        Endif        
        
    Else
        Write(6,1223)
1223    Format ('3D Multiscale not yet in the code')
        Stop
        
    Endif
    
    Allocate ( LinkedNodeConnectivityL (NumberOfLocalNodes,iNLocalNodes)) !Stores the linked connectivity used to calculate the sparse stiffness matrix    
    ! The LinkedConnectivity relates to the MAXIMUM nodes connected to one particular node (used to create sparse matrix size a priori and avoid an additional loop).
    LinkedNodeConnectivityL = 0
    LinkedNodeConnectivityL (:,iNLocalNodes) = 1

    
    Do Iel = 1, NumberOfLocalElements
	
        Read (5,*) i,(NodeL (Iel,j),j=1,NumberOfLocalNodesPerElement), MaterialSetL(Iel), MaterialTypeL(Iel)
		
        If (MaterialTypeL(Iel) == 1) Then   !Allows for any numbering order when inputting materials
            
		    NumberOfLocalElasticElement = NumberOfLocalElasticElement + 1
            
        ElseIf (MaterialTypeL(Iel) == 3) Then
            
		    NumberOfLocalViscoPlasticElement = NumberOfLocalViscoPlasticElement + 1
            
        ElseIf (MaterialTypeL(Iel) == 5) Then
            
		    NumberOfLocalViscoElasticElement = NumberOfLocalViscoElasticElement + 1
            
        Else
            
            Write(6,1016)
            Stop
            
        Endif

		Write(6,1019) i,(NodeL (Iel,j),j=1,NumberOfLocalNodesPerElement), MaterialSetL(Iel), MaterialTypeL(Iel)
    
        Call LinkedConnectivityLocal(Iel)
        
    Enddo
    
1012    Format ('Element is not in the current library. Please choose 3 (CST) or 4 (Quad) elements.')

1013    Format ('Element is not in the current library. Please choose 4 (Tetrahedron) or 8 (Brick) elements.')
            
1014    Format (//,10X,'Local Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Material Set Local',5X,'Material Type Local',//)

1015    Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4x,'Material Set Local',5X,'Material Type Local',//)

1016    Format ('Material type not in the current library')

1017    Format (//,10X,'Local Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4X,'Material Set Global',5X,'Material Type Local',//)

1018    Format (//,10X,'Local Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4X,'Node 5',4X,'Node 6',4X,'Node 7',4X,'Node 8',4X,'Material Set Local',5X,'Material Type Global',//)
            
1019    Format (12X,i5,10(5X,i5))    
    End Subroutine ReadLocalInputConnectivity        
    
    End Module InputConnectivity
