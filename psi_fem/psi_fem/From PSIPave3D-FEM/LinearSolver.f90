    Include 'mkl_dss.f90'
    Include 'mkl_pardiso.f90'
    
Module LinearSystemSolvers

    Use GlobalInputVariables
    Use InitializeGlobalVariables
        
    Use OMP_LIB
    Use MKL_DSS
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine PardisoSolver
    
    !
    !   This subroutine solves the linear system K * u = F, where:
    !
    !   K = Stiffness matrix in sparse form
    !   u = unknowns (displacements)
    !   F - Force matrix
    !    

    Integer, Parameter :: DP = Kind(1.0D0)
    Integer IPARAM(6), IPATH
    Real*8 TIME_SPARSE
    Real*8 RPARAM(5)
    Integer IROWINDEX(NZG)
    TYPE(MKL_DSS_HANDLE) :: handle ! Allocate storage for the solver handle.
    Integer perm(1)
    Integer*8 pt(64)
    Integer maxfct, mnum, mtype, phase, n, nrhs, error, msglvl
    Integer iparm(64)
    Integer idum
    Real*8 waltime1, waltime2, ddum
    Real*8, Allocatable, dimension ( : ) :: values( : )
    
    perm(1) = 0
    N = NGTotalDOF
    NZG_Solver = NZG

    IPATH = 1
   
    !.. Fill all arrays containing matrix data.
    DATA nrhs /1/, maxfct /1/, mnum /1/
    !..
    !.. Set up PARDISO control parameter
    !..
    
    !$omp parallel
    !$omp single
        nthr = OMP_GET_NUM_THREADS()
    Print*, 'OpenMP number of threads: ', nthr

    !$omp end single
    !$omp end parallel
    
  !  PRINT*, 'OpenMP number of threads: ', nthr
    
    Call MKL_SET_DYNAMIC (0)
    Call MKL_SET_NUM_THREADS(nthr)

  !  PRINT*, 'MKL  number of threads: ', mkl_get_max_threads()

    iparm = 0
    
    iparm(1) = 1 ! no solver default
    iparm(2) = 3 ! fill-in reordering from METIS
    iparm(3) = nthr ! numbers of processors
    iparm(4) = 0 ! no iterative-direct algorithm
    iparm(5) = 0 ! no user fill-in reducing permutation
    iparm(6) = 0 ! =0 solution on the first n compoments of x
    iparm(7) = 0 ! not in use
    iparm(8) = 9 ! numbers of iterative refinement steps
    iparm(9) = 0 ! not in use
    iparm(10) = 13 ! perturbe the pivot elements with 1E-13
    iparm(11) = 1 ! use nonsymmetric permutation and scaling MPS
    iparm(12) = 0 ! not in use
    iparm(13) = 1 ! maximum weighted matching algorithm is switched-on (default for non-symmetric)
    iparm(14) = 0 ! Output: number of perturbed pivots
    iparm(15) = 0 ! not in use
    iparm(16) = 0 ! not in use
    iparm(17) = 0 ! not in use
    iparm(18) = -1 ! Output: number of nonzeros in the factor LU
    iparm(19) = -1 ! Output: Mflops for LU factorization
    iparm(20) = 0 ! Output: Numbers of CG Iterations
    iparm(24) = 0 !Parallel factorization control (useful for more than 8 threads)
    iparm(60) = 0 !switches between in-core (IC) and out-of-core (OOC) 
    error = 0 ! initialize error flag
    msglvl = 0 ! print statistical information
    mtype = 2 ! real and symmetric positive definite

!C.. Initiliaze the internal solver memory pointer. This is only
!C necessary for the FIRST Call of the PARDISO solver.

    pt = 0

!C.. Reordering and Symbolic Factorization, This step also allocates
!C all memory that is necessary for the factorization
      phase = 11 ! only reordering and symbolic factorization
      Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, SparseValuesG, IRowCompact, JColCompact, idum, nrhs, iparm, msglvl, ddum, ddum, error)
    !  Write(*,*) 'Reordering completed ... '
      If (error .NE. 0) Then
         Write(*,*) 'The following ERROR was detected: ', error
         Stop 1
      End If
    !.. Factorization.
    phase = 22 ! only factorization
    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, SparseValuesG, IRowCompact, JColCompact, idum, nrhs, iparm, msglvl, ddum, ddum, error)
  !      Write(*,*) 'Factorization completed ... '
    IF (error .NE. 0) THEN
        Write(*,*) 'The following ERROR was detected: ', error
        Stop 1
    Endif   
    
    Allocate ( DeltaDisplacementG (NGTotalDOF) )  

    !.. Back substitution and iterative refinement
    iparm(8) = 2 ! max numbers of iterative refinement steps
    phase = 33 ! only factorization

    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, SparseValuesG, IRowCompact, JColCompact, idum, nrhs, iparm, msglvl, ForceMatrixG, DeltaDisplacementG, error)
 !   Write(*,*) 'Solve completed ... '

    !.. Termination and release of memory
    
    phase = -1 ! release internal memory
    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, ddum, idum, idum,idum, nrhs, iparm, msglvl, ddum, ddum, error)
!write(6,12)time,ForceMatrixG(3), ForceMatrixG(2), sparsevaluesg(20), sparsevaluesg(30)
 
    If (it == 1 .AND. iTerationFlag /= 2) Then    !Do not update stiffness and keep it stored for next iteration
        If ( Associated (SparseValuesG)) Deallocate (SparseValuesG)
    Endif

    !
    !   Add the solution (Displacements) to the global displacements.
    !
    DisplacementGold = DisplacementG
    DisplacementG = DisplacementG + DeltaDisplacementG
    !    write(6,*) time, displacementG(12), DeltaDisplacementG(12)

12 format (12e15.7)
    End Subroutine PardisoSolver
    
End Module LinearSystemSolvers