!**************************************************************************************
!
!  PROGRAM: PSIPave3D_FEM
!
!  THIS PROGRAM SOLVES TWO AND THREE DIMENSIONAL CONTINUUM PROBLEMS USING THE FINITE ELEMENT METHOD          
!
!  PURPOSE:  This is a Finite Element code that performs 2D and 3D
!            operations on complex geometry. Material models include linear elastic 
!           (both isotropic and orthotropic) and linear viscoelastic. Plasticity and ViscoPlasticity 
!           are to be included in the future.
!               
!            Solution types include plane stress and plane strain. 
!
!            2D Elements are of triangular CST type or 4 nodes Rectangular elements
!            3D Elements are of linear tetrahedron or 8-noded brick elements
!            A multiscale version of this code also exist. It performs 
!            simulations in two different scales.
!            Patent Pending
!
!  INPUT:   Input instructions can be accessed by locating the subroutine entitled GlobalInput
!           and scrolling down to the comments section.
!                          
!           The input is given by file INPUT.DAT, which contains all 
!           information necessary to run the code. This file is usually generated
!           by a mesh generator for pavement analysis embedded into PSIPave3D.
!           More specifiCally, the input includes: node coordinates and 
!           connectivity, Material properties and type, Loads. It can be used
!           with any FEM mesh.
!
!  OUTPUT:  The output generates displacement, stress and strain in the form of 
!           different output files to be visualized by TECPLOT and/or PSIPave3D.
!
!  LAST UPDATED: April,25 2014
!
!  AUTHOR: Roberto Soares, PhD.  rfsoares2@gmail.com. All rights reserved(R).
!
!  DISCLAIMER: The author does not warranty anything concerning any of the programs
!              or files which make up the "PSIPave3D FEM Programs". We accept no 
!              responsibility for any loss or damage of any kind which results 
!              from the use, or the purported 
!              use of the "PSIPave3D FEM Programs", or any files in the package, 
!              for any purpose whatsoever.
!
!
!**************************************************************************************

    Program PSIPave3D_FEM

    Use GlobalInput
    Use InitializeGlobalVariables
    Use Increment
    Use CalculateStifnessMatrix
    Use CalculateForceMatrix
    USe LinearSystemSolvers
    Use CalculateStressandStrains
    Use UpdateCZs
    Use CohesiveZoneTraction
    Use Output

    implicit Real*8 (A-H, O-Z)

    Call ReadInput  !Read all input data
    
    Call GlobalInitial  !Initialize all variables
  
    Do iCycl = 1, NumberOfCycles    !   Time stepping loop
        
        Call TimeCycleIncrement   
    
        Do incr = 1, NumberOfTimeSteps
            
            Call TimeCheckIncrement
            
            Call ForceIncrement

            If (iGlobalCohesiveZones /= 0) Call UpdateCZGlobal
            
            Do it = 1, iMaximumNumberofIterations

                Call GlobalStiffnessMatrix
            
                Call GlobalForceMatrix
            
                Call PardisoSolver
            
                Call CalculateStressandStrain
            
!                If (it == 1) Then

                    If (iGlobalCohesiveZones == 4) Call InterfaceTraction2D

  !              Endif

                Call GlobalOutput

            Enddo

            TractionNormalGIter = TractionNormalG
            TractionNormalGIter = TractionNormalG
            DamageGIter = DamageG
            SIGCOHNGIter = SIGCOHNG
            SIGCOHTGIter = SIGCOHTG
            SGOld = SG

        Enddo
        
    Enddo
    
    End program PSIPave3D_FEM