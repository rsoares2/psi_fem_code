Module SparseLocalConectivity
    
    Use LocalInputVariables
    Use GlobalInputVariables
    
    implicit None

    Contains    

    Subroutine  SparseInitialLocalConnectivity

    integer:: i, ij, j, k, index1, index2, index3, indexPosition, iDifference, iPosition
    !
    !   This subroutine assembles the JCOL and IROW arrays for the sparse matrix in CSR format
    !   JCOL AND IROW represent the vectors with node information. 
    !   
    !   There is also a need to organize the columns in ascending order. This is done in a dIfferent Subroutine (inside SPARSE INITIAL module).
    !   NZL = NUMBER OF NONZEROS IN Local SCALE
    
    Allocate ( IROWL(NumberOfLocalNodes+1) )
    Allocate ( JCOLL(NZL) )
    LinkedNodeConnectivityL(:,iNLocalNodes)=LinkedNodeConnectivityL(:,iNLocalNodes)-1   !NEED TO REDUCE 1, B/C II OR JJ IS ALSO INCLUDED.
    IPOSITION=0 !This variable keeps track of number of non-zeros and where to include
    
    !
    !   Update the Number of Non-Zeros to include the DOFs and not only the NumberOfLocalNodes
    !
    
    If(NumberOfDimensions == 2)Then
        NZL = (NZL*NumberOfDimensions)+((NZL*NumberOfDimensions)-NumberOfLocalNodes)
        Allocate(IRowCompactL(2*NumberOfLocalNodes+1))
    ELSE
        NZL = (NZL*NumberOfDimensions)+((NZL*NumberOfDimensions)-NumberOfLocalNodes)+((NZL*NumberOfDimensions)-NumberOfLocalNodes)-NumberOfLocalNodes
        Allocate(IRowCompactL(3*NumberOfLocalNodes+1))
    EndIf
    
    IROWL(1)=1
    IRowCompactL(1)=1
    
    Do I=1, NumberOfLocalNodes
        Do J=1,LinkedNodeConnectivityL(I,iNLocalNodes)
            IPOSITION=IPOSITION+1
            JCOLL(IPOSITION)=LinkedNodeConnectivityL(I,J)
        Enddo
        IROWL(I+1)=IPOSITION+1
        
        !
        ! Calculate IRowCompact, which includes the DOF. It is an "expanded" version of the IRow
        !
        
        If (NumberOfDimensions==2) Then
        
            Index1=(2*I-1)+1
            Index2=Index1+1
            IDifference=IRowL(I+1)-IRowL(I)
            IRowCompactL(Index1)=IDifference*NumberOfDimensions+IRowCompactL(Index1-1)
            IRowCompactL(Index2)=IDifference*NumberOfDimensions-1+IRowCompactL(Index1)
    
        ElseIf (NumberOfDimensions==3) Then
    
            !This statement below was copied from 3d global scale. Not working for 3D multiscale
            Index1=(3*I-2)+1
            Index2=Index1+1
            Index3=Index1+2
            IDifference=IRowL(I+1)-IRowL(I)
            IRowCompactL(Index1)=IDifference*NumberOfDimensions+IRowCompactL(Index1-1)
            IRowCompactL(Index2)=IDifference*NumberOfDimensions-1+IRowCompactL(Index1)    
            IRowCompactL(Index3)=IDifference*NumberOfDimensions-2+IRowCompactL(Index2)

        EndIf        
               
    Enddo
    
 !   If(NumberOfLocalNodes>81)Then  !this If is due to array bounds If NumberOfLocalNodes<27*3
      !  do ir=1,iNLocalNodes
    !        ie=LinkedNodeConnectivity(ir,iNLocalNodes)
   !         Write(778,182)i,ie
   !     Enddo
    182 format(2i8)
    !EndIf
    
    Deallocate(LinkedNodeConnectivityL)

    Call ReorganizeLocalSparseVectors_NEW  !This is in SparseInitial, however for this new way of populating the sparse matrix
    
    END Subroutine  SparseInitialLocalConnectivity

!*********************************************************************************************    
    
    Subroutine ReorganizeLocalSparseVectors_NEW
    
    integer:: i, ij, j, k, kk, index1, index2, index3, indexPosition, iDifference, n1, n2
!
!   This subroutine should be Called after SparseInitialConnectivity
!   to reorganize in ascending order JCol and IRow vectors, so the appropriate compact format can be used
!   It works for 2D and 3D!!!!

!
!   First, need to reorder JCOL and IROW
!    
    Allocate(JColCompactL(NZL)) !this is to include all degrees of freedom and not only the nodes

    kk=0
    DO I=1,NumberOfLocalNodes
        DO J=iRowL(I),IRowL(I+1)-1
            N1=JColL(J)
            DO K=j+1,IRowL(I+1)-1
                N2=JCOLL(K)
                IF(N1 > N2)THEN
                    JColL(J)=JColL(K)
                    JColL(K)=N1
                    N1=N2
                ENDIF
            ENDDO
        ENDDO

        IF(NumberOfDimensions == 2)THEN

            Index1=2*I-1
              
            IndexPosition=IRowCompactL(Index1)
                
            DO ij=IRowL(I),IRowL(I+1)-1
                
                JCOLCompactL(IndexPosition)=2*JColL(ij)-1
                IndexPosition=IndexPosition+1
                JCOLCompactL(IndexPosition)=2*JColL(ij)
                IndexPosition=IndexPosition+1
                
            ENDDO
            
            Index1=2*I
            IndexPosition=IRowCompactL(Index1)
            
            DO ij=IRowL(I),IRowL(I+1)-1
            
                !this is for the second line due to 2D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                IF (ij/=iRowL(I)) THEN
                    JCOLCompactL(IndexPosition)=2*JColL(ij)-1
                    IndexPosition=IndexPosition+1
                ENDIF
                    JCOLCompactL(IndexPosition)=2*JColL(ij)
                        IndexPosition=IndexPosition+1
          
            ENDDO
            
        ELSEIF(NumberOfDimensions == 3)THEN
            
            !This statement below was copied from 3d global scale. Not working for 3D multiscale
            Index1=3*I-2
            IndexPosition=IRowCompactL(Index1)
                
            DO ij=IRowL(I),IRowL(I+1)-1
                
                JCOLCompactL(IndexPosition)=3*JColL(ij)-2
                IndexPosition=IndexPosition+1
                JCOLCompactL(IndexPosition)=3*JColL(ij)-1
                IndexPosition=IndexPosition+1
                JCOLCompactL(IndexPosition)=3*JColL(ij)
                IndexPosition=IndexPosition+1
                
            ENDDO
            
            Index1=3*I-1
            IndexPosition=IRowCompactL(Index1)
            
            DO ij=IRowL(I),IRowL(I+1)-1
            
                !this is for the second and third lines contribution to the stiffness matrix due to 3D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                IF (ij/=iRowL(I)) THEN
                    JCOLCompactL(IndexPosition)=3*JColL(ij)-2
                    IndexPosition=IndexPosition+1
                ENDIF
                    JCOLCompactL(IndexPosition)=3*JColL(ij)-1
                    IndexPosition=IndexPosition+1
                    JCOLCompactL(IndexPosition)=3*JColL(ij)
                    IndexPosition=IndexPosition+1
          
            ENDDO
            
            Index1=3*I
            IndexPosition=IRowCompactL(Index1)
            
            DO ij=IRowL(I),IRowL(I+1)-1
            
                !this is for the second and third lines contribution to the stiffness matrix due to 3D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                IF (ij/=iRowL(I)) THEN
                    JCOLCompactL(IndexPosition)=3*JColL(ij)-2
                    IndexPosition=IndexPosition+1
                    JCOLCompactL(IndexPosition)=3*JColL(ij)-1
                    IndexPosition=IndexPosition+1
                ENDIF
                    JCOLCompactL(IndexPosition)=3*JColL(ij)
                    IndexPosition=IndexPosition+1
          
            ENDDO            
       ENDIF
                   
    ENDDO  
    
!
!   Now convert JCOL to include DOFs - JCOLCompact. The name comes from a previous way to set up these matrices.
!
    
    End Subroutine ReorganizeLocalSparseVectors_NEW    
    
End Module SparseLocalConectivity