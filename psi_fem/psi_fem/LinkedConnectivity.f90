Module LinkedConectivity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    use input_variables
    
    implicit none!Real*8 (A-H, O-Z)

    Contains    

    Subroutine LinkedConnectivity(i)
    
    integer :: M, N, iFlag, iSize, i, j, k, ik, l
    integer :: integerForScreenDisplay
    !   This subroutine prepares data for the sparse matrix format
    !   NZG = Number of Nonzeros in Global Scale
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    IntegerForScreenDisplay = NumberOfGlobalElements / 5

!!$OMP PARALLEL PRIVATE(M, N, iFlag, iSize, j, k, ik)
!!$OMP DO
    do J = 1, NumberOfGlobalNodesPerElement
        M = NodeG(I, J)
        do K = 1, NumberOfGlobalNodesPerElement
            N = NodeG(I, K)
            if (N >= M) then
                iFlag = 0
                IK = 1
                do while (iFlag == 0 .and. IK <= LinkedNodeConnectivity(M, iNGlobalNodes))
                    if (LinkedNodeConnectivity(M, IK) == N) then
                        iFlag = 1
                    else
                        IK = IK + 1
                    end if
                end do
                if (iFlag == 0) then
                    iSize = LinkedNodeConnectivity(M, iNGlobalNodes)
                    LinkedNodeConnectivity(M, iSize) = N
                    !!$OMP ATOMIC
                    NZG = NZG + 1
                    !!$OMP END ATOMIC
                    LinkedNodeConnectivity(M, iNGlobalNodes) = iSize + 1
                end if
            end if
        end do
    end do
!!$OMP END DO
!!$OMP END PARALLEL
    
   ! Keeping the old implementation without the do while loop until confident 
   !do J = 1, NumberOfGlobalNodesPerElement
   !  M = NodeG(I, J)
   !  do K = 1, NumberOfGlobalNodesPerElement
   !    iflag = 0
   !    N = NodeG(I, K)
   !    if (N >= M) then
   !      do IK = 1, LinkedNodeConnectivity(M, iNGlobalNodes)
   !        if (LinkedNodeConnectivity(M, IK) == N) then
   !          iflag = 1
   !          exit
   !        end if
   !      end do
   !      if (iflag /= 1) then
   !        iSize = LinkedNodeConnectivity(M, iNGlobalNodes)
   !        LinkedNodeConnectivity(M, iSize) = N
   !        NZG = NZG + 1 ! Flag to track number of non-zeros in the matrix
   !        iSize = iSize + 1
   !        LinkedNodeConnectivity(M, iNGlobalNodes) = iSize
   !      end if
   !    end if
   !  end do
   !end do

 
    if (NumberOfGlobalElements > 1000) then ! to avoid division by zero
        if (MOD(i, IntegerForScreenDisplay) == 0) then
            write(*, 809) i, NumberOfGlobalElements
        end if
    end if

    ! Format statement
    809 format(10x, 'Set up Sparse Matrix for Element = ', i0, ' of ', i0)
    end subroutine LinkedConnectivity

!*********************************************************************************************

    Subroutine LinkedConnectivityCZ(I)

    integer :: M, N, iFlag, iSize, i, j, k, ik, l, il, imod

    !
    !   This subroutine prepares data for the CZ sparse matrix format
    !
    !   NZG = NUMBER OF NONZEROS IN GLOBAL SCALE
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    DO L=1,2 !SINCE THE CZ IS IMPLEMENTED IN FOUR NODES. THEY ARE ACTUALLY 2 CZ IN FORM OF ONE
    
    IMOD=MOD(L,2)   !THIS IS TO CAPTURE THE REST EITHER 1 OR 0 AND SUBTRACT FROM IL TO GET 
                    !EITHER 2*I-1 OR 2*I
    IL=2*I-IMOD
    
        M=NINT1G(IL) !THIS IS FOR THE FIRST NODE OF CZ
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1G(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7721
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7721 CONTINUE
     
        N=NINT2G(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7722
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                    LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7722 CONTINUE
 
        M=NINT2G(IL) !THIS IS FOR THE FIRST NODE OF CZ - NOW REVERSE
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1G(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7723
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7723 CONTINUE
     
        N=NINT2G(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivity(M,iNGlobalNodes)
                    IF (LinkedNodeConnectivity(M,IK)==N) GOTO 7724
                ENDDO
    
                ISIZE=LinkedNodeConnectivity(M,iNGlobalNodes)
                LinkedNodeConnectivity(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivity(M,iNGlobalNodes)=LinkedNodeConnectivity(M,iNGlobalNodes)+1
            Endif
    7724 CONTINUE
     
    ENDDO
    
    END SUBROUTINE  LinkedConnectivityCZ
    
    Subroutine LinkedConnectivityLocal(i)
    
    integer :: M, N, iFlag, iSize, i, j, k, ik, l
    
    !
    !   This subroutine prepares data for the local sparse matrix format
    !
    !   NZL = Number of Nonzeros in Local Scale
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    Do J=1,NumberOfLocalNodesPerElement
    M = NodeL(I,J)
    Do K=1,NumberOfLocalNodesPerElement
    iflag = 0
        N = NodeL(I,K)
        If(N>=M)THEN
            Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                If (LinkedNodeConnectivityL(M,IK)==N) Then
                    iflag=1
                    exit
                Endif
            Enddo
            If (iflag/=1) then
                iSize = LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,iSize) = N
                NZL = NZL + 1       !    Flag to track number of non-zeros in the matrix
                iSize = iSize + 1
                LinkedNodeConnectivityL (M,iNLocalNodes) = iSize
            Endif
        Endif
     Enddo
    
    Enddo

    End Subroutine LinkedConnectivityLocal    

!*********************************************************************************************

    SUBROUTINE LinkedConnectivityCZL(I)

    integer :: M, N, iFlag, iSize, i, j, k, ik, l, il, imod
    !
    !   This subroutine prepares data for the CZ sparse matrix format
    !
    !   NZL = NUMBER OF NONZEROS IN LOCAL SCALE
    !
    !   This is done for nodes and not for DOF, it is multiplied in the next subroutine
    
    DO L=1,2 !SINCE THE CZ IS IMPLEMENTED IN FOUR NODES. THEY ARE ACTUALLY 2 CZ IN FORM OF ONE
    
    IMOD=MOD(L,2)   !THIS IS TO CAPTURE THE REST EITHER 1 OR 0 AND SUBTRACT FROM IL TO GET 
                    !EITHER 2*I-1 OR 2*I
    IL=2*I-IMOD
    
        M=NINT1L(IL) !THIS IS FOR THE FIRST NODE OF CZ
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1L(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7721
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZG=NZG+1
                LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7721 CONTINUE
     
        N=NINT2L(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7722
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZL=NZL+1
                    LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7722 CONTINUE
 
        M=NINT2L(IL) !THIS IS FOR THE FIRST NODE OF CZ - NOW REVERSE
        !SINCE CZ IS STORED IN NINT1 AND NINT2, THERE IS NO LOOP AND IT IS DONE INDIVIDUALLY
        N=NINT1L(IL)
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7723
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZL=NZL+1
                LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7723 CONTINUE
     
        N=NINT2L(IL) !THIS IS FOR THE SECOND NODE OF CZ
            IF(N>=M)THEN
                Do IK=1,LinkedNodeConnectivityL(M,iNLocalNodes)
                    IF (LinkedNodeConnectivityL(M,IK)==N) GOTO 7724
                ENDDO
    
                ISIZE=LinkedNodeConnectivityL(M,iNLocalNodes)
                LinkedNodeConnectivityL(M,ISIZE)=N
                NZL=NZL+1
                LinkedNodeConnectivityL(M,iNLocalNodes)=LinkedNodeConnectivityL(M,iNLocalNodes)+1
            Endif
    7724 CONTINUE
     
    ENDDO
    
    END SUBROUTINE  LinkedConnectivityCZL
    
End Module LinkedConectivity
    