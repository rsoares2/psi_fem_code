Module InputElasticityUzan
    
    Use GlobalInputVariables
    Use LocalInputVariables
    use input_variables
    
    implicit Real*8 (A-H, O-Z)
    
    Contains    
    
    Subroutine ReadGlobalInputElasticUzan
    
    ! 
    !   Read in material properties for isotropic elastic elements
    ! 

   ! Allocate ( ElasticPosition (NumberOfTotalMaterials) )  !Array to keep track number of elastic materials independent of matset number.
   ! ElasticPosition = 0

	Allocate ( EEGk (NumberOfTotalMaterials) )
	Allocate ( VNUGk (NumberOfTotalMaterials) )
	Allocate ( YieldGk (NumberOfTotalMaterials) )
	Allocate ( AlphaGk (NumberOfTotalMaterials) )
	Allocate ( StrainLimitG (NumberOfTotalMaterials) )
    If(.NOT.Allocated (Density))  Allocate ( Density (NumberOfTotalMaterials) )
    Allocate ( ck1(NumberOfTotalMaterials) )
    Allocate ( ck2(NumberOfTotalMaterials) )
    Allocate ( ck3(NumberOfTotalMaterials) )
    Allocate ( vnuk1(NumberOfTotalMaterials) )
    Allocate ( vnuk2(NumberOfTotalMaterials) )
    Allocate ( vnuk3(NumberOfTotalMaterials) )
    Allocate ( EEGkMinimum(NumberOfTotalMaterials) )
    Allocate ( VNUGkMinimum(NumberOfTotalMaterials) )
    

    Write (6,1023) numberOfElasticUzanMaterials 
1023 Format (/,10X,'There are ',I3,' 3-D isotropic Uzan elastic material sets', &
		//,10X,'Set N0.',9X,'E',10X,'NU',11X,'Y',9X,'Alpha',6x,'Density',2x,'StrainLimitG'/)
        
    Do I=1, numberOfElasticUzanMaterials
                
        Read (5,*) MSetiG, EEGk(I), VNUGk(I), YieldGk(I), AlphaGk(I), Density(I), StrainLimitG(i), ck1(i),ck2(i),ck3(i),EEGkMinimum(i), vnuk1(i),vnuk2(i),vnuk3(i), VNuGKMinimum(i)
    	!READ(5,*) Mseti,EEGk(Mseti),VNUGk(Mseti),YGk(Mseti),ALPHATGk(Mseti), Density(Mseti),
        Write(6,1024) MSetiG, EEGk(I), VNUGk(I), YieldGk(I), AlphaGk(I), Density(I), StrainLimitG(i), ck1(i),ck2(i),ck3(i),EEGkMinimum(i), vnuk1(i),vnuk2(i),vnuk3(i), VNuGKMinimum(i)
1024        Format(11X,I3,5X,15(1X,E11.4))

        !ElasticPosition (MsetiG) = i      !Array to store material numbers and positions         
                
    Enddo
            
    End Subroutine ReadGlobalInputElasticUzan

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    

End Module InputElasticityUzan