Module LinearElasticity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine IsotropicLinearElasticGlobal3D  (iel)

    !
    !   This subroutine is a constitutive package for linear elasticity
    !
    
    YoungsModulus = EEG (MaterialSetG (iel))    
    PoissonsRatio = VNUG (MaterialSetG (iel)) 
   ! YieldStress = YieldG (MaterialSetG (iel))

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2 
    D44 = C1 / 2.0
    
    C = 0.0 !Initialize C
    
    C(1,1) = D11
    C(1,2) = D12 
    C(1,3) = D12

    C(2,1) = D12 
    C(2,2) = D11 
    C(2,3) = D12

    C(3,1) = D12
    C(3,2) = D12
    C(3,3) = D11

    C(4,4) = D44

    C(5,5) = D44

    C(6,6) = D44
    
    End Subroutine IsotropicLinearElasticGlobal3D
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Subroutine IsotropicLinearElasticGlobal2D  (iel, C)

    !
    !   This subroutine is a constitutive package for global linear elasticity
    !
    Real*8:: C(3,3), test, test2

    YoungsModulus = EEG (MaterialSetG (iel))
 !  YoungsModulus = 10*exp(-StrainG(iel,2,1))!EEG (MaterialSetG (iel))   !Test Newton-Raphson
   ! If(IterationFlag > 0) Then
    !    test=StrainG(iel,2,1)**2
   !     test2=1e5*(StrainG(iel,2,1)**2)
   !    YoungsModulus = 1e5*(1-1e5*(StrainG(iel,2,1)**2))  !!Remove this for test
   ! Endif
    
    PoissonsRatio = VNUG (MaterialSetG (iel)) 
    YieldStress = YieldG (MaterialSetG (iel))

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2 
    D44 = C1 / 2.0
    
    C = 0.0 !Initialize C
    
    If (iSolutionType == 1) Then
        
        C(1,1) = (D11-D12*D12/D11) 
        C(1,2) = (D12-D12*D12/D11)
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
        C(3,3) = D44
    
    Elseif (iSolutionType == 2) Then

        C(1,1) = D11 
        C(1,2) = D12 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
	    C(3,3) = D44     
        
    Else

        Write(6,*)('Material model not in the code yet')
        Stop
    
    Endif

    End Subroutine IsotropicLinearElasticGlobal2D    

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    Subroutine IsotropicLinearElasticLocal2D (iel, C)

    !
    !   This subroutine is a constitutive package for local linear elasticity
    !
    Real*8 C(3,3)
    
    YoungsModulus = EEL (MaterialSetL (iel)) 
    PoissonsRatio = VNUL (MaterialSetL (iel)) 
    YieldStress = YieldL (MaterialSetL (iel))

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2 
    D44 = C1 / 2.0
    
    C = 0.0 !Initialize C
    
    If (iSolutionType == 1) Then
        
        C(1,1) = (D11-D12*D12/D11) 
        C(1,2) = (D12-D12*D12/D11) 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
        C(3,3) = D44 
    
    Elseif (iSolutionType == 2) Then

        C(1,1) = D11 
        C(1,2) = D12 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
	    C(3,3) = D44     
        
    Else

        Write(6,*)('Material model not in the code yet')
        Stop
    
    Endif
      
    End Subroutine IsotropicLinearElasticLocal2D
    
End Module LinearElasticity    