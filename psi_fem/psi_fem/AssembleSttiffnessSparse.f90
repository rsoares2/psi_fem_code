Module AssembleStiffnessMatrix

    Use GlobalInputVariables
    Use LocalInputVariables
    use input_variables
    
	implicit Real*8(A-H,O-Z)

	Contains

Subroutine AssembleGlobalStiffnessSparse3D(iel)
    !
    !   This Subroutine assembles the values from the 3D element stiffness matrix into the global sparse vector
    !   BASED ON THE Subroutine CalculateSparseNonZerosGlobal/SPARSECOHESIVE THAT CALCULATES JCOL AND IROW.
    !

    Do I = 1, NumberOfGlobalNodesPerElement
        IRDOFX = 3 * NODEG(IEL, I) - 2 !First fix
        IRDOFY = IRDOFX + 1
        IRDOFZ = IRDOFX + 2

        DO J = 1, NumberOfGlobalNodesPerElement
            ICDOFX = 3 * NODEG(IEL, J) - 2
            ICDOFY = ICDOFX + 1
            ICDOFZ = ICDOFX + 2

            IF (ICDOFX >= IRDOFX) THEN
                IF (IRDOFX == 1) THEN   !This is the first position at the matrix. It is different than the others.
                    DO K = 1, IRowCompact(IRDOFX + 1) - IRowCompact(IRDOFX)
                        IF (ICDOFX == JColCompact(K)) THEN
                            SparseValuesG(K) = SparseValuesG(K) + KElement(3 * I - 2, 3 * J - 2)
                            SparseValuesG(K + 1) = SparseValuesG(K + 1) + KElement(3 * I - 2, 3 * J - 1)
                            SparseValuesG(K + 2) = SparseValuesG(K + 2) + KElement(3 * I - 2, 3 * J)
                            IF (IRDOFX < ICDOFX) THEN
                                SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) - 1) = SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) - 1) + KElement(3 * I - 1, 3 * J - 2)
                                SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 2) = SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 2) + KElement(3 * I, 3 * J - 2)
                                SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 1) = SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 1) + KElement(3 * I, 3 * J - 1)
                            ENDIF
                            SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1)) = SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1)) + KElement(3 * I - 1, 3 * J - 1)
                            SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) + 1) = SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) + 1) + KElement(3 * I - 1, 3 * J)
                            SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1)) = SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1)) + KElement(3 * I, 3 * J)
                            EXIT
                        ENDIF
                    ENDDO
                ELSE
                    DO K = 1, IRowCompact(IRDOFX + 1) - IRowCompact(IRDOFX)
                        IF (ICDOFX == JColCompact(IRowCompact(IRDOFX) + K - 1)) THEN
                            SparseValuesG(IROWCompact(IRDOFX) + (K - 1)) = SparseValuesG(IROWCompact(IRDOFX) + (K - 1)) + KElement(3 * I - 2, 3 * J - 2)
                            SparseValuesG(IROWCompact(IRDOFX) + (K - 1) + 1) = SparseValuesG(IROWCompact(IRDOFX) + (K - 1) + 1) + KElement(3 * I - 2, 3 * J - 1)
                            SparseValuesG(IROWCompact(IRDOFX) + (K - 1) + 2) = SparseValuesG(IROWCompact(IRDOFX) + (K - 1) + 2) + KElement(3 * I - 2, 3 * J)

                            IF (IRDOFX < ICDOFX) THEN
                                SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) - 1) = SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) - 1) + KElement(3 * I - 1, 3 * J - 2)
                                SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 2) = SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 2) + KElement(3 * I, 3 * J - 2)
                                SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 1) = SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1) - 1) + KElement(3 * I, 3 * J - 1)
                            ENDIF
                            SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1)) = SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1)) + KElement(3 * I - 1, 3 * J - 1)
                            SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) + 1) = SparseValuesG(IROWCompact(IRDOFX + 1) + (K - 1) + 1) + KElement(3 * I - 1, 3 * J)
                            SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1)) = SparseValuesG(IROWCompact(IRDOFX + 2) + (K - 1)) + KElement(3 * I, 3 * J)
                            EXIT
                        ENDIF
                    ENDDO
                ENDIF
            ENDIF
        ENDDO
    ENDDO
End Subroutine AssembleGlobalStiffnessSparse3D

!*********************************************************************************************        
    
    Subroutine  AssembleGlobalStiffnessSparse2D (iel)

    !
    !   This Subroutine assembles the values from the 2D element stiffness matrix into the global sparse vector
    !   based on the Subroutine CalculateSparseNonZerosGlobal/SPARSECOHESIVE that calculates JCOL AND IROW.
    !
    !

	Do I=1,NumberOfGlobalNodesPerElement
		IRDOFX=2*NODEG(IEL,I)-1 !First fix
		IRDOFY=IRDOFX+1
		DO J=1,NumberOfGlobalNodesPerElement
			ICDOFX=2*NODEG(IEL,J)-1
			ICDOFY=ICDOFX+1
			IF(ICDOFX<IRDOFX) Goto 88
			
			IF (IRDOFX == 1) THEN
				DO K=1,IRowCompact(IRDOFX+1)-IRowCompact(IRDOFX)
					IF(ICDOFX == JColCompact(K)) THEN
						SparseValuesG(K)=SparseValuesG(K)+KElement(2*I-1,2*J-1)
						SparseValuesG(K+1)=SparseValuesG(K+1)+KElement(2*I-1,2*J)
						IF(IRDOFX<ICDOFX)Then
		    				SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)=SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)+KElement(2*I,2*J-1)
			            Endif
						SparseValuesG(IROWCompact(IRDOFX+1) + (K-1)) = SparseValuesG(IROWCompact(IRDOFX+1)+(K-1))+KElement(2*I,2*J)
						Goto 88
					Endif
				Enddo
			Else
                DO K=1,IRowCompact(IRDOFX+1)-IRowCompact(IRDOFX)
                    IF(ICDOFX.EQ.JColCompact(IRowCompact(IRDOFX)+K-1)) THEN
						SparseValuesG((IRowCompact(IRDOFX)+K-1))=SparseValuesG((IRowCompact(IRDOFX)+K-1))+KElement(2*I-1,2*J-1)
						SparseValuesG((IRowCompact(IRDOFX)+K-1)+1)=SparseValuesG((IRowCompact(IRDOFX)+K-1)+1)+KElement(2*I-1,2*J)
						IF(IRDOFX<ICDOFX)Then
		    				SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)=SparseValuesG(IROWCompact(IRDOFX+1)+(K-1)-1)+KElement(2*I,2*J-1)
			            Endif
						    SparseValuesG(IRowCompact(IRDOFX+1)+(K-1))=SparseValuesG(IRowCompact(IRDOFX+1)+(K-1))+KElement(2*I,2*J)
						Goto 88
					Endif
				Enddo
			Endif
   88	Enddo
    Enddo
    
    End Subroutine AssembleGlobalStiffnessSparse2D
    
!*********************************************************************************************

	Subroutine AssembleLocalStiffnessSparse2D (JEL)

    !
    !   This Subroutine assembles the values from the 2D element stiffness matrix into the global sparse vector
    !   based on the Subroutine CalculateSparseNonZerosGlobal/SPARSECOHESIVE that calculates JCOL AND IROW.
    !

	DO I=1,NumberOfLocalNodesPerElement
		IRDOFX=2*NODEL(jel,I)-1 !First fix
		IRDOFY=IRDOFX+1
		DO J=1,NumberOfLocalNodesPerElement
			ICDOFX=2*NODEL(jel,J)-1
			ICDOFY=ICDOFX+1
			IF(ICDOFX<IRDOFX) Goto 88
			
			IF (IRDOFX == 1) THEN
				DO K=1,IRowCompactL(IRDOFX+1)-IRowCompactL(IRDOFX)
					IF(ICDOFX == JColCompactL(K)) THEN
						SparseValuesL(K)=SparseValuesL(K)+KElementL(2*I-1,2*J-1)
						SparseValuesL(K+1)=SparseValuesL(K+1)+KElementL(2*I-1,2*J)
						IF(IRDOFX<ICDOFX)Then
		    				SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)=SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)+KElementL(2*I,2*J-1)
			            Endif
						SparseValuesL(IROWCompactL(IRDOFX+1) + (K-1)) = SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1))+KElementL(2*I,2*J)
						Goto 88
					Endif
				Enddo
			Else
                DO K=1,IRowCompactL(IRDOFX+1)-IRowCompactL(IRDOFX)
                    IF(ICDOFX == JColCompactL(IRowCompactL(IRDOFX)+K-1)) THEN
						SparseValuesL((IRowCompactL(IRDOFX)+K-1))=SparseValuesL((IRowCompactL(IRDOFX)+K-1))+KElementL(2*I-1,2*J-1)
						SparseValuesL((IRowCompactL(IRDOFX)+K-1)+1)=SparseValuesL((IRowCompactL(IRDOFX)+K-1)+1)+KElementL(2*I-1,2*J)
						IF(IRDOFX<ICDOFX)Then
		    				SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)=SparseValuesL(IROWCompactL(IRDOFX+1)+(K-1)-1)+KElementL(2*I,2*J-1)
			            Endif
						    SparseValuesL(IRowCompactL(IRDOFX+1)+(K-1))=SparseValuesL(IRowCompactL(IRDOFX+1)+(K-1))+KElementL(2*I,2*J)
						Goto 88
					Endif
				Enddo
			Endif
   88	Enddo
    Enddo

	END Subroutine AssembleLocalStiffnessSparse2D

!*********************************************************************************************

	Subroutine AssembleLocalFRMatrix2D (Jel, FR_Element)

    !
    !   This Subroutine assembles the values from the 2D element FR matrix into the global one
    !

    Real*8:: FR_Element(6)

	Do  i = 1, 3

        K = 2 * i - 1
		IRDOFX = 2 * NodeL(jel, i)-1
		FRGlobal(IRDOFX) = FRGlobal(IRDOFX) - FR_Element(K)

		K = 2 * i
		IRDOFY = IRDOFX+1
		FRGlobal(IRDOFY) = FRGlobal(IRDOFY) - FR_Element(K)
        
    Enddo

    End Subroutine AssembleLocalFRMatrix2D

End Module AssembleStiffnessMatrix