module LocalInputHeader
    
    use LocalInputVariables
        
    implicit none 
    integer :: i
   
    Contains

    subroutine readLocalInputHeader
    
        ! This subroutine applies when the simulation has more than one scale (multiscale cases).
        ! part i: number of local analyses, number of local nodes per element
        ! the subroutine readLocalInputHeader reads the following parameters:
        ! 1. numberOfLocalAnalyses - number of local RVE's to be analyzed
        ! 2. numberOfLocalNodesPerElement - number of nodes in each element - local scale
    
        write(6, 1003)
        read(5, *) numberOfLocalAnalyses, numberOfLocalNodesPerElement
        write(6, 1001) numberOfLocalAnalyses, numberOfLocalNodesPerElement

    1001 format (5x, 'The Local Input File is as follows:', //, 10x, 'Part I: Number of local analyses, number of local nodes per element', //, &
         &15x, 'Number of local analyses: ', i7, /, 15x, 'Number of nodes per element: ', i7, /, 15x)

    1003 format (5x, 'Multiscale Analysis In Effect', /)

        ! part ii: nodes, elements, cohesive zones, boundary conditions
    
        ! 1. numberOfLocalNodes - number of local nodes
        ! 2. numberOfLocalElements - number of local elements
        ! 3. numberOfLocalDisplacementBC - number of local displacement boundary conditions
        ! 4. iLocalCohesiveZones - = 0, no cohesive zones
        ! 5. iLocalBC = 0 - transfer strain from global to local scale
        !               = 1 - transfer stress from global to local scale
    
        read(5, *) numberOfLocalNodes, numberOfLocalElements, numberOfLocalDisplacementBC, iLocalCohesiveZones, iLocalBC
        write(6, 1002) numberOfLocalNodes, numberOfLocalElements, numberOfLocalDisplacementBC, iLocalCohesiveZones, iLocalBC

    1002 format (/, 10x, 'Part II: Nodes, elements, cohesive zones, boundary conditions', //, &
         &15x, 'Number of local nodes: ', i7, /, 15x, 'Number of local elements: ', &
         &i7, /, 15x, 'Number of local displacement boundary conditions: ', i7, /, 15x, 'Local cohesive zone flag: ', i5, /, 15x, &
         &'iLocalBC Flag =', i2, ' ( = 0: Need to supply nodes on the boundary)', /, 33x, '( = 1: Automatic calculation of RVE boundary nodes)') 
    end subroutine readLocalInputHeader
    
end module LocalInputHeader