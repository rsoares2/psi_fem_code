module CalculateStifnessMatrix3D

    use GlobalInputVariables
    use initialize_global_variables
    
    use ShapeFunctions
    use Quadrature
    use ReadMaterialType
    use AssembleStiffnessMatrix
    
    use OMP_LIB
    
	implicit none

	contains

    subroutine CalculateGlobalStiffnessMatrixSparse3D
        ! This subroutine calculates the global sparse stiffness matrix for 3D meshes
        integer :: nIcon(2), iElastic, iViscoElastic
        integer :: iel, iIntPoint
        
        do iEl = 1, numberOfGlobalElements ! Major loop over all elements
            if (mod(iEl, iPrintToScreen) == 0) then ! Avoids printing every node, only prints intervals of 5.
                write(*, 805) iEl
            805 format (10x, 'Set up Global Stiffness Matrix for Element =', i8)
            endif

            ! Initialize the element stiffness matrix
            kElement = 0.0D0

            ! Calculate element stiffness matrix
            call SetUpQuadraturePoints(nGIntegrationPoints, numberOfGlobalNodesPerElement)

            do iIntPoint = 1, nGIntegrationPoints
                call ShapeFunction3D(iEl, iIntPoint) ! Set up B matrix
                call ReadGlobalMaterialType3D(iEl, iIntPoint, 1) ! Add material properties (1, means called from stiffness matrix)
                call CalculateElementStiffnessMatrix3D
            enddo

            call AssembleGlobalStiffnessSparse3D(iEl) ! Assembles each local element into a global stiffness matrix
            dSigRG = 0.0D0 ! Necessary if viscoelasticity is used
        enddo

        call PostCalculationStiffness3D
        call ApplyBCStiffness3D
    end subroutine CalculateGlobalStiffnessMatrixSparse3D

    
    Subroutine CalculateElementStiffnessMatrix3D
   
    !   
    !   This subroutine calculates the element stiffness matrix 
    !   
    !   It first multiplies B transpose matrix (deriv. of shape function) * C matrix (Stiffness/Modulus) * B matrix
    !   and numeriCally integrates. Using DGEMM form MKL to multiply matrix.
            
            Call DGEMM( 'T'  , 'N'  , iSizeBMatrix, 6 , 6 , 1.0D0 , BG , 6 , C , 6 , 0.0D0 , KElement_Temp , iSizeBMatrix ) !MKL matrix multiplication function

            Call DGEMM( 'N'  , 'N'  , 12 , 12 , 6 , 1.0D0 , KElement_Temp , 12 , BG , 6 , 0.0D0 , KElement , 12 ) 
            
            KElement = KElement* DETJ * Weight  !Uses output from DGEMM function and integrates
   
    End Subroutine CalculateElementStiffnessMatrix3D

!*********************************************************************************************        
    
    Subroutine ApplyBCStiffness3D
   
    integer :: jc, nthr, jj, i
    !   
    !   This subroutine applies boundary conditions to the global stiffness matrix 
    !   

	JC=1

    !$omp parallel private(JJ)
    nthr = OMP_GET_NUM_THREADS()
    !Print*, 'OpenMP number of threads: ', nthr

    !$omp do
    Do i = 1, NumberOfDisplacementBC
        jj = NGlobalDOF (i, KTime)
        SparseValuesG (IROWCompact(jj)) = KBig
    Enddo
    !$omp end do

    !$omp end parallel
    
end subroutine ApplyBCStiffness3D
   
end module calculateStifnessMatrix3D
    