Module SparseConectivity
    
    Use GlobalInputVariables
    use input_variables
    use psi_fem_utilities, only: PrintReadInputToScreen

    implicit Real (A-H, O-Z)

    Contains    

    Subroutine  SparseInitialConnectivity

    !
    !   This subroutine assembles the JCOL and IROW arrays for the sparse matrix in CSR format
    !   JCOL AND IROW represent the vectors with node information. 
    !   
    !   There is also a need to organize the columns in ascending order. This is done in a dIfferent Subroutine (inside SPARSE INITIAL module).
    !   NZG = NUMBER OF NONZEROS IN GLOBAL SCALE

    Allocate ( IROW(NumberOfGlobalNodes+1) )
    Allocate ( JCOL(NZG) )
    LinkedNodeConnectivity(:,iNGlobalNodes)=LinkedNodeConnectivity(:,iNGlobalNodes)-1   !NEED TO REDUCE 1, B/C II OR JJ IS ALSO INCLUDED.
    IPOSITION=0 !This variable keeps track of number of non-zeros and where to include
    
    !
    !   Update the Number of Non-Zeros to include the DOFs and not only the NumberOfGlobalNodes
    !
    
    If(NumberOfDimensions==2)Then
        NZG = (NZG*NumberOfDimensions)+((NZG*NumberOfDimensions)-NumberOfGlobalNodes)
        Allocate(IRowCompact(2*NumberOfGlobalNodes+1))
    ELSE
        NZG = (NZG*NumberOfDimensions)+((NZG*NumberOfDimensions)-NumberOfGlobalNodes)+((NZG*NumberOfDimensions)-NumberOfGlobalNodes)-NumberOfGlobalNodes
        Allocate(IRowCompact(3*NumberOfGlobalNodes+1))
    EndIf
    
    IROW(1)=1
    IRowCompact(1)=1
    
    Do I=1, NumberOfGlobalNodes
        Do J=1,LinkedNodeConnectivity(I,iNGlobalNodes)
            IPOSITION=IPOSITION+1
            JCOL(IPOSITION)=LinkedNodeConnectivity(I,J)
        Enddo
        IROW(I+1)=IPOSITION+1
        
        !
        ! Calculate IRowCompact, which includes the DOF. It is an "expanded" version of the IRow
        !
        
        If (NumberOfDimensions==2) Then
        
            Index1=(2*I-1)+1
            Index2=Index1+1
            IDIfference=IRow(I+1)-IRow(I)
            IRowCompact(Index1)=IDIfference*NumberOfDimensions+IRowCompact(Index1-1)
            IRowCompact(Index2)=IDIfference*NumberOfDimensions-1+IRowCompact(Index1)
    
        ElseIf (NumberOfDimensions==3) Then
   
            Index1=(3*I-2)+1
            Index2=Index1+1
            Index3=Index1+2
            IDIfference=IRow(I+1)-IRow(I)
            IRowCompact(Index1)=IDIfference*NumberOfDimensions+IRowCompact(Index1-1)
            IRowCompact(Index2)=IDIfference*NumberOfDimensions-1+IRowCompact(Index1)    
            IRowCompact(Index3)=IDIfference*NumberOfDimensions-2+IRowCompact(Index2)

        EndIf        
               
    Enddo
    
 !   If(NumberOfGlobalNodes>81)Then  !this If is due to array bounds If NumberOfGlobalNodes<27*3
      !  do ir=1,iNGlobalNodes
    !        ie=LinkedNodeConnectivity(ir,iNGlobalNodes)
   !         Write(778,182)i,ie
   !     Enddo
    182 format(2i8)
    !EndIf
    
    Deallocate(LinkedNodeConnectivity)

    Call ReorganizeGlobalSparseVectors_NEW  !This is in SparseInitial, however for this new way of populating the sparse matrix
    
    call PrintReadInputToScreen (Enum_SparseInitialConnectivity)

    END Subroutine  SparseInitialConnectivity

    Subroutine reorganizeGlobalSparseVectors_NEW

        allocate(jColCompact(nZG))
        
        kk = 0
        do i = 1, numberOfGlobalNodes
            do j = iRow(i), iRow(i + 1) - 1
                n1 = jCol(j)
                do k = j + 1, iRow(i + 1) - 1
                    n2 = jCol(k)
                    if (n1 > n2) then
                        jCol(j) = jCol(k)
                        jCol(k) = n1
                        n1 = n2
                    endif
                enddo
            enddo

            if (numberOfDimensions == 2) then
                index1 = 2 * i - 1
                indexPosition = iRowCompact(index1)

                do ij = iRow(i), iRow(i + 1) - 1
                    jColCompact(indexPosition) = 2 * jCol(ij) - 1
                    indexPosition = indexPosition + 1
                    jColCompact(indexPosition) = 2 * jCol(ij)
                    indexPosition = indexPosition + 1
                enddo

                index1 = 2 * i
                indexPosition = iRowCompact(index1)

                !this is for the second line due to 2D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
        
                do ij = iRow(i), iRow(i + 1) - 1
                    if (ij /= iRow(i)) then
                        jColCompact(indexPosition) = 2 * jCol(ij) - 1
                        indexPosition = indexPosition + 1
                    endif
                    jColCompact(indexPosition) = 2 * jCol(ij)
                    indexPosition = indexPosition + 1
                enddo

            elseif (numberOfDimensions == 3) then
                index1 = 3 * i - 2
                indexPosition = iRowCompact(index1)

                do ij = iRow(i), iRow(i + 1) - 1
                    jColCompact(indexPosition) = 3 * jCol(ij) - 2
                    indexPosition = indexPosition + 1
                    jColCompact(indexPosition) = 3 * jCol(ij) - 1
                    indexPosition = indexPosition + 1
                    jColCompact(indexPosition) = 3 * jCol(ij)
                    indexPosition = indexPosition + 1
                enddo

                index1 = 3 * i - 1
                indexPosition = iRowCompact(index1)

                !this is for the second and third lines contribution to the stiffness matrix due to 3D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                do ij = iRow(i), iRow(i + 1) - 1
                    if (ij /= iRow(i)) then
                        jColCompact(indexPosition) = 3 * jCol(ij) - 2
                        indexPosition = indexPosition + 1
                    endif
                    jColCompact(indexPosition) = 3 * jCol(ij) - 1
                    indexPosition = indexPosition + 1
                    jColCompact(indexPosition) = 3 * jCol(ij)
                    indexPosition = indexPosition + 1
                enddo

                index1 = 3 * i
                indexPosition = iRowCompact(index1)

                !this is for the second and third lines contribution to the stiffness matrix due to 3D nature of problem.
                !We are storing only symmetric part, therefore any index i<j is not stored
                do ij = iRow(i), iRow(i + 1) - 1
                    if (ij /= iRow(i)) then
                        jColCompact(indexPosition) = 3 * jCol(ij) - 2
                        indexPosition = indexPosition + 1
                        jColCompact(indexPosition) = 3 * jCol(ij) - 1
                        indexPosition = indexPosition + 1
                    endif
                    jColCompact(indexPosition) = 3 * jCol(ij)
                    indexPosition = indexPosition + 1
                enddo
            endif
        enddo

    End Subroutine reorganizeGlobalSparseVectors_NEW    

End Module SparseConectivity    