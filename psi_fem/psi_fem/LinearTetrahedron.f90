Module TetrahedronShapeFunction

    Use GlobalInputVariables
    use input_variables
    
	implicit none !Real*8 (A-H,O-Z)

	Contains

	Subroutine ShapeTetrahedronGlobal (iel)
	
    !   This subroutine calculates the shape functions for 4-noded linear tetrahedrons

    real(kind=8) :: BC(12, 6)
    real(kind=8) :: DXDS(3, 3)
    real(kind=8) :: DSDX(3, 3)
    real(kind=8) :: DPSIX(4)
    real(kind=8) :: DPSIY(4)
    real(kind=8) :: DPSIZ(4)
    real(kind=8) :: PSI(4)
    real(kind=8) :: XGauss(3)
    real(kind=8) :: invDetJ
    real(kind=8), parameter :: XI = 0.2500000000  ! Quadrature of 1 point over a tetrahedron
    real(kind=8), parameter :: ETA = 0.2500000000
    real(kind=8), parameter :: ZETA = 0.2500000000
    real(kind=8), parameter :: Weight = 1.0/6.0
    real(kind=8) :: cofactors(3, 3)
    real(kind=8) :: dpsi
    real(kind=8), dimension(4, 3) :: Ci
    integer :: i
    integer :: iel
    integer :: i3m2
    integer :: i3m1
    integer :: i3

    !   Transform coordinates. 
    !   Convert:  X=x1*N1+x2*N2+x3*N3+x4*N4
    !   To:       X=x1*(1-Ksi-Eta-Si)+x2*Ksi+x3*Eta+x4*Si

    ! Calculate first X1, X21, X31, X41 and then Y1, Y21... and Z1, Z21, Z31, Z41.

    Ci(1, :) = [x_coord(NODEG(IEL, 1)), y_coord(NODEG(IEL, 1)), z_coord(NODEG(IEL, 1))]

    do i = 2, 4
        Ci(i, :) = [x_coord(NODEG(IEL, i)), y_coord(NODEG(IEL, i)), z_coord(NODEG(IEL, i))] - Ci(1, :)
    end do
    
    DXDS = 0.0
    
    !DXDS(:,1) is DXDXI, DYDXI, DZDXI
    !DXDS(:,2) is DXDETA, DYDETA, DZDETA
    !DXDS(:,3) is DXDZETA, DYDZETA, DZDZETA
    do i = 1, 3
        XGauss(i) = Ci(1, i) + Ci(2, i) * XI + Ci(3, i) * ETA + Ci(4, i) * ZETA
        DXDS(:, i) = Ci(2:4, i)
    end do
    
    ! CALCULATE COFACTORS AND DETJ
    cofactors(1, 1) = DXDS(2, 2) * DXDS(3, 3) - DXDS(3, 2) * DXDS(2, 3)
    cofactors(1, 2) = DXDS(3, 1) * DXDS(2, 3) - DXDS(2, 1) * DXDS(3, 3)
    cofactors(1, 3) = DXDS(2, 1) * DXDS(3, 2) - DXDS(3, 1) * DXDS(2, 2)
    cofactors(2, 1) = DXDS(3, 2) * DXDS(1, 3) - DXDS(1, 2) * DXDS(3, 3)
    cofactors(2, 2) = DXDS(1, 1) * DXDS(3, 3) - DXDS(3, 1) * DXDS(1, 3)
    cofactors(2, 3) = DXDS(3, 1) * DXDS(1, 2) - DXDS(1, 1) * DXDS(3, 2)
    cofactors(3, 1) = DXDS(1, 2) * DXDS(2, 3) - DXDS(2, 2) * DXDS(1, 3)
    cofactors(3, 2) = DXDS(2, 1) * DXDS(1, 3) - DXDS(1, 1) * DXDS(2, 3)
    cofactors(3, 3) = DXDS(1, 1) * DXDS(2, 2) - DXDS(2, 1) * DXDS(1, 2)

    DETJ = DOT_PRODUCT(DXDS(1, 1:3), cofactors(1, :))        

    if (abs(DETJ) <= 0.0D-10) then
        write(6, *) 'IN ShapeTetrahedronGlobal - IEL = ', IEL, '   DET J = ', DETJ
        write(6, *) 'Cofactors = ', cofactors(1, 1), cofactors(1, 2), cofactors(1, 3)
        write(6, *) 'DXDS = ', DXDS(1,1), DXDS(1,2), DXDS(1,3)
        stop
    endif

    ! CALCULATE DSDX
    invDetJ = 1.0 / DETJ

    do i = 1, 3
        dsdx(:, i) = cofactors(:, i) * invDetJ
    end do

    ! CALCULATE D(PSI)/DX
    do i = 1, 4
        if (i == 1) then
            dpsix(i) = -sum(DSDX(:,1))
            dpsiy(i) = -sum(DSDX(:,2))
            dpsiz(i) = -sum(DSDX(:,3))
        else
            dpsix(i) = DSDX(i-1,1)
            dpsiy(i) = DSDX(i-1,2)
            dpsiz(i) = DSDX(i-1,3)
        endif
    end do
    
    ! INITIALIZE B MATRIX
    
    BG = 0.
    
    ! FORM B AND BT (Sequence is XX, YY, ZZ, YZ, XZ, XY)
    do i = 1, NumberOfGlobalNodesPerElement
        I3 = 3 * i
        I3M2 = I3 - 2
        I3M1 = I3 - 1
        DPSI = DPSIX(i)

        BG(1,I3M2) = BG(1,I3M2) + DPSI
        BG(5,I3M2) = BG(5,I3M2) + DPSIZ(i)
        BG(6,I3M2) = BG(6,I3M2) + DPSIY(i)

        DPSI = DPSIY(i)

        BG(2,I3M1) = BG(2,I3M1) + DPSI
        BG(4,I3M1) = BG(4,I3M1) + DPSIZ(i)
        BG(6,I3M1) = BG(6,I3M1) + DPSIX(i)

        DPSI = DPSIZ(i)

        BG(3,I3) = BG(3,I3) + DPSI
        BG(4,I3) = BG(4,I3) + DPSIY(i)
        BG(5,I3) = BG(5,I3) + DPSIX(i)
    enddo      

    End Subroutine ShapeTetrahedronGlobal

      Subroutine TetrahedronCentroid(i,a1,a2,a3)

        Real(kind=8) :: a1,a2,a3
        integer:: i
        
        a1 = (x_coord(NODEG(I,1))+x_coord(NODEG(I,2))+x_coord(NODEG(I,3))+x_coord(NODEG(I,4)))/4
        a2 = (y_coord(NODEG(I,1))+y_coord(NODEG(I,2))+y_coord(NODEG(I,3))+y_coord(NODEG(I,4)))/4
        a3 = (z_coord(NODEG(I,1))+z_coord(NODEG(I,2))+z_coord(NODEG(I,3))+z_coord(NODEG(I,4)))/4


      End Subroutine TetrahedronCentroid

    End Module TetrahedronShapeFunction