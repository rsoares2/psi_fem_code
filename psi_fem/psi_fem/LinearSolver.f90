    Include 'mkl_dss.f90'
    Include 'mkl_pardiso.f90'

    Module linear_system_solvers

    Use GlobalInputVariables
    use initialize_global_variables
    use input_variables
    use psi_fem_utilities
    
    Use OMP_LIB
    Use MKL_DSS

    implicit Real*8(A-H,O-Z)

    Contains

    Subroutine PardisoSolver

    !
    !   This subroutine solves the linear system K * u = F, where:
    !
    !   K = Stiffness matrix in sparse form
    !   u = unknowns (displacements)
    !   F - Force matrix
    !

    Integer, Parameter :: DP = Kind(1.0D0)
    Integer IPARAM(6), IPATH
    Real*8 TIME_SPARSE
    Real*8 RPARAM(5)
    Integer IROWINDEX(NZG)
    TYPE(MKL_DSS_HANDLE) :: handle ! Allocate storage for the solver handle.
    Integer perm(1)
    Integer*8 pt(64)
    Integer maxfct, mnum, mtype, phase, n, nrhs, error, msglvl
    Integer iparm(64)
    Integer idum
    Real*8 waltime1, waltime2, ddum
    Real*8, Allocatable, dimension ( : ) :: values( : )

    perm(1) = 0
    N = NGTotalDOF
    NZG_Solver = NZG

    IPATH = 1

    !.. Fill all arrays containing matrix data.
    DATA nrhs /1/, maxfct /1/, mnum /1/
    !..
    !.. Set up PARDISO control parameter
    !..

    !$omp parallel
    !$omp single
    nthr = OMP_GET_NUM_THREADS()
    Print*, 'OpenMP number of threads: ', nthr

    !$omp end single
    !$omp end parallel

    !  PRINT*, 'OpenMP number of threads: ', nthr

    Call MKL_SET_DYNAMIC (0)
    Call MKL_SET_NUM_THREADS(nthr)

    !  PRINT*, 'MKL  number of threads: ', mkl_get_max_threads()
    !https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-fortran/top/sparse-solver-routines/intel-mkl-pardiso-parallel-direct-sparse-solver-interface/pardiso-iparm-parameter.html
    iparm = 0

    iparm(1) = 1 ! Input: = 0 --> iparm (2) - iparm (64) are filled with default values.
    iparm(2) = 2 ! Input: Fill-in reducing ordering for the input matrix. = 2 -->	The nested dissection algorithm from the METIS package [Karypis98] .
    iparm(3) = 0 ! Reserved. Set to zero.
    iparm(4) = 0 ! Input: no iterative-direct algorithm
    iparm(5) = 0 ! Input: This parameter controls whether user supplied fill-in reducing permutation. = 0 --> User permutation in the perm array is ignored.
    iparm(6) = 0 ! Input: Write solution on x. = 0 --> The array x contains the solution; right-hand side vector b is kept unchanged.
    iparm(7) = 0 ! Output: Number of iterative refinement steps performed.
    iparm(8) = 9 ! Iterative refinement step. On entry to the solve and iterative refinement step, iparm (8) must be set to the maximum number of iterative refinement steps that the solver performs
    iparm(9) = 0 ! Reserved. Set to zero.
    iparm(10) = 8 ! Input: Pivoting perturbation. The default value for symmetric indefinite matrices ( mtype =-2, mtype =-4, mtype =6), eps = 10 -8 .
    iparm(11) = 0 ! Input: Scaling vectors. = 0 --> Disable scaling. Default for symmetric indefinite matrices.
    iparm(12) = 0 ! Input: Solve with transposed or conjugate transposed matrix A. = 0 --> Solve a linear system AX = B .
    iparm(13) = 0 ! Input: Improved accuracy using (non-) symmetric weighted matching. = 0 --> Disable matching. Default for symmetric indefinite matrices.
    iparm(14) = 0 ! Output: number of perturbed pivots
    iparm(15) = 0 ! Output: Peak memory on symbolic factorization. The total peak memory in kilobytes that the solver needs during the analysis and symbolic factorization phase. This value is only computed in phase 1.
    iparm(16) = 0 ! Output: Permanent memory on symbolic factorization.
    iparm(17) = 0 ! Output: Size of factors/Peak memory on numerical factorization and solution.
    iparm(18) = -1 ! Output: number of nonzeros in the factor LU
    iparm(19) = -1 ! Output: Mflops for LU factorization
    iparm(20) = 0 ! Output: Numbers of CG Iterations
    iparm(21) = 0 ! Input: Pivoting for symmetric indefinite matrices.
    iparm(22) = 0 ! Output: Inertia: number of positive eigenvalues.
    iparm(23) = 0 ! Output: Inertia: number of negative eigenvalues.
    iparm(24) = 0 ! Input: Parallel factorization control (useful for more than 8 threads)
    iparm(27) = 1 ! Input: Matrix checker. = 1 Intel� MKL PARDISO checks integer arrays ia and ja . In particular, Intel� MKL PARDISO checks whether column indices are sorted in increasing order within each row.
    iparm(28) = 0 ! Input: Single or double precision Intel� MKL PARDISO. = 0 --> Input arrays ( a , x and b ) and all internal arrays must be presented in double precision.
    iparm(29) = 0 ! Reserved. Set to zero.
    iparm(30) = 0 ! Output: Number of zero or negative pivots.
    iparm(31) = 0 ! Input: Partial solve and computing selected components of the solution vectors.
    iparm(32) = 0 ! Reserved. Set to zero.
    iparm(33) = 0 ! Reserved. Set to zero.
    iparm(60) = 0 ! Intel� MKL PARDISO mode. Switches between in-core (IC) and out-of-core (OOC)
    error = 0 ! initialize error flag
    msglvl = 1 ! print statistical information
    mtype = 2 ! real and symmetric positive definite

    !C.. Initiliaze the internal solver memory pointer. This is only
    !C necessary for the FIRST Call of the PARDISO solver.

    pt = 0
    ! Using CSR3 Matrix Storage Format
    !https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-fortran/top/sparse-solver-routines/intel-mkl-pardiso-parallel-direct-sparse-solver-interface/pardiso.html
    
    !C.. Reordering and Symbolic Factorization, This step also allocates
    !C all memory that is necessary for the factorization
    phase = 11 ! only reordering and symbolic factorization
    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, SparseValuesG, IRowCompact, JColCompact, idum, nrhs, iparm, msglvl, ddum, ddum, error)
    !  Write(*,*) 'Reordering completed ... '
    If (error .NE. 0) Then
        Write(*,*) 'The following ERROR was detected: ', error
        Stop 1
    End If
    !.. Factorization.
    phase = 22 ! only factorization
    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, SparseValuesG, IRowCompact, JColCompact, idum, nrhs, iparm, msglvl, ddum, ddum, error)
    !      Write(*,*) 'Factorization completed ... '
    IF (error .NE. 0) THEN
        Write(*,*) 'The following ERROR was detected: ', error
        Stop 1
    Endif

    Allocate ( DeltaDisplacementG (NGTotalDOF) )

    !.. Back substitution and iterative refinement
    iparm(8) = 2 ! max numbers of iterative refinement steps
    phase = 33 ! only factorization

    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, SparseValuesG, IRowCompact, JColCompact, idum, nrhs, iparm, msglvl, ForceMatrixG, DeltaDisplacementG, error)
    !   Write(*,*) 'Solve completed ... '

    !.. Termination and release of memory

    phase = -1 ! release internal memory
    Call pardiso (pt, maxfct, mnum, mtype, phase, NGTotalDOF, ddum, idum, idum,idum, nrhs, iparm, msglvl, ddum, ddum, error)
    !write(6,12)time,ForceMatrixG(3), ForceMatrixG(2), sparsevaluesg(20), sparsevaluesg(30)

    !If (it == 1 .AND. iTerationFlag /= 2) Then    !Do not update stiffness and keep it stored for next iteration
    If (iTerationFlag == 1 .or. it == 1 .AND. iTerationFlag == 2) Then
        If ( Associated (SparseValuesG)) Deallocate (SparseValuesG)
    Endif

    !
    !   Add the solution (Displacements) to the global displacements.
    !
    DisplacementGold = DisplacementG
    DisplacementG = DisplacementG + DeltaDisplacementG
write(*,*)'DeltaDisplacementG 5', DeltaDisplacementG(5)
12  format (12e15.7)
    End Subroutine PardisoSolver

    Subroutine SparseSolver

    INTEGER, PARAMETER :: DP = KIND(1.0D0)
    INTEGER IPARAM(6), IPAT
    REAL(DP) RPARAM(5)
    !EXTERNAL DL4LXG, DLSLXG, WRRRN
    TYPE(MKL_DSS_HANDLE) :: handle ! Allocate storage for the solver handle.
    INTEGER perm(1)
    REAL(DP), ALLOCATABLE, dimension ( : ) :: solution( : )

    perm(1) = 0
    IPATH = 1

    ! Initialize the solver.
    error = dss_create( handle, MKL_DSS_DEFAULTS )
    IF (error /= MKL_DSS_SUCCESS) GOTO 999
    ! Define the non-zero structure of the matrix
    error = dss_define_structure( handle, MKL_DSS_SYMMETRIC, IRowCompact, NGTotalDOF, NGTotalDOF, JColCompact, NZG )

    !print*, 'IRowCompact: '
    !print*, IRowCompact
    !print*, 'JColCompact:'
    !print*, JcolCompact

    IF (error /= MKL_DSS_SUCCESS) GOTO 999
    ! Reorder the matrix.

    error = dss_reorder( handle, MKL_DSS_DEFAULTS, perm )
    IF (error /= MKL_DSS_SUCCESS) GOTO 999
    Write(*,800)
800 format (10x,'Sparse solver - Reorder the matrix')

    ! Factor the matrix.
    error = dss_factor_real( handle, MKL_DSS_DEFAULTS, SparseValuesG )
    IF (error /= MKL_DSS_SUCCESS) GOTO 999
    Write(*,801)
801 format (10x,'Sparse solver - Factor the matrix (LU)')

    ! Allocate the "DeltaDisplacementG" solution vector and solve the problem.
    ALLOCATE( DeltaDisplacementG( NGTotalDOF ) )
    error = dss_solve_real(handle, MKL_DSS_DEFAULTS, ForceMatrixG, 1, DeltaDisplacementG )
    IF (error /= MKL_DSS_SUCCESS) GOTO 999
    Write(*,802)
802 format (10x,'Sparse solver - Solve the matrix')
write(*,952) DeltaDisplacementG(1:5)
952 Format('in Linear Solver',/,5d15.4)
    ! Print Out the determinant of the matrix
    error = dss_delete( handle, MKL_DSS_DEFAULTS )
    IF (error /= MKL_DSS_SUCCESS) GOTO 999

    If (iTerationFlag == 1 .or. it == 1 .AND. iTerationFlag == 2) Then    
        !IF ( ASSOCIATED( IRowCompact) ) DEALLOCATE( IRowCompact )
        !IF ( ASSOCIATED( JColCompact ) ) DEALLOCATE( JColCompact )
        IF ( ASSOCIATED( SparseValuesG ) ) DEALLOCATE( SparseValuesG )
    Endif
    DisplacementGold = DisplacementG
    DisplacementG = DisplacementG + DeltaDisplacementG

12  format (12e15.7)

999 CONTINUE

    IPARAM(5)=1E7

   ! ForceMatrixG = DeltaDisplacementG

  !  IF ( ALLOCATED( solution ) ) DEALLOCATE( solution )
 !   IF ( ALLOCATED( DeltaDisplacementG ) ) DEALLOCATE( DeltaDisplacementG )

    call PrintReadInputToScreen(Enum_SparseSolver)

    END SUBROUTINE SPARSESOLVER

    End Module linear_system_solvers