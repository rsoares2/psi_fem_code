module increment

    Use GlobalInputVariables
    use input_variables
    
    implicit none !Real*8(A-H,O-Z)

    Contains

    subroutine TimeCycleIncrement
        
        real(kind=8) :: time_End
    
        ! This subroutine increments the current time for multiple cycles

        time_End = timLim
        kTime = 1
        dTime = deltaTime(kTime)
        fac = timeFactor(kTime)
        timLim = time_End + timeEndTB(kTime)

    end subroutine TimeCycleIncrement

    subroutine TimeCheckIncrement

        ! This subroutine checks the current time and increments to the next time step
        
        real(kind=8) :: time_End

        if (abs(timlim - time) < 1e-8) then
            time = timlim
        endif

        if (time >= timlim) then
            kTime = kTime + 1
            dTime = deltaTime(kTime)
            fac = timeFactor(kTime)
        endif

        if (iCycl == 1) then
            timLim = timeEndTB(kTime)
        else
            timLim = time_End + timeEndTB(kTime)    !this time_End may need to be a global variable if more than 1 cycle
        endif

        time = time + dTime

    end subroutine TimeCheckIncrement

    subroutine ForceIncrement

        if (numberOfForces /= 0) then
            if (iPavement == 1 .and. iBodyForce == 1 .and. incr == 1) then
                !fglg = 0
               
                GlobalNodeForcesIncrement(:)%x = 0.0
                GlobalNodeForcesIncrement(:)%y = 0.0
                GlobalNodeForcesIncrement(:)%z = 0.0
            else
                ! do i = 1, ngTotalDOF
                !    fglg(i) = forceg(i) / fac + fglg(i)
                !enddo
                GlobalNodeForcesIncrement(:)%x = GlobalNodeForcesIncrement(:)%x + GlobalNodeForces(:)%x / fac
                GlobalNodeForcesIncrement(:)%y = GlobalNodeForcesIncrement(:)%y + GlobalNodeForces(:)%y / fac
                GlobalNodeForcesIncrement(:)%z = GlobalNodeForcesIncrement(:)%z + GlobalNodeForces(:)%z / fac
            endif
        endif

    end subroutine ForceIncrement

end module increment