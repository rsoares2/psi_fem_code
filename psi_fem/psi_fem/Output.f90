Module OutputModule
    
    use GlobalInputVariables
    use input_variables
    use Omp_lib
    use TecplotOutputModule
    use TecplotOutputFactorModule
    use WriteGlobalOutput
    use WriteGlobalOutputBinary

    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine Output2DG

    If (iOutputGlobal == 1) Call WriteOutputG
   
  !  If (iOutputLocal == 1) Call WriteLocalOutput

    If (iTecplotGlobal == 1) Then
        
        Call WriteTecplotGlobalOutput
        Call WriteTecplotGlobalFactorOutput

    Endif
    
  !  If (iTecplotLocal == 1) Call WriteTecplotLocalOutput

  !  If (iPavement == 1) Call WritePavementOutput

    End Subroutine Output2DG

    Subroutine Output3DG

    If (iOutputGlobal == 1) Call WriteOutputG
   
    If (iOutputGlobalBinary == 1) Call WriteOutputBinaryG
    
  !  If (iTecplotGlobal == 1) Then
  !      Call WriteTecplotGlobalOutput
  !      Call WriteTecplotGlobalFactorOutput
  !  Endif
    
!    If (iTecplotGlobalBinary == 1) Call WriteTecplotGlobalOutputBinary

  !  If (iOutputLocal == 1) Call WriteLocalOutput
  !  If (iTecplotLocal == 1) Call WriteTecplotLocalOutput
  !  If (iPavement == 1) Call WritePavementOutput

    End Subroutine Output3DG
    
End Module OutputModule