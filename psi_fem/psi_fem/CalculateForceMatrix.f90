Module calculate_force_matrix

    Use GlobalInputVariables
    use input_variables
    Use CalculateForceMatrix3D
    Use CalculateForceMatrix2D
    Use CalculateForceMatrix2DL
    use psi_fem_utilities, only: PrintReadInputToScreen
    
	implicit Real*8(A-H,O-Z)

	Contains

subroutine GlobalForceMatrix

    if (NumberOfDimensions == 2) then
        call InitializeGlobalForceMatrix2D
        call CalculateGlobalForceMatrix2D
    elseif (NumberOfDimensions == 3) then
        call InitializeGlobalForceMatrix3D
        call CalculateGlobalForceMatrix3D
    endif
    
    call PrintReadInputToScreen(Enum_AssembleForceMatrix)
    
end subroutine GlobalForceMatrix
    
subroutine LocalForceMatrix

    !   This subroutine calculates the local "force" matrix
    !
    !   There is no real force matrix in local scale, so this only calculates
    !   the contribution from CZ to the FR matrix. (old FRTRACTION)

    if (NumberOfDimensions == 2) then
        call InitializeLocalForceMatrix2D
        call CalculateLocalForceMatrix2D
    elseif (NumberOfDimensions == 3) then
        !Call InitializeLocalForceMatrix3D
        !Call CalculateLocalForceMatrix3D
    endif
end subroutine LocalForceMatrix
    
End Module calculate_force_matrix