Module WriteGlobalOutputBinary

    Use GlobalInputVariables
    use initialize_global_variables
    Use TetrahedronShapeFunction
        
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine WriteOutputBinaryG
    
    !
    !   This subroutine prints outputs for the global scale
    !    
    
    !If (NumberOfDimensions == 2 ) Then
        
        !Call WriteDisplacementOutput2DBinary
        !Call WriteStressOutput2DBinary
            
    If (NumberOfDimensions == 3 ) Then 
        
        Call WriteDisplacementOutput3DBinary
        Call WriteStressOutput3DBinary
        
    Endif    
    
    End Subroutine WriteOutputBinaryG
    
!*******************************************************************************

    Subroutine WriteDisplacementOutput2DBinary

    Open (Unit = 33, File = 'GlobalDisplacement.OUT', Status = "Old", Position = "Append")
    
        Do i = 1, NumberOfGlobalNodes

            Write (33,2046) i,it, Time, DisplacementG(2*i-1),DisplacementG(2*i)
    
        Enddo

    Close(33)

    2046	Format(2I5,6E19.10)   
       
    End Subroutine WriteDisplacementOutput2DBinary

!*******************************************************************************

    Subroutine WriteDisplacementOutput3DBinary
    
   ! Open (Unit = 33, File = 'GlobalDisplacement.OUT', Status = "Old", Position = "Append")
    if (incr==1) then
       OPEN (UNIT = 333,FILE = 'GlobalDisplacement.bin',form='unformatted',access='stream',status='replace')
    end if     
        Do i = 1, NumberOfGlobalNodes

            Write (333) i,Time, DisplacementG(3*i-2),DisplacementG(3*i-1), DisplacementG(3*i)
    
        Enddo
    if (incr==2)then
       Close(333)
    endif
    ! need to check on output variables and how to print binary or non-binary. 
    ! need to format binary output
    ! need to check print output in block
    ! flags to print input
    ! check for efficiency loops and unecessary loops
    ! check on file status append, unkown, old
    !
    
    !2046	Format(i0,7E19.10)   
2046    Format (i0,(1x,F0.2),3(1x,G0.3))
            
    End Subroutine WriteDisplacementOutput3DBinary

!*******************************************************************************

    Subroutine WriteStressOutput3DBinary
    
    Open (Unit = 13, File = 'GlobalStress.OUT', Status = "Old", Position = "Append")  !Needed because the files are written with "append".
    Open (Unit= 14, File = "GlobalStrain.OUT", Status = "Old", Position = "Append")  !This statement will restart the files after each simulation.
    
    If (NumberOfGlobalNodesPerElement == 4)Then
        Write(13,1008)

        Do i = 1, NumberofGlobalElements
        
	        Do intp = 1, NGIntegrationPoints
                Call TetrahedronCentroid(I,XBAR,YBAR,ZBAR)  !Plots centroid of element
                If(MaterialTypeG(i) == 6) Then
    	    		Write(13,1015) I,XBar, YBar, ZBar,(SG(I,J,intp),J=1,6), I1_Inv(i), J2_Inv(i), MaterialSetG(i),ModulusI1J2(i), SGOctahedral(i),ck1(MaterialSetG(i))*Pa*(abs(I1_Inv(i)/Pa))**ck2(MaterialSetG(i)) * (SGOctahedral(i)/Pa+1)**ck3(MaterialSetG(i))
                Else
    	    		Write(13,1015) I,XBar, YBar, ZBar,(SG(I,J,intp),J=1,6), I1_Inv(i), J2_Inv(i), MaterialSetG(i),ModulusI1J2(i), SGOctahedral(i)
                Endif
    			Write(14,1018) I,XBar, YBar, ZBar,(StrainG(I,J,intp),J=1,6)
            Enddo
        
2092        FORMAT(10(1x,G0.5))  
 !1015	FORMAT(12X,1I6,11(1X,E15.7),i7,3E15.7) 
 1015   Format (i0,11(1x,G0.5),i0,3(1x,G0.5))
 !1018	FORMAT(12X,1I6,12(1X,E15.7),i7) 
 1018   Format (i0,11(1x,G0.5))
 1008   Format(10X,"ElemNumber",6x,"X",14x,"Y",15x,"Z",12x,"StressXX",8x,"StressYY",8x,"StressZZ",8x,"StressYZ",8x,"StressXZ",8x,"StressXY",11x,"I1",14x,"J2",8x,"MatNumber")
        Enddo
    Endif

    Close(33)

    2046	Format(2I5,6E19.10)   

            
    End Subroutine WriteStressOutput3DBinary

!*******************************************************************************

End Module WriteGlobalOutputBinary