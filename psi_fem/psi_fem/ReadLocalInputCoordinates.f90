module ReadLocalInputCoordinates
    
    use GlobalInputVariables
    use LocalInputVariables
    use input_variables
   
    implicit none
    
    integer :: i, iel

    Contains

    Subroutine Read2DInputLocalCoordinates
        Integer :: ix, iy
        Real*8 :: XMax, YMax
    
        Allocate (XL(NumberOfLocalNodes))
        Allocate (YL(NumberOfLocalNodes))

        Write(6, 1008)
    1008 Format (//, 10X, 'Local Nodal Coordinates are: ', //, 8X, 'Node N0.', 11X, 'XL', 16X, 'YL', /)
     
        Do iel = 1, NumberOfLocalNodes
            Read(5, *) i, XL(iel), YL(iel)
            Write(6, 1009) i, XL(iel), YL(iel)
        End Do
    1009 Format (8X, I5, 7X, D15.7, 3X, D15.7)
    
        !
        !   Calculate the maximum dimensions of RVE (xmax and ymax)
        !   Also calculate the nodes on the boundary to transfer displacement BC (used in RVEDisplacement)
        !
    
        If (iLocalBC == 1) Then
            XMax = maxval(XL)
            YMax = maxval(YL)
        
            Do i = 1, NumberOfLocalNodes
                If (XL(i) == XMax) Then
                    NNXSide = NNXSide + 1
                End If
            
                If (YL(i) == YMax) Then
                    NNYSide = NNYSide + 1
                End If
            End Do
        
            Allocate (NodNumX(NNXSide))
            Allocate (NodNumY(NNYSide))
            ix = 0
            iy = 0
            Do i = 1, NumberOfLocalNodes
                If (XL(i) == XMax) Then
                    ix = ix + 1					
                    NodNumX(ix) = i
                End If
                
                If (YL(i) == YMax) Then
                    iy = iy + 1						
                    NodNumY(iy) = i
                End If
            End Do
        End If
    End Subroutine Read2DInputLocalCoordinates
    
end module ReadLocalInputCoordinates