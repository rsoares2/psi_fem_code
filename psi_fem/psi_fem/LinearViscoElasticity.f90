Module LinearVE
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    Use ViscoelasticStress
    Use ViscoelasticStressLocal

    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine LinearViscoelastic2D  (iel, DSIGRG,IViscoElastic, iIntPoint, iLocation)

    Real*8:: DSigRG(4), CCG(4,4)
    
    !
    !   This subroutine is a constitutive package for linear viscoelasticity
    !

    MSet = MaterialSetG (iel)
    C = 0
    CCG = 0
    
   ! Mset = ViscoElasticPosition (Mset)

    CCG(1,1) = CInf22G(MSet)
    CCG(1,2) = CInf23G(MSet)
    CCG(1,3) = CInf12G(MSet)
    CCG(2,2) = CInf22G(MSet)
    CCG(3,3) = CInf11G(MSet)
    CCG(4,4) = CInf44G(MSet)
    
    Do i = 1, NumberOfTerms11G (MSet)
        
        CCG(1,1) = CCG(1,1) + ETA22G(MSet,i) / DTime * (1.-DEXP(-CL22G(MSet,i) * DTime/ETA22G(MSet,i)))
        CCG(1,2) = CCG(1,2) + ETA23G(MSet,i) / DTime * (1.-DEXP(-CL23G(MSet,i) * DTime/ETA23G(MSet,i)))
        CCG(1,3) = CCG(1,3) + ETA12G(MSet,i) / DTime * (1.-DEXP(-CL12G(MSet,i) * DTime/ETA12G(MSet,i)))

        CCG(2,2) = CCG(2,2) + ETA22G(MSet,i) / DTime * (1.-DEXP(-CL22G(MSet,i) * DTime/ETA22G(MSet,i)))

        CCG(3,3) = CCG(3,3) + ETA11G(MSet,i) / DTime * (1.-DEXP(-CL11G(MSet,i) * DTime/ETA11G(MSet,i)))
        CCG(4,4) = CCG(4,4) + ETA44G(MSet,i) / DTime * (1.-DEXP(-CL44G(MSet,i) * DTime/ETA44G(MSet,i)))
      
    Enddo
    
    CCG(2,1) = CCG(1,2)
    CCG(2,3) = CCG(1,3)
    CCG(3,1) = CCG(1,3)
    CCG(3,2) = CCG(2,3)
   
	C(1,1) = CCG(1,1)
	C(1,2) = CCG(1,2)
	C(2,1) = C(1,2)
	C(2,2) = CCG(2,2)
	C(3,3) = CCG(4,4)
    
    If (iLocation == 2) Then
        
        Call ViscoelasticStress2D (iel, iIntPoint, DSigRG, iLocation)
        
    Endif
    
    End  Subroutine LinearViscoelastic2D

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Subroutine LinearViscoelastic3D  (iel, DSIGRG,IViscoElastic, iIntPoint, iLocation)

    !
    !   This subroutine is a constitutive package for linear viscoelasticity for 3D elements
    !
    Real*8:: DSigRG(6)
    
    MSet = MaterialSetG (iel)

   ! Mset = ViscoElasticPosition (Mset)
    
    C(1,1) = CInf11G(MSet)
    C(1,2) = CInf12G(MSet)
    C(1,3) = CInf13G(MSet)
    C(2,2) = CInf22G(MSet)
    C(2,3) = CInf23G(MSet)
    C(3,3) = CInf33G(MSet)
    C(4,4) = CInf44G(MSet)
    C(5,5) = CInf55G(MSet)
    C(6,6) = CInf66G(MSet)

    Do i = 1, NumberOfTerms11G (MSet)
        
        C(1,1) = C(1,1) + ETA11G(MSet,i) / DTime * (1.-DEXP(-CL11G(MSet,i) * DTime/ETA11G(MSet,i)))
        C(1,2) = C(1,2) + ETA12G(MSet,i) / DTime * (1.-DEXP(-CL12G(MSet,i) * DTime/ETA12G(MSet,i)))
        C(1,3) = C(1,3) + ETA13G(MSet,i) / DTime * (1.-DEXP(-CL13G(MSet,i) * DTime/ETA13G(MSet,i)))
        C(2,2) = C(2,2) + ETA22G(MSet,i) / DTime * (1.-DEXP(-CL22G(MSet,i) * DTime/ETA22G(MSet,i)))
        C(2,3) = C(2,3) + ETA23G(MSet,i) / DTime * (1.-DEXP(-CL23G(MSet,i) * DTime/ETA23G(MSet,i)))
        C(3,3) = C(3,3) + ETA33G(MSet,i) / DTime * (1.-DEXP(-CL33G(MSet,i) * DTime/ETA33G(MSet,i)))
        C(4,4) = C(4,4) + ETA44G(MSet,i) / DTime * (1.-DEXP(-CL44G(MSet,i) * DTime/ETA44G(MSet,i)))
        C(5,5) = C(5,5) + ETA55G(MSet,i) / DTime * (1.-DEXP(-CL55G(MSet,i) * DTime/ETA55G(MSet,i)))
        C(6,6) = C(6,6) + ETA66G(MSet,i) / DTime * (1.-DEXP(-CL66G(MSet,i) * DTime/ETA66G(MSet,i)))
        
    Enddo
    
    C(2,1) = C(1,2)
    C(3,1) = C(1,3)
    C(3,2) = C(2,3)
    
    If (iLocation == 2) Then
        
        Call ViscoelasticStress3D (iel, iIntPoint, DSigRG, iLocation)
        
    Endif
    
    End  Subroutine LinearViscoelastic3D

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Subroutine LinearViscoelasticLocal2D  (jel, C, DSigRL,IViscoElastic, iIntPoint, iLocation)

    Real*8:: DSigRL(4), CCL(4,4), C(3,3), DSigRLAvg(4)
    
    !
    !   This subroutine is a constitutive package for linear viscoelasticity
    !

    MSet = MaterialSetL (jel)
    C = 0   !need to address this C, which gets deallocated when called from Global Stress in second time step.
    CCL = 0
    
   ! Mset = ViscoElasticPosition (Mset)

    CCL(1,1) = CInf22L(MSet)
    CCL(1,2) = CInf23L(MSet)
    CCL(1,3) = CInf12L(MSet)
    CCL(2,2) = CInf22L(MSet)
    CCL(3,3) = CInf11L(MSet)
    CCL(4,4) = CInf44L(MSet)
    
    Do i = 1, NumberOfTerms11L (MSet)
        
        CCL(1,1) = CCL(1,1) + ETA22L(MSet,i) / DTime * (1.-DEXP(-CL22L(MSet,i) * DTime/ETA22L(MSet,i)))
        CCL(1,2) = CCL(1,2) + ETA23L(MSet,i) / DTime * (1.-DEXP(-CL23L(MSet,i) * DTime/ETA23L(MSet,i)))
        CCL(1,3) = CCL(1,3) + ETA12L(MSet,i) / DTime * (1.-DEXP(-CL12L(MSet,i) * DTime/ETA12L(MSet,i)))

        CCL(2,2) = CCL(2,2) + ETA22L(MSet,i) / DTime * (1.-DEXP(-CL22L(MSet,i) * DTime/ETA22L(MSet,i)))

        CCL(3,3) = CCL(3,3) + ETA11L(MSet,i) / DTime * (1.-DEXP(-CL11L(MSet,i) * DTime/ETA11L(MSet,i)))
        CCL(4,4) = CCL(4,4) + ETA44L(MSet,i) / DTime * (1.-DEXP(-CL44L(MSet,i) * DTime/ETA44L(MSet,i)))
      
    Enddo
    
    CCL(2,1) = CCL(1,2)
    CCL(2,3) = CCL(1,3)
    CCL(3,1) = CCL(1,3)
    CCL(3,2) = CCL(2,3)
   
	C(1,1) = CCL(1,1)
	C(1,2) = CCL(1,2)
	C(2,1) = C(1,2)
	C(2,2) = CCL(2,2)
	C(3,3) = CCL(4,4)
    
    If (iLocation == 2) Then

        Call ViscoelasticStress2DLocal (iel, jel, C, DSIGRLAvg,IViscoElastic, iIntPoint, iLocation)
        
    Endif
    
    End  Subroutine LinearViscoelasticLocal2D  

End Module LinearVE