module input_variables
    implicit none
   
    enum, bind(c)
        enumerator :: Enum_StartReadInput = 1
        enumerator :: Enum_ReadInputHeader
        enumerator :: Enum_ReadInputCoordinates
        enumerator :: Enum_ReadInputConnectivity
        enumerator :: Enum_ReadGlobalInputBCForces
        enumerator :: Enum_ReadGlobalInputMaterials
        enumerator :: Enum_SparseInitialConnectivity
        enumerator :: Enum_PrintOutputFromInput
        enumerator :: Enum_InitializeVariables
        enumerator :: Enum_GlobalStiffnessMatrix
        enumerator :: Enum_AssembleForceMatrix
        enumerator :: Enum_SparseSolver
        enumerator :: Enum_CalculateStressesStrains
        enumerator :: Enum_CalculateOutputs
    end enum
    
    ! Part I: Time steps, dimensions, single/multiscale
    integer :: numberOfTimeSteps
    integer :: numberOfDimensions
    integer :: numberOfScales
    integer :: numberOfTimeBlocks
    integer :: numberOfGlobalNodesPerElement

    ! Part II: Nodes, elements
    integer :: numberOfGlobalNodes
    integer :: numberOfGlobalElements
    integer :: numberOfForces

    ! Boundary Conditions
    integer :: numberOfDisplacementBC
    integer :: displacementBCType
    
    ! Cohesive zones, boundary conditions, body forces
    integer :: iBodyForce
    integer :: iGlobalCohesiveZones
    integer :: iSolutionType

    ! Part III: Output control flags, output file, tecplot, stress, strain
    integer :: iOutputGlobal
    integer :: iOutputGlobalBinary
    integer :: iTecplotGlobal
    integer :: iTecplotGlobalBinary
    integer :: iOutputLocal
    integer :: iTecplotLocal
    integer :: iPavement

    ! Part IV: Iteration control flags, tolerance
    integer :: iTerationFlag
    real(kind=8) :: tolerance
    integer :: iMaximumNumberofIterations
    integer :: it

    ! Part V: Timeblocks
!
!   From Module InputHeader, Subroutine ReadInputHeader
!
    real(kind=8), allocatable :: timeEndTB(:)
    real(kind=8), allocatable :: timeFactor(:)
    real(kind=8), allocatable :: deltaTime(:)
    
end module input_variables