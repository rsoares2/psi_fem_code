module LocalInputConnectivity
    
    use LocalInputVariables
    use input_variables
    use LinkedConectivity

    implicit none
    
    integer :: i, iel

    Contains
    
    Subroutine ReadLocalInputConnectivity
    
    !
    !   This subroutine reads the local nodal connectivity of elements of the following type
    !
    !   1. CST triangles (3 nodes) - 2D
    !   2. Quad (4 nodes) - 2D !Only implemented in an earlier multiscale version
    !
    !   It is followed by material set (how many materials) and material type (elastic, v/e, v/p...)
    !
    
    integer :: iel, i, j

    Allocate ( NodeL (NumberOfLocalElements, NumberOfLocalNodesPerElement) )
    Allocate ( MaterialSetL (NumberOfLocalElements) )
    Allocate ( MaterialTypeL (NumberOfLocalElements) )
    
    if (numberOfDimensions == 2) then
        if (numberOfLocalNodesPerElement == 3) then
            nlIntegrationPoints = 1 ! CST has only one integration point
            iNLocalNodes = 6 * 3 ! It allocates memory to this linked array.
            write(6, 1014)
        elseif (numberOfLocalNodesPerElement == 4) then
            nlIntegrationPoints = 4 ! Quad have 4 integration points
            iNLocalNodes = 6 * 4 ! It allocates memory to this linked array. (Review number for Quad elements)
            write(6, 1015)
        else
            write(6, 1012)
            stop
        endif
    else
        write(6, 1223)
        stop
    endif
1223     Format ('Material type not in the current library')
    
    Allocate ( LinkedNodeConnectivityL (NumberOfLocalNodes,iNLocalNodes)) !Stores the linked connectivity used to calculate the sparse stiffness matrix    
    ! The LinkedConnectivity relates to the MAXIMUM nodes connected to one particular node (used to create sparse matrix size a priori and avoid an additional loop).
    LinkedNodeConnectivityL = 0
    LinkedNodeConnectivityL (:,iNLocalNodes) = 1

    Do Iel = 1, NumberOfLocalElements
        Read (5,*) i, (NodeL(Iel,j), j=1, NumberOfLocalNodesPerElement), MaterialSetL(Iel), MaterialTypeL(Iel)
        If (MaterialTypeL(Iel) == 1) Then
            NumberOfLocalElasticElement = NumberOfLocalElasticElement + 1
        ElseIf (MaterialTypeL(Iel) == 3) Then
            NumberOfLocalViscoPlasticElement = NumberOfLocalViscoPlasticElement + 1
        ElseIf (MaterialTypeL(Iel) == 5) Then
            NumberOfLocalViscoElasticElement = NumberOfLocalViscoElasticElement + 1
        Else
            Write(6, 1016)
            Stop
        Endif
        Write(6, 1019) i, (NodeL(Iel,j), j=1, NumberOfLocalNodesPerElement), MaterialSetL(Iel), MaterialTypeL(Iel)
        Call LinkedConnectivityLocal(Iel)
    Enddo

1012    Format ('Element is not in the current library. Please choose 3 (CST) or 4 (Quad) elements.')

1013    Format ('Element is not in the current library. Please choose 4 (Tetrahedron) or 8 (Brick) elements.')
            
1014    Format (//,10X,'Local Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Material Set Local',5X,'Material Type Local',//)

1015    Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4x,'Material Set Local',5X,'Material Type Local',//)

1016    Format ('Material type not in the current library')

1017    Format (//,10X,'Local Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4X,'Material Set Global',5X,'Material Type Local',//)

1018    Format (//,10X,'Local Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &     
        4X,'Node 3',4X,'Node 4',4X,'Node 5',4X,'Node 6',4X,'Node 7',4X,'Node 8',4X,'Material Set Local',5X,'Material Type Global',//)
            
1019    Format (12X,i5,10(5X,i5))    
    End Subroutine ReadLocalInputConnectivity        

end module LocalInputConnectivity
