Module InputHeader
    
    use GlobalInputVariables
    use LocalInputVariables, only: iMultiscale
    use input_variables
    use psi_fem_utilities, only: PrintReadInputToScreen
        
    implicit none 
    integer :: i
   
    Contains

    subroutine readInputHeader
      integer :: input_file_unit, output_file_unit
      character(len=20) :: input_file_name, output_file_name

      call openFiles(input_file_unit, input_file_name, output_file_unit, output_file_name)

      call readPartI(input_file_unit, output_file_unit)
      call readPartII(input_file_unit, output_file_unit)
      call readPartIII(input_file_unit, output_file_unit)
      call readPartIV(input_file_unit, output_file_unit)
      call readPartV(input_file_unit, output_file_unit)

      call printReadInputToScreen(Enum_ReadInputHeader)

    end subroutine readInputHeader

    subroutine openFiles(input_file_unit, input_file_name, output_file_unit, output_file_name)
      integer, intent(inout) :: input_file_unit, output_file_unit
      character(len=20), intent(inout) :: input_file_name, output_file_name

      input_file_unit = 5
      input_file_name = 'input.dat'
      open(unit=input_file_unit, file=input_file_name, status='Old')

      output_file_unit = 6
      output_file_name = 'output.out'
      open(unit=output_file_unit, file=output_file_name, status='Unknown')

    end subroutine openFiles

    subroutine readPartI(input_file_unit, output_file_unit)
      integer, intent(in) :: input_file_unit, output_file_unit
      ! The subroutine ReadInputHeader reads the following parameters:
      !
      ! Part I: Time steps, dimensions, single/multiscale
      !
      ! 1. numberOfTimeSteps - Number of solution time steps
      ! 2. numberOfDimensions - 2D = 2, 3D = 3
      ! 3. numberOfScales - Single scale = 1, Multiscale = 2
      ! 4. numberOfTimeBlocks - Time blocks are used to apply load/displacement patterns
      ! 5. numberOfGlobalNodesPerElement - Number of nodes in each element - Global scale

      read(input_file_unit, *) numberOfTimeSteps, numberOfDimensions, numberOfScales, numberOfTimeBlocks, numberOfGlobalNodesPerElement, iOutputGlobal
      if (iOutputGlobal) write(6, 1001) numberOfTimeSteps, numberOfDimensions, numberOfScales, numberOfTimeBlocks, numberOfGlobalNodesPerElement, iOutputGlobal

    1001  format (5x, 'The Input File is as follows:', //, 10x, 'Part I: Time steps, dimensions, single/multiscale', //, &
        & 15x, 'Number of solution time steps: ', i7, /, &
        & 15x, 'Dimensionality of the problem: ', i7, /, &
        & 15x, 'Number of scales: ', i7, /, &
        & 15x, 'Number of time blocks: ', i7, /, &
        & 15x, 'Number of global nodes per element: ', i7, /, &
        & 15x, 'Flag to print output: ', i7)

    end subroutine readPartI
        
    subroutine readPartII(input_file_unit, output_file_unit)
      integer, intent(in) :: input_file_unit, output_file_unit
      if (numberOfScales /= 1) iMultiscale = 1 ! This flag = 1 indicates multiscale is in effect
      !
      ! Part II: Nodes, elements, cohesive zones, boundary conditions, body forces
      !
      ! 1. numberOfGlobalNodes - Number of nodes in the global scale
      ! 2. numberOfGlobalElements - Number of elements in the global scale
      ! 3. numberOfForces - Number of forces
      ! 4. numberOfDisplacementBC - Number of displacement boundary conditions
      ! 5. displacementBCType = 1 Force BC, = 0, Displacement BC. No need to add zeros for force only if using displacements
      ! 6. iBodyForce - Flag to control body forces. = 1, density required; = 0, no body forces.
      ! 7. iGlobalCohesiveZones - = 0, no cohesive zones
      ! 8. iSolutionType = 0 if using 3D
      !   = 1 if Plane Stress
      !   = 2 if Plane Strain
      !   = 3 if Generalized Plane Strain

      read(input_file_unit, *) numberOfGlobalNodes, numberOfGlobalElements, numberOfForces, numberOfDisplacementBC, displacementBCType, iBodyForce, iGlobalCohesiveZones, iSolutionType
      if (iOutputGlobal) write(6, 1002) numberOfGlobalNodes, numberOfGlobalElements, numberOfForces, numberOfDisplacementBC, displacementBCType, iBodyForce, iGlobalCohesiveZones, iSolutionType

    1002  format (/, 10x, 'Part II: Nodes, elements, cohesive zones, boundary conditions, body forces', //, &
        & 15x, 'Number of global nodes: ', i7, /, &
        & 15x, 'Number of global elements: ', i7, /, &
        & 15x, 'Number of forces: ', i7, /, &
        & 15x, 'Number of displacement boundary conditions: ', i7, /, &
        & 15x, 'Displacement boundary condition type (=1 Force, =0 Displ.): ', i7, /, &
        & 15x, 'Body force flag: ', i5, /, &
        & 15x, 'Global cohesive zone flag: ', i5, /, &
        & 15x, 'Solution Type: ', i5)
        
    end subroutine readPartII

    subroutine readPartIII(input_file_unit, output_file_unit)
      integer, intent(in) :: input_file_unit, output_file_unit
      !
      ! Part III: Output control flags, output file, tecplot, stress, strain
      !
      ! 1. iOutputGlobalBinary --> = 1 prints binary global output file
      ! 2. iTecplotGlobal --> = 1 prints global tecplot file
      ! 3. iTecplotGlobalBinary --> = 1 prints binary global tecplot file
      ! 4. iOutputLocal --> = 1 prints local output file (Not yet Implemented)
      ! 5. iTecplotLocal --> = 1 prints local tecplot file (Not yet Implemented)
      ! 6. iPavement --> = (Not yet Implemented)
      
      read(input_file_unit, *) iOutputGlobalBinary, iTecplotGlobal, iTecplotGlobalBinary, iOutputLocal, iTecplotLocal, iPavement
      if (iOutputGlobal) write(6, 1003) iOutputGlobalBinary, iTecplotGlobal, iTecplotGlobalBinary, iOutputLocal, iTecplotLocal, iPavement
    1003  format (/, 10x, 'Part III: Output control flags, output file, tecplot, stress, strain', //, &
        & 15x, 'Global binary output flag: ', i7, /, &
        & 15x, 'Global Tecplot file: ', i7, /, &
        & 15x, 'Global Tecplot binary file: ', i7, /, &
        & 15x, 'Local output flag: ', i7, /, &
        & 15x, 'Local Tecplot file: ', i7, /, &
        & 15x, 'iPavement flag: ', i7)
      
    end subroutine readPartIII

    subroutine readPartIV(input_file_unit, output_file_unit)
      integer, intent(in) :: input_file_unit, output_file_unit
      !
      ! Part IV: Iteration control flags, tolerance, maximum number of iterations
      !
      ! 1. iterationFlag = 0 if no iterations included
      !                  = 1 if Newton-Raphson iteration scheme is used (update Stiffness Matrix)
      !                  = 2 if Modified Newton-Raphson iteration scheme is used (do not update Stiffness Matrix)
      ! 2. tolerance = Tolerance check for convergence
      ! 3. iMaximumNumberofIterations = Maximum number of iterations

      read(input_file_unit, *) iterationFlag, tolerance, iMaximumNumberofIterations
      if (iOutputGlobal) write(6, 1007) iterationFlag, tolerance, iMaximumNumberofIterations

    1007	format (/, 10x, 'Part IV: Iteration control flags, tolerance, maximum number of iterations', //, &
            15x, 'Iteration Flag: ', i7, /, &
		    15x, 'Tolerance: ', E15.7, /, &
		    15x, 'Maximum number of iterations: ', i7)
    
      if (numberOfTimeBlocks == 0) then
        
        write(6, 1004) 
        1004 format ('The number of time blocks must be different than zero')
        stop
        
      endif
    end subroutine readPartIV
    
    subroutine readPartV(input_file_unit, output_file_unit)
      integer, intent(in) :: input_file_unit, output_file_unit
      !
      ! Part V: Timeblocks - Load and displacement shapes
      !
      ! 1. timeEndTB(i) - Ending time of ith time block
      ! 2. timeFactor(i) - Multiplicative factor for all inputs during ith time block
      ! 3. deltaTime(i) - Time increment during ith time block (time step)
      !

      allocate (timeEndTB(numberOfTimeBlocks))
      allocate (timeFactor(numberOfTimeBlocks))
      allocate (deltaTime(numberOfTimeBlocks))

      if (iOutputGlobal) write(6, 1005)
    1005	format (/, 10x, 'Part IV: Timeblocks', /, /, 17x, 'Timeblock', 9x, 'Time Factor', 7x, 'Time Step', /)

      do i = 1, numberOfTimeBlocks
        read(input_file_unit, *) timeEndTB(i), timeFactor(i), deltaTime(i)
        if (iOutputGlobal) write(6, 1006) timeEndTB(i), timeFactor(i), deltaTime(i)
      enddo

    1006 format(14X,E15.8,4X,E15.8,4X,E15.8)
    end subroutine readPartV
   
end module InputHeader