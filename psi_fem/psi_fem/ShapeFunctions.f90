Module ShapeFunctions
    
    Use GlobalInputVariables
    Use TetrahedronShapeFunction
    Use CSTShapeFunction
    use input_variables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine ShapeFunction3D (iel, iIntPoint)
     
    If ( NumberOfGlobalNodesPerElement == 4 ) Then
        
        Call ShapeTetrahedronGlobal (iel)  ! 4-noded linear tetrahedron

    ElseIf ( NumberOfGlobalNodesPerElement == 8 ) Then            
            
        !Call ShapeBrick (iel)    !8-node brick element
            
    Endif        
        
    End Subroutine ShapeFunction3D

!*********************************************************************************************   
    
    Subroutine ShapeFunction2D (iel, iIntPoint, NumberOfNodesPerElement, iLocalGlobal)
    
    Integer:: iLocalGlobal

    If ( NumberOfNodesPerElement == 3 ) Then
        
        If (iLocalGlobal == 0 ) Then
            !This can be eliminated if subroutines are combined into one

            Call ShapeCSTGlobal (iel)  !3-node CST Triangle elements

        Elseif (iLocalGlobal == 1) Then

            Call ShapeCSTLocal (iel)  !3-node CST Triangle elements
        
        Endif

    ElseIf ( NumberOfNodesPerElement == 4 ) Then            
            
      !  Call ShapeQuadGlobal (iel) !4-node quadrilateral element
            
    Endif
            
    End Subroutine ShapeFunction2D
    
    End Module ShapeFunctions