Module ReadMaterialTypeLocal

    Use GlobalInputVariables
    Use LocalInputVariables

    Use ShapeFunctions
    Use Quadrature
    Use LinearElasticity
    Use LinearVE
    Use Functions
        
	implicit Real*8(A-H,O-Z)

    Contains

	Subroutine ReadLocalMaterialType2D (jel, iIntPoint, iLocation, C)

    !
    !   This subroutine calculates the material properties for each individual element in the local mesh
    !    
    
    Real*8 DSigRG(4), CAvg (3,3), DSigRLAvg(4), C(3,3), DSigRL(4)
    
    DSigRL = 0.0
        
    CAvg = 0.0  !Need to implement C at previous time step instead of 
    !...calculating the average at first time step.
                
    If (IVisL == 1) DSigRAvg = 0.0
            
    IViscoElasticL = 0
                
    MType = MaterialTypeL(jel)  !Assign material type
	MSet = MaterialSetL(jel)    !Assign material set
 
    If (MType == 1) Then !This is for linear elastic constituive relation

	    Call IsotropicLinearElasticLocal2D (jel, C)
    	IElasticL = IElasticL + 1
	    
    ElseIf (MType == 3) Then !This is for Viscoplastic constitutive relation
    
    !       IViscoPlastic = IViscoPlastic + 1	!IViscoPlastic is used to avoid allocating SIG to all element, when only a few are V/E
    !	    Call BODNER2D (IEL,C,1,MSETI,IINTPT)
        Write(6,*) 'Viscoplasticity is not implemented at local scale'
        Stop
                     
    ElseIf (MType == 5) Then !This is for linear Viscoelastic constitutive relation
        
        IViscoElasticL = IViscoElasticL + 1	!IViscoElasticL is used to avoid allocating SIL to all element, when only a few are V/E

	    Call LinearViscoelasticLocal2D (jel, C, DSigRL,IViscoElasticL, iIntPoint, iLocation)!needs to be the average subroutine
	    
    !    DSigRAvg(iel,:) = DSigRAvg(iel,:) + DSigRLAvg(:) * Area
    !    DSigRAvg = 0.0D0
    Endif
                
 !   Do k = 1, 3
 !       Do j = 1, 3
 !           CAvg(k, j) = CAvg(k, j) + C(k, j) * Area
 !       Enddo
 !   Enddo
                
   ! If (iVisL == 1) Then
   !     DSigRAvg(iel,:) = DSigRAvg(iel,:)/AreaTotal
   ! Endif
                
   !     CAvg = CAvg/AreaTotal
   !     C = CAvg
                
            !call readmateriallocal
            !call material average
            !need to incorporate cglobal
                
    
    End Subroutine ReadLocalMaterialType2D
 
    End Module ReadMaterialTypeLocal