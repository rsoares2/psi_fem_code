Module Output

    Use GlobalInputVariables
    use initialize_global_variables
    use input_variables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleForceMatrix
    Use CohesiveZoneForce2D
    Use OutputModule
    Use CalculateStressandStrain3D
    Use CalculateStressandStrain2D

    Use OMP_LIB
    
	implicit none

	Contains

	Subroutine GlobalOutput
    
    !   This subroutine prints outputs for the global scale
    
    integer, parameter :: iGlobalOutputToPrint = 105

    If (NumberOfDimensions == 2 ) Then
        
        Call Output2DG
        Call PostCalculationStress2D
                    
    ElseIf (NumberOfDimensions == 3 ) Then 
        
        Call Output3DG
        Call PostCalculationStress3D

    Endif    
    
    If(Allocated(ModulusI1J2)) Deallocate (ModulusI1J2)

    call PrintReadInputToScreen(Enum_CalculateOutputs)
    
    End Subroutine GlobalOutput
    
    
End Module Output