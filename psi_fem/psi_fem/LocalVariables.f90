Module LocalInputVariables

!
!   This Module stores all Global variables initiated from the ReadLocalInput
!

!   From Module InputCoordinates, Subroutine Read2DLocalInputCoordinates
!

    Integer:: NumberOfLocalAnalyses, NumberOfLocalNodesPerElement
    Integer:: NumberOfLocalNodes, NumberOfLocalElements, NumberOfLocalDisplacementBC, iLocalCohesiveZones, iLocalBC
    
	Real*8, Pointer, dimension ( : ) :: XL ( : )
	Real*8, Pointer, dimension ( : ) :: YL ( : )
	Real*8, Pointer, dimension ( : ) :: ZL ( : )
    
    Integer:: NNXSide, NNYSide
    
    Real*8, Allocatable, Dimension ( : ) :: XDimension ( : )
    Real*8, Allocatable, Dimension ( : ) :: YDimension ( : )
    Integer, Allocatable, Dimension ( : ) :: NodNumX ( : )
    Integer, Allocatable, Dimension ( : ) :: NodNumY( : )
    
    Integer, Allocatable, Dimension ( : ) :: iMultiscaleElements( : )
    
!
!   From Module InputCoordinates, Subroutine Read2DInputCoonnectivity
!
 	Integer, Allocatable, dimension ( : ) :: NodeL ( :, : )
	Integer, Allocatable, dimension ( : ) :: MaterialSetL ( : )
	Integer, Allocatable, dimension ( : ) :: MaterialTypeL ( : )
    
    Integer:: NLIntegrationPoints, iNLocalNodes
    Integer, Allocatable, dimension ( : ) :: LinkedNodeConnectivityL ( :, : ) !   Variable to calculate sparse initial data
    Integer:: NZL   !Number ofnon-zeros in local sparse matrix
    
!
!   From Module InputBCandForce, Subroutine ReadLocalInputBC
!
     
	Integer, Pointer, dimension ( : ) :: NumberOfLocalDisplacement ( : )
	Integer, Allocatable, dimension ( : ) :: NLocalDOF ( :, : )    

    Integer:: NLTotalDOF
        
!
!  From Module InputMaterials, Subroutine ReadLocalInputMaterials
!
    Integer:: MaterialNumber1L, MaterialNumber2L, MaterialNumber3L, MaterialNumber4L, MaterialNumber5L
    Integer:: NumberOfTotalMaterialsL
    
    Integer, Allocatable, dimension ( : ) :: ElasticPositionL ( : )
    Integer, Allocatable, dimension ( : ) :: ElasticPlasticPositionL ( : )
    Integer, Allocatable, dimension ( : ) :: ViscoElasticPositionL ( : )
    Integer, Allocatable, dimension ( : ) :: ViscoPlasticPositionL ( : )

!
!  From Module InputMaterials, Subroutine ReadLocaInputElastic
!
 	Real*8, Allocatable, dimension ( : ) :: EEL ( : )   ! Young's modulus
 	Real*8, Allocatable, dimension ( : ) :: VNUL ( : )  ! Poisson's ratio
	Real*8, Allocatable, dimension ( : ) :: AlphaL ( : )   ! Angle (used only in fiber composite)
    Real*8, Allocatable, dimension ( : ) :: YieldL ( : )    ! Yield Stress

	!Real*8, Allocatable, dimension ( : ) :: Density ( : )    ! Include body forces

!
!  From Module InputMaterials, Subroutine ReadGlobalInputViscoElastic
!

    Real*8, Allocatable, dimension ( : ) :: CInf11L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf12L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf13L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf22L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf23L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf33L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf44L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf55L( : )
    Real*8, Allocatable, dimension ( : ) :: CInf66L( : )


    Real*8, Allocatable, dimension ( : ) :: CL11L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL12L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL13L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL22L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL23L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL33L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL44L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL55L( :, : )
    Real*8, Allocatable, dimension ( : ) :: CL66L( :, : )

    Real*8, Allocatable, dimension ( : ) :: ETA11L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA12L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA13L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA22L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA23L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA33L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA44L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA55L( :, : )
    Real*8, Allocatable, dimension ( : ) :: ETA66L( :, : )
	
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms11L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms12L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms13L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms22L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms23L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms33L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms44L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms55L( : )
    Integer, Allocatable, dimension ( : ) :: NumberOfTerms66L( : )	 

	Integer:: NLPronyL ! Maximum number of prony terms

!
!  From Module AllenCZModel, Subroutine ReadLocalInputAllenCZModel
!
    Integer:: NLocalInterfElementsL, NLocalInterfMaterialL, NumberPronyTermsCZL, iMatCZL
    
    Real*8, Allocatable, dimension ( : ) :: SIGMAXL( : )	 
    Real*8, Allocatable, dimension ( : ) :: DELTAL( : )	 
    Real*8, Allocatable, dimension ( : ) :: DBFACL( : )	 
    Real*8, Pointer, dimension ( : ) :: WIDTHL( : )	 
    Real*8, Pointer, dimension ( : ) :: PHIAVL( : )	 
	 
    Integer, Pointer, dimension ( : ) :: IDEBNDL( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT1L( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT2L( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT3L( : )	 
    Integer, Allocatable, dimension ( : ) :: NINT4L( : )	 
	 
    Real*8, Allocatable, dimension ( : ) :: SigmaNormalL( : )	 
    Real*8, Allocatable, dimension ( : ) :: SigmaTangL( : )	 
    Real*8, Allocatable, dimension ( : ) :: RML( : )	 
    Real*8, Allocatable, dimension ( : ) :: ECInfL( : )	 
    Real*8, Allocatable, dimension ( : ) :: ECL( :, : )	 
    Real*8, Allocatable, dimension ( : ) :: ETACL( :, : )
    Real*8, Allocatable, dimension ( : ) :: RAMBDA1L( : )	 
    Real*8, Allocatable, dimension ( : ) :: RAMBDA2L( : )	 
	 
    Real*8, Allocatable, dimension ( : ) :: DeltaNormalL( : )	 
    Real*8, Allocatable, dimension ( : ) :: DeltaTangL( : )	 
    Real*8, Allocatable, dimension ( : ) :: AlphL( : )	    !AlphaL is used in thermo-elasticity
	 
    Integer, Pointer, dimension ( : ) :: iMaterialCZL( : )	 
      
    Real*8, Pointer, dimension ( : ) :: SIGCOHNL( :, : )	 
    Real*8, Pointer, dimension ( : ) :: SIGCOHTL( :, : )	 

    Real*8, Pointer, dimension ( : ) :: OLAMBDAL( : )	 
    Real*8, Pointer, dimension ( : ) :: UNOLDL( : )	 
    Real*8, Pointer, dimension ( : ) :: UTOLDL( : )	 
    Real*8, Allocatable, dimension ( : ) :: USOLDL( : )	 
    Real*8, Pointer, dimension ( : ) :: DAMAGEL( : )	 
    Real*8, Pointer, dimension ( : ) :: TractionTangentL( : )	 
    Real*8, Pointer, dimension ( : ) :: TractionNormalL( : )	 
    Real*8, Allocatable, dimension ( : ) :: TCSL( : )	 
    Real*8, Allocatable, dimension ( : ) :: TCSGL( : )	      

!
!   From Module SparseLocalConectivity, Subroutine SparseInitialLocalConnectivity
!
	 
    Integer, Pointer, dimension ( : ) :: IROW1L( :, : ) !FOR SPARSE SOLVER
    Integer, Pointer, dimension ( : ) :: IROWL( : ) !FOR SPARSE SOLVER - ROWS
    Integer, Pointer, dimension ( : ) :: JCOLL( : ) !FOR SPARSE SOLVER - COLUMNS
    Integer, Pointer, dimension ( : ) :: IROWSUML ( : ) !FOR SPARSE SOLVER - SUM OF NROWS
    Integer, Pointer, dimension ( : ) :: IROWNZL ( : ) !FOR SPARSE SOLVER
	 
    Integer, Pointer, dimension ( : ) :: IRowCompactL( : )	 
    Integer, Pointer, dimension ( : ) :: JcolCompactL( : )

!
!   From Module InitializeLocalVariables, Subroutine LocalInitial
!
    
    Real*8, Pointer, dimension ( : ) :: FORCEL( : )	 	 
    Real*8, Allocatable, dimension ( : ) :: SL( :, :, : )
    Real*8, Allocatable, dimension ( : ) :: DSTRANL( : )	 	
    Real*8, Allocatable, dimension ( : ) :: StrainL( :, :, : )
    Real*8, Pointer, dimension ( : ) :: DisplacementL( : )	 

    Real*8, Allocatable, dimension ( : ) :: SIL ( :, :, :, :, :, : )
    Real*8, Allocatable, dimension ( : ) :: SiLAvg ( :, :, :, :, :, : )
    Real*8, Allocatable, dimension ( : ) :: CC3L ( :, : )
    Real*8 :: TL
    Integer :: IBRangeL, iPrintToScreenL
    
    Integer:: iMultiscale
!
!   From Viscoelasticity
!
    Integer:: iVisL, NumberOfLocalViscoElasticElement, IViscoElasticL

    Real*8, Allocatable, dimension ( : ) :: DSigRAvg( :, : )
   
!
!   From Module CalculateStifnessMatrix, Subroutine GlobalStiffnessMatrix
!	 	 
    Real*8, Pointer, dimension ( : ) :: SparseValuesL( : )
    Real*8, Allocatable, dimension ( : ) :: BL( :, : ) 
    Real*8, Allocatable, dimension ( : ) :: KElementL( :, : )      
    Real*8, Allocatable, dimension ( : ) :: KElement_TempL( :, : )
	Real*8, Allocatable, dimension ( : ) :: CL ( :, : )     	 

!
!   From Module "ForceMatrix" in Local
!	 	 
 !   Real*8, Allocatable, dimension ( : ) :: FElement ( : )
 !   Real*8, Allocatable, dimension ( : ) :: SEigen ( : )    
    Real*8, Allocatable, dimension ( : ) :: DSigrL ( : )    
!    Real*8, Allocatable, dimension ( : ) :: ForceMatrixG ( : )   

    Real*8, Allocatable, dimension ( : ) :: FRGlobal ( : )    

End Module LocalInputVariables