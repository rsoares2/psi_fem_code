Module CalculateStifnessMatrix2DG

    Use GlobalInputVariables
    use initialize_global_variables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleStiffnessMatrix
    Use CohesiveZoneStiffness2D

    Use Functions    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine CalculateGlobalStiffnessMatrixSparse2D
    
    !
    !   This subroutine calculates the global sparse stiffness matrix for 2D meshes
    !    
    
	Integer::NICON(2), iElastic, iViscoElastic
    Real*8:: C(3,3)
    
    Do iel = 1, NumberOfGlobalElements  !Major loop over all elements
    
        If (mod(iel, iPrintToScreen) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) iel
 805        Format (10x,'Set up Global Stiffness Matrix for Element =',I8)
            
        Endif        

!   Initialize the element stiffness matrix

        KElement = 0.0D0
        
!
!   Calculate element stiffness matrix
!

        Call SetUpQuadraturePoints (NGIntegrationPoints, NumberOfGlobalNodesPerElement)  ! Set up quadrature points and weights

        Do iIntPoint = 1, NGIntegrationPoints
        
            Call ShapeFunction2D (iel, iIntPoint, NumberOfGlobalNodesPerElement, 0)  !Set up B matrix
            
            Call ReadGlobalMaterialType2D (iel, iIntPoint, 1, C)  !Add material properties (1, means Called from stifnness matrix)
            
            Call CalculateElementStiffnessMatrix2D (C)
            
        Enddo
    
        Call AssembleGlobalStiffnessSparse2D (iel) !Assembles each local element into a global stiffness matrix
        
        DSIGRG = 0.D0 !Necessary if viscoelasticity is used (MAY NOT BE NECESSARY!) DSigRG is not used in stiff. matrix

    Enddo
    
    Call PostCalculationStiffness2D
    
    If (iGlobalCohesiveZones /=0) Call InterfaceStiffness2D
    
    Call ApplyBCStiffness2D
    
    End Subroutine CalculateGlobalStiffnessMatrixSparse2D

!*********************************************************************************************  
    
    Subroutine CalculateElementStiffnessMatrix2D (C)

    Real*8:: C(3,3)
    !   
    !   This subroutine calculates the element stiffness matrix 
    !   
    !   It first multiplies B transpose matrix (deriv. of shape function) * C matrix (Stiffness/Modulus) * B matrix
    !   and numeriCally integrates. Using DGEMM form MKL to multiply matrix.
            
            Call DGEMM( 'T'  , 'N'  , iSizeBMatrix, 3 , 3 , 1.0D0 , BG , 3 , C , 3 , 0.0D0 , KElement_Temp , iSizeBMatrix ) !MKL matrix multiplication function

            Call DGEMM( 'N'  , 'N'  , 6 , 6 , 3 , 1.0D0 , KElement_Temp , 6 , BG , 3 , 0.0D0 , KElement , 6 ) 
            
            KElement = KElement * Area * Weight  !Uses output from DGEMM function and integrates
   
    End Subroutine CalculateElementStiffnessMatrix2D

!*********************************************************************************************        
    
    Subroutine ApplyBCStiffness2D
   
    !   
    !   This subroutine applies boundary conditions to the global stiffness matrix 
    !   

	JC=1
    !$omp parallel

    !$omp single
        nthr = OMP_GET_NUM_THREADS()
   ! Print*, 'OpenMP number of threads: ', nthr
    !$omp end single

      
    
    !$omp do private(JJ)
	
    Do i = 1, NumberOfDisplacementBC
	    jj = NGlobalDOF (i, KTime)
        SparseValuesG (IROWCompact(jj)) = KBig
    Enddo
    
    !$omp end do 
    !$omp end parallel
    
    
   End Subroutine ApplyBCStiffness2D
   
End Module CalculateStifnessMatrix2DG
    