module InputCoordinates
    
    use GlobalInputVariables
    use input_variables
    use psi_fem_utilities, only: PrintReadInputToScreen
   
    implicit none
    
    integer :: i, iel

    Contains    

    subroutine read_input_coordinates
        ! Subroutine to read the x, y, and z coordinates of nodes.
        ! x_coord = Global x coordinate of node iel
        ! y_coord = Global y coordinate of node iel
        ! z_coord = Global z coordinate of node iel (if in 3D)

        integer :: iel, i

        allocate(x_coord(NumberOfGlobalNodes))
        allocate(y_coord(NumberOfGlobalNodes))
        if (NumberOfDimensions == 3) allocate(z_coord(NumberOfGlobalNodes))

        select case (NumberOfDimensions)
        case (2)
            if (iOutputGlobal) write(6,1008)
            1008 format (/,10x,'Global Nodal Coordinates are: ',/,8x,'Node N0.',8x,'X_coord',16x,'Y_coord',/)
        case (3)
            if (iOutputGlobal) write(6,1010)
            1010 format (/,10x,'Global Nodal Coordinates are: ',//,10x,'Node N0.',11x,'X_coord',8x,'Y_coord',8x,'Z_coord',/)
        case default
            write(*,*) 'Error: Invalid number of dimensions. Must be 2 or 3.'
            stop
        end select

        do iel = 1, NumberOfGlobalNodes
            select case (NumberOfDimensions)
            case (2)
                read (5,*) i, x_coord(iel), y_coord(iel)
                if (iOutputGlobal) write (6,1009) i, x_coord(iel), y_coord(iel)
                1009 format (10x, i5,2x,'(',d15.7,',',d15.7,')')
            case (3)
                read (5,*) i, x_coord(iel), y_coord(iel), z_coord(iel)
                if (iOutputGlobal) write (6,1011) i, x_coord(iel), y_coord(iel), z_coord(iel)
                1011 format (10x, i5, 8x, d15.7, ',', d15.7, ',', d15.7)
            case default
                write(*,*) 'Error: Invalid number of dimensions. Must be 2 or 3.'
                stop
            end select
        enddo
        
        call PrintReadInputToScreen(Enum_ReadInputCoordinates)
    
    end subroutine read_input_coordinates
    
end module InputCoordinates