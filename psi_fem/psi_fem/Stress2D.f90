Module CalculateStressandStrain2D
    
    Use GlobalInputVariables
    use input_variables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use IterationModule

    Use LocalScale_Main
    
    implicit Real*8 (A-H, O-Z)

    Contains    

	Subroutine CalculateGlobalStress2D
    
    !
    !   This subroutine calculates the stresses and strains for 2D cases
    !
    Real*8::Cold(3,3)
    iViscoelastic = 0
    
    DeltaStrainG = 0    
    
    Do iel = 1, NumberOfGlobalElements  !Major loop over all elements
    
!
!   Calculate strain and stress
!

        Call SetUpQuadraturePoints (NGIntegrationPoints, NumberOfGlobalNodesPerElement)  ! Set up quadrature points and weights

        Do iIntPoint = 1, NGIntegrationPoints
        
            Call ShapeFunction2D (iel, iIntPoint, NumberOfGlobalNodesPerElement, 0)  !Set up B matrix
            
            Call CalculateStrain2D (iel, iIntPoint)
            
            If (iMultiscale == 1) Call SetUpLocalScale(iel)
            !need to implement when not all elements are multiscaled
            !needs to check and call sublocal only for the ones belonging to iMultiscaledElements

            Call ReadGlobalMaterialType2D (iel, iIntPoint, 0, C)  !Add material properties (1, means Called from stifnness matrix)
            
            Call CalculateStress2D (iel, iIntPoint, C)

        SG(iel,:,iIntPoint) = SG(iel,:,iIntPoint) + DeltaStressG(:) !Calculate total stress
   
     !   Write(888,12) time, iel, it, SG(iel,1,iIntPoint), DeltaStressG(1),SGOld(iel,1,iIntPoint), DisplacementG(2)
       12 format (e15.7,2i5, 4e15.7)

            DSIGRG = 0.D0 !Necessary if viscoelasticity is used

    If (MaterialTypeG (iel) == 2) Then

        EPSP(iel,:,:) = EPSP(iel,:,:) + DEPSP(iel,:,:) 
        DEPSPT(iel,:,:) = DEPSPT(iel,:,:) + DEPSP(iel,:,:) 
        
      IF (ISolutionType.GT.1) GO TO 6901
      EPSP(IEL,4,iIntPoint)=EPSP(IEL,4,iIntPoint)+DEPSP(IEL,4,iIntPoint)
      DEPSPT(IEL,4,iIntPoint) = DEPSPT(IEL,4,iIntPoint)+DEPSP(IEL,4,iIntPoint)

                Write(6, 1017) incr, iel, it, (EPSP(iel,j,1), j = 1, iRange)
                 1017 Format(3i5,6E15.7)

        !    Enddo
        
       ! Enddo

        !Endif
 6901 CONTINUE 

    Endif
  !    DO 951 J=1,4
!  951 DST(IEL,J,iIntPoint)=DST(IEL,J,iIntPoint)+DS(IEL,J,iIntPoint)
  602 CONTINUE
!      S(IEL,J,iIntPoint)=SO(IEL,J,iIntPoint)+DST(IEL,J,iIntPoint)

        
        Enddo
        
        If (mod(iel, iPrintToScreen) == 0) Then    !this line is to avoid printing every node. But only prints in intervals of 5.
	        
            Write (*,805) iel
 805        Format (10x,'Calculate Global Stress for Element =',I8)
            
        Endif
        
    Enddo
    
    If (it > 1) Call IterationCheck

   ! Call PostCalculationStress2D
    
    End Subroutine CalculateGlobalStress2D
	
!*********************************************************************************************        

    Subroutine CalculateStrain2D (iel, iIntPoint)

    Real*8:: Q(iSizeBMatrix)
    !
    !   This subroutine calculates the strain increment
    !
    Q = 0.0
    Do i = 1, NumberOfGlobalNodesPerElement
    
        ii = 2 * i
        Q (ii-1) = DeltaDisplacementG (NodeG (iel, i) * 2 - 1)
        Q (ii) = DeltaDisplacementG (NodeG (iel, i) * 2)
        
    Enddo
    
    ! Multiply BG and Q --> strain displacement equations
      
    DeltaStrainG (iel,1:3,iIntPoint) = MATMUL(BG, Q)
    
    StrainGold(iel,:,iIntPoint) = StrainG (iel,:,iIntPoint)
    StrainG (iel,:,iIntPoint) = StrainG (iel,:,iIntPoint) + DeltaStrainG (iel,:,iIntPoint)  
    
    End Subroutine CalculateStrain2D

!*********************************************************************************************        
    
    Subroutine CalculateStress2D (iel, iIntPoint, C)

    !
    !   This subroutine calculates the stress increment
    !
    
    Real*8 DSigRG(4), C(3,3)
    
        DeltaStressG = MatMul(C, DeltaStrainG (iel,1:3,iIntPoint))    

    If (MaterialTypeG(Iel) == 5) Then
        
        Call ViscoelasticStress2D (iel, iIntPoint, DSigRG, iLocation)
        
        DeltaStressG = DeltaStressG + DSigRG
    
    Endif
    
    End Subroutine CalculateStress2D
    
!*********************************************************************************************        
    
    Subroutine PostCalculationStress2D

    !
    !   This subroutine deallocates variables and reset others
    !
    
    Deallocate (DeltaStressG)
    Deallocate (DeltaStressGOld)
    IViscoElastic = 0
	IElastic = 0
    If (Allocated(DEPSP)) DEPSP = 0

    If (Allocated(BG)) Deallocate(BG)
    Deallocate (C)
    
 !   Deallocate (DeltaStrainG)
    Deallocate (ForceMatrixG)
    If (Allocated (DeltaDisplacementG)) Deallocate (DeltaDisplacementG)    
    
    End Subroutine PostCalculationStress2D

!*********************************************************************************************
    
End Module CalculateStressandStrain2D