!
! PROGRAM: PSI_FEM
!
! Description: Finite element solver for 2D and 3D continuum problems.
!
! Purpose: Performs operations on complex geometries with various material models.
! 
! Features: Handles plane stress/strain, cohesive zones, 
!           elastic/viscoelastic properties, multiscale simulations.
!
! Input: Instructions in the "input" subroutine, using the "input.dat" file.
!
! Output: Generates displacement, stress, and strain files for visualization.
!
! Last Updated: May 20, 2023
! Author: Roberto Soares, PhD, PEng.rfsoares2@gmail.com. All rights reserved (R).
!
! Disclaimer: The PSI FEM Programs and files are provided without warranty. 
! The author is not liable for any loss or damage resulting from their use.
    
program psi_fem

  use initialize_global_variables, only: globalInitial
  use psi_fem_utilities, only: printElapsedTime
  use global_input
  use GlobalInputVariables, only: numberOfCycles
  use GlobalInputVariables, only: numberOfCycles, tractionNormalG, tractionNormalGIter, &
                                damageG, damageGIter, sigCohNG, sigCohNGIter, &
                                sigCohTG, sigCohTGIter, sg, sgOld,&
                                starttime, icycl, incr, endtime
  use increment, only: timeCheckIncrement, timeCycleIncrement, forceIncrement
  use calculate_stiffness_matrix, only: GlobalStiffnessMatrix
  use calculate_force_matrix, only: GlobalForceMatrix
  use linear_system_solvers, only: SparseSolver, PardisoSolver
  use calculate_stresses_and_strains, only: CalculateStressandStrain
  use output, only: GlobalOutput
  use input_variables, only: numberOfTimeSteps, numberOfTimeBlocks, &
                             numberOfGlobalNodesPerElement, numberOfGlobalNodes, &
                             numberOfGlobalElements, numberOfForces, iGlobalCohesiveZones, &
                             iMaximumNumberofIterations

  implicit none

  call cpu_time(startTime)

  call readInput
  call globalInitial

  do iCycl = 1, numberOfCycles
    call timeCycleIncrement

    do incr = 1, numberOfTimeSteps
      call timeCheckIncrement
      call forceIncrement

      do it = 1, iMaximumNumberOfIterations
        call globalStiffnessMatrix
        call globalForceMatrix
        !call PardisoSolver
        call sparseSolver
        call calculateStressAndStrain
        call globalOutput
      end do

      if (iGlobalCohesiveZones /= 0) call updateCZIterationVariables()
    end do
  end do

  call cpu_time(endTime)
  call printElapsedTime(iCycl-1, incr-1, it, 'Completed FEM', startTime, endTime)

end program psi_fem
    
    subroutine updateCZIterationVariables()

      tractionNormalGIter = tractionNormalG
      tractionNormalGIter = tractionNormalG
      damageGIter = damageG
      sigCohNGIter = sigCohNG
      sigCohTGIter = sigCohTG
      sgOld = sg

    end subroutine updateCZIterationVariables  
