module initialize_global_variables

    use GlobalInputVariables
    use LocalInputVariables
    use LinearElasticity
    use InitializeLocalVariables
    use input_variables
    use psi_fem_utilities
    use irange_enum, only: RANGE_2D, RANGE_3D
    
    implicit none
    integer :: iPrintToScreen
    real(kind=8) :: Gravity
    real(kind=8) :: KBig
    
    contains

    subroutine GlobalInitial

        integer :: i, NDBC
    
        call initializeFirstVariables
        call initializeTime
        call initializeStressStrain
        call InitializeOtherVariables


    Open (Unit = 13,FILE ='GlobalStress.OUT', Status = "Replace")  !Needed because the files are written with "append".
    Close (13)
    Open (Unit= 14,FILE ="GlobalStrain.OUT", Status = "Replace")  !This statement will restart the files after each simulation.
    Close (14)
    Open (Unit = 33,FILE ='GlobalDisplacement.OUT', Status = "Replace")
    Close(33)
    Open (Unit = 999,FILE ='OutputStrains.OUT', Status = "Replace")
    Close (999)

    If(NumberOfScales > 1) Call LocalInitial

    call PrintReadInputToScreen(Enum_InitializeVariables)
    
    End Subroutine GlobalInitial

    !**************************************************************************************

    subroutine initializeGlobalStiffnessMatrix3D

        allocate (bg(6, iSizeBMatrix))   ! This is the B Matrix (shape functions)
        allocate (kElement(iSizeBMatrix, iSizeBMatrix))
        allocate (kElement_Temp(iSizeBMatrix, iSizeBMatrix / 2))

        iViscoElastic = 0
        iElastic = 0

        ! Initialize sparse stiffness matrix
        allocate (sparseValuesG(nzG))
        sparseValuesG = 0.0d0

        allocate (c(6, 6))
        c = 0.0

    end subroutine initializeGlobalStiffnessMatrix3D
    !**************************************************************************************

    Subroutine PostCalculationStiffness3D

    IViscoElastic=0
    IViscoPlastic=0
    Ielastic=0
    iPlastic = 0

    IF (Allocated (BG)) Deallocate(BG)
    Deallocate (KElement, KElement_Temp)
    Deallocate (C)

    End Subroutine PostCalculationStiffness3D

    !**************************************************************************************

    Subroutine InitializeGlobalForceMatrix3D

    ! Initialize force matrix

    Allocate (BG (6,iSizeBMatrix))   !This is the B Matrix (shape functions)
    Allocate (FElement (iSizeBMatrix))
    Allocate (C (6,6))
    C = 0.0

    Allocate (ForceMatrixG (NGTotalDOF))
    ForceMatrixG = 0

    IViscoElastic = 0
    Ielastic = 0
    iPlastic = 0

    End Subroutine InitializeGlobalForceMatrix3D

    !**************************************************************************************

    Subroutine InitializeGlobalStress3D

    Allocate (BG (6,iSizeBMatrix))   !This is the B Matrix (shape functions)

    IViscoElastic = 0
    Ielastic = 0
    iPlastic = 0

    !   Initialize the element strains
    !     Allocate (DeltaStrainG (NumberOfGlobalElements, iRange, NGIntegrationPoints))

    DeltaStrainG = 0.0D0

    Allocate (DeltaStressG (iRange))

    DeltaStressG = 0.0D0

    Allocate (C (6,6))
    C = 0.0

    End Subroutine InitializeGlobalStress3D

    !**************************************************************************************

    Subroutine InitializeGlobalStiffnessMatrix2D

    Allocate (BG (3,iSizeBMatrix))   !This is the B Matrix (shape functions)
    Allocate (KElement (iSizeBMatrix, iSizeBMatrix))
    Allocate (KElement_Temp (iSizeBMatrix, iSizeBMatrix/2))

    IViscoElastic = 0
    Ielastic = 0
    iPlastic = 0

    ! Initialize sparse stiffness matrix
    Allocate (SparseValuesG (NZG) )
    SparseValuesG = 0.0D0

    Allocate (C (3, 3))
    C = 0.0

    End Subroutine InitializeGlobalStiffnessMatrix2D

    !**************************************************************************************

    Subroutine PostCalculationStiffness2D

    IViscoElastic=0
    IViscoPlastic=0
    IElastic=0
    iPlastic=0

    IF (Allocated (BG)) Deallocate(BG)
    Deallocate (KElement, KElement_Temp)
    Deallocate (C)

    End Subroutine PostCalculationStiffness2D

    !**************************************************************************************

    Subroutine PostCalculationStiffness2DL

    IViscoElastic=0
    IViscoPlastic=0
    IElastic=0

    IF (Allocated (BG)) Deallocate(BG)
    Deallocate (KElementL, KElement_TempL)
    !Deallocate (C)

    End Subroutine PostCalculationStiffness2DL

    !**************************************************************************************

    Subroutine InitializeGlobalForceMatrix2D

    ! Initialize force matrix

    Allocate (BG (3,iSizeBMatrix))   !This is the B Matrix (shape functions)
    Allocate (FElement (iSizeBMatrix))
    Allocate (C (3, 3))
    C = 0.0

    Allocate (ForceMatrixG (NGTotalDOF))
    ForceMatrixG = 0

    IViscoElastic = 0
    Ielastic = 0
    iPlastic = 0

    End Subroutine InitializeGlobalForceMatrix2D

    !**************************************************************************************

    Subroutine InitializeGlobalStress2D

    Allocate (BG (3,iSizeBMatrix))   !This is the B Matrix (shape functions)

    IViscoElastic = 0
    Ielastic = 0
    iPlastic = 0

    !   Initialize the element strains
    ! Allocate (DeltaStrainG (NumberOfGlobalElements, iRange, NGIntegrationPoints))

    !  DeltaStrainG = 0.0D0

    Allocate (DeltaStressG (iRange))
    Allocate (DeltaStressGOld (iRange))

    DeltaStressG = 0.0D0

    Allocate (C (3, 3))
    C = 0.0

    End Subroutine InitializeGlobalStress2D

    subroutine initializeFirstVariables

        integer :: i!, NDBC
        Real(kind=8), parameter :: Pa = 101325 !Atmospheric pressure. Used in Uzan's model stress calculation
        KBig = 1.E35   !Used to apply boundary conditions
        Gravity = 9.81

        select case (NumberOfDimensions)
            case (2)
                iRange = RANGE_2D
                tG = 1.0
            case (3)
                iRange = RANGE_3D
            case default
                write(*,*) 'Error: Invalid number of dimensions. Must be 2 or 3.'
                stop
        end select
    
        if (IterationFlag <= 1) iMaximumNumberofIterations = 1
        
    iSizeBMatrix  = NumberOfDimensions * NumberOfGlobalNodesPerElement    !Range of the B matrix
    iPrintToScreen = max(NumberOfGlobalElements / 5, 1)
    
    end subroutine initializeFirstVariables

    subroutine initializeTime    
    
        integer :: i, NDBC

        DTime = 1.0
        KTime = 1.0
        FAC = 1.0
        TIME = 0.0

        If (NumberOfTimeBlocks /= 0) Then

            DTime = DeltaTime(1)
            Fac = TimeFactor(1)
            TimLim = TimeEndTB(1)
            KTIME = 1
            NDBC = NumberOfGlobalDisplacement(1)

        Endif    
        
    end subroutine initializeTime
    
    subroutine initializeStressStrain
        allocate(SG(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        allocate(SGOld(NumberOfGlobalElements, iRange, NGIntegrationPoints)) !Old Strain. Implemented for Plasticity
        allocate(StrainG(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        allocate(DisplacementG(NGTotalDOF))
        allocate(DisplacementGold(NGTotalDOF))
        allocate(StrainGOld(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        ! The allocate statement came from InitializeGlobalStress2D, because DeltaStrain is needed in plasticity.
        allocate(DeltaStrainG(NumberOfGlobalElements, iRange, NGIntegrationPoints))

        DeltaStrainG = 0.0; DisplacementG = 0.0
        SG = 0.0
        StrainG = 0.0
        
    end subroutine initializeStressStrain
    
    subroutine InitializeOtherVariables
    
    integer :: i
    
    if (numberOfPlasticMaterials /= 0) then  ! If there is CZ a priori. If there is automatic insertion, skip
        allocate(alpha11(NumberOfGlobalElements, NGIntegrationPoints))
        allocate(alpha2(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        allocate(epBar(NumberOfGlobalElements, NGIntegrationPoints))
        allocate(iPlas(NumberOfGlobalElements))
        allocate(epsp(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        allocate(depsp(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        allocate(depspt(NumberOfGlobalElements, iRange, NGIntegrationPoints))
        allocate(ds(NumberOfGlobalElements, iRange, NGIntegrationPoints))

        do i = 1, NumberOfGlobalElements
            alpha11(i, :) = YieldGDP(MaterialSetG(i)) / sqrt(3.)
        end do
        alpha2 = 0.0
        iPlas = 0
        epBar = 0.0
        epsp = 0.0
        depsp = 0.0
        depspt = 0.0
        ds = 0.0
    end if

    if (iGlobalCohesiveZones /= 0 .or. iGlobalCohesiveZones <= 4) then  ! If there is CZ a priori. If there is automatic insertion, skip
        allocate(iDebndG(NGlobalInterfElementsG))
        allocate(damageG(NGlobalInterfElementsG))
        allocate(damageGIter(NGlobalInterfElementsG))
        iDebndG = 0
        damageG = 0.0
        damageGIter = 0.0
    end if

    If(iGlobalCohesiveZones == 4 ) Then  !Only allocate here if using Allen CZ a priori

        Allocate ( OLAMBDAG(NGlobalInterfElementsG))
        Allocate ( SIGCOHTG(NGlobalInterfElementsG,NumberPronyTermsCZ))
        Allocate ( SIGCOHNG(NGlobalInterfElementsG,NumberPronyTermsCZ))

        Allocate ( SIGCOHTGIter(NGlobalInterfElementsG,NumberPronyTermsCZ))
        Allocate ( SIGCOHNGIter(NGlobalInterfElementsG,NumberPronyTermsCZ))

        OLAMBDAG = 0.
        SIGCOHTG = 0.
        SIGCOHNG = 0.
        SIGCOHTGIter = 0.
        SIGCOHNGIter = 0.

    Endif

    If (iVisG == 1) Then !If there is V/E material

        If (.NOT. Allocated (SIG)) Allocate ( SIG (NumberOfGlobalViscoElasticElement, NGPRONYG, iRange, iRange, NGIntegrationPoints))
        If (.NOT. Allocated (CC3G)) Allocate ( CC3G (iRange,iRange))
        SIG = 0.

    Endif
    
    end subroutine InitializeOtherVariables
    
    end module initialize_global_variables

    module irange_enum
        implicit none

        ! Size of stress matrix
        enum, bind(c)
            enumerator :: RANGE_2D = 4
            enumerator :: RANGE_3D = 6
        end enum

    end module irange_enum
    