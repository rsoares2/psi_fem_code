Module cohesive_zone_traction

    Use GlobalInputVariables
    use initialize_global_variables
    use input_variables
    Use AssembleStiffnessMatrix
    Use AllenCZTractionCalculation
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine InterfaceTraction2D
    
    Integer::NICON(2)
    REAL*8 RKINT(4,4)
    
    !
    !   This subroutine calculates the force matrix components for 2D cohesive zones (interface elements)
    !    
    
    Eta = 1.    !necessary?
    
    Do i = 1, NGlobalInterfElementsG 

        I1 = NINT1G(I)
        I2 = NINT2G(I)

        CALL InterfaceTraction2DG(I,I1,I2)
		
        NICON(1) = I1 
        NICON(2) = I2
		
    Enddo
    
    If (It >= 1e10) Then    !not sure if this loop is necessary

    DO 7271 II=1,NGlobalInterfElementsG
        N1X=2*NINT1G(II)-1 
        N1Y=N1X+1 
        N2X=2*NINT2G(II)-1 
        N2Y=N2X+1 
        UX=DisplacementG(N2X)-DisplacementG(N1X) 
        UY=DisplacementG(N2Y)-DisplacementG(N1Y)
        PHI=PHIAVG(II)
        UN=UX*DCOS(PHI)+UY*DSIN(PHI) 
        UT=-UX*DSIN(PHI)+UY*DCOS(PHI) 
        UNOLDG(II)=UN
        UTOLDG(II)=UT
        IF(iGlobalCohesiveZones == 4)THEN
	        DELTASUBN=DELTANormalG(iMaterialCZG(II))
	        DELTASUBT=DELTATangG(iMaterialCZG(II))
	        OLAMBDAG(II)=DSQRT((UNOLDG(II)/DELTASUBN)**2+(UTOLDG(II)/DELTASUBT)**2)
        ENDIF

    7271 CONTINUE  
    
    Endif
         
    End Subroutine InterfaceTraction2D

!*********************************************************************************************    
    
    Subroutine InterfaceTraction2DG (ii,I1,I2)
    
    REAL*8 RKINT(4,4)
    !
    !   This subroutine calculates the normal and tangential force components (FN and FT) for interface elements 
    !

	Phi = PHIAVG(II) 
	W = WIDTHG(II)

    ! 
    !   Calculate interface displacements UN1,UN2,UT1,UT2
    ! 

	N1X = 2*I1-1 
	N1Y = N1X+1 
	N2X = 2*I2-1 
	N2Y = N2X+1 
	UX = DisplacementG(N2X)-DisplacementG(N1X)    !should it be DeltaDisplacement?
	UY = DisplacementG(N2Y)-DisplacementG(N1Y) 
	UN = UX*DCOS(PHI)+UY*DSIN(PHI) 
	UT = -UX*DSIN(PHI)+UY*DCOS(PHI)
	
	!If (iGlobalCohesiveZones==1) Call NeedlemanModelG
	!If (iGlobalCohesiveZones==2) Call TvergaardModelG
	!If (iGlobalCohesiveZones==3) Call ModifiedTvergaardModelG
	 If (iGlobalCohesiveZones == 4) Call AllenCZModelTractionG (ii, W, Phi, UN, UT, N1X, N1Y, N2X, N2Y)
     
    End Subroutine InterfaceTraction2DG
    
End Module cohesive_zone_traction