Module ViscoelasticStressLocal
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains

	Subroutine ViscoelasticStress2DLocal (iel, jel, C, DSIGRLAvg,IViscoElastic, iIntPoint, iLocation)
    
    Real*8:: C1(4,4), CA(4,4), CB(4,4), DSIGRLAvg(4)
    Real*8:: SIA(4,4), SIB(4,4), C(3,3)
    !
    !   This subroutine calculates the stresses for viscoelastic 3D cases
    !
    
    Do m = 1, NLPronyL
    
        Call CalculateC1Matrix2D (C1, m, jel, iIntPoint)
    
    !
    ! SIG is a term to calculate Delta SigmaR
    !

    DSIGR1=-C1(1,1)*SiLAvg(iViscoelastic, jel, M,1,1, iIntPoint)-C1(1,2)*SiLAvg(iViscoelastic, jel, M,1,2, iIntPoint)-C1(1,3)*SiLAvg(iViscoelastic, jel, M,1,3, iIntPoint)
    DSIGR2=-C1(2,1)*SiLAvg(iViscoelastic, jel, M,2,1, iIntPoint)-C1(2,2)*SiLAvg(iViscoelastic, jel, M,2,2, iIntPoint)-C1(2,3)*SiLAvg(iViscoelastic, jel, M,2,3, iIntPoint)
    DSIGR3=-C1(3,1)*SiLAvg(iViscoelastic, jel, M,3,1, iIntPoint)-C1(3,2)*SiLAvg(iViscoelastic, jel, M,3,2, iIntPoint)-C1(3,3)*SiLAvg(iViscoelastic, jel, M,3,3, iIntPoint)
    DSIGR4=-C1(4,4)*SiLAvg(iViscoelastic, jel, M,4,4, iIntPoint)    
	DSIGRLAvg(1)=DSIGRLAvg(1)+DSIGR1
	DSIGRLAvg(2)=DSIGRLAvg(2)+DSIGR2
	DSIGRLAvg(3)=DSIGRLAvg(3)+DSIGR4
	DSIGRLAvg(4)=DSIGRLAvg(4)+DSIGR3   
    !     Do i = 1, 4
    !        Do j = 1, 4
     !           DSigRG(i) = DSigRG(i) - C1(i,j) * SIG(iViscoelastic, m, i, j, iIntPoint)
      !      Enddo
       ! Enddo 
    Enddo
    
    if (iLocation == 2) Return  !It only needs to calculate DSigRG in the Force Matrix
    
    Do m = 1, NLPronyL
	
        SIA = 0.
        SIB = 0.
    
        Call CalculateCAandCBMatrix2D (CA,CB,M,jEL)
    
    DO 348 J=1,3
    DO 348 K=1,3
        SIA(J,K)=CA(J,K)*SiLAvg(iViscoelastic, jel, M,J,K,iIntPoint)
348 CONTINUE
    SIA(4,4)=CA(4,4)*SiLAvg(iViscoelastic, jel, M,4,4,iIntPoint)
    
    SIB = 0.
   !   DO 490 II=1,4
!	  DO 490 JJ=1,4
     ! SIB(II,JJ)=CB(II,JJ)*DeltaStrainG(iel,JJ,iIntPoint)/DTIME
 ! 490 CONTINUE
      SIB(1,1)=CB(1,1)*DeltaStrainG(iel,1,iIntPoint)/DTIME
      SIB(1,2)=CB(1,2)*DeltaStrainG(iel,2,iIntPoint)/DTIME
      SIB(2,1)=CB(2,1)*DeltaStrainG(iel,1,iIntPoint)/DTIME
      SIB(2,2)=CB(2,2)*DeltaStrainG(iel,2,iIntPoint)/DTIME
      SIB(3,1)=CB(3,1)*DeltaStrainG(iel,1,iIntPoint)/DTIME
      SIB(3,2)=CB(3,2)*DeltaStrainG(iel,2,iIntPoint)/DTIME
      SIB(4,4)=CB(4,4)*DeltaStrainG(iel,3,iIntPoint)/DTIME
      
      DO 98 J=1,4
      DO 98 K=1,4
        SiLAvg(iViscoelastic, jel, M,J,K,iIntPoint)=SIA(J,K)+SIB(J,K)
   98 CONTINUE
    Enddo
    

    
    End Subroutine ViscoelasticStress2DLocal  

!*********************************************************************************************
    
	Subroutine CalculateC1Matrix2D (C1, m, iel, iIntPoint)
    
    !
    !   This subroutine calculates the C1 matrix for viscoelastic 3D cases
    !
    Real*8 C1(4,4)

    MSet = MaterialSetL (iel)
    
    C1 = 0
    
    C1(1,1) = 1.-DEXP(-CL22G(MSet,m)*DTime/Eta22G(MSet,m))
    C1(1,2) = 1.-DEXP(-CL23G(MSet,m)*DTime/Eta23G(MSet,m))
    C1(1,3) = 1.-DEXP(-CL12G(MSet,m)*DTime/Eta12G(MSet,m))
    
    C1(2,1) = C1(1,2)
    C1(2,2) = 1.-DEXP(-CL22G(MSet,m)*DTime/Eta22G(MSet,m))
    C1(2,3) = C1(1,3)
    
    C1(3,1) = C1(1,3)
    C1(3,2) = C1(2,3)
    C1(3,3) = 1.-DEXP(-CL11G(MSet,m)*DTime/Eta11G(MSet,m))
    
    C1(4,4) = 1.-DEXP(-CL44G(MSet,m)*DTime/Eta44G(MSet,m))
        
    End Subroutine CalculateC1Matrix2D

!*********************************************************************************************
    
    Subroutine CalculateCAandCBMatrix2D (CA,CB,m,iel)

!
!   This subroutine calculates the 3-D orthotropic viscoelastic CA and CB matrices
!

      implicit real*8 (A-H,O-Z)
      Real*8 CA(4,4),CB(4,4)

	MSet = MaterialSetL(IEL)
    !MSet = ViscoElasticPosition (MSet)

    CA(1,1) = DEXP(-CL22G(MSet,m) * DTime/ETA22G(MSet,m))
    CA(1,2) = DEXP(-CL23G(MSet,m) * DTime/ETA23G(MSet,m))
    CA(1,3) = DEXP(-CL12G(MSet,m) * DTime/ETA12G(MSet,m))
    CA(2,1) = CA(1,2)
    CA(2,2) = DEXP(-CL22G(MSet,m) * DTime/ETA22G(MSet,m))
    CA(2,3) = CA(1,3)
    CA(3,1) = CA(1,3)
    CA(3,2) = CA(2,3)
    CA(3,3) = DEXP(-CL11G(MSet,m) * DTime/ETA11G(MSet,m))
    CA(4,4) = DEXP(-CL44G(MSet,m) * DTime/ETA44G(MSet,m))

    CB(1,1) = ETA22G(MSet,m) * (1.-CA(1,1))
    CB(1,2) = ETA23G(MSet,m) * (1.-CA(1,2))
    CB(1,3) = ETA12G(MSet,m) * (1.-CA(1,3))
    CB(2,1) = CB(1,2)
    CB(2,2) = ETA22G(MSet,m) * (1.-CA(2,2))
    CB(2,3) = CB(1,3)
    CB(3,1) = CB(1,3)
    CB(3,2) = CB(2,3)
    CB(3,3) = ETA11G(MSet,m) * (1.-CA(3,3))
    CB(4,4) = ETA44G(MSet,m) * (1.-CA(4,4))
      
    End Subroutine CalculateCAandCBMatrix2D

!*********************************************************************************************    
    
End Module ViscoelasticStressLocal
    