Module CSTShapeFunction

    Use GlobalInputVariables
    Use LocalInputVariables
    use input_variables
    
	implicit Real*8 (A-H,O-Z)

	Contains

	Subroutine ShapeCSTGlobal (iel)
	
    !
    !   This subroutine calculates the shape functions for 3-noded CST linear triangles (2D)
    !
    !   It also calculates the B Matrix
    ! 

    X1 = x_coord(NODEG(IEL,1)) 
    X2 = x_coord(NODEG(IEL,2)) 
    X3 = x_coord(NODEG(IEL,3)) 
    Y1 = y_coord(NODEG(IEL,1)) 
    Y2 = y_coord(NODEG(IEL,2)) 
    Y3 = y_coord(NODEG(IEL,3))
    AREA = (X2*Y3+X1*Y2+X3*Y1-X2*Y1-X3*Y2-X1*Y3)/2.D0
	
    !
    ! Initialize B Matrix (derivative of shape functions)
    !
    !   [B] = [b1, b2, b3]
    !         [c1, c2, c3]
    !

    BG=0.
      
    BG(1,1) = (Y2-Y3)/2./AREA 
    BG(2,2) = (X3-X2)/2./AREA 
    BG(1,3) = (Y3-Y1)/2./AREA 
    BG(2,4) = (X1-X3)/2./AREA 
    BG(1,5) = (Y1-Y2)/2./AREA 
    BG(2,6) = (X2-X1)/2./AREA

    BG(3,1) = BG(2,2) 
    BG(3,2) = BG(1,1) 
    BG(3,3) = BG(2,4)
    BG(3,4) = BG(1,3) 
    BG(3,5) = BG(2,6) 
    BG(3,6) = BG(1,5) 

    End Subroutine ShapeCSTGlobal

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
	Subroutine ShapeCSTLocal (iel)  !will try to combine this subroutine into one
	! only issue is, should XL/x_coord be passed as argument and NodeG/NodeL
    ! Then, needs to allocate general X() and Node(). More memory?

    !
    !   This subroutine calculates the shape functions for 3-noded CST linear triangles (2D)
    !
    !   It also calculates the B Matrix
    ! 

    X1 = XL(NODEL(IEL,1))
    X2 = XL(NODEL(IEL,2)) 
    X3 = XL(NODEL(IEL,3)) 
    Y1 = YL(NODEL(IEL,1)) 
    Y2 = YL(NODEL(IEL,2)) 
    Y3 = YL(NODEL(IEL,3))
    AREA = (X2*Y3+X1*Y2+X3*Y1-X2*Y1-X3*Y2-X1*Y3)/2.D0
	
    !
    ! Initialize B Matrix (derivative of shape functions)
    !
    
    BL=0.
      
    BL(1,1) = (Y2-Y3)/2./AREA
    BL(2,2) = (X3-X2)/2./AREA 
    BL(1,3) = (Y3-Y1)/2./AREA 
    BL(2,4) = (X1-X3)/2./AREA 
    BL(1,5) = (Y1-Y2)/2./AREA 
    BL(2,6) = (X2-X1)/2./AREA

    BL(3,1) = BL(2,2) 
    BL(3,2) = BL(1,1) 
    BL(3,3) = BL(2,4)
    BL(3,4) = BL(1,3) 
    BL(3,5) = BL(2,6) 
    BL(3,6) = BL(1,5) 

    End Subroutine ShapeCSTLocal
End Module CSTShapeFunction