    Module PavementOutput

    Use GlobalInputVariables
    use initialize_global_variables
    use input_variables
    
    Use OMP_LIB

    implicit Real*8(A-H,O-Z)

    Contains

    Subroutine GlobalOutput

    !
    !   This subroutine calculates the outputs for the global scale on a pavement simulation
    !

    Open (Unit = 33, File = 'OutputStrains.OUT', Status = "Old", Position = "Append")

    If (NumberOfDimensions == 3 ) Then  !This subroutine is only valid for 3D analysis.
        ! It calculates the

        Call TransferStrainToNodes
        Call CalculatePeakDeflection
        !Call WriteStressAtNodes

    Endif

    End Subroutine GlobalOutput


    Subroutine TransferStrainToNodes

    ! This subroutine is to transfer stresses/strains to nodes. It is currently ONLY working
    ! for linear tetrahedrons with a single integration point. The interpolation becomes a
    ! simple average of elements and connecting nodes.

    !Two methods are developed: Method 1 where each element contributes 1/4 to each node
    ! Method 2: average by the number of elements connected to each node.

    If(.NOT.Allocated(StressAtNodes)) Allocate (StressAtNodes(NumberOfGlobalNodes,6))
    If(.NOT.Allocated(StrainAtNodes)) Allocate (StrainAtNodes(NumberOfGlobalNodes,6))
    IF(.NOT.ALLOCATED(ElementsConnectedToNodes)) Allocate (ElementsConnectedToNodes(NumberOfGlobalNodes))

    StressAtNodes = 0
    StrainAtNodes = 0
    ElementsConnectedToNodes = 0 !This variable calculates how many elements are connected to one node

    iMethod = 2
    If (iMethod == 1) Then  !This is the method of each element contributes to 1/4 to the node

        Do i = 1,NGEL

            Do j=1,NGNPEREL

                iLocation = NODEG(i,j)
                StressAtNodes(iLocation,:) = StressAtNodes(iLocation,:) + SG(i,: ,1)/4    !This needs to be modified if element has more than one Int. Point.
                StrainAtNodes(iLocation,:) = StrainAtNodes(iLocation,:) + DeltaStrainG(i,: ,1)/4    !This needs to be modified if element has more than one Int. Point.
                !The dividing factor 4 comes from tetrahedron having 4 nodes and 1/4 of each stress goes to each node.

            Enddo

        Enddo

    ElseIf (iMethod == 2) Then  !This is the method of averaging by the number of elements connected to the node

        Do i = 1,NGEL

            Do j=1,NGNPEREL

                iLocation = NODEG(i,j)
                ElementsConnectedToNodes(iLocation) = ElementsConnectedToNodes(iLocation) + 1
                StressAtNodes(iLocation,:) = StressAtNodes(iLocation,:) + SG(i,: ,1)    !This needs to be modified if element has more than one Int. Point.
                StrainAtNodes(iLocation,:) = StrainAtNodes(iLocation,:) + DeltaStrainG(i,: ,1)    !This needs to be modified if element has more than one Int. Point.
                !The dividing factor 4 comes from tetrahedron having 4 nodes and 1/4 of each stress goes to each node.

            Enddo

        Enddo

        Do i = 1,NGN
            StressAtNodes(i,:) = StressAtNodes(i,:)/ElementsConnectedToNodes(i)
            StrainAtNodes(i,:) = StrainAtNodes(i,:)/ElementsConnectedToNodes(i)
        Enddo
    Endif

122 format(2i7,10e15.7)

    End Subroutine TransferStrainToNodes

    !****************************************************************************************

    Subroutine CalculatePeakDeflection

    !
    !   This subroutine calculates the peak deflection in a pavement case.
    !   It searches for the highest deflection on the top surface of a pavement. That includes
    !   the nodes on the lane and shoulder. It will also include the multilane scenarios. This way
    !   there will be no need tosearch only where the force is applied.
    !

    Real*8:: MaxDeflection (NumberOfForces)
    INTEGER:: iPosition(1)
    INTEGER :: iSGPosition(1), iHMACPosition(1), ILocateee(1)

    ! Real*8:: StressAtNodes(NGN),StrainAtNodes(NGN)
    ! INTEGER:: iMaxSGLocation(1),iMaxStrainLocation(1),iMinSGLocation(1),iMinStrainLocation(1),iPosition(1)
    !  Real*8:: SGXXTC(maxval(matsetg),2),SGYYTC(maxval(matsetg),2),SGShearTC(maxval(matsetg))    !Set up peak horizontal and vertical stress/strain. TC = Tensile/Compressive

    Write(999,*)
    Write(999,*)
    Write(999,111)
    Write(999,112)
    Write(999,113)
    Write(999,114)
    Write(999,115)
    Write(999,116)
    Write(999,117)
    !111 Format("       PPPPPP     SSSSS    III   PPPPPP     AAA     V     V   EEEEEEE     333333   DDDDD      ")
    !112 Format("       P     P   S     S    I    P     P   A   A    V     V   E                 3  D    D     ")
    !113 Format("       P     P   S          I    P     P  A     A   V     V   E                 3  D     D   ")
    !114 Format("       PPPPPP     SSSSS     I    PPPPPP   AAAAAAA   V     V   EEEEEE      333333   D      D ")
    !115 Format("       P               S    I    P        A     A    V   V    E                 3  D     D ")
    !116 Format("       P         S     S    I    P        A     A     V V     E                 3  D    D ")
    !117 Format("       P          SSSSS    III   P        A     A      V      EEEEEEE     333333   DDDDD ",3/)
    !Write(999,118)
    !118 Format("-------------------------------------------------------------------------------------------",/)

111 Format("       PPPPPP     SSSSS    III    SSSSS    U     U   III   TTTTTTT   EEEEEEE     333333   DDDDD      ")
112 Format("       P     P   S     S    I    S     S   U     U    I       T      E                 3  D    D     ")
113 Format("       P     P   S          I    S         U     U    I       T      E                 3  D     D   ")
114 Format("       PPPPPP     SSSSS     I     SSSSS    U     U    I       T      EEEEEE      333333   D      D ")
115 Format("       P               S    I          S   U     U    I       T      E                 3  D     D ")
116 Format("       P         S     S    I    S     S   U     U    I       T      E                 3  D    D ")
117 Format("       P          SSSSS    III    SSSSS     UUUUU    III      T      EEEEEEE     333333   DDDDD ",3/)
    Write(999,118)
118 Format("-------------------------------------------------------------------------------------------",/)
    Write(999,119)
119 Format(8x,"Conventional Outputs",//)


    !
    !     Calculate Maximum surface deflection
    !

    Do i=1, NumberOfForces

        iPosition = 3*iNode(i)-1
        !  MaxDeflection(i) = FGG(iPosition(1))
        write(8888,232)i,iposition(1),maxdeflection(i),DeltaDisplacementG(iPosition(1))
232     format (2i7,2e15.7)

    Enddo

    DeflectionMax = -minval(MaxDeflection)*1e3
    ILocateee = minloc(MaxDeflection(:))

    Write(999,32)DeflectionMax, Inode(ILocateee)
32  Format(8x,"Peak Surface Deflection (mm): ",/,5x,F10.3,i5)

    End Subroutine CalculatePeakDeflection

    End Module PavementOutput