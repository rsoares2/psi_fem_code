Module UzanModel
    
    Use GlobalInputVariables
    Use LocalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine UzanIsotropicNonLinearElasticGlobal3D  (iel, iLocation)

    !
    !   This subroutine is a constitutive package for nonlinear elasticity using the modified Uzan model currently adopted by MEPDG
    !
    

    ak1 = ck1(MaterialSetG (iel))   !coefficients out of regression analysis k1, k2, k3 for modulus
    ak2 = ck2(MaterialSetG (iel))   !Mr = K1*Pa * (I1/Pa)^k2 * (tau_Octah/Pa+1)^k3
    ak3 = ck3(MaterialSetG (iel))

    avnuk1 = vnuk1(MaterialSetG (iel))           !coefficients out of regression analysis k1, k2, k3 for Poisson's ratio
    avnuk2 = vnuk2(MaterialSetG (iel))
    avnuk3 = vnuk3(MaterialSetG (iel))

    Pa = 101325 !atmospheric pressure constant

    If (incr==1) Then
        YoungsModulus = EEGkMinimum(MaterialSetG (iel))
        PoissonsRatio = VNUGkMinimum(MaterialSetG (iel))

    Else
        If (I1_Inv(IEL) >= 0) Then
            YoungsModulus = EEGkMinimum(MaterialSetG (iel))
            PoissonsRatio = VNUGkMinimum(MaterialSetG (iel))

        Else
          !  VNUI = avnuk1*(abs(I1_Calc(IEL)))**avnuk2 * (J2_StressCalc(iel))**avnuk3
            
           ! If ( VNUI > 0.5.or.VNUI < 0.05) Then
                PoissonsRatio = VNUGkMinimum(MaterialSetG (iel))
           ! Endif

            !EMI = ak1*(abs(I1_Calc(IEL)))**ak2 * (J2_StressCalc(iel))**ak3
            YoungsModulus = ak1*Pa*(abs(I1_Inv(IEL)/Pa))**ak2 * (SGOctahedral(iel)/Pa+1)**ak3  !Uzan MEPDG
        Endif
    Endif

    YieldStress = YieldGk (MaterialSetG (iel))

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2
    D44 = C1 / 2.0
    
    C = 0.0 !Initialize C
    
    C(1,1) = D11
    C(1,2) = D12 
    C(1,3) = D12

    C(2,1) = D12 
    C(2,2) = D11 
    C(2,3) = D12

    C(3,1) = D12
    C(3,2) = D12
    C(3,3) = D11

    C(4,4) = D44

    C(5,5) = D44

    C(6,6) = D44
    
    If (iLocation == 0) Then
        ModulusI1J2(iel) = YoungsModulus
    Endif

    End Subroutine UzanIsotropicNonLinearElasticGlobal3D
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Subroutine UzanIsotropicLinearNonElasticGlobal2D  (iel, C)

    !
    !   This subroutine is a constitutive package for global linear elasticity
    !
    Real*8:: C(3,3), test, test2

    YoungsModulus = EEG (MaterialSetG (iel))
    !YoungsModulus = 10*exp(-StrainG(iel,2,1))!EEG (MaterialSetG (iel))   !Test Newton-Raphson
  !  If(IterationFlag > 0) Then
    !    test=StrainG(iel,2,1)**2
    !    test2=1e5*(StrainG(iel,2,1)**2)
       ! YoungsModulus = 1e5*(1-1e5*(StrainG(iel,2,1)**2))  !!Remove this for test
   ! Endif
    
    PoissonsRatio = VNUG (MaterialSetG (iel)) 
    YieldStress = YieldG (MaterialSetG (iel))

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2 
    D44 = C1 / 2.0
    
    C = 0.0 !Initialize C
    
    If (iSolutionType == 1) Then
        
        C(1,1) = (D11-D12*D12/D11) 
        C(1,2) = (D12-D12*D12/D11)
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
        C(3,3) = D44
    
    Elseif (iSolutionType == 2) Then

        C(1,1) = D11 
        C(1,2) = D12 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
	    C(3,3) = D44     
        
    Else

        Write(6,*)('Material model not in the code yet')
        Stop
    
    Endif

    End Subroutine UzanIsotropicLinearNonElasticGlobal2D    

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    Subroutine UzanIsotropicNonLinearElasticLocal2D (iel, C)

    !
    !   This subroutine is a constitutive package for local linear elasticity
    !
    Real*8 C(3,3)
    
    YoungsModulus = EEL (MaterialSetL (iel)) 
    PoissonsRatio = VNUL (MaterialSetL (iel)) 
    YieldStress = YieldL (MaterialSetL (iel))

    C1 = YoungsModulus / (1.+PoissonsRatio) 
    C2 = C1 / (1.-2.*PoissonsRatio) 
    D11 = C2 * (1.-PoissonsRatio) 
    D12 = PoissonsRatio * C2 
    D44 = C1 / 2.0
    
    C = 0.0 !Initialize C
    
    If (iSolutionType == 1) Then
        
        C(1,1) = (D11-D12*D12/D11) 
        C(1,2) = (D12-D12*D12/D11) 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
        C(3,3) = D44 
    
    Elseif (iSolutionType == 2) Then

        C(1,1) = D11 
        C(1,2) = D12 
        C(2,1) = C(1,2) 
        C(2,2) = C(1,1) 
	    C(3,3) = D44     
        
    Else

        Write(6,*)('Material model not in the code yet')
        Stop
    
    Endif
      
    End Subroutine UzanIsotropicNonLinearElasticLocal2D
    
End Module UzanModel