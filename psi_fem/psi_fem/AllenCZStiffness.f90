    Module AllenCZStiffness

    Use GlobalInputVariables
    use initialize_global_variables
    
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleStiffnessMatrix
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine AllenCZModelStiffnessG(ii, W, Phi, UN, UT, RKInt)
    
!
!   This is Allen Viscoelastic CZ model - the stiffness terms
!

	Real*8 LAM1,LAM2,LAMBDA,LAMBDADOT, Phi, W
	Real*8 RKINT(4,4)
	Real*8 DUM1(NumberPronyTermsCZ),E(NumberPronyTermsCZ),ETAA(NumberPronyTermsCZ) 
    
!   IF(IDEBNDG(ii).EQ.1) GO TO 800
	DeltaSubN = DeltaNormalG (iMaterialCZG(ii))
	DeltaSubT = DeltaTangG(iMaterialCZG(ii))
	RMC = RMG(iMaterialCZG(ii))
	EInf = ECInfG(iMaterialCZG(ii))
	A = ALPHG(iMaterialCZG(ii))
	ALPHA1 = DAMAGEG(ii)
	OLDLAMBDA = OLAMBDAG(ii)
	OLDUN = UNOLDG(ii)
	OLDUT = UTOLDG(ii)

	Do j = 1, NumberPronyTermsCZ
		
        E(J) = ECG(iMaterialCZG(ii),j)
		ETAA(J) = ETACG(iMaterialCZG(ii),j)
        
	Enddo

	If (UN < 0.) Then
        
		Lambda = DSQRT((UT/DeltaSubT)**2)
	
    Else
        
		Lambda = DSQRT((UN/DeltaSubN)**2+(UT/DeltaSubT)**2)
	
    Endif
	
    If (Lambda.LT.1.D-20) Then
		
        Lambda=0.
    
    Endif
    
	DeltaLambda = Lambda - OldLambda
!	IF(DELTALAMBDA.LT.0.)THEN
!	RNC=0.
!	END IF
!	IF(DELTALAMBDA.EQ.0.)THEN
!	RNC=0.
!	END IF
	LambdaDot = DeltaLambda/DTime
	
	If(LambdaDot >= 0.AND.Alpha1 <= 1) Then
	
        Alpha1Dot = A * Lambda ** RMC
    
    ElseIf (LambdaDot < 0.OR.ALPHA1 > 1) Then
	    
        Alpha1Dot = 0
	
    Endif
	
	!ALPHA1DOT=A*(LAMBDA**RMC)!*((LAMBDADOT/BB)**RNC)
    DeltaAlpha1 = Alpha1Dot * Dtime
    !IF(FAC.GT.0) THEN
	Alpha1 = Alpha1 + DeltaAlpha1
	!ELSE
	!ALPHA1=ALPHA1
	!ENDIF

    !IF(ALPHA1.GE.1.) THEN	
    !    GO TO 800
!ENDIF
	EDeltaT = EInf
	Do j = 1, NumberPronyTermsCZ

        EDeltaT = EDeltaT + (ETAA(J)*(1-DEXP(-E(J)/ETAA(J)*DTime)))/DTime
	
    Enddo

! What happens to stiffness if debond = 1 or alpha = 1?
! When apply force, the element doesn't "release". It only releases with displacement
! What if un is negative?
! rigid stiffness to avoid compression?


    If (IDEBNDG(II) < 1) Then
        
		If (Alpha1 < 1.0) Then
            


!	IF(UN.LT.0.) GO TO 597
!	  IF(OLDLAMBDA.LT.1.D-20) GO TO 598
!      IF(LAMBDA.LT.1.D-20) GO TO 597
            RKN = (1.0 - Alpha1) * EDeltaT/DeltaSubN * W * TG
	        GO TO 600
            597 CONTINUE
            RKN = EDeltaT/DeltaSubN * W * TG
            GO TO 600
  !598 CONTINUE
!	RKN=(1.-ALPHA1)*EDELTAT/DELTASUBN*W*TG
!	WRITE(6,5432)RKN
!	5432 FORMAT(5X,' RKN = ',D15.7)
  600 CONTINUE
    !  IF(OLDLAMBDA.LT.1.D-20) GO TO 697
!	IF(DABS(UT).LT.1.D-20) GO TO 698
      RKT = (1.-ALPHA1) * EDeltaT/DeltaSubT * W * TG
!	  WRITE(6,2334)RKT,ALPHA1
! 2334 FORMAT(5X,'RKT = ',E15.7,5X,'ALPHA = ',E15.7)
! GO TO 7035
  !697 CONTINUE
  !    RKT=EDELTAT/DELTASUBT*W*TG
      GO TO 7035
    
      
 ! 698 CONTINUE
	!  RKT=(1.-ALPHA1)*EDELTAT/DELTASUBT*W*TG
!	WRITE(6,1221) ALPHA1,EDELTAT,DELTASUBT,W,TG
! 1221 FORMAT(5X,'ALPHA1 = ',D15.7,5X,'EDELTAT = ',D15.7,5X,&
!     'DELTASUBT = ',D15.7,5X,'W = ',D15.7,5X,'T(1) = ',D15.7,//)
	  GO TO 7035
    Endif
    Endif
	IF(UN.LT.0.)THEN
	RKN=EDELTAT/DELTASUBN*W*TG
	ELSE
      RKN=1.D-5
	END IF
      RKT=1.D-5


 7035 CONTINUE

!
!
! END ALLEN MODEL
!
!
! CALCULATE RKINT MATRIX 
!
	C1=DCOS(PHI)
	S1=DSIN(PHI)
	C2=-SIN(PHI)
	S2=COS(PHI)

    RKINT(1,1)=RKN*C1**2+RKT*C2**2 
    RKINT(1,2)=RKN*C1*S1+RKT*C2*S2
    RKINT(1,3)=-RKN*C1**2-RKT*C2**2 
    RKINT(1,4)=-RKN*C1*S1-RKT*C2*S2 
    RKINT(2,2)=RKN*S1**2+RKT*S2**2 
    RKINT(2,3)=-RKN*C1*S1-RKT*C2*S2 
    RKINT(2,4)=-RKN*S1**2-RKT*S2**2 
    RKINT(3,3)=RKN*C1**2+RKT*C2**2 
    RKINT(3,4)=RKN*C1*S1+RKT*C2*S2 
    RKINT(4,4)=RKN*S1**2+RKT*S2**2

    Do j = 1, 4 
        J1 = J + 1 
        Do i = J1, 4 
            RKINT(I,J)=RKINT(J,I)
        Enddo
    Enddo

    End Subroutine AllenCZModelStiffnessG

!*************************************************************************************

	Subroutine AllenCZModelStiffnessL(ii, W, Phi, UN, UT, RKInt)
    
!
!   This is Allen Viscoelastic CZ model - the stiffness terms for local scale
!

	Real*8 LAM1,LAM2,LAMBDA,LAMBDADOT, Phi, W
	Real*8 RKINT(4,4)
	Real*8 DUM1(NumberPronyTermsCZL),E(NumberPronyTermsCZL),ETAA(NumberPronyTermsCZL) 
    
!   IF(IDEBNDG(ii).EQ.1) GO TO 800
	DeltaSubN = DeltaNormalL (iMaterialCZL(ii))
	DeltaSubT = DeltaTangL(iMaterialCZL(ii))
	RMC = RML(iMaterialCZL(ii))
	EInf = ECInfL(iMaterialCZL(ii))
	A = ALPHL(iMaterialCZL(ii))
	ALPHA1 = DAMAGEL(ii)
	OLDLAMBDA = OLAMBDAL(ii)
	OLDUN = UNOLDL(ii)
	OLDUT = UTOLDL(ii)

	Do j = 1, NumberPronyTermsCZL
		
        E(J) = ECL(iMaterialCZL(ii),j)
		ETAA(J) = ETACL(iMaterialCZL(ii),j)
        
	Enddo

	If (UN < 0.) Then
        
		Lambda = DSQRT((UT/DeltaSubT)**2)
	
    Else
        
		Lambda = DSQRT((UN/DeltaSubN)**2+(UT/DeltaSubT)**2)
	
    Endif
	
    If (Lambda.LT.1.D-20) Then
		
        Lambda=0.
    
    Endif
    
	DeltaLambda = Lambda - OldLambda
!	IF(DELTALAMBDA.LT.0.)THEN
!	RNC=0.
!	END IF
!	IF(DELTALAMBDA.EQ.0.)THEN
!	RNC=0.
!	END IF
	LambdaDot = DeltaLambda/DTime
	
	If(LambdaDot >= 0.AND.Alpha1 <= 1) Then
	
        Alpha1Dot = A * Lambda ** RMC
    
    ElseIf (LambdaDot < 0.OR.ALPHA1 > 1) Then
	    
        Alpha1Dot = 0
	
    Endif
	
	!ALPHA1DOT=A*(LAMBDA**RMC)!*((LAMBDADOT/BB)**RNC)
    DeltaAlpha1 = Alpha1Dot * Dtime
    !IF(FAC.GT.0) THEN
	Alpha1 = Alpha1 + DeltaAlpha1
	!ELSE
	!ALPHA1=ALPHA1
	!ENDIF

    !IF(ALPHA1.GE.1.) THEN	
    !    GO TO 800
!ENDIF
	EDeltaT = EInf
	Do j = 1, NumberPronyTermsCZL

        EDeltaT = EDeltaT + (ETAA(J)*(1-DEXP(-E(J)/ETAA(J)*DTime)))/DTime
	
    Enddo

! What happens to stiffness if debond = 1 or alpha = 1?
! When apply force, the element doesn't "release". It only releases with displacement
! What if un is negative?
! rigid stiffness to avoid compression?


    If (IDEBNDL(II) < 1) Then
        
		If (Alpha1 < 1.0) Then
            


!	IF(UN.LT.0.) GO TO 597
!	  IF(OLDLAMBDA.LT.1.D-20) GO TO 598
!      IF(LAMBDA.LT.1.D-20) GO TO 597
            RKN = (1.0 - Alpha1) * EDeltaT/DeltaSubN * W * TL
	        GO TO 600
            597 CONTINUE
            RKN = EDeltaT/DeltaSubN * W * TL
            GO TO 600
  !598 CONTINUE
!	RKN=(1.-ALPHA1)*EDELTAT/DELTASUBN*W*TG
!	WRITE(6,5432)RKN
!	5432 FORMAT(5X,' RKN = ',D15.7)
  600 CONTINUE
    !  IF(OLDLAMBDA.LT.1.D-20) GO TO 697
!	IF(DABS(UT).LT.1.D-20) GO TO 698
      RKT = (1.-ALPHA1) * EDeltaT/DeltaSubT * W * TL
!	  WRITE(6,2334)RKT,ALPHA1
! 2334 FORMAT(5X,'RKT = ',E15.7,5X,'ALPHA = ',E15.7)
! GO TO 7035
  !697 CONTINUE
  !    RKT=EDELTAT/DELTASUBT*W*TG
      GO TO 7035
    
      
 ! 698 CONTINUE
	!  RKT=(1.-ALPHA1)*EDELTAT/DELTASUBT*W*TG
!	WRITE(6,1221) ALPHA1,EDELTAT,DELTASUBT,W,TG
! 1221 FORMAT(5X,'ALPHA1 = ',D15.7,5X,'EDELTAT = ',D15.7,5X,&
!     'DELTASUBT = ',D15.7,5X,'W = ',D15.7,5X,'T(1) = ',D15.7,//)
	  GO TO 7035
    Endif
    Endif
	IF(UN.LT.0.)THEN
	RKN=EDELTAT/DELTASUBN*W*TL
	ELSE
      RKN=1.D-5
	END IF
      RKT=1.D-5


 7035 CONTINUE

!
!
! END ALLEN MODEL
!
!
! CALCULATE RKINT MATRIX 
!
	C1=DCOS(PHI) 
	S1=DSIN(PHI) 
	C2=-SIN(PHI)
	S2=COS(PHI)

    RKINT(1,1)=RKN*C1**2+RKT*C2**2 
    RKINT(1,2)=RKN*C1*S1+RKT*C2*S2 
    RKINT(1,3)=-RKN*C1**2-RKT*C2**2 
    RKINT(1,4)=-RKN*C1*S1-RKT*C2*S2 
    RKINT(2,2)=RKN*S1**2+RKT*S2**2 
    RKINT(2,3)=-RKN*C1*S1-RKT*C2*S2 
    RKINT(2,4)=-RKN*S1**2-RKT*S2**2 
    RKINT(3,3)=RKN*C1**2+RKT*C2**2 
    RKINT(3,4)=RKN*C1*S1+RKT*C2*S2 
    RKINT(4,4)=RKN*S1**2+RKT*S2**2

    Do j = 1, 4 
        J1 = J + 1 
        Do i = J1, 4 
            RKINT(I,J)=RKINT(J,I)
        Enddo
    Enddo

    End Subroutine AllenCZModelStiffnessL
        
End Module AllenCZStiffness