Module AssembleForceMatrix

    Use GlobalInputVariables
    use input_variables
    
	implicit Real*8(A-H,O-Z)

    Contains

	Subroutine AssembleGlobalForceMatrix2D (iel)

    !
    !   This subroutine assembles the values from the elemtent force matrix into the global force matrix
    !
    
    Do k = 1, NumberOfGlobalNodesPerElement
        
        N2 = NodeG(iel, k) * 2 - 1
        ii = 2*(k - 1) + 1 
        ForceMatrixG (N2 : N2+1) = ForceMatrixG (N2 : N2+1) - FElement (ii : ii+1) 
        
    Enddo
        
    End Subroutine AssembleGlobalForceMatrix2D	

    Subroutine AssembleGlobalForceMatrix3D (iel)

    !
    !   This subroutine assembles the values from the element force matrix into the global force matrix
    !
    
    Do k = 1, NumberOfGlobalNodesPerElement
        
        N3 = NodeG (iel,k) * 3 - 2 
        II = 3*(k-1) + 1 
        ForceMatrixG (N3 : N3+2) = ForceMatrixG (N3 : N3+2) - FElement (II : II+2)
        
    Enddo
        
    End Subroutine AssembleGlobalForceMatrix3D
        
End Module AssembleForceMatrix
        