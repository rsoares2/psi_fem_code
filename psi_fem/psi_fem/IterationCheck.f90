!    include 'blas.f90'

    Module IterationModule
    
    Use GlobalInputVariables
    
    implicit Real*8 (A-H, O-Z)

    Contains    

	Subroutine IterationCheck
	Real*8 DisplacementNorm, DeltaDisplacementNorm

    !
    !   This subroutine increments the iteration and check for convergence
    !

    DisplacementNorm = dnrm2(NGTotalDOF, DisplacementG, 1)
    DeltaDisplacementNorm = dnrm2(NGTotalDOF, DeltaDisplacementG, 1)
    Fraction = DeltaDisplacementNorm/DisplacementNorm

    If (it > iMaximumNumberofIterations) Then
        Write(*,102)
        102 Format (10x, ' Iteration convergence not obtained')
        Stop
    Endif

    Write(*,33) incr,it, Fraction
    33 Format (25x,'Incr #',i5,'   Iteration # ', i5, e15.7)
    
    If (Fraction < Tolerance) Then
        
        it = iMaximumNumberofIterations

    Endif

    End Subroutine IterationCheck

End Module IterationModule