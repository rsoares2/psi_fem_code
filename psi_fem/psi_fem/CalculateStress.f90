Module calculate_stresses_and_strains
    
    Use GlobalInputVariables
    use initialize_global_variables
    Use CalculateStressandStrain3D
    Use CalculateStressandStrain2D
    
    implicit Real*8 (A-H, O-Z)

    Contains    

	Subroutine CalculateStressandStrain
	
    !
    !   This subroutine calculates the global stresses and strains after the linear system solution
    !
    If (NumberOfDimensions == 2 ) Then
        
        Call InitializeGlobalStress2D
        
        Call CalculateGlobalStress2D
    
    ElseIf (NumberOfDimensions == 3 ) Then 

        Call InitializeGlobalStress3D
        
        Call CalculateGlobalStress3D
        
    Endif
    
    call PrintReadInputToScreen(Enum_CalculateStressesStrains)
    
    End Subroutine CalculateStressandStrain
        
 End Module calculate_stresses_and_strains