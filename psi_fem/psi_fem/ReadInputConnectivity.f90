module InputConnectivity
    
    use GlobalInputVariables
    use LinkedConectivity!, only: LinkedConnectivity
    use input_variables
    use psi_fem_utilities, only: PrintReadInputToScreen
    !use OMP_LIB
    implicit none
    
    contains

    subroutine ReadInputConnectivity

    !
    !   This subroutine reads the nodal connectivity of elements of the following type
    !
    !   1. CST triangles (3 nodes) - 2D
    !   2. Quad (4 nodes) - 2D
    !   3. Tetrahedron (4 nodes) - 3D
    !   4. Bricks (8 nodes) - 3D
    !
    !   It is followed by material set (how many materials) and material type (elastic, v/e, v/p...)
    !
    
    integer :: iel, i, j

    allocate(NodeG(NumberOfGlobalElements, NumberOfGlobalNodesPerElement))
    allocate(MaterialSetG(NumberOfGlobalElements))
    allocate(MaterialTypeG(NumberOfGlobalElements))

    select case (NumberOfDimensions)
    case (2)
        select case (NumberOfGlobalNodesPerElement)
        case (3)  ! CST Elements
            NGIntegrationPoints = 1  ! CST has only one integration point
            iNGlobalNodes = 6 * 3  ! It allocates memory to this linked array.
            if (iOutputGlobal) write(6, 1014)
        case (4)  ! Quad Elements
            NGIntegrationPoints = 4  ! Quad have 4 integration points
            iNGlobalNodes = 6 * 4  ! It allocates memory to this linked array. (Review number for Quad elements)
            if (iOutputGlobal) write(6, 1015)
        case default
            if (iOutputGlobal) write(6, 1012)
            stop
        end select
    case (3)
        select case (NumberOfGlobalNodesPerElement)
        case (4)  ! Tetrahedron Elements
            NGIntegrationPoints = 1  ! Tetrahedron has only one integration point
            iNGlobalNodes = (8 + 4) * 3  ! It allocates memory to this linked array.
            if (iOutputGlobal) write(6, 1017)
        case (8)  ! Brick Elements
            NGIntegrationPoints = 1  ! Brick has only one integration point
            iNGlobalNodes = 27 * 3  ! It allocates memory to this linked array.
            if (iOutputGlobal) write(6, 1018)
        case default
            if (iOutputGlobal) write(6, 1013)
            stop
        end select
    case default
        if (iOutputGlobal) write(6, 1007)
        stop
    end select

    Allocate ( LinkedNodeConnectivity (NumberOfGlobalNodes,iNGlobalNodes)) !Stores the linked connectivity used to calculate the sparse stiffness matrix    
    ! The LinkedConnectivity relates to the MAXIMUM nodes connected to one particular node (used to create sparse matrix size a priori and avoid an additional loop).
    LinkedNodeConnectivity = 0
    LinkedNodeConnectivity (:,iNGlobalNodes) = 1
    
!!$OMP PARALLEL PRIVATE(iel) &
!!$OMP& SHARED(numberOfGlobalElements, numberOfGlobalNodesPerElement, nodeG, materialSetG, materialTypeG, iOutputGlobal) &
!!$OMP& REDUCTION(+:numberOfGlobalElasticElement, numberOfGlobalDruckerPragerElement, numberOfGlobalViscoPlasticElement, numberOfGlobalViscoElasticElement, numberOfGlobalUzanElasticElement)
    do iel = 1, numberOfGlobalElements
        read(5, *) i, (nodeG(iel, j), j = 1, numberOfGlobalNodesPerElement), materialSetG(iel), materialTypeG(iel)
    
        select case (materialTypeG(iel)) ! allows for any numbering order when inputting materials
            case (1)    ! linear elastic
                numberOfGlobalElasticElement = numberOfGlobalElasticElement + 1
            case (2)    ! plasticity
                numberOfGlobalDruckerPragerElement = numberOfGlobalDruckerPragerElement + 1
            case (3)    ! viscoplasticity
                numberOfGlobalViscoPlasticElement = numberOfGlobalViscoPlasticElement + 1
            case (5)    ! viscoelasticity
                numberOfGlobalViscoElasticElement = numberOfGlobalViscoElasticElement + 1
            case (6)    ! nonlinear elastic
                numberOfGlobalUzanElasticElement = numberOfGlobalUzanElasticElement + 1
            case default
                write(6, 1016)
                stop
        end select

        if (iOutputGlobal) write(6, 1019) i, (nodeG(iel, j), j = 1, numberOfGlobalNodesPerElement), materialSetG(iel), materialTypeG(iel)
        call linkedConnectivity(iel)
    
    end do
!!$OMP END PARALLEL

    ! Formats used above
    
1007 Format ('The number of dimensions must be 2 for 2D or 3 for 3D')
1012 Format ('Element is not in the current library. Please choose 3 (CST) or 4 (Quad) elements.')
1013 Format ('Element is not in the current library. Please choose 4 (Tetrahedron) or 8 (Brick) elements.')
1014 Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &
              4X,'Node 3',4X,'Material Set Global',5X,'Material Type Global',//)
1015 Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &
              4X,'Node 3',4X,'Node 4',4x,'Material Set Global',5X,'Material Type Global',//)
1016 Format ('Material type not in the current library')
1017 Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &
              4X,'Node 3',4X,'Node 4',4X,'Material Set Global',5X,'Material Type Global',//)
1018 Format (//,10X,'Global Numbering of Elements is:',//,10X,'Element N0.',4X,'Node 1',4X,'Node 2', &
              4X,'Node 3',4X,'Node 4',4X,'Node 5',4X,'Node 6',4X,'Node 7',4X,'Node 8', &
              4X,'Material Set Global',5X,'Material Type Global',//)
1019 Format (12X,i5,2x,10(5X,i5))

    call PrintReadInputToScreen (Enum_ReadInputConnectivity)

    End Subroutine ReadInputConnectivity
   
end Module InputConnectivity
