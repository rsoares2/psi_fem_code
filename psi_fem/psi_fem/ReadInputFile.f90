module global_input
    use ReadInputFileModuleManager
    implicit none

    contains

    subroutine readInput

        call psiFemVersion
        call readInputHeader
        call read_input_coordinates
        call readInputConnectivity
        call readGlobalInputBCandForce
        call readGlobalInputMaterials

        if (iGlobalCohesiveZones /= 0) call readGlobalInputInterfaceModel
        call sparseInitialConnectivity  ! Sets up connectivity into CSR sparse format

        read (5, *) numberOfCycles
        if (iOutputGlobal) then
            if (numberOfCycles == 1) then
                write(6, 1050) numberOfCycles, 'cycle applied'
            else
                write(6, 1050) numberOfCycles, 'cycles applied'
            endif
        endif
        1050 format (/,3X,I6,2X,A,/)
        call printOutputFromInput   ! Controls outputs (Tecplot file, stresses, strains, etc.)

        if (numberOfScales > 1) call readLocalInput
    end subroutine readInput

    end module global_input

    module ReadInputFileModuleManager
        use psi_fem_utilities, only: psiFemVersion
        use InputHeader, only: readInputHeader
        use input_variables
        use GlobalInputVariables, only: numberOfCycles
        use InputCoordinates, only: read_input_coordinates
        use InputConnectivity, only: readInputConnectivity
        use InputBCandForce, only: readGlobalInputBCandForce
        use InputMaterials, only: readGlobalInputMaterials
        use InputCohesiveZones, only: readGlobalInputInterfaceModel
        use SparseConectivity, only: sparseInitialConnectivity
        use OutputFromInput, only: printOutputFromInput
        use local_input, only: readLocalInput
    end module ReadInputFileModuleManager