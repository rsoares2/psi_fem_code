module local_input
    use LocalInputHeader, only: readLocalInputHeader
    use InputCoordinates
    use ReadLocalInputCoordinates
    use LocalInputConnectivity, only: ReadLocalInputConnectivity
    use InputBCandForce, only: ReadLocalInputBC
    use InputMaterials, only: readLocalInputMaterials
    use InputCohesiveZones, only:ReadLocalInputInterfaceModel
    use SparseLocalConectivity, only: SparseInitialLocalConnectivity
    use MultiscaleElements, only: readMultiscaleElements

    implicit none

    contains

    subroutine readLocalInput
        call readLocalInputHeader    ! Reads overall variables, nodes, elements, flags, etc.
        call readMultiscaleElements ! Reads nodes on the boundary and elements to be multiscaled
        call read2DInputLocalCoordinates   ! Reads coordinates X, Y and Z
        call readLocalInputConnectivity  ! Reads element connectivity and material set and type (elastic, v/e, etc.)
        call readLocalInputBC  ! Reads local boundary conditions
        call readLocalInputMaterials   ! Reads material properties for each material set

        if (iLocalCohesiveZones /= 0) call readLocalInputInterfaceModel  ! Reads cohesive zone properties
        call sparseInitialLocalConnectivity  ! Sets up connectivity into CSR sparse format
    end subroutine readLocalInput

end module local_input