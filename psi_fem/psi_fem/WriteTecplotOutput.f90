Module TecplotOutput
    
    Use GlobalInputVariables
    Use Omp_lib
        
    implicit Real*8 (A-H, O-Z)

    Contains    

    Subroutine WriteTecplotGlobalOutput

        If (iMultiscale = 0) Then

            If (NumberOfDimensions == 2) Then
            
	            If(NumberOfGlobalNodesPerElement ==3) Call WriteToTecplotCST
        
    	        If(NumberOfGlobalNodesPerElement == 4) Call WriteToTecplot4Rect
        
            ElseIf (NumberOfDimensions == 3) Then

                If(NumberOfGlobalNodesPerElement == 4) Call WriteToTecplot3D

                If(NumberOfGlobalNodesPerElement == 4) Call WriteToTecplot3DFactor !Stress factor
                    
        	    If(NumberOfGlobalNodesPerElement == 8) Call WriteToTecplot3D

            Endif
            
        Else 
        
            If(NGNPEREL==3) Call WriteToTecplotCSTDamage
    
        Endif
        
    Endif

    End Subroutine WriteTecplotGlobalOutput
    
End Module TecplotOutput