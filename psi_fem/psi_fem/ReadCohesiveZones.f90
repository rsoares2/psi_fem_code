Module InputCohesiveZones
    
    Use GlobalInputVariables
    Use LocalInputVariables
    Use AllenCZModel
    Use AllenCZModel3D    
    use input_variables
    
    implicit Real (A-H, O-Z)

    Contains    
    
    Subroutine ReadGlobalInputInterfaceModel	
	
    !
	!   This subroutine reads input for different types of interface elements, such as
	!   Needleman Interface Model, Tvergaard Model using piecewise linear interfaace function, 
	!   Modified Tvergaard Model using cubic interface function and Allen viscoelastic
	!   cohesive zone model
	!
	
    Write(6,4003) 
 4003 Format (/,10X,'Interface Elements in Effect',/)

    If ( NumberOfDimensions == 2 ) Then

        If ( NumberOfGlobalNodesPerElement == 3 ) Then  !CST Elements

	        !If (iGlobalCohesiveZones==1) Call ReadGlobalInputNeedlemanModel
	        !If (iGlobalCohesiveZones==2) Call ReadGlobalInputTvergaardModel
	        !If (iGlobalCohesiveZones==3) Call ReadGlobalInputModifiedTvergaardModel
	        If (iGlobalCohesiveZones==4) Call ReadGlobalInputAllenCZModel2D
            
        ElseIf ( NumberOfGlobalNodesPerElement == 4 ) Then  !Quad Elements

            Write (6,1012)
            Stop
            
        Else
            
            Write (6,1012)
            Stop
        
        Endif
        
    ElseIf ( NumberOfDimensions == 3 ) Then

        If ( NumberOfGlobalNodesPerElement == 4 ) Then  !Tetrahedron Elements
            
	        !If (iGlobalCohesiveZones==1) Call ReadGlobalInputNeedlemanModel
	        !If (iGlobalCohesiveZones==2) Call ReadGlobalInputTvergaardModel
	        !If (iGlobalCohesiveZones==3) Call ReadGlobalInputModifiedTvergaardModel
	        If (iGlobalCohesiveZones==4) Call ReadGlobalInputAllenCZModel3D

        ElseIf ( NumberOfGlobalNodesPerElement == 8 ) Then  !Brick Elements
            
            Write (6,1012)
            Stop

        Else
            
            Write (6,1012)
            Stop
            
        Endif

    Else
        
        Write (6,1012) 
        Stop
        
    Endif
	
    If (iGlobalCohesiveZones>4) Then
        Write(6,*)'The interface model is not in the code. Choose from 1 - 4'
        STOP
    Endif
	
1012    Format ('Interface Element is not in the current library')

    End Subroutine ReadGlobalInputInterfaceModel	

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    Subroutine ReadLocalInputInterfaceModel	
	
    !
	!   This subroutine reads input for different types of interface elements, such as
	!   Needleman Interface Model, Tvergaard Model using piecewise linear interfaace function, 
	!   Modified Tvergaard Model using cubic interface function and Allen viscoelastic
	!   cohesive zone model
	!
	
    Write(6,4003) 
 4003 Format (/,10X,'Interface Elements in Effect',/)

	!If (iLocalCohesiveZones==1) Call ReadLocalInputNeedlemanModel
	!If (iLocalCohesiveZones==2) Call ReadLocalInputTvergaardModel
	!If (iLocalCohesiveZones==3) Call ReadLocalInputModifiedTvergaardModel
	 If (iLocalCohesiveZones==4) Call ReadLocalInputAllenCZModel
	
    If (iLocalCohesiveZones>4) Then
        Write(6,*)'The interface model is not in the code. Choose from 1 - 4'
        STOP
    Endif
	
    End Subroutine ReadLocalInputInterfaceModel	
    
    End Module InputCohesiveZones
