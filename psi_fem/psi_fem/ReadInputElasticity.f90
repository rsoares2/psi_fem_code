Module InputElasticity
    
    Use GlobalInputVariables
    Use LocalInputVariables
    use input_variables
    
    implicit none !Real*8 (A-H, O-Z)
    
    Contains    
    
    subroutine readGlobalInputElastic

        integer :: i, mSetiG
        ! Read in material properties for isotropic elastic elements

       ! allocate (elasticPosition(numberOfTotalMaterials))  ! Array to keep track number of elastic materials independent of matset number.
       ! elasticPosition = 0

        allocate (eEG(numberOfTotalMaterials), vNUG(numberOfTotalMaterials), yieldG(numberOfTotalMaterials), &
                 alphaG(numberOfTotalMaterials), strainLimitG(numberOfTotalMaterials), density(numberOfTotalMaterials))

        If (iOutputGlobal) write (6,1023) numberOfElasticMaterials
    1023 format (/,10X,'There are ',I3,' 3-D isotropic elastic material sets', &
            //,10X,'Set N0.',9X,'E',10X,'NU',11X,'Y',9X,'Alpha',6x,'Density',2x,'StrainLimitG'/)

        do i = 1, numberOfElasticMaterials
            read (5,*) mSetiG, eEG(i), vNUG(i), density(i)
            If (iOutputGlobal) write(6,1024) mSetiG, eEG(i), vNUG(i), density(i)
    1024    format(11X,I3,5X,8(1X,E11.4))
          !  elasticPosition(mSetiG) = i      ! Array to store material numbers and positions
        end do

    end subroutine readGlobalInputElastic
    
    Subroutine ReadLocalInputElastic
    
        integer :: i, mSetiL
    !   Read in material properties for local isotropic elastic elements

    Allocate ( ElasticPositionL (NumberOfTotalMaterialsL) )  !Array to keep track number of elastic materials independent of matset number.
    ElasticPositionL = 0

	Allocate ( EEL (MaterialNumber1L) )
	Allocate ( VNUL (MaterialNumber1L) )
	Allocate ( YieldL (MaterialNumber1L) )
	!Allocate ( AlphaL (MaterialNumber1L) )  used for thermal code
    
    Write (6,1023) MaterialNumber1L
1023 Format (/,10X,'There are ',I3,' local 2-D isotropic elastic material sets', &
		//,10X,'Set N0.',12X,'E',10X,'NU',11X,'Y',/) 
        
    Do i = 1, MaterialNumber1L
                
        Read (5,*) MsetiL,EEL(I),VNUL(I),YieldL(I)  !,AlphaL(I)
        Write(6,1025) MsetiL,EEL(I),VNUL(I),YieldL(I)   !,AlphaL(I)
1025        Format(11X,I3,4X,5(1X,E11.4)) 

        ElasticPositionL (MsetiL) = i      !Array to store material numbers and positions         
                
    Enddo
            
    End Subroutine ReadLocalInputElastic

End Module InputElasticity