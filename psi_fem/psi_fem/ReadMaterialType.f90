Module ReadMaterialType

    Use GlobalInputVariables
    
    Use ShapeFunctions
    Use Quadrature
    Use LinearElasticity
    Use LinearVE
    Use Functions
    Use ElastoPastic
    Use UzanModel

	implicit Real*8(A-H,O-Z)

    Contains

	Subroutine ReadGlobalMaterialType2D (iel, iIntPoint, iLocation, C)

    !
    !   This subroutine calculates the global sparse stiffness matrix for 3D meshes
    !   If there is a multiscale analysis, call subroutine to calculate material stiffness at t=0
    !    
    
    Real*8 DSigRG(4), CAvg(3,3), DSigRLAvg(4), C(3,3), DSigRL(4)
    
    DSigRG = 0.0
    
    MType = MaterialTypeG (iel) !Assign material type
    MSet = MaterialSetG (iel) !Assign material set
    
    If (iMultiscale == 0) Then 
        
        If (MaterialTypeG (iel) == 1) Then !This is for linear elastic constitutive relation

	        Call IsotropicLinearElasticGlobal2D (iel, C)
    	    IElastic = IElastic + 1
	    
        ElseIf (MaterialTypeG (iel) == 2) Then !This is for Viscoplastic constitutive relation

            IPlastic = IPlastic + 1

	        Call DruckerPrager2D (iel, iIntPoint, iLocation, Mset, C)
    

        ElseIf (MaterialTypeG (iel) == 3) Then !This is for Viscoplastic constitutive relation
    
            IViscoPlastic = IViscoPlastic + 1	!IViscoPlastic is used to avoid allocating SIG to all element, when only a few are V/E
    !	    Call BODNER2D (IEL,C,1,MSETI,IINTPT)
		
        ElseIf (MaterialTypeG (iel) == 5) Then !This is for linear Viscoelastic constitutive relation
        
            IViscoElastic = IViscoElastic + 1	!IViscoElastic is used to avoid allocating SIG to all element, when only a few are V/E

	        Call LinearViscoelastic2D (iel, DSIGRG,IViscoElastic, iIntPoint, iLocation)
	    
            If (iLocation == 2) Then    !If Called from Force Matrix
            
                If (.Not. Allocated (SEigen)) Allocate ( SEigen (3))
                SEigen(1:3) = DSigRG(1:3)   !If there are more than one integration point, need to implement DSIGR(, , IINTPT)
                DSIGRG = 0

            Endif
        

        Else

            Write (6,6003) 
    6003    Format(10X,'Material type not in current library',/)
	        Stop 
    
        Endif
    
    Elseif (iMultiscale == 1) Then
            
        !Perform a binary search to check if current global element is in the multiscale list
        Call MultiscaleElementSearch(iel, isMultiscale)

              !iMultiscaleElements(i)==iel

        If (isMultiscale == 1) Then

            CAvg = 0.0  !Need to implement C at previous time step instead of 
            !...calculating the average at first time step.
                
            If (IVisL == 1) DSigRAvg = 0.0
            
            IViscoElasticL = 0

            Do jel = 1, NumberOfLocalElements
                    
		        MType = MaterialTypeL(jel)
		        MSet = MaterialSetL(jel)

                X1 = XL(NodeL(jel,1)) 
                X2 = XL(NodeL(jel,2)) 
                X3 = XL(NodeL(jel,3)) 
                Y1 = YL(NodeL(jel,1)) 
                Y2 = YL(NodeL(jel,2)) 
                Y3 = YL(NodeL(jel,3))

		        Area = (X2*Y3+X1*Y2+X3*Y1-X2*Y1-X3*Y2-X1*Y3)/2.

                AreaTotal = AreaTotal + Area
 
                If (MType == 1) Then !This is for linear elastic constituive relation

	                Call IsotropicLinearElasticLocal2D (jel, C)
    	            IElastic = IElastic + 1
	    
                ElseIf (MType == 3) Then !This is for Viscoplastic constitutive relation
    
            !       IViscoPlastic = IViscoPlastic + 1	!IViscoPlastic is used to avoid allocating SIG to all element, when only a few are V/E
            !	    Call BODNER2D (IEL,C,1,MSETI,IINTPT)
                    Write(6,*) 'Viscoplasticity is not implemented at local scale'
                    Stop
                     
                ElseIf (MType == 5) Then !This is for linear Viscoelastic constitutive relation
        
                    IViscoElasticL = IViscoElasticL + 1	!IViscoElastic is used to avoid allocating SIG to all element, when only a few are V/E

	                Call LinearViscoelasticLocal2D (jel, C, DSigRL,IViscoElasticL, iIntPoint, iLocation)!needs to be the average subroutine
	    
                    DSigRAvg(iel,:) = DSigRAvg(iel,:) + DSigRLAvg(:) * Area
                    DSigRAvg = 0.0D0
                Endif
                
                Do k = 1, 3
                    Do j = 1, 3
                        CAvg(k, j) = CAvg(k, j) + C(k, j) * Area
                    Enddo
                Enddo
                                    
            Enddo
                
            If (iVisL == 1) Then
                DSigRAvg(iel,:) = DSigRAvg(iel,:)/AreaTotal
            Endif
                
            CAvg = CAvg/AreaTotal
            C = CAvg
            AreaTotal = 0
            !call readmateriallocal
            !call material average
            !need to incorporate cglobal
                
        Else
                
            If (MaterialTypeG (iel) == 1) Then !This is for linear elastic constituive relation

	            Call IsotropicLinearElasticGlobal2D (iel, C)
    	        IElastic = IElastic + 1
	    
            ElseIf (MaterialTypeG (iel) == 3) Then !This is for Viscoplastic constitutive relation
    
                IViscoPlastic = IViscoPlastic + 1	!IViscoPlastic is used to avoid allocating SIG to all element, when only a few are V/E
        !	    Call BODNER2D (IEL,C,1,MSETI,IINTPT)
		
            ElseIf (MaterialTypeG (iel) == 5) Then !This is for linear Viscoelastic constitutive relation
        
                IViscoElastic = IViscoElastic + 1	!IViscoElastic is used to avoid allocating SIG to all element, when only a few are V/E

	            Call LinearViscoelastic2D (iel, DSIGRG,IViscoElastic, iIntPoint, iLocation)
	    
                If (iLocation == 2) Then    !If Called from Force Matrix
            
                    If (.Not. Allocated (SEigen)) Allocate ( SEigen (3))
                    SEigen(1:3) = DSigRG(1:3)   !If there are more than one integration point, need to implement DSIGR(, , IINTPT)
                    DSIGRG = 0

                Endif
        
            Else

                Write (6,6003) 
                Stop 
    
            Endif

        Endif
       
    Endif
    
    End Subroutine ReadGlobalMaterialType2D

    Subroutine ReadGlobalMaterialType3D (iel, iIntPoint, iLocation)

    !
    !   This subroutine reads the global material properties for all material models
    !    
    Real*8 DSigRG(6)
    
    DSigRG = 0.0
    
    MType = MaterialTypeG (iel) !Assign material type
    MSet = MaterialSetG (iel) !Assign material set
                
    If (MaterialTypeG (iel) == 1) Then !This is for linear elastic constituive relation

	    Call IsotropicLinearElasticGlobal3D (iel)
    	IElastic = IElastic + 1
	    
    ElseIf (MaterialTypeG (iel) == 3) Then !This is for Viscoplastic constitutive relation
    
        IViscoPlastic = IViscoPlastic + 1	!IViscoPlastic is used to avoid allocating SIG to all element, when only a few are V/E
!	    Call BODNER3D (IEL,C,1,MSETI,IINTPT)
		
    ElseIf (MaterialTypeG (iel) == 5) Then !This is for linear Viscoelastic constitutive relation
        
        IViscoElastic = IViscoElastic + 1	!IViscoElastic is used to avoid allocating SIG to all element, when only a few are V/E

	    Call LinearViscoelastic3D (iel, DSIGRG,IViscoElastic, iIntPoint, iLocation)
	    
        If (iLocation == 2) Then    !If Called from Force Matrix
         
            If (.Not. Allocated (SEigen)) Allocate (SEigen (6))
            SEIGEN = DSIGRG  !If there are more than one integration point, need to implement DSIGR(, , IINTPT)
            DSIGRG = 0

        Endif

    ElseIf (MaterialTypeG (iel) == 6) Then !This is for linear elastic constituive relation

	    Call UzanIsotropicNonLinearElasticGlobal3D (iel, iLocation)
    	IUzanElastic = IUzanElastic + 1
        
    Else

        Write (6,6003) 
6003    Format(10X,'Material type not in current library',/) 
	    Stop 
    
    Endif    
    
    End Subroutine ReadGlobalMaterialType3D
 
    End Module ReadMaterialType