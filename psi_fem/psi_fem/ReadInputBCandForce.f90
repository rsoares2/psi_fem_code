Module InputBCandForce
    
    use GlobalInputVariables
    use LocalInputVariables
    use input_variables
    use psi_fem_utilities, only: PrintReadInputToScreen
    
    implicit Real (A-H, O-Z)

    Contains    

    Subroutine ReadGlobalInputBCandForce
    
    !   This subroutine reads the boundary conditions (BC) and forces applied to the problem
    
    Call ReadInputBC
    
    If (NumberOfForces /= 0) Call ReadInputForce 
    
    call PrintReadInputToScreen (Enum_ReadGlobalInputBCForces)

    End Subroutine ReadGlobalInputBCandForce

    subroutine readInputBC
        ! Global degree of freedom (nGlobalDOF(i)) is calculated as:
        ! - For 2D model: X = 2n - 1, Y = 2n
        ! - For 3D model: X = 3n - 2, Y = 3n - 1, Z = 3n

        allocate(numberOfGlobalDisplacement(numberOfTimeBlocks))
        read(5,*) (numberOfGlobalDisplacement(i), i = 1, numberOfTimeBlocks)

        allocate(nGlobalDOF(numberOfGlobalDisplacement(numberOfTimeBlocks), numberOfTimeBlocks))
        allocate(displacementIncrementG(numberOfGlobalDisplacement(numberOfTimeBlocks), numberOfTimeBlocks))

        do j = 1, numberOfTimeBlocks
            if (iOutputGlobal) write(6, 1019) j, numberOfGlobalDisplacement(j)
            read(5,*) (nGlobalDOF(i,j), i = 1, numberOfGlobalDisplacement(j))
            if (iOutputGlobal) then
                write(6, 1020)
                write(6, 1021) (nGlobalDOF(i,j), i = 1, numberOfGlobalDisplacement(j))
            endif

            1019 format(/, 5x, 'Input Global Displacement Data for Timeblock Number', i3, /, 5x, 'The Number of Displacement BC is ', i9, /)
            1020 format(/, 10x, 'The Global Disp. Boundary Conditions are Applied at', /, 10x, 'Degrees of Freedom', /)
            1021 format(10x, 10i8)

            ! If a boundary is fixed, these will be zero. If displacement is applied, it will be non-zero.
            ! Note that these will be divided by timeFactor(i)

            if (displacementBCType == 0) then
                read(input_file_unit,*) (displacementIncrementG(i,j), i = 1, numberOfGlobalDisplacement(j))
            else
                displacementIncrementG = 0
            endif

            if (iOutputGlobal) then
                write(6, 1022)
                write(6, 1023) (displacementIncrementG(i,j), i = 1, numberOfGlobalDisplacement(j))
            endif
        end do

        1022 format(/, 10x, 'the global displacement increments are', /)
        1023 format(4(2x,e15.7))

        nGTotalDOF = numberOfDimensions * numberOfGlobalNodes

    end subroutine readInputBC
!*********************************************************************************************    

    Subroutine ReadInputForce

        !allocate(FGLG(NGTotalDOF))
        !FGLG = 0

        allocate(iNode(NumberOfForces))
        allocate(GlobalNodeForces(NumberOfForces))
        allocate(GlobalNodeForcesIncrement(NumberOfForces))

        Select case (NumberOfDimensions)

        case (2)
            If (iOutputGlobal) Write(6,1021)
    1021    Format(//,10X,'Global forces are applied as follows', //, 10X, 'Node N0.', 8X, 'Force X', 12X, 'Force Y', /)

            Do i = 1, NumberOfForces
                Read(input_file_unit,*) iNode(i), GlobalNodeForces(i)%x, GlobalNodeForces(i)%y
                If (iOutputGlobal) Write(6,1022) iNode(i), GlobalNodeForces(i)%x, GlobalNodeForces(i)%y
    1022        Format(10X, i5, 2(5X, D15.7))
               ! NN1 = 2 * iNode(i) - 1
               ! NN2 = NN1 + 1
               ! FGLG(NN1) = FGLG(NN1) + GlobalNodeForces(i)%x
               ! FGLG(NN2) = FGLG(NN2) + GlobalNodeForces(i)%y
            Enddo
                GlobalNodeForcesIncrement(:) = GlobalNodeForces(:)

        case (3)
            If (iOutputGlobal) Write(6,1024)
    1024    Format(//,10X,'Global forces are applied as follows', //, 10X, 'Node N0.', 8X, 'Force X', 12X, 'Force Y', 12X, 'Force Z', /)

            Do i = 1, NumberOfForces
                Read(5,*) iNode(i), GlobalNodeForces(i)%x, GlobalNodeForces(i)%y, GlobalNodeForces(i)%z
                If (iOutputGlobal) Write(6,1023) iNode(i), GlobalNodeForces(i)%x, GlobalNodeForces(i)%y, GlobalNodeForces(i)%z
    1023        Format(10X, i5, 3(5X, D15.7))
               ! NN1 = 3 * iNode(i) - 2
               ! NN2 = NN1 + 1
               ! NN3 = NN2 + 1
               ! FGLG(NN1) = FGLG(NN1) + GlobalNodeForces(i)%x
               ! FGLG(NN2) = FGLG(NN2) + GlobalNodeForces(i)%y
               ! FGLG(NN3) = FGLG(NN3) + GlobalNodeForces(i)%z
            Enddo
                GlobalNodeForcesIncrement(:) = GlobalNodeForces(:)

        case default
            If (iOutputGlobal) Write(6,1007)
    1007    Format('The number of dimensions must be 2 for 2D or 3 for 3D')
            stop

        end select

    End Subroutine ReadInputForce

    subroutine ReadLocalInputBC
  
    !
    !   The local displacement boundary conditions selects all the nodes around the boundaries.
    !   This is used to apply displacements from the global to the local scale.
    !

    !	NLocalDOF(I) - Local degree of freedom of ith degree of fredoom to be input
    !
    !   For 2D: 
    !   X: 2*NodeNumber-1
    !   Y: 2*NodeNumber
    !
    !   For 3D: 
    !   X: 3*NodeNumber-2
    !   Y: 3*NodeNumber-1
    !   Z: 3*NodeNumber
    !

    !Allocate ( NumberOfLocalDisplacement ( NumberOfTimeBlocks ))  !Number of displacement boundary conditions in each timeblock
    
    !Read(5,*) ( NumberOfLocalDisplacement(i), i = 1, NumberOfTimeBlocks)
    
    Allocate ( NLocalDOF (NumberOfLocalDisplacementBC, NumberOfTimeBlocks) ) !What are the degree of freedoms in each timeblock
    
    Read (5,*) (NLocalDOF(i,1),i=1,NumberOfLocalDisplacementBC) 
    Write (6,1020) 
    Write (6,1021) (NLocalDOF(i,1),i=1,NumberOfLocalDisplacementBC)
    
    Do j = 2, NumberOfTimeBlocks
		NLocalDOF(:,j) = NLocalDOF(:,1)
    Enddo
    
1019	Format(/,5X,'Input local displacement data for timeblock number'&
		,I3,//,5X,'The number of displacement bc is ',I9,/)
    
1020	Format(/,10X,'The local disp. boundary conditions are applied at degrees of freedom',/) 

1021    Format (10x, 10i8)

    NLTotalDOF = NumberOfDimensions * NumberOfLocalNodes  !Number of total Local degrees of freedom
     
    End Subroutine ReadLocalInputBC
    
End Module InputBCandForce