Module calculate_stiffness_matrix

    Use GlobalInputVariables
    use initialize_global_variables, only: InitializeGlobalStiffnessMatrix2D, InitializeGlobalStiffnessMatrix3D
    Use CalculateStifnessMatrix3D
    Use CalculateStifnessMatrix2DG
    Use CalculateStifnessMatrix2DL
    use psi_fem_utilities

	implicit none !Real*8(A-H,O-Z)

	Contains

    subroutine globalStiffnessMatrix

	! This subroutine calculates the global stiffness matrix in sparse format
	    
	! Use iterationFlag == 1 to test recompiling stiffness matrix at every time step
    ! Only enters here to calculate stiffness matrix once if not using Newton-Raphson     
    if (shouldCalculateStiffnessMatrix(iterationFlag, Incr, it)) then		   
            ! (stiffness matrix is constant, only force vector changes)
		if (numberOfDimensions == 2) then
			call initializeGlobalStiffnessMatrix2D
			call calculateGlobalStiffnessMatrixSparse2D
		elseif (numberOfDimensions == 3) then
			call initializeGlobalStiffnessMatrix3D
			call calculateGlobalStiffnessMatrixSparse3D
        endif
    endif

	    call printReadInputToScreen(Enum_GlobalStiffnessMatrix)

    end subroutine globalStiffnessMatrix

    logical function shouldCalculateStiffnessMatrix(iterationFlag, Incr, it)
      integer, intent(in) :: iterationFlag, Incr, it

      shouldCalculateStiffnessMatrix = (iterationFlag == 0 .and. Incr == 1) .or. &
                                       (it == 1 .and. iterationFlag == 2) .or. &
                                       (iterationFlag == 1)
    end function shouldCalculateStiffnessMatrix    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	Subroutine LocalStiffnessMatrix
	
    !
    !   This subroutine calculates the Local stiffness matrix in sparse format
    !
    
    If (NumberOfDimensions == 2 ) Then
        
        Call InitializeLocalStiffnessMatrix2D
        
        Call CalculateLocalStiffnessMatrixSparse2D
    
    ElseIf (NumberOfDimensions == 3 ) Then 

       ! Call InitializeLocalStiffnessMatrix3D
        
       ! Call CalculateLocalStiffnessMatrixSparse3D
        
    Endif
    
    End Subroutine LocalStiffnessMatrix

End Module calculate_stiffness_matrix