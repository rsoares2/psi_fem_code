    Module AllenCZForce

    Use GlobalInputVariables
    use initialize_global_variables
    use input_variables
    Use ShapeFunctions
    Use Quadrature
    Use ReadMaterialType
    Use AssembleStiffnessMatrix
    
    Use OMP_LIB
    
	implicit Real*8(A-H,O-Z)

	Contains

	Subroutine AllenCZModelForceG (i, W, Phi, UN, UT, N1X, N1Y, N2X, N2Y)
    
    Real*8 SIGTT(NumberPronyTermsCZ),SIGTN(NumberPronyTermsCZ)
	Real*8 DUM1(NumberPronyTermsCZ),EA(NumberPronyTermsCZ),ETAA(NumberPronyTermsCZ)
	Real*8:: FInt(4), Lambda, LambdaDot
    !    IF(IDEBNDG(I).EQ.1) GO TO 7240
    DeltaSubN=DeltaNormalG(iMaterialCZG(i)) 
    DeltaSubT=DeltaTangG(iMaterialCZG(i))
    A=ALPHG(iMaterialCZG(i)) 
    RMC=RMG(iMaterialCZG(i))
    SIGMFN=SigmaNormalG(iMaterialCZG(i))
    SIGMFT=SigmaTangG(iMaterialCZG(i))
    EINF=ECINFG(iMaterialCZG(i))
    ALPHA1 = DAMAGEGIter(I)
    TN = TractionNormalGIter(I)
    TT = TractionTangentGIter(I)

    OldLambda = OLAMBDAG(I)
    OLDUN = UNOLDG(I)
    OLDUT = UTOLDG(I)

    DELTAUN = UN-OLDUN
    DELTAUT = UT-OLDUT

    Do j = 1, NumberPronyTermsCZ
        
        EA(j)=ECG(iMaterialCZG(i),j)
        ETAA(j)=ETACG(iMaterialCZG(i),j)
        SIGTN(j)=SIGCOHNGIter(i,j)
        SIGTT(j)=SIGCOHTGIter(i,j)
        
    Enddo

	If(UN < 0) Then
	    Lambda = DSQRT((UT/DeltaSubT)**2)
    Else
        Lambda = DSQRT((UN/DeltaSubN)**2+(UT/DeltaSubT)**2)
	Endif

    If(Lambda < 1.E-20) Then
        
        Lambda = 0.
      
    Endif
      
    DeltaLambda=Lambda-OLDLambda
    LambdaDot = DeltaLambda/DTime

    Alpha1Dot = A * Lambda**RMC
    DeltaAlpha1 = Alpha1Dot *DTime
    
    If (FAC > 0) Then
	    Alpha1 = Alpha1 + DeltaAlpha1
    Else
		Alpha1 = Alpha1
    Endif

    !  IF(ALPHA1.GE.1.) THEN
  !    TN=0.
   !   TT=0.
   !   GO TO 900
   !   ENDIF
!	GO TO 7341
 !7240 CONTINUE
 ! !    TN=0.
!	TT=0.
!	GO TO 900
! 7341 CONTINUE
    EDELTAT=EINF
	DELTASIGMABN = 0.
	DELTASIGMARN = 0.
	DELTASIGMABT = 0.
	DELTASIGMART = 0.
!	WRITE(123,7772) NGPRONY,DTIME
! 7772 FORMAT(5X,'NGPRONY = ',I5,5X,'DTIME = ',D15.7,//)
    Do j= 1, NumberPronyTermsCZ
        DELTASIGMARN = DELTASIGMARN-(1-DEXP(-EA(J)/ETAA(J)*DTIME))*SIGTN(J)     
        DELTASIGMABN = DELTASIGMABN+SIGTN(J)
        DELTASIGMART = DELTASIGMART-(1-DEXP(-EA(J)/ETAA(J)*DTIME))*SIGTT(J)     
        DELTASIGMABT = DELTASIGMABT+SIGTT(J)
        SIGTN(J)=DEXP(-EA(J)/ETAA(J)*DTIME)*SIGTN(J)+ETAA(J)*DELTAUN/DTIME*(1-DEXP(-EA(J)/ETAA(J)*DTIME))

        SIGTT(J)=DEXP(-EA(J)/ETAA(J)*DTIME)*SIGTT(J)+ETAA(J)*DELTAUT/DTIME*(1-DEXP(-EA(J)/ETAA(J)*DTIME))
        DUM1(J)=ETAA(J)*(1-DEXP(-EA(J)/ETAA(J)*DTIME))
        EDELTAT=EDELTAT+DUM1(J)/DTIME
    Enddo
    DELTASIGMABN=DELTASIGMABN+EINF*OLDUN
    DELTASIGMABT=DELTASIGMABT+EINF*OLDUT
 !     WRITE(6,1117) DELTASIGMAR,DELTASIGMAB,EDELTAT
 !1117 FORMAT(5X,'DELTASIGMAR = ',D15.7,5X,'DELTASIGMAB = ',D15.7,/,5X,'EDELTAT = ',D15.7,//)
 !     WRITE(6,1118) (SIGT(J),J=1,NGPRONY)
 !1118 FORMAT(5X,'SIGT = ',4D15.7,//)
 !     WRITE(6,1119) (DUM1(J),J=1,NGPRONY)
 !1119 FORMAT(5X,'DUM1 = ',4D15.7,//)
      IF(UN.LT.0.) GO TO 597
      IF(OLDLAMBDA.LT.1.D-20) GO TO 598
      RKN=(1.-ALPHA1)*EDELTAT/DELTASUBN
      DELTATNR=-DELTAALPHA1/DELTASUBN*(DELTASIGMABN)-DELTAALPHA1*SIGMFN+(1-ALPHA1)/DELTASUBN*DELTASIGMARN
!      WRITE(6,5671) OLDUN,DELTASUBN,OLDLAMBDA,DELTAALPHA1
! 5671 FORMAT(5X,'OLDUN = ',D15.7,5X,'DELTASUBN = ',D15.7,5X,
!     1'OLDLAMBDA = ',D15.7,5X,'DELTAALPHA1 = ',D15.7,//)
!      WRITE(6,5672) DELTASIGMAB,SIGMFN,ALPHA1,DELTASIGMAR
! 5672 FORMAT(5X,'DELTASIGMAB = ',D15.7,5X,'SIGMFN = ',D15.7,5X,
!     1'ALPHA1 = ',D15.7,5X,'DELATSIGMAR = ',D15.7,//)
      DELTATN=RKN*DELTAUN+DELTATNR

      GO TO 600
  597 CONTINUE
    RKN=EDELTAT/DELTASUBN
	!TN=RKN*UN
    DELTATNR=DELTASIGMARN/DELTASUBN
	DELTATN=RKN*DELTAUN+DELTATNR

	GO TO 600
  598 CONTINUE
      RKN=(1.-ALPHA1)*EDELTAT/DELTASUBN
      DELTATNR=0.
      DELTATN=RKN*DELTAUN+DELTATNR
  600 CONTINUE
      IF(OLDLAMBDA.LT.1.D-20) GO TO 698
      IF(DABS(UT).LT.1.D-20) GO TO 698
      RKT=(1.-ALPHA1)*EDELTAT/DELTASUBT
      DELTATTR=-DELTAALPHA1/DELTASUBT*(DELTASIGMABT)-DELTAALPHA1*SIGMFT+(1-ALPHA1)/DELTASUBT*DELTASIGMART
      DELTATT=RKT*DELTAUT+DELTATTR
!	WRITE(6,1211) ALPHA1,DELTASIGMART,DELTATTR,DELTAUT
! 1211 FORMAT(5X,'ALPHA1 = ',D15.7,5X,'DELTASIGMAR = ',D15.7,5X,'DELTATTR = ',D15.7,5X,'DELTAUT = ',D15.7,//)

      GO TO 800
  698 CONTINUE
      RKT=(1.-ALPHA1)*EDELTAT/DELTASUBT
      DELTATTR=0.
      DELTATT=RKT*DELTAUT+DELTATTR
!	WRITE(6,1221) ALPHA1,EDELTAT,DELTATTR
! 1221 FORMAT(5X,'ALPHA1 = ',D15.7,5X,'EDELTAT = ',D15.7,5X,
!     1'DELTATTR = ',D15.7,//)
  800 CONTINUE
      IF(ALPHA1.GE.1.) THEN
      TN=0.
      TT=0.
      GO TO 900
      ENDIF
 7240 CONTINUE
      IF(IDEBNDG(I).EQ.1)THEN
      TN=0.
	TT=0.
	GO TO 900
	END IF
!      WRITE(6,1120) RKN,RKT,DELTATNR,DELTATN,DELTATTR,DELTATT
! 1120 FORMAT(5X,'RKN = ',D15.7,5X,'RKT = ',D15.7,/,
!     15X,'DELTATNR = ',D15.7,5X,'DELTATN = ',D15.7,/,
!     25X,'DELTATTR = ',D15.7,5X,'DELTATT = ',D15.7,//)
   !   IF(UN.LT.0.) GO TO 899
      TN=TN+DELTATN
  899 CONTINUE
      TT=TT+DELTATT
  900 CONTINUE
!	WRITE(6,1121) I,TN,DELTATN
!  1121 FORMAT(5X,'ELEMENT NO. = ',I5,5X,'TN = ',D15.7,5X,'DELTATN = ',D15.7,//)
      FN=-TN*W*TG
      FT=-TT*W*TG
 6541 CONTINUE
      FINT(1)=FN*DCOS(PHI)-FT*DSIN(PHI)
      FINT(2)=FN*DSIN(PHI)+FT*DCOS(PHI)
      FINT(3)=-FN*DCOS(PHI)+FT*DSIN(PHI)
      FINT(4)=-FN*DSIN(PHI)-FT*DCOS(PHI)
!      WRITE(6,2007) (FINT(K),K=1,4)
! 2007 FORMAT(10X,'FINT = ',4E15.7,/)
 3010 CONTINUE
      ForceMatrixG(N1X)=ForceMatrixG(N1X)-FINT(1)
      ForceMatrixG(N1Y)=ForceMatrixG(N1Y)-FINT(2) 
      ForceMatrixG(N2X)=ForceMatrixG(N2X)-FINT(3) 
      ForceMatrixG(N2Y)=ForceMatrixG(N2Y)-FINT(4) 
    
    End Subroutine AllenCZModelForceG

!*****************************************************************************************
!
!   This subroutine is exactly the same as the one above (AllenCZModelForceG), expect for the material
!   properties. They should be passed as argument. Not time at this point to complete this task. Feb 4, 2014
!
	Subroutine AllenCZModelForceL (i, W, Phi, UN, UT, N1X, N1Y, N2X, N2Y)
    
    Real*8 SIGTT(NumberPronyTermsCZ),SIGTN(NumberPronyTermsCZ)
	Real*8 DUM1(NumberPronyTermsCZ),EA(NumberPronyTermsCZ),ETAA(NumberPronyTermsCZ)
	Real*8:: FInt(4)
    !    IF(IDEBNDG(I).EQ.1) GO TO 7240
    DeltaSubN=DeltaNormalL(iMaterialCZL(i)) 
    DeltaSubT=DeltaTangL(iMaterialCZL(i))
    A=ALPHL(iMaterialCZL(i)) 
    RMC=RML(iMaterialCZL(i))
    SIGMFN=SigmaNormalL(iMaterialCZL(i))
    SIGMFT=SigmaTangL(iMaterialCZL(i))
    EINF=ECINFL(iMaterialCZL(i))
    ALPHA1 = DAMAGEL(I)
    TN = TractionNormalL(I)
    TT = TractionTangentL(I)

    OLDLAMBDA = OLAMBDAL(I)
    OLDUN = UNOLDL(I)
    OLDUT = UTOLDL(I)

    DELTAUN = UN-OLDUN
    DELTAUT = UT-OLDUT

    Do j = 1, NumberPronyTermsCZ    !This should be for the local element
        
        EA(j)=ECL(iMaterialCZL(i),j)
        ETAA(j)=ETACL(iMaterialCZL(i),j)
        SIGTN(j)=SIGCOHNL(i,j)
        SIGTT(j)=SIGCOHTL(i,j)
        
    Enddo

	If(UN < 0.) Then
	    Lambda = DSQRT((UT/DeltaSubT)**2)
    Else
        Lambda = DSQRT((UN/DeltaSubN)**2+(UT/DeltaSubT)**2)
	Endif

    If(Lambda < 1.D-20) Then
        
        Lambda = 0.
      
    Endif
      
    DeltaLambda=Lambda-OLDLambda
    LambdaDot = DeltaLambda/DTime

    Alpha1Dot = A * Lambda**RMC
    DeltaAlpha1 = Alpha1Dot *DTime
    
    If (FAC > 0) Then
	    Alpha1 = Alpha1 + DeltaAlpha1
    Else
		Alpha1 = Alpha1
    Endif

    !  IF(ALPHA1.GE.1.) THEN
  !    TN=0.
   !   TT=0.
   !   GO TO 900
   !   ENDIF
!	GO TO 7341
 !7240 CONTINUE
 ! !    TN=0.
!	TT=0.
!	GO TO 900
! 7341 CONTINUE
    EDELTAT=EINF
	DELTASIGMABN = 0.
	DELTASIGMARN = 0.
	DELTASIGMABT = 0.
	DELTASIGMART = 0.
!	WRITE(123,7772) NGPRONY,DTIME
! 7772 FORMAT(5X,'NGPRONY = ',I5,5X,'DTIME = ',D15.7,//)
    Do j= 1, NumberPronyTermsCZ
        DELTASIGMARN = DELTASIGMARN-(1-DEXP(-EA(J)/ETAA(J)*DTIME))*SIGTN(J)     
        DELTASIGMABN = DELTASIGMABN+SIGTN(J)
        DELTASIGMART = DELTASIGMART-(1-DEXP(-EA(J)/ETAA(J)*DTIME))*SIGTT(J)     
        DELTASIGMABT = DELTASIGMABT+SIGTT(J)
        SIGTN(J)=DEXP(-EA(J)/ETAA(J)*DTIME)*SIGTN(J)+ETAA(J)*DELTAUN/DTIME*(1-DEXP(-EA(J)/ETAA(J)*DTIME))

        SIGTT(J)=DEXP(-EA(J)/ETAA(J)*DTIME)*SIGTT(J)+ETAA(J)*DELTAUT/DTIME*(1-DEXP(-EA(J)/ETAA(J)*DTIME))
        DUM1(J)=ETAA(J)*(1-DEXP(-EA(J)/ETAA(J)*DTIME))
        EDELTAT=EDELTAT+DUM1(J)/DTIME
    Enddo
    DELTASIGMABN=DELTASIGMABN+EINF*OLDUN
    DELTASIGMABT=DELTASIGMABT+EINF*OLDUT
 !     WRITE(6,1117) DELTASIGMAR,DELTASIGMAB,EDELTAT
 !1117 FORMAT(5X,'DELTASIGMAR = ',D15.7,5X,'DELTASIGMAB = ',D15.7,/,5X,'EDELTAT = ',D15.7,//)
 !     WRITE(6,1118) (SIGT(J),J=1,NGPRONY)
 !1118 FORMAT(5X,'SIGT = ',4D15.7,//)
 !     WRITE(6,1119) (DUM1(J),J=1,NGPRONY)
 !1119 FORMAT(5X,'DUM1 = ',4D15.7,//)
      IF(UN.LT.0.) GO TO 597
      IF(OLDLAMBDA.LT.1.D-20) GO TO 598
      RKN=(1.-ALPHA1)*EDELTAT/DELTASUBN
      DELTATNR=-DELTAALPHA1/DELTASUBN*(DELTASIGMABN)-DELTAALPHA1*SIGMFN+(1-ALPHA1)/DELTASUBN*DELTASIGMARN
!      WRITE(6,5671) OLDUN,DELTASUBN,OLDLAMBDA,DELTAALPHA1
! 5671 FORMAT(5X,'OLDUN = ',D15.7,5X,'DELTASUBN = ',D15.7,5X,
!     1'OLDLAMBDA = ',D15.7,5X,'DELTAALPHA1 = ',D15.7,//)
!      WRITE(6,5672) DELTASIGMAB,SIGMFN,ALPHA1,DELTASIGMAR
! 5672 FORMAT(5X,'DELTASIGMAB = ',D15.7,5X,'SIGMFN = ',D15.7,5X,
!     1'ALPHA1 = ',D15.7,5X,'DELATSIGMAR = ',D15.7,//)
      DELTATN=RKN*DELTAUN+DELTATNR

      GO TO 600
  597 CONTINUE
    RKN=EDELTAT/DELTASUBN
	!TN=RKN*UN
    DELTATNR=DELTASIGMARN/DELTASUBN
	DELTATN=RKN*DELTAUN+DELTATNR

	GO TO 600
  598 CONTINUE
      RKN=(1.-ALPHA1)*EDELTAT/DELTASUBN
      DELTATNR=0.
      DELTATN=RKN*DELTAUN+DELTATNR
  600 CONTINUE
      IF(OLDLAMBDA.LT.1.D-20) GO TO 698
      IF(DABS(UT).LT.1.D-20) GO TO 698
      RKT=(1.-ALPHA1)*EDELTAT/DELTASUBT
      DELTATTR=-DELTAALPHA1/DELTASUBT*(DELTASIGMABT)-DELTAALPHA1*SIGMFT+(1-ALPHA1)/DELTASUBT*DELTASIGMART
      DELTATT=RKT*DELTAUT+DELTATTR
!	WRITE(6,1211) ALPHA1,DELTASIGMART,DELTATTR,DELTAUT
! 1211 FORMAT(5X,'ALPHA1 = ',D15.7,5X,'DELTASIGMAR = ',D15.7,5X,'DELTATTR = ',D15.7,5X,'DELTAUT = ',D15.7,//)

      GO TO 800
  698 CONTINUE
      RKT=(1.-ALPHA1)*EDELTAT/DELTASUBT
      DELTATTR=0.
      DELTATT=RKT*DELTAUT+DELTATTR
!	WRITE(6,1221) ALPHA1,EDELTAT,DELTATTR
! 1221 FORMAT(5X,'ALPHA1 = ',D15.7,5X,'EDELTAT = ',D15.7,5X,
!     1'DELTATTR = ',D15.7,//)
  800 CONTINUE
      IF(ALPHA1.GE.1.) THEN
      TN=0.
      TT=0.
      GO TO 900
      ENDIF
 7240 CONTINUE
      IF(IDEBNDL(I).EQ.1)THEN
      TN=0.
	TT=0.
	GO TO 900
	END IF
!      WRITE(6,1120) RKN,RKT,DELTATNR,DELTATN,DELTATTR,DELTATT
! 1120 FORMAT(5X,'RKN = ',D15.7,5X,'RKT = ',D15.7,/,
!     15X,'DELTATNR = ',D15.7,5X,'DELTATN = ',D15.7,/,
!     25X,'DELTATTR = ',D15.7,5X,'DELTATT = ',D15.7,//)
   !   IF(UN.LT.0.) GO TO 899
      TN=TN+DELTATN
  899 CONTINUE
      TT=TT+DELTATT
  900 CONTINUE
!	WRITE(6,1121) I,TN,DELTATN
!  1121 FORMAT(5X,'ELEMENT NO. = ',I5,5X,'TN = ',D15.7,5X,'DELTATN = ',D15.7,//)
      FN=-TN*W*TG
      FT=-TT*W*TG
 6541 CONTINUE
      FINT(1)=FN*DCOS(PHI)-FT*DSIN(PHI) 
      FINT(2)=FN*DSIN(PHI)+FT*DCOS(PHI) 
      FINT(3)=-FN*DCOS(PHI)+FT*DSIN(PHI) 
      FINT(4)=-FN*DSIN(PHI)-FT*DCOS(PHI) 
!      WRITE(6,2007) (FINT(K),K=1,4) 
! 2007 FORMAT(10X,'FINT = ',4E15.7,/) 
 3010 CONTINUE
      FRGlobal(N1X) = FRGlobal(N1X)-FINT(1) 
      FRGlobal(N1Y) = FRGlobal(N1Y)-FINT(2) 
      FRGlobal(N2X) = FRGlobal(N2X)-FINT(3) 
      FRGlobal(N2Y) = FRGlobal(N2Y)-FINT(4) 
    
    End Subroutine AllenCZModelForceL

!*******************************************************************************
    
    End Module AllenCZForce