module psi_fem_utilities
    use GlobalInputVariables
    use input_variables
    implicit none

    contains

    subroutine printElapsedTime(iCycl, incr, it, subroutineName, startTime, endTime)
        integer, intent(in) :: iCycl, incr, it
        character(len=*), intent(in) :: subroutineName
        real(kind=8), intent(in) :: startTime, endTime
        real(kind=8) :: elapsedTime
        integer :: elapsedTimeMin, elapsedTimeSec
    
        elapsedTime = endTime - startTime
        elapsedTimeMin = int(elapsedTime / 60.0_8)
        elapsedTimeSec = mod(int(elapsedTime), 60)
        write (*, '(4x, A, I0, A, I0, A45, 5x, A, I0, A, I0, A)') "Cycle:", iCycl, " Time Step:", incr, subroutineName, " Time Elapsed: ", elapsedTimeMin, " min ", elapsedTimeSec, " s"
    end subroutine printElapsedTime

    subroutine PSIFemVersion
        character(12) :: versionNumber, versionIncrement, version
        versionNumber = "3"
        versionIncrement = ".11"
        version = trim(versionNumber) // trim(versionIncrement)
        write(*,'(/,2A,//,A,/)') '    psi_fem version ', trim(version), '    Last Updated: May 18th, 2023   '
        call PrintReadInputToScreen(Enum_StartReadInput)
    end subroutine PSIFemVersion
    
    subroutine PrintReadInputToScreen(step)
       
        implicit none
        integer, intent(in) :: step
        character(len=100) :: message
        
        select case(step)
            case(Enum_StartReadInput)
                message = 'Completed: Start ReadInput '
            case(Enum_ReadInputHeader)
                message = 'Completed: Read Input Header '
            case(Enum_ReadInputCoordinates)         
                message = 'Completed: Read Input Coordinates '
            case(Enum_ReadInputConnectivity)         
                message = 'Completed: Read Input Connectivity '
            case(Enum_ReadGlobalInputBCForces)         
                message = 'Completed: Read Global Input BC and Forces '
            case(Enum_ReadGlobalInputMaterials)         
                message = 'Completed: Read Global Input Materials '
            case(Enum_SparseInitialConnectivity)         
                message = 'Completed: Sparse Initial Connectivity '
            case(Enum_PrintOutputFromInput)         
                message = 'Completed: Print Output From Input '
            case(Enum_InitializeVariables)         
                message = 'Completed: Initialize Variables '
            case(Enum_GlobalStiffnessMatrix)
                message = 'Completed: Global Stiffness Matrix '
            case(Enum_AssembleForceMatrix)
                message = 'Completed: Assemble Force Matrix '
            case(Enum_SparseSolver)
                message = 'Completed: Sparse Solver '
            case(Enum_CalculateStressesStrains)
                message = 'Completed: Calculate Stresses/Strains '
            case(Enum_CalculateOutputs)
                message = 'Completed: Calculate Outputs '
            case default
                message = 'Invalid step number'
        end select
            
        call cpu_time(endTime)

        call printElapsedTime(iCycl, incr, it, trim(message), startTime, endTime)
            
    end subroutine PrintReadInputToScreen
    
end module psi_fem_utilities
