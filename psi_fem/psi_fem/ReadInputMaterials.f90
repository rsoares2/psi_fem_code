Module InputMaterials
    
    Use GlobalInputVariables
    use input_variables
    Use LocalInputVariables    
    Use InputViscoElasticity
    Use LocalInputViscoElasticity
    Use InputPlasticity
    Use InputElasticity
    Use InputElasticityUzan
    use psi_fem_utilities, only: PrintReadInputToScreen
    
    implicit none

    contains
    
    subroutine readGlobalInputMaterials
        integer, parameter :: numMaterialTypes = 6
        integer :: i
        
        ! Define an array of subroutine pointers
        type readGlobalInputSubroutine
            procedure(), pointer, nopass :: readGlobalInput
        end type readGlobalInputSubroutine

        type(readGlobalInputSubroutine) :: readGlobalInputSubroutines(numMaterialTypes)

        allocate (materialNumbers(numMaterialTypes))

        ! Read the global material properties associated with each element
        read(5,*) materialNumbers
        
        numberOfElasticMaterials = materialNumbers(1)
        numberOfPlasticMaterials = materialNumbers(2)
        numberOfElastoPlasticMaterials = materialNumbers(3)
        numberOfViscoPlasticMaterials = materialNumbers(4)
        numberOfViscoElasticMaterials = materialNumbers(5)
        numberOfElasticUzanMaterials = materialNumbers(6)
        
        ! Calculate total number of materials
        numberOfTotalMaterials = sum(materialNumbers)

        ! Assign subroutines to the array
        readGlobalInputSubroutines(1)%readGlobalInput => readGlobalInputElastic
        ! readGlobalInputSubroutines(2)%readGlobalInput => readGlobalInputDruckerPrager
        ! readGlobalInputSubroutines(3)%readGlobalInput => readGlobalInputViscoPlastic
        ! readGlobalInputSubroutines(4)%readGlobalInput => readGlobalInputElasticOrthotropic
        readGlobalInputSubroutines(5)%readGlobalInput => readGlobalInputViscoElastic
        readGlobalInputSubroutines(6)%readGlobalInput => readGlobalInputElasticUzan

        do i = 1, numMaterialTypes
            if (materialNumbers(i) /= 0 .and. associated(readGlobalInputSubroutines(i)%readGlobalInput)) then
                call readGlobalInputSubroutines(i)%readGlobalInput
            end if
        end do

        call printReadInputToScreen(enum_readGlobalInputMaterials)

    end subroutine readGlobalInputMaterials

    Subroutine ReadLocalInputMaterials
    
    !
    !   This subroutine reads the local material properties associated with each element
    !

	Read(5,*) MaterialNumber1L, MaterialNumber2L, MaterialNumber3L, MaterialNumber4L, MaterialNumber5L !Number of materials of each kind 
      
    NumberOfTotalMaterialsL = MaterialNumber1L + MaterialNumber2L + MaterialNumber3L + MaterialNumber4L + MaterialNumber5L 
     
    If (MaterialNumber1L /= 0) Call ReadLocalInputElastic
  !  If (MaterialNumber2L /= 0) Call ReadLocalInputElastic_Plastic
  !  If (MaterialNumber3L /= 0) Call ReadLocalInputViscoPlastic
  !  If (MaterialNumber4L /= 0) Call ReadLocalInputElasticOrthotropic
    If (MaterialNumber5L /= 0) Call ReadLocalInputViscoElastic
 
    End Subroutine ReadLocalInputMaterials
    
End Module InputMaterials